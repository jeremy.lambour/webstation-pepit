package org.ws.service.core.service.equipment.skilift;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.equipment.SkiLift;
import com.techconsulting.ws.dao.repository.equipment.SkiLiftRepository;
import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.service.equipment.skilift.SkiLiftService;

/**
 * Method test for SkiLift service
 * 
 * @author p_lem
 *
 */
public class SkiLiftServiceTest {
	
	@InjectMocks
	SkiLiftService mockSkiLiftService;
	
	@Mock
	SkiLiftRepository mockSkiliftRepo;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testGetListSkiLift. It tests the method getListSkiLift of the
	 * {@link SkiLiftService} class.
	 */
	@Test
	public void testGetListSkiLift() {
		List<SkiLift> listSkilift = new ArrayList<>();
		List<SkiLiftDto> listSkiliftDto = new ArrayList<>();
		Mockito.when(mockSkiliftRepo.findAll()).thenReturn(listSkilift);
		assertEquals(mockSkiLiftService.getListSkiLift(), listSkiliftDto);
	}
}

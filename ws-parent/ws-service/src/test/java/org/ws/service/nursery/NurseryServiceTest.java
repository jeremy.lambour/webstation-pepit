package org.ws.service.nursery;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.nursery.Nursery;
import com.techconsulting.ws.dao.repository.nursery.NurseryRepository;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryDto;
import com.techconsulting.ws.service.core.service.nursery.NurseryService;

public class NurseryServiceTest {

	@InjectMocks
	private NurseryService service;
	
	@Mock
	private NurseryRepository repository;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void NurseryServiceReturnRightNurseryWithGoodIdentifier() {
		Integer searchId = 2;
		Nursery nursery = new Nursery();
		nursery.setNurseryId(searchId);
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(nursery);
		NurseryDto dto = service.getNurseryById(searchId);
		Mockito.verify(repository,Mockito.atLeast(1)).findOne(Mockito.anyInt());
		assertEquals(searchId, dto.getNurseryId());
	}
	
	@Test
	public void NurseryServiceReturnAllNursery() {
		Nursery nursery = new Nursery();
		List<Nursery> nurserys = new ArrayList<>();
		nurserys.add(nursery);
		nurserys.add(nursery);
		Mockito.when(repository.findAll()).thenReturn(nurserys);
		List<NurseryDto> dto = service.getAllNursery();
		Mockito.verify(repository,Mockito.atLeast(1)).findAll();
		assertEquals(dto.size(), nurserys.size());
	}
}

package org.ws.service.core.service.reservation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.reservation.Reservation;
import com.techconsulting.ws.dao.repository.reservation.ReservationRepository;
import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;
import com.techconsulting.ws.service.core.service.reservation.ReservationService;

/**
 * Method test for Reservation service
 * 
 * @author p_lem
 *
 */
public class ReservationServiceTest {
	
	@InjectMocks
	ReservationService mockReservationService;
	
	@Mock
	ReservationRepository mockReservationRepo;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertReservation. It tests the method insertReservation of the
	 * {@link ReservationService} class.
	 */
	@Test
	public void testInsertReservation() {
		Reservation reserv = new Reservation();
		ReservationDto reservDto = new ReservationDto();
		Mockito.when(mockReservationRepo.save(reserv)).thenReturn(reserv);
		assertEquals(mockReservationService.insertReservation(reservDto).getClass(), ReservationDto.class);
	}
	
	/**
	 * Method testDeleteReservation. It tests the method deleteReservation of the
	 * {@link ReservationService} class.
	 */
	@Test
	public void testDeleteReservation() {
		assertEquals(true, mockReservationService.deleteReservation(1));
	}
}

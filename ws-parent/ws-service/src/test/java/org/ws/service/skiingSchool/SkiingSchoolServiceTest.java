package org.ws.service.skiingSchool;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.skiingSchool.SkiingSchool;
import com.techconsulting.ws.dao.repository.skiingSchool.SkiingSchoolRepository;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.SkiingSchoolDto;
import com.techconsulting.ws.service.core.service.skiingSchool.SkiingSchoolService;

public class SkiingSchoolServiceTest {

	@InjectMocks
	private SkiingSchoolService service;
	
	@Mock 
	private SkiingSchoolRepository repository;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void isSkiingSchoolCreated() {
		SkiingSchool school = new SkiingSchool();
		school.setSkiingSchoolId(2);
		Mockito.when(repository.save(Mockito.any(SkiingSchool.class))).thenReturn(school);
		SkiingSchoolDto dto = service.createSkiingSchool(new SkiingSchoolDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(SkiingSchool.class));
		assertEquals(school.getSkiingSchoolId(),dto.getSkiingSchoolId());
	}
	
	@Test
	public void isSkiingSchoolUpdated() {
		SkiingSchool school = new SkiingSchool();
		school.setSkiingSchoolId(2);
		Mockito.when(repository.save(Mockito.any(SkiingSchool.class))).thenReturn(school);
		SkiingSchoolDto dto = service.updateSkiingSchool(new SkiingSchoolDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(SkiingSchool.class));
		assertEquals(school.getSkiingSchoolId(),dto.getSkiingSchoolId());
	}
	
	@Test
	public void isLessonDeleted() {
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(null);
		Boolean isDeleted = service.deleteSkiingSchool(2);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, true);
	}

	@Test
	public void isLessonNotDeleted() {
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(new SkiingSchool());
		Boolean isDeleted = service.deleteSkiingSchool(2);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, false);
	}
	
	@Test
	public void isAllSkiingSchoold() {
		List<SkiingSchool> list = new ArrayList<>();
		list.add(new SkiingSchool());
		Mockito.when(repository.findAll()).thenReturn(list);
		List<SkiingSchoolDto> dtos = service.findAllSkiingSchool();
		Mockito.verify(repository, Mockito.atLeast(1)).findAll();
		assertEquals(list.size(), dtos.size());
	}
	
	
}

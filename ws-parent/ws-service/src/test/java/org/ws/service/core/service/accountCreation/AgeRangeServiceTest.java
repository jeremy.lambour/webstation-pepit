package org.ws.service.core.service.accountCreation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.auth.AgeRange;
import com.techconsulting.ws.dao.repository.auth.AgeRangeRepository;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.service.accountcreation.AgeRangeService;

/**
 * Method test for age_range service
 * 
 * @author p_lem
 *
 */
public class AgeRangeServiceTest {

	@InjectMocks
	private AgeRangeService mockAgeRangeService;

	@Mock
	private AgeRangeRepository mockAgeRangeRepository;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/*
	 * Test method for method find range from Age range service
	 */
	@Test
	public void testFindAgeRange() {
		Mockito.when(mockAgeRangeRepository.findOne(Mockito.anyInt())).thenReturn(Mockito.any(AgeRange.class));
		assertEquals(mockAgeRangeService.findAgeRange(0).getClass(), AgeRangeDto.class);
	}

}

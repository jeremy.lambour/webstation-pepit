package org.ws.service.core.service.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.repository.common.ScheduleRepository;
import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;
import com.techconsulting.ws.service.core.service.common.ScheduleService;

/**
 * Test's Class for the {@link ScheduleService}
 */
public class ScheduleServiceTest {

	@InjectMocks
	ScheduleService scheduleService;

	@Mock
	private ScheduleRepository mockScheduleRepo;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertSchedule. It tests the method insertSchedule of the
	 * {@link ScheduleService} class. It should return a not null {@link ScheduleDTO}
	 */
	@Test
	public void testInsertSchedule() {

		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScheduleId(Integer.valueOf(3));

		assertNotNull(scheduleService.insertSchedule(scheduleDto));
	}
	
	/**
	 * Method testFindScheduleById. It tests the method findScheduleById of the
	 * {@link ScheduleService} class. It should return an object {@link ScheduleDTO}
	 */
	@Test
	public void testFindScheduleById() {

		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScheduleId(Integer.valueOf(1));

		assertEquals(scheduleService.findScheduleById(Mockito.anyInt()).getClass(), scheduleDto.getClass());
	}
}

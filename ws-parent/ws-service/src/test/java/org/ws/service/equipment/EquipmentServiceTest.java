package org.ws.service.equipment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.equipment.Equipment;
import com.techconsulting.ws.dao.repository.auth.AgeRangeRepository;
import com.techconsulting.ws.dao.repository.auth.UserRepository;
import com.techconsulting.ws.dao.repository.equipment.EquipmentRepository;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.service.equipment.EquipmentService;
import com.techconsulting.ws.service.core.service.mail.IMailService;

/**
 * Test's Class for the {@link EquipmentService}
 */
public class EquipmentServiceTest {

	@InjectMocks
	EquipmentService equipmentService;

	@Mock
	private IMailService emailService;

	@Mock
	private EquipmentRepository mockEquipmentRepository;

	@Mock
	private UserRepository mockUserRepo;

	@Mock
	private AgeRangeRepository mockAgeRangeRepo;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testInsertEquipment. It tests the method insertEquipment of the
	 * {@link EquipmentService} class. It should return a not null
	 * {@link EquipmentDTO}
	 */
	@Test
	public void testInsertEquipment() {

		EquipmentDto equipmentDto = new EquipmentDto();
		equipmentDto.setEquipmentId(Integer.valueOf(3));

		assertNotNull(equipmentService.insertEquipment(equipmentDto));
	}

	/**
	 * Method testFindEquipmentById. It tests the method findEquipmentById of the
	 * {@link EquipmentService} class. It should return an object
	 * {@link EquipmentDTO}
	 */
	@Test
	public void testFindEquipmentById() {

		EquipmentDto equipmentDto = new EquipmentDto();
		equipmentDto.setEquipmentId(Integer.valueOf(1));

		assertEquals(equipmentService.findEquipmentById(Mockito.anyInt()).getClass(), equipmentDto.getClass());
	}

	/**
	 * Method testFindAll. It tests the method findAll of the
	 * {@link EquipmentService} class. It should return a list of
	 * {@link EquipmentDTO}
	 */
	@Test
	public void testFindAll() {
		List<Equipment> listEquipmentEntity = new ArrayList<>();
		List<EquipmentDto> listEquipmentDto = new ArrayList<>();
		Mockito.when(mockEquipmentRepository.findAll()).thenReturn(listEquipmentEntity);
		assertEquals(equipmentService.findAll(), listEquipmentDto);
	}

}
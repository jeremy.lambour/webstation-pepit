package org.ws.service.common.comment.association;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.repository.common.comment.CommentRepository;
import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.common.comment.CommentService;

/**
 * Test's Class for the {@link CommentService}
 */
public class CommentServiceTest {

	@InjectMocks
	CommentService commentServiceTest;

	@Mock
	private CommentRepository mockCommentRepo;
	
	private CommentDTO commentDtoTest;
	
	private UserDto userDtoTest;
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Before
	public void initModel() {
		this.userDtoTest = new UserDto();
		this.userDtoTest.setUserId(0);
		this.userDtoTest.setUsername("");
		
		this.commentDtoTest = new CommentDTO();
		this.commentDtoTest.setCommentId(0);
		this.commentDtoTest.setContent("");
		this.commentDtoTest.setFrom(this.userDtoTest);
	}
	
	/**
	 * Method testInsertComment. It tests the method insertComment of the
	 * {@link CommentService} class. It should return a not null
	 * {@link CommentDTO}
	 */
	@Test
	public void testInsertComment() {
		assertNotNull(this.commentServiceTest.insertComment(this.commentDtoTest));
	}
	
	/**
	 * Method testFindCommentById. It tests the method findCommentById of the
	 * {@link CommentService} class. It should return an object
	 * {@link CommentDTO}
	 */
	@Test
	public void testFindCommentById() {
		assertEquals(this.commentServiceTest.findCommentById(Mockito.anyInt()).getClass(), this.commentDtoTest.getClass());
	}
	
	/**
	 * Method testDeleteByCommentId. It tests the methoddeleteByCommentId of the
	 * {@link CommentService} class. It should return a boolean
	 */
	@Test
	public void testDeleteByCommentId() {
		assertTrue(this.commentServiceTest.deleteByCommentId(Mockito.anyInt()));
	}
}

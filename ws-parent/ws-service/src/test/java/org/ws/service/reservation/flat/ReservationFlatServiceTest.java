package org.ws.service.reservation.flat;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.repository.reservation.flat.ReservationFlatRepository;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.service.reservation.flat.ReservationFlatService;

public class ReservationFlatServiceTest {

	@InjectMocks
	ReservationFlatService reservationFlatService;	

	@Mock
	private ReservationFlatRepository mockReservationFlatRepo;
	
	
	
	

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testFindByReservationFlatIdFlatDto. It tests the method findByReservationFlatIdFlatDto of the
	 * {@link ReservationFlatService} class. It should return a list of {@link ReservationFlatDto}
	 */
	@Test
	public void testFindByReservationFlatIdFlatDto() {
		
		List<ReservationFlatDto> listReservationFlatDto = new ArrayList<>();

		assertEquals(reservationFlatService.findByReservationFlatIdFlatDto(Mockito.any()), listReservationFlatDto);
	}
	
	/**
	 * Method testFindByReservationFlatIdReservationDtoUserUserId. It tests the method findByReservationFlatIdReservationDtoUserUserId of the
	 * {@link ReservationFlatService} class. It should return a list of {@link ReservationFlatDto}
	 */
	@Test
	public void testFindByReservationFlatIdReservationDtoUserUserId() {
		
		List<ReservationFlatDto> listReservationFlatDto = new ArrayList<>();

		assertEquals(reservationFlatService.findByReservationFlatIdReservationDtoUserUserId(Mockito.anyInt()), listReservationFlatDto);
	}


}

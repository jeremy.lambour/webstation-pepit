package org.ws.service.core.service.shop;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.shop.ShopScheduleEntity;
import com.techconsulting.ws.dao.repository.shop.ShopScheduleRepository;
import com.techconsulting.ws.service.core.modeldto.shop.ShopScheduleDto;
import com.techconsulting.ws.service.core.service.shop.ShopScheduleService;

/**
 * Test's Class for the {@link ShopScheduleService}
 */
public class ShopScheduleServiceTest {
	
	@InjectMocks
	ShopScheduleService shopScheduleService;

	@Mock
	private ShopScheduleRepository mockShopScheduleRepo;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testFindByShopScheduleIdShopEntityShopId. It tests the method findByShopScheduleIdShopEntityShopId of the
	 * {@link ShopScheduleService} class. It should return an object {@link ShopScheduleDTO}
	 */
	@Test
	public void testFindByShopScheduleIdShopEntityShopId() {
		List<ShopScheduleDto> listShopScheduleDto = new ArrayList<>();
		List<ShopScheduleEntity> listShopSchedule = new ArrayList<>();
		Mockito.when(mockShopScheduleRepo.findByShopScheduleIdShopEntityShopId(Mockito.anyInt())).thenReturn(listShopSchedule);
		assertEquals(shopScheduleService.findByShopScheduleIdShopEntityShopId(Mockito.anyInt()), listShopScheduleDto);
	}
	

	/**
	 * Method testInsertShopSchedule. It tests the method insertShopSchedule of the
	 * {@link ShopScheduleService} class. It should return a not null {@link ShopScheduleDTO}
	 */
	@Test
	public void testInsertShopSchedule() {

		ShopScheduleDto shopScheduleDto = new ShopScheduleDto();

		assertNotNull(shopScheduleService.insertShopSchedule(shopScheduleDto));
	}

}

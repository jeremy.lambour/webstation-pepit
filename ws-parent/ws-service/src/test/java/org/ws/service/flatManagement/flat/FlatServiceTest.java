package org.ws.service.flatManagement.flat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.dao.repository.flatManagement.flat.FlatRepository;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.FlatService;

/**
 * Test's Class for the {@link FlatService}
 */
public class FlatServiceTest {

	@InjectMocks
	FlatService flatService;

	@Mock
	private FlatRepository mockFlatRepo;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertFlat. It tests the method insertFlat of the
	 * {@link FlatService} class. It should return a not null {@link FlatDto}
	 */
	@Test
	public void testInsertFlat() {

		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(Integer.valueOf(1));

		assertNotNull(flatService.insertFlat(flatDto));
	}
	
	/**
	 * Method testFindByHotelDto. It tests the method findByHotelDto of the
	 * {@link FlatService} class. It should return a list of {@link FlatDTO}
	 */
	@Test
	public void testFindByHotelDto() {
		List<FlatEntity> listFlatEntity = new ArrayList<>();
		List<FlatDto> listFlatDto = new ArrayList<>();
		Mockito.when(mockFlatRepo.findByHotelEntity(Mockito.any())).thenReturn(listFlatEntity);
		assertEquals(flatService.findByHotelDto(Mockito.any()), listFlatDto);
	}
	
	/**
	 * Method testFindFlatById. It tests the method findFlatById of the
	 * {@link FlatService} class. It should return an object {@link FlatDTO}
	 */
	@Test
	public void testFindFlatById() {

		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(Integer.valueOf(1));

		assertEquals(flatService.findFlatById(Mockito.anyInt()).getClass(), flatDto.getClass());
	}
	
	/**
	 * Method testFindByFlatCategoryDtoAndHotelDto. It tests the method findByFlatCategoryDtoAndHotelDto of the
	 * {@link FlatService} class. It should return a list of {@link FlatDTO}
	 */
	@Test
	public void testFindByFlatCategoryDtoAndHotelDto() {
		List<FlatEntity> listFlatEntity = new ArrayList<>();
		List<FlatDto> listFlatDto = new ArrayList<>();
		FlatCategoryDto flatCategoryDto = new FlatCategoryDto();
		HotelDto hotelDto = new HotelDto();
		Mockito.when(mockFlatRepo.findByFlatCategoryEntityAndHotelEntity(Mockito.any(), Mockito.any())).thenReturn(listFlatEntity);
		assertEquals(flatService.findByFlatCategoryDtoAndHotelDto(flatCategoryDto, hotelDto), listFlatDto);
	}
	
	/**
	 * Method testSetFlatDescriptionByFlat. It tests the method setFlatDescriptionByFlat of the
	 * {@link FlatService} class. It should return a {@link FlatEntity}
	 */
	@Test
	public void testSetFlatDescriptionByFlat() {
		FlatEntity flat = new FlatEntity();
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(1);
		Mockito.when(mockFlatRepo.findOne(Mockito.anyInt())).thenReturn(flat);
		assertEquals(flatService.setFlatDescriptionByFlat(flatDto, ""), flat);
	}
	
	/**
	 * Method testSetFlatGuidanceByFlat. It tests the method setFlatGuidanceByFlat of the
	 * {@link FlatService} class. It should return a {@link FlatEntity}
	 */
	@Test
	public void testSetFlatGuidanceByFlat() {
		FlatEntity flat = new FlatEntity();
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(1);
		Mockito.when(mockFlatRepo.findOne(Mockito.anyInt())).thenReturn(flat);
		assertEquals(flatService.setFlatGuidanceByFlat(flatDto, ""), flat);
	}
	
	/**
	 * Method testSetFlatCapacityByFlat. It tests the method setFlatCapacityByFlat of the
	 * {@link FlatService} class. It should return a {@link FlatEntity}
	 */
	@Test
	public void testSetFlatCapacityByFlat() {
		FlatEntity flat = new FlatEntity();
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(1);
		Mockito.when(mockFlatRepo.findOne(Mockito.anyInt())).thenReturn(flat);
		assertEquals(flatService.setFlatCapacityByFlat(flatDto, 1), flat);
	}
	
	/**
	 * Method testDeleteByFlatId. It tests the method deleteByFlatId of the
	 * {@link FlatService} class. It should return a {@link boolean} check
	 */
	@Test
	public void testDeleteByFlatId() {
		assertEquals(true, flatService.deleteByFlatId(1));
	}
}

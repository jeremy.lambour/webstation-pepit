package org.ws.service.core.service.accountCreation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.auth.Authority;
import com.techconsulting.ws.dao.repository.auth.AuthorityRepository;
import com.techconsulting.ws.service.core.modeldto.user.AuthorityDto;
import com.techconsulting.ws.service.core.service.accountcreation.AuthorityService;

/**
 * Method test for authority service
 * 
 * @author p_lem
 *
 */
public class AuthorityServiceTest {
		
	@InjectMocks
	AuthorityService mockAuthorityService;
	
	@Mock
	AuthorityRepository mockUserRoleRepo;

	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/*
	 * Test method for method insert role from AuthorityService
	 */
	@Test
	public void testInsertRole() {
		Authority userRole = new Authority();
		AuthorityDto userRoleDto = new AuthorityDto();
		Mockito.when(mockUserRoleRepo.save(userRole)).thenReturn(Mockito.any(Authority.class));
		assertEquals(mockAuthorityService.insertRole(userRoleDto).getClass(), AuthorityDto.class);
	}
	
	/*
	 * Test method for method findByName from AuthorityService
	 */
	@Test
	public void testFindByName() {
		Authority authorityDto = new Authority();
		Mockito.when(mockUserRoleRepo.findByAuthorityName(Mockito.anyString())).thenReturn(authorityDto);
		assertEquals(mockAuthorityService.findByName("").getClass(), AuthorityDto.class);
	}
	
}

package org.ws.service.core.service.accountCreation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.repository.auth.AgeRangeRepository;
import com.techconsulting.ws.dao.repository.auth.UserRepository;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.modeldto.user.AuthorityDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.accountcreation.AccountService;
import com.techconsulting.ws.service.core.service.accountcreation.AgeRangeService;
import com.techconsulting.ws.service.core.service.accountcreation.AuthorityService;
import com.techconsulting.ws.service.core.service.accountcreation.IAccountService;

/**
 * Method test for account service
 * 
 * @author p_lem
 *
 */
public class AccountServiceTest {

	@InjectMocks
	private AccountService mockAccountService;

	@Mock
	private AgeRangeService mockAgeRangeService;

	@Mock
	private AuthorityService mockAuthorityService;

	@Mock
	private UserRepository mockUserRepository;

	@Mock
	private AgeRangeRepository mockAgeRangeRepository;

	@Mock
	private PasswordEncoder mockPasswordEncoder;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test method for method create account from {@link IAccountService}
	 */
	@Test
	public void testCreateAccount() {

		AuthorityDto authority = new AuthorityDto();
		authority.setAuthorityIdDto(1);
		authority.setAuthorityDtoName("USER");

		UserDto userDto = new UserDto();
		userDto.setUserId(Integer.valueOf(3));
		userDto.setPassword("");
		userDto.setUserRoleDto(authority);

		User user = new User();
		user.setUserId(Integer.valueOf(3));

		AgeRangeDto ageRangeDto = new AgeRangeDto();
		ageRangeDto.setAgeRangeId(0);

		Mockito.when(mockAuthorityService.findByName(Matchers.anyString())).thenReturn(authority);
		Mockito.when(mockPasswordEncoder.encode(userDto.getPassword())).thenReturn("");
		Mockito.when(mockAgeRangeService.findAgeRange(Matchers.anyInt())).thenReturn(ageRangeDto);
		Mockito.when(mockUserRepository.save(Matchers.any(User.class))).thenReturn(user);
		assertEquals(mockAccountService.createAccount(userDto).getUserId(), user.getUserId());
	}
	
	/**
	 * Test method for method update account from {@link IAccountService}
	 */
	@Test
	public void testUpdateUser() {
		User user = new User();
		user.setUserId(0);
		AuthorityDto authority = new AuthorityDto();
		authority.setAuthorityIdDto(1);
		authority.setAuthorityDtoName("USER");

		UserDto userDto = new UserDto();
		userDto.setUserId(Integer.valueOf(3));
		userDto.setUsername("");
		userDto.setPassword("");
		userDto.setUserRoleDto(authority);
		
		AgeRangeDto ageRange = new AgeRangeDto();
		Mockito.when(mockUserRepository.findOne(Mockito.anyInt())).thenReturn(user);
		Mockito.when(mockAgeRangeService.findAgeRange(Mockito.anyInt())).thenReturn(ageRange);
		Mockito.when(mockUserRepository.save(Matchers.any(User.class))).thenReturn(user);
		assertEquals(mockAccountService.updateUser(userDto).getClass(), userDto.getClass());
	}
	
	/**
	 * Test method for method compute age from {@link IAccountService}
	 */
	@Test
	public void testComputeAge() {
		assertEquals(0, mockAccountService.computeAge(-1));
		assertEquals(2, mockAccountService.computeAge(5));
		assertEquals(3, mockAccountService.computeAge(10));
		assertEquals(4, mockAccountService.computeAge(20));
		assertEquals(5, mockAccountService.computeAge(70));
		assertEquals(6, mockAccountService.computeAge(80));
	}
}

package org.ws.service.common;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.common.FestivePeriodEntity;
import com.techconsulting.ws.dao.repository.common.FestivePeriodRepository;
import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.service.common.FestivePeriodService;

/**
 * Test's Class for the {@link FestivePeriodService}
 */
public class FestivePeriodServiceTest {

	@InjectMocks
	FestivePeriodService festivePeriodService;

	@Mock
	private FestivePeriodRepository mockFestivePeriodRepo;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testFindAll. It tests the method findAll of the {@link FestivePeriodService}
	 * class. It should return a list of {@link FestivePeriodDTO}
	 */
	@Test
	public void testFindAll() {
		List<FestivePeriodEntity> listFestivePeriodEntity = new ArrayList<>();
		List<FestivePeriodDto> listFestivePeriodDto = new ArrayList<>();
		Mockito.when(mockFestivePeriodRepo.findAll()).thenReturn(listFestivePeriodEntity);
		assertEquals(festivePeriodService.findAll(), listFestivePeriodDto);
	}
	
	/**
	 * Method testFindByFestivePeriodName. It tests the method findByFestivePeriodName of the
	 * {@link FestivePeriodService} class. It should return an object {@link FestivePeriodDTO}
	 */
	@Test
	public void testFindByFestivePeriodName() {

		FestivePeriodDto festivePeriodDto = new FestivePeriodDto();
		festivePeriodDto.setFestivePeriodId(Integer.valueOf(1));

		assertEquals(festivePeriodService.findByFestivePeriodName(Mockito.anyString()).getClass(), festivePeriodDto.getClass());
	}

}

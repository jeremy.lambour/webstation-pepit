package org.ws.service.skiingSchool;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.skiingSchool.Planning;
import com.techconsulting.ws.dao.repository.skiingSchool.PlanningRepository;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.PlanningDto;
import com.techconsulting.ws.service.core.service.common.SeasonService;
import com.techconsulting.ws.service.core.service.skiingSchool.LessonService;
import com.techconsulting.ws.service.core.service.skiingSchool.PlanningService;

public class PlanningServiceTest {

	@InjectMocks
	private PlanningService service;
	
	@Mock
	private PlanningRepository repository;
	
	@Mock
	private LessonService lessonService;
	
	@Mock
	private SeasonService seasonServce;
	
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void isPlanningReallyCreated() {
		Planning planning = new Planning();
		planning.setPlanningId(2);
		Mockito.when(repository.save(Mockito.any(Planning.class))).thenReturn(planning);
		PlanningDto dto = service.createPlanning(new PlanningDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(Planning.class));
		assertEquals(dto.getPlanningId(), planning.getPlanningId());
	}
	
	@Test
	public void isPlanningReallyUpdated() {
		Planning planning = new Planning();
		planning.setPlanningId(2);
		Mockito.when(repository.save(Mockito.any(Planning.class))).thenReturn(planning);
		PlanningDto dto = service.updatePlanning(new PlanningDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(Planning.class));
		assertEquals(dto.getPlanningId(), planning.getPlanningId());
	}
	
	@Test
	public void isPlanningDeleted() {
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(null);
		Boolean isDeleted = service.deleteplanning(2);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, true);
	}

	@Test
	public void isPlanningNotDeleted() {
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(new Planning());
		Boolean isDeleted =service.deleteplanning(2);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, false);
	}
	
	@Test
	public void isPlanningCorrectlyGetWithGivenMonitorIdAndSeasonId() {
		List<Planning> list = new ArrayList<>();
		Planning planning = new Planning();
		planning.setPlanningId(2);
		list.add(planning);
		Mockito.when(repository.findByPlanningMonitorUserIdAndPlanningSeasonSeasonId(Mockito.anyInt(), Mockito.anyInt())).thenReturn(list);
		List<PlanningDto> dtos = service.getAllPlanningByMonitorAnSeasonId(3, 2);
		Mockito.verify(repository, Mockito.atLeast(1)).findByPlanningMonitorUserIdAndPlanningSeasonSeasonId(Mockito.anyInt(), Mockito.anyInt());
		assertEquals(dtos.size(), list.size());
	}
	
	@Test
	public void isPlanningCorrectlyGetWithGivenPlanningId() {
		Planning planning = new Planning();
		planning.setPlanningId(2);
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(planning);
		PlanningDto dto = service.getPlanningById(2);
		Mockito.verify(repository, Mockito.atLeast(1)).findOne(Mockito.anyInt());
		assertEquals(planning.getPlanningId(), dto.getPlanningId());
	}
	
	
	
	
	
}

package org.ws.service.common;

import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.repository.common.SeasonRepository;
import com.techconsulting.ws.service.core.service.common.SeasonService;

/**
 * Test's Class for the {@link SeasonService}
 */
public class SeasonServiceTest {

	@InjectMocks
	SeasonService seasonService;

	@Mock
	private SeasonRepository mockSeasonRepo;
	

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
}

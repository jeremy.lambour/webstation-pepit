package org.ws.service.core.service.reservation.equipment;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.reservation.equipment.ReservationEquipment;
import com.techconsulting.ws.dao.repository.equipment.EquipmentRepository;
import com.techconsulting.ws.dao.repository.reservation.equipment.ReservationEquipmentRepository;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;
import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.service.mail.MailService;
import com.techconsulting.ws.service.core.service.reservation.ReservationService;
import com.techconsulting.ws.service.core.service.reservation.equipment.EquipmentReservationService;

/**
 * Method test for EquipmentReservation service
 * 
 * @author p_lem
 *
 */
public class EquipmentReservationServiceTest {

	@InjectMocks
	EquipmentReservationService mockEquipmentReservationService;

	@Mock
	ReservationEquipmentRepository mockReservationEquipmentRepo;

	@Mock
	EquipmentRepository mockEquipmentRepo;

	@Mock
	ReservationService mockReservationService;

	@Mock
	MailService mockMailService;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testInsertReservEquip. It tests the method insertReservEquip of the
	 * {@link EquipmentReservationService} class.
	 */
	@Test
	public void testInsertReservEquip() {
		ReservationEquipmentDto reservEquipDto = new ReservationEquipmentDto();
		ReservationDto reservDto = new ReservationDto();
		ReservationEquipment reservEquip = new ReservationEquipment();
		Mockito.when(mockReservationService.insertReservation(reservDto)).thenReturn(reservDto);
		Mockito.when(mockReservationEquipmentRepo.save(reservEquip)).thenReturn(reservEquip);
		assertEquals(mockEquipmentReservationService.insertReservEquip(reservEquipDto).getClass(),
				ReservationEquipmentDto.class);
	}

	/**
	 * Method testGetListReservEquipUser. It tests the method getListReservEquipUser
	 * of the {@link EquipmentReservationService} class.
	 */
	@Test
	public void testGetListReservEquipUser() {
		List<ReservationEquipmentDto> listReservEquipDto = new ArrayList<>();
		List<ReservationEquipment> listReservEquip = new ArrayList<>();
		Mockito.when(mockReservationEquipmentRepo.findByReservEquipmentEntityPkReservationUserUserId(Mockito.anyInt()))
				.thenReturn(listReservEquip);
		assertEquals(mockEquipmentReservationService.getListReservEquipUser(1), listReservEquipDto);
	}

	/**
	 * Method testCheckAvailability. It tests the method checkAvailability of the
	 * {@link EquipmentReservationService} class.
	 */
	@Test
	public void testCheckAvailability() {
		ReservationEquipmentDto reservEquipDto = new ReservationEquipmentDto();
		EquipmentDto equiDto = new EquipmentDto();
		equiDto.setEquipmentStock(Integer.valueOf(10));
		equiDto.setEquipmentId(Integer.valueOf(1));
		reservEquipDto.setEquipmentDto(equiDto);
		reservEquipDto.setQuantity(Integer.valueOf(5));
		List<ReservationEquipment> listReservEquip = new ArrayList<>();
		Mockito.when(mockReservationEquipmentRepo.findByReservEquipmentEntityPkEquipmentEquipmentId(Mockito.anyInt()))
				.thenReturn(listReservEquip);
		assertEquals(true, mockEquipmentReservationService.checkAvailability(reservEquipDto));

	}

	/**
	 * Method testCheckStock. It tests the method checkStock of the
	 * {@link EquipmentReservationService} class.
	 */
	@Test
	public void testCheckStock() {
		LocalDateTime now = LocalDateTime.now();
		Timestamp t1 = Timestamp.valueOf(now);
		Timestamp t2 = Timestamp.valueOf(now.plusWeeks(1));
		Timestamp t3 = Timestamp.valueOf(now.plusDays(1));
		Timestamp t4 = Timestamp.valueOf(now.plusDays(3));

		ReservationEquipmentDto reservEquipDto = new ReservationEquipmentDto();
		EquipmentDto equiDto = new EquipmentDto();
		equiDto.setEquipmentStock(Integer.valueOf(10));
		equiDto.setEquipmentId(Integer.valueOf(1));
		reservEquipDto.setEquipmentDto(equiDto);
		reservEquipDto.setQuantity(Integer.valueOf(5));

		reservEquipDto.setDateStart(t3);
		reservEquipDto.setDateEnd(t4);

		List<ReservationEquipmentDto> listReservationEquipment = new ArrayList<>();
		ReservationEquipmentDto reservEquipDto2 = new ReservationEquipmentDto();

		reservEquipDto2.setDateStart(t1);
		reservEquipDto2.setDateEnd(t2);
		reservEquipDto2.setQuantity(2);
		listReservationEquipment.add(reservEquipDto2);

		assertEquals(true, mockEquipmentReservationService.checkStock(reservEquipDto, listReservationEquipment));
	}

	/**
	 * Method testDeleteEquipmentReservation. It tests the method
	 * deleteEquipmentReservation of the {@link EquipmentReservationService} class.
	 */
	@Test
	public void testDeleteEquipmentReservation() {
		ReservationDto reservDto = new ReservationDto();
		reservDto.setReservationId(Integer.valueOf(1));
		EquipmentDto equipmentDto = new EquipmentDto();
		equipmentDto.setEquipmentId(Integer.valueOf(1));
		ReservationEquipmentDto reservEquip = new ReservationEquipmentDto();
		reservEquip.setEquipmentDto(equipmentDto);
		reservEquip.setReservationDto(reservDto);
		assertEquals(true, mockEquipmentReservationService.deleteEquipmentReservation(reservEquip));
	}
}

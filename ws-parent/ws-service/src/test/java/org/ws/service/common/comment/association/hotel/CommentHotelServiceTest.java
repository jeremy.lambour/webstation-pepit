package org.ws.service.common.comment.association.hotel;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.common.comment.association.hotel.CommentHotel;
import com.techconsulting.ws.dao.repository.common.comment.association.flat.CommentHotelRepository;
import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.hotel.CommentHotelDTO;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.common.comment.association.hotel.CommentHotelService;

/**
 * Test's Class for the {@link CommentHotelService}
 */
public class CommentHotelServiceTest {

	@InjectMocks
	CommentHotelService mockCommentHotelServiceTest;

	@Mock
	private CommentHotelRepository mockCommentHotelRepo;
	
	private CommentDTO commentDtoTest;
	
	private UserDto userDtoTest;
	
	private HotelDto hotelTestDto;
	
	private CommentHotelDTO commentHotelTestDto;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Init model used by tests
	 */
	@Before
	public void initModel() {
		this.userDtoTest = new UserDto();
		this.userDtoTest.setUserId(0);
		this.userDtoTest.setUsername("");
		
		this.commentDtoTest = new CommentDTO();
		this.commentDtoTest.setCommentId(0);
		this.commentDtoTest.setContent("");
		this.commentDtoTest.setFrom(this.userDtoTest);
		
		this.hotelTestDto = new HotelDto();
		this.hotelTestDto.setHotelId(0);
		
		this.commentHotelTestDto = new CommentHotelDTO();
		this.commentHotelTestDto.setCommentDto(this.commentDtoTest);
		this.commentHotelTestDto.setHotelDto(this.hotelTestDto);
	}
	
	/**
	 * Method testInsertCommentEquipment. It tests the method insertCommentEquipment of the
	 * {@link CommentHotelService} class. It should return a not null
	 * {@link CommentHotelDTO}
	 */
	@Test
	public void testInsertCommentHotel() {
		assertNotNull(this.mockCommentHotelServiceTest.insertCommentHotel(this.commentHotelTestDto));
	}
	
	/**
	 * Method testFindCommentHotelById. It tests the method findCommentHotelById of the
	 * {@link CommentHotelService} class. It should return a list of
	 * {@link CommentHotelDTO}
	 */
	@Test
	public void testFindCommentHotelById() {
		Mockito.when(this.mockCommentHotelRepo.findByCommentHotelPkHotelHotelId(this.commentHotelTestDto.getHotelDto().getHotelId())).thenReturn(new ArrayList<CommentHotel>());
		Mockito.when(this.mockCommentHotelServiceTest.findListCommentHotelByIdHotel(this.commentHotelTestDto.getHotelDto().getHotelId())).thenReturn(new ArrayList<CommentHotelDTO>());
	}
	
	/**
	 * Method testDeleteByCommentHotel. It tests the method deleteByCommentHotel of the
	 * {@link CommentHotelService} class. It should return a {@link boolean}
	 */
	@Test
	public void testDeleteByCommentHotel() {
		Mockito.when(this.mockCommentHotelRepo.deleteByCommentHotelPkCommentCommentId(this.commentHotelTestDto.getCommentDto().getCommentId())).thenReturn(Mockito.anyBoolean());
		Mockito.when(this.mockCommentHotelRepo.deleteByCommentHotelPkHotelHotelId(this.commentHotelTestDto.getHotelDto().getHotelId())).thenReturn(Mockito.anyBoolean());
		Mockito.when(this.mockCommentHotelServiceTest.deleteByCommentHotel(this.commentHotelTestDto)).thenReturn(true);
	}
}

package org.ws.service.skiingSchool;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.skiingSchool.Lesson;
import com.techconsulting.ws.dao.entity.skiingSchool.Planning;
import com.techconsulting.ws.dao.repository.skiingSchool.LessonRepository;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.LessonDto;
import com.techconsulting.ws.service.core.service.skiingSchool.LessonService;
import com.techconsulting.ws.service.core.service.skiingSchool.PlanningService;

public class LessonServiceTest {

	@InjectMocks
	private LessonService service;

	@Mock
	private LessonRepository repository;

	@Mock
	private PlanningService planningService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void isPlanningServiceReallyCreated() {
		Lesson lesson = new Lesson();
		lesson.setLessonId(2);
		Planning plan = new Planning();
		plan.setPlanningId(2);
		lesson.setLessonPlanning(plan);
		Mockito.when(repository.save(Mockito.any(Lesson.class))).thenReturn(lesson);
		LessonDto dto = service.createLesson(new LessonDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(Lesson.class));
		assertEquals(lesson.getLessonId(), dto.getLessonId());
	}

	@Test
	public void isPlanningServiceReallyUpdated() {
		Lesson lesson = new Lesson();
		lesson.setLessonId(2);
		Planning plan = new Planning();
		plan.setPlanningId(2);
		lesson.setLessonPlanning(plan);
		Mockito.when(repository.save(Mockito.any(Lesson.class))).thenReturn(lesson);
		LessonDto dto = service.updateLesson(new LessonDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(Lesson.class));
		assertEquals(lesson.getLessonId(), dto.getLessonId());
	}

	@Test
	public void isLessonDeleted() {
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(null);
		Boolean isDeleted = service.deleteLesson(2);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, true);
	}

	@Test
	public void isLessonNotDeleted() {
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(new Lesson());
		Boolean isDeleted = service.deleteLesson(2);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, false);
	}
	
	@Test
	public void isLessonsGetWithPlanningId() {
		Lesson lesson = new Lesson();
		lesson.setLessonId(2);
		Planning plan = new Planning();
		plan.setPlanningId(2);
		lesson.setLessonPlanning(plan);
		List<Lesson> list = new LinkedList<Lesson>();
		list.add(lesson);
		list.add(lesson);
		Mockito.when(repository.findByLessonPlanningPlanningId(Mockito.anyInt())).thenReturn(list);
		List<LessonDto> dtos = service.getLessonsByPLanningId(2);
		Mockito.verify(repository, Mockito.atLeast(1)).findByLessonPlanningPlanningId(Mockito.anyInt());
		assertEquals(dtos.size(), list.size());
	}
}

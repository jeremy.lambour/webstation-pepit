package org.ws.service.core.service.shop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.shop.ShopEntity;
import com.techconsulting.ws.dao.repository.auth.AgeRangeRepository;
import com.techconsulting.ws.dao.repository.auth.UserRepository;
import com.techconsulting.ws.dao.repository.shop.ShopRepository;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.shop.ShopService;

/**
 * Test's Class for the {@link ShopService}
 */
public class ShopServiceTest {

	@InjectMocks
	ShopService shopService;

	@Mock
	private ShopRepository mockShopRepo;
	
	@Mock
	private UserRepository mockUserRepo;
	
	@Mock
	private AgeRangeRepository mockAgeRangeRepo;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertShop. It tests the method insertShop of the
	 * {@link ShopService} class. It should return a not null {@link ShopDTO}
	 */
	@Test
	public void testInsertShop() {

		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(3));

		assertNotNull(shopService.insertShop(shopDto));
	}
	
	/**
	 * Method testFindShopById. It tests the method findShopById of the
	 * {@link ShopService} class. It should return an object {@link ShopDTO}
	 */
	@Test
	public void testFindShopById() {

		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(1));

		assertEquals(shopService.findShopById(Mockito.anyInt()).getClass(), shopDto.getClass());
	}
	
	/**
	 * Method testFindAll. It tests the method findAll of the {@link ShopService}
	 * class. It should return a list of {@link ShopDTO}
	 */
	@Test
	public void testFindAll() {
		List<ShopEntity> listShopEntity = new ArrayList<>();
		List<ShopDto> listShopDto = new ArrayList<>();
		Mockito.when(mockShopRepo.findAll()).thenReturn(listShopEntity);
		assertEquals(shopService.findAll(), listShopDto);
	}

	/**
	 * Method testSetShopNameByShop. It tests the method setShopNameByShop of the {@link ShopService}
	 * class. It should return a list of {@link ShopEntity}
	 */
	@Test
	public void testSetShopNameByShop() {
		ShopEntity shop = new ShopEntity();
		shop.setShopName("test");
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(1);;
		Mockito.when(mockShopRepo.findOne(Mockito.anyInt())).thenReturn(shop);
		assertEquals(shopService.setShopNameByShop(shopDto,"test").getClass(), ShopEntity.class);
	}
	
	/**
	 * Method testSetShopAddressByShop. It tests the method setShopAddressByShop of the {@link ShopService}
	 * class. It should return a list of {@link ShopEntity}
	 */
	@Test
	public void testSetShopAddressByShop() {
		ShopEntity shop = new ShopEntity();
		shop.setShopName("test");
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(1);;
		Mockito.when(mockShopRepo.findOne(Mockito.anyInt())).thenReturn(shop);
		assertEquals(shopService.setShopAddressByShop(shopDto,"test").getClass(), ShopEntity.class);
	}
	
	/**
	 * Method testSetShopPhoneNumberByShop. It tests the method setShopPhoneNumberByShop of the {@link ShopService}
	 * class. It should return a list of {@link ShopEntity}
	 */
	@Test
	public void testSetShopPhoneNumberByShop() {
		ShopEntity shop = new ShopEntity();
		shop.setShopName("test");
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(1);;
		Mockito.when(mockShopRepo.findOne(Mockito.anyInt())).thenReturn(shop);
		assertEquals(shopService.setShopPhoneNumberByShop(shopDto,"test").getClass(), ShopEntity.class);
	}
	
	/**
	 * Method testSetShopUserDtoByShop. It tests the method setShopUserDtoByShop of the {@link ShopService}
	 * class. It should return a list of {@link ShopEntity}
	 */
	@Test
	public void testSetShopUserDtoByShop() {
		ShopEntity shop = new ShopEntity();
		shop.setShopName("test");
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(1);;
		UserDto userDto = new UserDto();
		Mockito.when(mockShopRepo.findOne(Mockito.anyInt())).thenReturn(shop);
		assertEquals(shopService.setShopUserDtoByShop(shopDto,userDto).getClass(), ShopEntity.class);
	}
	
	/**
	 * Method testDeleteByShopId. It tests the method deleteByShopId of the {@link ShopService}
	 * class. It should return a list of {@link boolean} check
	 */
	@Test
	public void testDeleteByShopId() {
		assertEquals(true, shopService.deleteByShopId(1));
	}

	
}
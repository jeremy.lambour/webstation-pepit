package org.ws.service.flatManagement.hotel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;
import com.techconsulting.ws.dao.repository.flatManagement.hotel.HotelRepository;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.flatManagement.hotel.HotelService;

/**
 * Test's Class for the {@link HotelService}
 */
public class HotelServiceTest {

	@InjectMocks
	HotelService hotelService;

	@Mock
	private HotelRepository mockHotelRepo;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testInsertHotel. It tests the method insertHotel of the
	 * {@link HotelService} class. It should return a not null {@link HotelDto}
	 */
	@Test
	public void testInsertHotel() {

		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));

		assertNotNull(hotelService.insertHotel(hotelDto));
	}
	
	/**
	 * Method testFindAll. It tests the method findAll of the {@link HotelService}
	 * class. It should return a list of {@link HotelDto}
	 */
	@Test
	public void testFindAll() {
		List<HotelEntity> listHotelEntity = new ArrayList<>();
		List<HotelDto> listHotelDto = new ArrayList<>();
		Mockito.when(mockHotelRepo.findAll()).thenReturn(listHotelEntity);
		assertEquals(hotelService.findAll(), listHotelDto);
	}
	
	/**
	 * Method testFindHotelById. It tests the method findHotelById of the
	 * {@link HotelService} class. It should return an object {@link HotelDto}
	 */
	@Test
	public void testFindHotelById() {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));

		assertEquals(hotelService.findHotelById(Mockito.anyInt()).getClass(), hotelDto.getClass());
	}
	
	/**
	 * Method testSetHotelNameByHotel. It tests the method setHotelNameByHotel of the
	 * {@link HotelService} class. It should return an object {@link HotelEntity}
	 */
	@Test
	public void testSetHotelNameByHotel() {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		HotelEntity hotel = new HotelEntity();
		hotel.setHotelId(Integer.valueOf(1));
		Mockito.when(mockHotelRepo.findOne(Mockito.anyInt())).thenReturn(hotel);
		assertEquals(hotelService.setHotelNameByHotel(hotelDto, "").getClass(), HotelEntity.class);
	}
	
	/**
	 * Method testSetHotelAddressByHotel. It tests the method setHotelAddressByHotel of the
	 * {@link HotelService} class. It should return an object {@link HotelEntity}
	 */
	@Test
	public void testSetHotelAddressByHotel() {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		HotelEntity hotel = new HotelEntity();
		hotel.setHotelId(Integer.valueOf(1));
		Mockito.when(mockHotelRepo.findOne(Mockito.anyInt())).thenReturn(hotel);
		assertEquals(hotelService.setHotelAddressByHotel(hotelDto, "").getClass(), HotelEntity.class);
	}
	
	/**
	 * Method testSetHotelRoomsNumberByHotel. It tests the method setHotelRoomsNumberByHotel of the
	 * {@link HotelService} class. It should return an object {@link HotelEntity}
	 */
	@Test
	public void testSetHotelRoomsNumberByHotel() {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		HotelEntity hotel = new HotelEntity();
		hotel.setHotelId(Integer.valueOf(1));
		Mockito.when(mockHotelRepo.findOne(Mockito.anyInt())).thenReturn(hotel);
		assertEquals(hotelService.setHotelRoomsNumberByHotel(hotelDto, 1).getClass(), HotelEntity.class);
	}
	
	/**
	 * Method testSetHotelPhoneNumberByHotel. It tests the method setHotelPhoneNumberByHotel of the
	 * {@link HotelService} class. It should return an object {@link HotelEntity}
	 */
	@Test
	public void testSetHotelPhoneNumberByHotel() {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		HotelEntity hotel = new HotelEntity();
		hotel.setHotelId(Integer.valueOf(1));
		Mockito.when(mockHotelRepo.findOne(Mockito.anyInt())).thenReturn(hotel);
		assertEquals(hotelService.setHotelPhoneNumberByHotel(hotelDto, "").getClass(), HotelEntity.class);
	}
	
	/**
	 * Method testSetHotelMailAddressByHotel. It tests the method setHotelMailAddressByHotel of the
	 * {@link HotelService} class. It should return an object {@link HotelEntity}
	 */
	@Test
	public void testSetHotelMailAddressByHotel() {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		HotelEntity hotel = new HotelEntity();
		hotel.setHotelId(Integer.valueOf(1));
		Mockito.when(mockHotelRepo.findOne(Mockito.anyInt())).thenReturn(hotel);
		assertEquals(hotelService.setHotelMailAddressByHotel(hotelDto, "").getClass(), HotelEntity.class);
	}
	
	/**
	 * Method testSetHotelUserDtoByHotel. It tests the method setHotelUserDtoByHotel of the
	 * {@link HotelService} class. It should return an object {@link HotelEntity}
	 */
	@Test
	public void testSetHotelUserDtoByHotel() {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		HotelEntity hotel = new HotelEntity();
		hotel.setHotelId(Integer.valueOf(1));
		Mockito.when(mockHotelRepo.findOne(Mockito.anyInt())).thenReturn(hotel);
		assertEquals(hotelService.setHotelUserDtoByHotel(hotelDto, new UserDto()).getClass(), HotelEntity.class);
	}
	
	/**
	 * Method testDeleteByHotelId. It tests the method testDeleteByHotelId of the
	 * {@link HotelService} class. It should return a {@link boolean} check
	 */
	@Test
	public void testDeleteByHotelId() {
		assertEquals(true, hotelService.deleteByHotelId(1));
	}
}

package org.ws.service.nursery;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.nursery.NurseryPlanning;
import com.techconsulting.ws.dao.entity.nursery.NurseryPlanningSlot;
import com.techconsulting.ws.dao.repository.nursery.NurseryPlanningSlotRepository;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningDto;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningSlotDto;
import com.techconsulting.ws.service.core.service.nursery.NurseryPlanningService;
import com.techconsulting.ws.service.core.service.nursery.NurseryPlanningSlotService;

public class NurseryPlanningSlotServiceTest {

	@InjectMocks
	private NurseryPlanningSlotService service;

	@Mock
	private NurseryPlanningSlotRepository repository;

	@Mock
	private NurseryPlanningService nurseryPlanningService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void isNurseryPlanningSlotCreated() {
		NurseryPlanningSlot slot = new NurseryPlanningSlot();
		slot.setNurseryPlanningSlotId(2);
		slot.setNurseryPlanning(new NurseryPlanning());

		Mockito.when(nurseryPlanningService.getNurseryPlanningById(Mockito.anyInt()))
				.thenReturn(new NurseryPlanningDto());
		Mockito.when(repository.save(Mockito.any(NurseryPlanningSlot.class))).thenReturn(slot);
		NurseryPlanningSlotDto dto = service.createNurseryPlanningSlot(new NurseryPlanningSlotDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(NurseryPlanningSlot.class));
		assertEquals(slot.getNurseryPlanningSlotId(), dto.getNurseryPlanningSlotId());
	}

	@Test
	public void isNurseryPlanningSlotUpdated() {
		NurseryPlanningSlot slot = new NurseryPlanningSlot();
		slot.setNurseryPlanningSlotId(2);
		slot.setNurseryPlanning(new NurseryPlanning());
		Mockito.when(nurseryPlanningService.getNurseryPlanningById(Mockito.anyInt()))
				.thenReturn(new NurseryPlanningDto());
		Mockito.when(repository.save(Mockito.any(NurseryPlanningSlot.class))).thenReturn(slot);
		NurseryPlanningSlotDto dto = service.updateNurseryPlanningSlot(new NurseryPlanningSlotDto());
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(NurseryPlanningSlot.class));
		assertEquals(slot.getNurseryPlanningSlotId(), dto.getNurseryPlanningSlotId());
	}

	@Test
	public void isNurseryPlanningSlotDeleted() {
		Integer id = 2;
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(new NurseryPlanningSlot());
		Boolean isDeleted = service.deleteNurseryPlanningSlot(id);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, false);
	}

	@Test
	public void isNurseryPlanningSlotNotDeleted() {
		Integer id = 2;
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(new NurseryPlanningSlot());
		Boolean isDeleted = service.deleteNurseryPlanningSlot(id);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted, false);
	}

	@Test
	public void isNurseryPlanningSlotCorrectlyGetWithPlanningId() {
		List<NurseryPlanningSlot> list = new ArrayList<>();
		NurseryPlanningSlot slot = new NurseryPlanningSlot();
		slot.setNurseryPlanning(new NurseryPlanning());
		list.add(slot);
		Mockito.when(repository.findByNurseryPlanningNurseryPlanningId(Mockito.anyInt())).thenReturn(list);
		List<NurseryPlanningSlotDto> dtos = service.getNurseryPlanningSlotByNurseryPlanningId(2);
		Mockito.verify(repository, Mockito.atLeast(1)).findByNurseryPlanningNurseryPlanningId(Mockito.anyInt());
		assertEquals(list.size(), dtos.size());
	}

}

package org.ws.service.common.comment.association.equipment;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.common.comment.association.equipment.CommentEquipment;
import com.techconsulting.ws.dao.repository.common.comment.association.equipment.CommentEquipmentRepository;
import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.equipment.CommentEquipmentDTO;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.common.comment.association.equipment.CommentEquipmentService;

/**
 * Test's Class for the {@link CommentEquipmentService}
 */
public class CommentEquipmentServiceTest {

	@InjectMocks
	CommentEquipmentService mockCommentEquipmentServiceTest;

	@Mock
	private CommentEquipmentRepository mockCommentEquimentRepo;
	
	private CommentDTO commentDtoTest;
	
	private UserDto userDtoTest;
	
	private EquipmentDto equipmentTestDto;
	
	private CommentEquipmentDTO commentEquipmentTestDto;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Init model used by tests
	 */
	@Before
	public void initModel() {
		this.userDtoTest = new UserDto();
		this.userDtoTest.setUserId(0);
		this.userDtoTest.setUsername("");
		
		this.commentDtoTest = new CommentDTO();
		this.commentDtoTest.setCommentId(0);
		this.commentDtoTest.setContent("");
		this.commentDtoTest.setFrom(this.userDtoTest);
		
		this.equipmentTestDto = new EquipmentDto();
		this.equipmentTestDto.setEquipmentId(0);
		
		this.commentEquipmentTestDto = new CommentEquipmentDTO();
		this.commentEquipmentTestDto.setCommentDto(this.commentDtoTest);
		this.commentEquipmentTestDto.setEquipmentDto(this.equipmentTestDto);
	}
	
	/**
	 * Method testInsertCommentEquipment. It tests the method insertCommentEquipment of the
	 * {@link CommentEquipmentService} class. It should return a not null
	 * {@link CommentEquipmentDTO}
	 */
	@Test
	public void testInsertCommentEquipment() {
		assertNotNull(this.mockCommentEquipmentServiceTest.insertCommentEquipment(this.commentEquipmentTestDto));
	}
	
	/**
	 * Method testFindCommentEquipmentById. It tests the method findCommentEquipmentById of the
	 * {@link CommentEquipmentService} class. It should return a list of
	 * {@link CommentEquipmentDTO}
	 */
	@Test
	public void testFindCommentEquipmentById() {
		Mockito.when(this.mockCommentEquimentRepo.findByCommentEquipmentPkEquipmentEquipmentId(this.commentEquipmentTestDto.getEquipmentDto().getEquipmentId())).thenReturn(new ArrayList<CommentEquipment>());
		Mockito.when(this.mockCommentEquipmentServiceTest.findByCommentEquipmentPkEquipment(this.commentEquipmentTestDto.getEquipmentDto().getEquipmentId())).thenReturn(new ArrayList<CommentEquipmentDTO>());
	}
	
	/**
	 * Method testDeleteByCommentEquipment. It tests the method deleteByCommentEquipment of the
	 * {@link CommentEquipmentService} class. It should return a {@link boolean}
	 */
	@Test
	public void testDeleteByCommentEquipment() {
		Mockito.when(this.mockCommentEquimentRepo.deleteByCommentEquipmentPkCommentCommentId(this.commentEquipmentTestDto.getCommentDto().getCommentId())).thenReturn(Mockito.anyBoolean());
		Mockito.when(this.mockCommentEquimentRepo.deleteByCommentEquipmentPkEquipmentEquipmentId(this.commentEquipmentTestDto.getEquipmentDto().getEquipmentId())).thenReturn(Mockito.anyBoolean());
		Mockito.when(this.mockCommentEquipmentServiceTest.deleteByCommentEquipment(this.commentEquipmentTestDto)).thenReturn(true);
	}
}

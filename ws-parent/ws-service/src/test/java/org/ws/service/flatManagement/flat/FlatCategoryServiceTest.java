package org.ws.service.flatManagement.flat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatCategoryEntity;
import com.techconsulting.ws.dao.repository.flatManagement.flat.FlatCategoryRepository;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.FlatCategoryService;

/**
 * Test's Class for the {@link FlatCategoryService}
 */
public class FlatCategoryServiceTest {

	@InjectMocks
	FlatCategoryService flatCategoryService;

	@Mock
	private FlatCategoryRepository mockFlatCategoryRepo;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertFlatCategory. It tests the method insertFlatCategory of the
	 * {@link FlatCategoryService} class. It should return a not null {@link FlatCategoryDto}
	 */
	@Test
	public void testInsertFlatCategory() {

		FlatCategoryDto flatCategoryDto = new FlatCategoryDto();
		flatCategoryDto.setFlatCategoryId(Integer.valueOf(1));

		assertNotNull(flatCategoryService.insertFlatCategory(flatCategoryDto));
	}
	
	/**
	 * Method testFindAll. It tests the method findAll of the {@link FlatCategoryService}
	 * class. It should return a list of {@link FlatCategoryDto}
	 */
	@Test
	public void testFindAll() {
		List<FlatCategoryEntity> listFlatCategoryEntity = new ArrayList<>();
		List<FlatCategoryDto> listFlatCategoryDto = new ArrayList<>();
		Mockito.when(mockFlatCategoryRepo.findAll()).thenReturn(listFlatCategoryEntity);
		assertEquals(flatCategoryService.findAll(), listFlatCategoryDto);
	}
	
	/**
	 * Method testFindFlatCategoryById. It tests the method findFlatCategoryById of the
	 * {@link FlatCategoryService} class. It should return an object {@link FlatCategoryDto}
	 */
	@Test
	public void testFindFlatCategoryById() {

		FlatCategoryDto flatCategoryDto = new FlatCategoryDto();
		flatCategoryDto.setFlatCategoryId(Integer.valueOf(1));

		assertEquals(flatCategoryService.findFlatCategoryById(Mockito.anyInt()).getClass(), flatCategoryDto.getClass());
	}
}

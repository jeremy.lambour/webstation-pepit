package org.ws.service.nursery;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.nursery.NurseryPlanning;
import com.techconsulting.ws.dao.repository.nursery.NurseryPlanningRepository;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningDto;
import com.techconsulting.ws.service.core.service.nursery.NurseryPlanningService;
import com.techconsulting.ws.service.core.service.nursery.NurseryPlanningSlotService;

public class NurseryPlanningServiceTest {

	@InjectMocks
	private NurseryPlanningService service;
	
	@Mock
	private NurseryPlanningRepository repository;
	
	@Mock
	private NurseryPlanningSlotService slotService;
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void NurseryPlanningServiceReturnNurseryPlanning() {
		List<NurseryPlanning> plannings = new LinkedList<NurseryPlanning>();
		plannings.add(new NurseryPlanning());
		Mockito.when(repository.findByNurseryNurseryIdAndSeasonSeasonId(Mockito.anyInt(), Mockito.anyInt())).thenReturn(plannings);
		List<NurseryPlanningDto> nurseryPlanning = service.getNurseryPlanningByNurseryIdAndSeasonId(2, 3);
		Mockito.verify(repository, Mockito.atLeast(1)).findByNurseryNurseryIdAndSeasonSeasonId(Mockito.anyInt(),Mockito.anyInt());
		assertEquals(nurseryPlanning.size(), plannings.size());
	}
	
	@Test
	public void isCreatedNurseryPlanningNotAlteredDuringPersist() {
		NurseryPlanningDto nursery = new NurseryPlanningDto();
		nursery.setNurseryPlanningName("test planning");
		NurseryPlanning entity = new NurseryPlanning();
		entity.setNurseryPlanningName(nursery.getNurseryPlanningName());
		Mockito.when(repository.save(Mockito.any(NurseryPlanning.class))).thenReturn(entity);
		NurseryPlanningDto created = service.createNurseryPlanning(nursery);
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(NurseryPlanning.class));
		assertEquals(created.getNurseryPlanningName(), nursery.getNurseryPlanningName());
	}
	
	
	@Test
	public void NurseryPlanningServiceReturnUpdatedNurseryPlanning() {
		NurseryPlanningDto nursery = new NurseryPlanningDto();
		nursery.setNurseryPlanningName("test planning");
		NurseryPlanning entity = new NurseryPlanning();
		entity.setNurseryPlanningName(nursery.getNurseryPlanningName());
		Mockito.when(repository.save(Mockito.any(NurseryPlanning.class))).thenReturn(entity);
		NurseryPlanningDto updated = service.updateNurseryPlanning(nursery);
		Mockito.verify(repository, Mockito.atLeast(1)).save(Mockito.any(NurseryPlanning.class));
		assertEquals(updated.getNurseryPlanningName(), nursery.getNurseryPlanningName());
	}
	
	@Test
	public void isNurseryPlanningDeleted() {
		Integer id = 2;
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(null);
		Boolean isDeleted = service.deleteNurseryPlanning(id);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted,true);
	}
	
	@Test
	public void isNurseryPlanningNotDeleted() {
		Integer id = 2;
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(new NurseryPlanning());
		Boolean isDeleted = service.deleteNurseryPlanning(id);
		Mockito.verify(repository, Mockito.atLeast(1)).delete(Mockito.anyInt());
		assertEquals(isDeleted,false);
	}
	
	@Test
	public void isNurseryPlanningFindWithId() {
		NurseryPlanning planning = new NurseryPlanning();
		planning.setNurseryPlanningId(2);
		Mockito.when(repository.findOne(Mockito.anyInt())).thenReturn(planning);
		Mockito.when(slotService.getNurseryPlanningSlotByNurseryPlanningId(Mockito.anyInt())).thenReturn(new ArrayList<>());
		NurseryPlanningDto find = service.getNurseryPlanningById(2);
		Mockito.verify(repository, Mockito.atLeast(1)).findOne(Mockito.anyInt());
		Mockito.verify(slotService, Mockito.atLeast(1)).getNurseryPlanningSlotByNurseryPlanningId(Mockito.anyInt());
		assertEquals(find.getNurseryPlanningId(),planning.getNurseryPlanningId());
	}
	
	
}

package org.ws.service.flatManagement.flat;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.repository.flatManagement.flat.DefineRepository;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.DefineDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.DefineService;


/**
 * Test's Class for the {@link DefineService}
 */
public class DefineServiceTest {

	@InjectMocks
	DefineService defineService;

	@Mock
	private DefineRepository mockDefineRepo;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testFindByFestivePeriodDtoAndFlatDto. It tests the method findByFestivePeriodDtoAndFlatDto of the
	 * {@link DefineService} class. It should return an object {@link DefineDto}
	 */
	@Test
	public void testFindByFestivePeriodDtoAndFlatDto() {

		DefineDto defineDto = new DefineDto();

		assertEquals(defineService.findByFestivePeriodDtoAndFlatDto(Mockito.any(), Mockito.any()).getClass(), defineDto.getClass());
	}

}

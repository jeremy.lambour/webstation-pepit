package org.ws.service.core.service.reservation.equipment.skilift;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.entity.equipment.SkiLift;
import com.techconsulting.ws.dao.entity.reservation.lift.ReservationLift;
import com.techconsulting.ws.dao.entity.reservation.lift.ReservationLiftPK;
import com.techconsulting.ws.dao.repository.reservation.lift.ReservationSkiLiftRepository;
import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.reservation.equipment.skilift.SkiLiftReservationService;

/**
 * Method test for SkiLiftReservation service
 * 
 * @author p_lem
 *
 */
public class SkiLiftReservationServiceTest {

	@InjectMocks
	SkiLiftReservationService mockSkiLiftReservationService;
	
	@Mock
	ReservationSkiLiftRepository mockReservationSkiLiftRepo;
	
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertReservSkiLift. It tests the method insertReservSkiLift of the
	 * {@link SkiLiftReservationService} class.
	 */
	@Test
	public void testInsertReservSkiLift() {
		ReservationLiftDto reservSkiLiftDto = new ReservationLiftDto();
		ReservationLift reservSkiLift = new ReservationLift();
		ReservationLiftPK reservationLiftPK = new ReservationLiftPK();
		User user = new User();
		SkiLift skiLift = new SkiLift();
		reservationLiftPK.setSkiLift(skiLift);
		reservationLiftPK.setUser(user);
		reservSkiLift.setReservLiftPk(reservationLiftPK);
		
		Mockito.when(mockReservationSkiLiftRepo.save(Mockito.any(ReservationLift.class))).thenReturn(reservSkiLift);
		assertEquals(mockSkiLiftReservationService.insertReservSkiLift(reservSkiLiftDto).getClass(), ReservationLiftDto.class);
	}
	
	/**
	 * Method testGetListReservSkiLiftUser. It tests the method getListReservSkiLiftUser of the
	 * {@link SkiLiftReservationService} class.
	 */
	@Test
	public void testGetListReservSkiLiftUser() {
		List<ReservationLift> listReservLift = new ArrayList<>();
		List<ReservationLiftDto> listReservLiftDto = new ArrayList<>();
		Mockito.when(mockReservationSkiLiftRepo.findByReservLiftPkUserUserId(Mockito.anyInt())).thenReturn(listReservLift);
		assertEquals(mockSkiLiftReservationService.getListReservSkiLiftUser(1), listReservLiftDto);
	}
	
	/**
	 * Method testDeleteSkiLiftReservation. It tests the method deleteSkiLiftReservation of the
	 * {@link SkiLiftReservationService} class.
	 */
	@Test
	public void testDeleteSkiLiftReservation() {
		ReservationLiftDto reservSkiLift = new ReservationLiftDto();
		UserDto userDto = new UserDto();
		userDto.setUserId(Integer.valueOf(1));
		SkiLiftDto skiLiftDto = new SkiLiftDto();
		skiLiftDto.setSkiLiftId(Integer.valueOf(1));
		reservSkiLift.setUserDto(userDto);
		reservSkiLift.setSkiLiftDto(skiLiftDto);
		assertEquals(true, mockSkiLiftReservationService.deleteSkiLiftReservation(reservSkiLift));
	}
}

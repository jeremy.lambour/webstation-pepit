package com.techconsulting.ws.service.core.service.reservation.equipment.skilift;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;

public interface ISkiLiftReservationService {

	public ReservationLiftDto insertReservSkiLift(ReservationLiftDto reservSkiLiftDto);
	
	public List<ReservationLiftDto> getListReservSkiLiftUser(int idUser);
	
	public boolean deleteSkiLiftReservation(ReservationLiftDto reservLift);
}

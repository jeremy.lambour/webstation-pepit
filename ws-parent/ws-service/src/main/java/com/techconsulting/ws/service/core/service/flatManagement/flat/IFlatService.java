package com.techconsulting.ws.service.core.service.flatManagement.flat;

import java.util.List;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;

/**
 * The Interface of {@link FlatDto}
 */
public interface IFlatService {
	FlatDto insertFlat(FlatDto flatDTO);
	List<FlatDto> findByHotelDto(HotelDto hotelDto);
	FlatDto findFlatById(Integer idFlat);
	List<FlatDto> findByFlatCategoryDtoAndHotelDto(FlatCategoryDto flatCategoryDto, HotelDto hotelDto);
	FlatEntity setFlatDescriptionByFlat(FlatDto flatDto, String description);
	FlatEntity setFlatGuidanceByFlat(FlatDto flatDto, String guidance);
	FlatEntity setFlatCapacityByFlat(FlatDto flatDto, Integer capacity);
	boolean deleteByFlatId(Integer idFlat);
	List<FlatDto> findAllFlat();
}

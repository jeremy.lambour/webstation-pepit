package com.techconsulting.ws.service.core.parser.equipment;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.equipment.Equipment;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.parser.ShopParser;

/**
 * Parser for {@link Equipment} and {@link EquipmentDto}
 * 
 * @author p_lem
 *
 */
public class EquipmentParser {

	/**
	 * Constructor of EquipmentParser
	 */
	private EquipmentParser() {
		super();
	}

	/**
	 * Convert a {@link Equipment} entity to a {@link EquipmentDto}
	 * 
	 * @param {@link Equipment} to set
	 * @return {@link EquipmentDto} object
	 */
	public static EquipmentDto convertToDto(Equipment equip) {
		EquipmentDto equipDTO;
		if (equip != null) {
			equipDTO = new EquipmentDto();
			equipDTO.setEquipmentId(equip.getEquipmentId());
			equipDTO.setEquipmentName(equip.getEquipmentName());
			equipDTO.setEquipmentDescription(equip.getEquipmentDescription());
			equipDTO.setEquipmentState(equip.getEquipmentState());
			equipDTO.setEquipmentStock(equip.getEquipmentStock());
			equipDTO.setEquipmentOut(equip.getEquipmentOut());
			equipDTO.setEquipmentSize(equip.getEquipmentSize());
			equipDTO.setPriceLocation(equip.getPriceLocation());
			equipDTO.setShop(ShopParser.convertToDto(equip.getShop()));
			return equipDTO;
		}
		return new EquipmentDto();
	}

	/**
	 * Convert a {@link EquipmentDto} to a {@link Equipment} entity
	 * 
	 * @param {@link EquipmentDto} to set
	 * @return {@link Equipment}
	 */
	public static Equipment convertToEntity(EquipmentDto equipDTO) {
		Equipment equip;
		if (equipDTO != null) {
			equip = new Equipment();
			equip.setEquipmentId(equipDTO.getEquipmentId());
			equip.setEquipmentName(equipDTO.getEquipmentName());
			equip.setEquipmentDescription(equipDTO.getEquipmentDescription());
			equip.setEquipmentState(equipDTO.getEquipmentState());
			equip.setEquipmentStock(equipDTO.getEquipmentStock());
			equip.setEquipmentOut(equipDTO.getEquipmentOut());
			equip.setEquipmentSize(equipDTO.getEquipmentSize());
			equip.setPriceLocation(equipDTO.getPriceLocation());
			equip.setShop(ShopParser.convertToEntity(equipDTO.getShop()));
			return equip;
		}
		return new Equipment();
	}

	/**
	 * Convert a list of {@link Equipment} to a list of {@link EquipmentDto}
	 * 
	 * @param listEquip to set
	 * @return listEquipDto
	 */
	public static List<EquipmentDto> convertListToDTO(List<Equipment> listEquip) {
		return listEquip.stream().map(x -> convertToDto(x)).collect(Collectors.toList());

	}

	/**
	 * Convert a list of {@link EquipmentDto} to a list of {@link Equipment}
	 * 
	 * @param listEquipDto to set
	 * @return listEquip
	 */
	public static List<Equipment> convertListToEntity(List<EquipmentDto> listEquipDto) {
		return listEquipDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

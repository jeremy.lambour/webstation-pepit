package com.techconsulting.ws.service.core.service.reservation.flat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.reservation.flat.ReservationFlat;
import com.techconsulting.ws.dao.repository.reservation.flat.ReservationFlatRepository;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.parser.FlatParser;
import com.techconsulting.ws.service.core.parser.reservation.flat.ReservationFlatParser;
import com.techconsulting.ws.service.core.service.reservation.IReservation;

/**
 * Service for manage Reservation for flat
 * 
 * @author Clara
 *
 */
@Service
@Transactional(readOnly = false)
public class ReservationFlatService implements IReservationFlat {

	@Autowired
	ReservationFlatRepository reservationFlatRepo;

	@Autowired
	IReservation iReservation;

	/**
	 * Method that finds a list of {@link ReservationFlatDto}s from a flat
	 * identifier
	 * 
	 * @Param - {@link FlatDto} flatDto, we want to find {@link ReservationFlatDto}s
	 *        that have this flatDto
	 * @Return - A list of {@link RerservationFlatDto}s
	 */
	@Override
	public List<ReservationFlatDto> findByReservationFlatIdFlatDto(FlatDto flatDto) {
		return ReservationFlatParser
				.convertListToDTO(reservationFlatRepo.findByReservationFlatIdFlat(FlatParser.convertToEntity(flatDto)));
	}

	/**
	 * Method that finds a list of {@link ReservationFlatDto}s from a user
	 * identifier
	 * 
	 * @Param - Integer userId, we want to find {@link ReservationFlatDto}s that
	 *        have this user
	 * @Return - A list of {@link RerservationFlatDto}s
	 */
	@Override
	public List<ReservationFlatDto> findByReservationFlatIdReservationDtoUserUserId(Integer userId) {
		return ReservationFlatParser
				.convertListToDTO(reservationFlatRepo.findByReservationFlatIdReservationUserUserId(userId));
	}

	/**
	 * Method that inserts an {@link ReservationFlat} in DataBase
	 * 
	 * @Param - {@link ReservationFlatDto} rFlat
	 * @Return - the {@link ReservationFlatDto} inserted
	 */
	@Override
	public ReservationFlatDto insertReservationFlat(ReservationFlatDto rFlat) {
		rFlat.setReservationDto(iReservation.insertReservation(rFlat.getReservationDto()));
		ReservationFlat reservationFlat = ReservationFlatParser.convertToEntity(rFlat);
		return ReservationFlatParser.convertToDto(reservationFlatRepo.save(reservationFlat));
	}

	/**
	 * Method that finds a list of all the reserved flats  {@link FlatDto}
	 * @return the list of all the reserved flats
	 */
	
	@Override
	public List<ReservationFlatDto> findAll() {
		return ReservationFlatParser.convertListToDTO(reservationFlatRepo.findAll());
	}
}

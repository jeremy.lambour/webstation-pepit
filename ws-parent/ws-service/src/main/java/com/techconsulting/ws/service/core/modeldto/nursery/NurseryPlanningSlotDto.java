package com.techconsulting.ws.service.core.modeldto.nursery;

import java.sql.Timestamp;

public class NurseryPlanningSlotDto {

	
	private Integer nurseryPlanningSlotId;
	
	
	private Integer nurseryPlanning;
	
	private Timestamp date;
	
	
	private Integer duration;
	
	private Timestamp startTime;
	

	private Timestamp endTime;
	
	private Integer nurserySlotNumberPlaces;


	public Integer getNurseryPlanningSlotId() {
		return nurseryPlanningSlotId;
	}


	public Integer getNurseryPlanning() {
		return nurseryPlanning;
	}


	public Timestamp getDate() {
		return date;
	}


	public Integer getDuration() {
		return duration;
	}


	public Timestamp getStartTime() {
		return startTime;
	}


	public Timestamp getEndTime() {
		return endTime;
	}


	public void setNurseryPlanningSlotId(Integer nurseryPlanningSlotId) {
		this.nurseryPlanningSlotId = nurseryPlanningSlotId;
	}


	public void setNurseryPlanning(Integer nurseryPlanning) {
		this.nurseryPlanning = nurseryPlanning;
	}


	public void setDate(Timestamp date) {
		this.date = date;
	}


	public void setDuration(Integer duration) {
		this.duration = duration;
	}


	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}


	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}


	public Integer getNurserySlotNumberPlaces() {
		return nurserySlotNumberPlaces;
	}


	public void setNurserySlotNumberPlaces(Integer nurserySlotNumberPlaces) {
		this.nurserySlotNumberPlaces = nurserySlotNumberPlaces;
	}
	
	
	
}

package com.techconsulting.ws.service.core.modeldto.reservation.flat;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;

/**
 * The class for reservation of a flat
 * 
 * @author Clara
 *
 */

public class ReservationFlatDto {

	/**
	 * The {@link FlatDto}
	 */
	private FlatDto flatDto;

	/**
	 * The {@link ReservationDto}
	 */
	private ReservationDto reservationDto;

	/**
	 * The reservation startDate
	 */
	private Date startDate;

	/**
	 * The reservation endDate
	 */
	private Date endDate;

	/**
	 * The reservation price
	 */
	private Double price;

	public ReservationFlatDto() {
		super();
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the flatDto
	 */
	public FlatDto getFlatDto() {
		return flatDto;
	}

	/**
	 * @param flatDto the flatDto to set
	 */
	public void setFlatDto(FlatDto flatDto) {
		this.flatDto = flatDto;
	}

	/**
	 * @return the reservationDto
	 */
	public ReservationDto getReservationDto() {
		return reservationDto;
	}

	/**
	 * @param reservationDto the reservationDto to set
	 */
	public void setReservationDto(ReservationDto reservationDto) {
		this.reservationDto = reservationDto;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * Getter of informations about reservation's flat (appartment)
	 * 
	 * @return String - the informations
	 */
	public String getInfos() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		StringBuilder sB = new StringBuilder();
		sB.append("Type appartement :" + this.flatDto.getFlatDescription());
		sB.append("Capacité :" + this.flatDto.getFlatCapacity());
		sB.append("Prix : " + this.price + "\n");
		sB.append("Date de début :" + formatter.format(new Date(startDate.getTime())) + "\n");
		sB.append("Date de fin :" + formatter.format(new Date(endDate.getTime())) + "\n");
		return sB.toString();
	}

}

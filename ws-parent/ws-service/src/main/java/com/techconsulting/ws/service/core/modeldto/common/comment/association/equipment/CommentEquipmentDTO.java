package com.techconsulting.ws.service.core.modeldto.common.comment.association.equipment;

import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;

public class CommentEquipmentDTO {
	/**
	 * the comment's equipment 
	 */
	private CommentDTO commentDto;

	/**
	 * The Equipment who is commented
	 */
	private EquipmentDto equipmentDto;
	
	/**
	 * Generic constructor of CommentEquipmentDTO
	 */
	public CommentEquipmentDTO() {
		super();
	}
	
	/**
	 * Getter of {@link CommentDTO}
	 * 
	 * @return {@link CommentDTO}
	 */
	public CommentDTO getCommentDto() {
		return commentDto;
	}
	
	/**
	 * Setter of {@link CommentDTO}
	 * 
	 * @param {@link CommentDTO}
	 */
	public void setCommentDto(CommentDTO commentDto) {
		this.commentDto = commentDto;
	}

	/**
	 * Getter of {@link EquipmentDto}
	 * 
	 * @return {@link EquipmentDto}
	 */
	public EquipmentDto getEquipmentDto() {
		return equipmentDto;
	}
	
	/**
	 * Setter of {@link EquipmentDto}
	 * 
	 * @param {@link EquipmentDto}
	 */
	public void setEquipmentDto(EquipmentDto equipmentDto) {
		this.equipmentDto = equipmentDto;
	}
	
	
	
	
}

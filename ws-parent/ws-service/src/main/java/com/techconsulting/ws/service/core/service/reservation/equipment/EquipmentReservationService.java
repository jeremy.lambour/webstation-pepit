package com.techconsulting.ws.service.core.service.reservation.equipment;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.dao.entity.reservation.equipment.ReservationEquipment;
import com.techconsulting.ws.dao.repository.equipment.EquipmentRepository;
import com.techconsulting.ws.dao.repository.reservation.equipment.ReservationEquipmentRepository;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.parser.reservation.equipment.ReservationEquipmentParser;
import com.techconsulting.ws.service.core.service.reservation.IReservation;

@Service
@Transactional(readOnly = false)
public class EquipmentReservationService implements IEquipmentReservationService {

	@Autowired
	ReservationEquipmentRepository reserEquipRepo;

	@Autowired
	IReservation iReservation;

	@Autowired
	EquipmentRepository equipmentRepo;

	/**
	 * Method that inserts an {@link ReservationEquipment} in DataBase
	 * 
	 * @Param - {@link ReservationEquipmentDto} reservEquip
	 * @Return - the {@link ReservationEquipmentDto} inserted
	 */
	@Override
	public ReservationEquipmentDto insertReservEquip(ReservationEquipmentDto reservEquipDto) {
		reservEquipDto.setReservationDto(iReservation.insertReservation(reservEquipDto.getReservationDto()));
		ReservationEquipment reservEquip = ReservationEquipmentParser.convertToEntity(reservEquipDto);
		return ReservationEquipmentParser.convertToDto(reserEquipRepo.save(reservEquip));
	}

	/**
	 * Method that return a list of {@link ReservationEquipment} for a specific user
	 * id
	 * 
	 * @Param - {@link Integer} idUser
	 * @Return - the list of {@link ReservationEquipmentDto}
	 */
	@Override
	public List<ReservationEquipmentDto> getListReservEquipUser(int idUser) {
		return ReservationEquipmentParser
				.convertListToDTO(this.reserEquipRepo.findByReservEquipmentEntityPkReservationUserUserId(idUser));
	}

	/**
	 * Method that check the availability of an {@link EquipmentDto} by
	 * {@link ReservationEquipmentDto} in DataBase
	 * 
	 * @Param - {@link ReservationEquipmentDto} reservEquip
	 * @Return - the availability's boolean
	 */
	@Override
	public boolean checkAvailability(ReservationEquipmentDto reservEquip) {

		List<ReservationEquipmentDto> listReservEquipDto = ReservationEquipmentParser.convertListToDTO(reserEquipRepo
				.findByReservEquipmentEntityPkEquipmentEquipmentId(reservEquip.getEquipmentDto().getEquipmentId()));
		boolean available = true;

		if (listReservEquipDto.isEmpty()
				&& (reservEquip.getEquipmentDto().getEquipmentStock() >= reservEquip.getQuantity())) {
			return available;
		} else {
			available = checkStock(reservEquip, listReservEquipDto);
		}

		return available;
	}

	/**
	 * Method that compute the total of reservation's quantity
	 * 
	 * @param - {@link ReservationEquipmentDto} reservEquip
	 * @param - {@link ReservationEquipmentDto} listReservEquipDto
	 * @return - the availability's boolean
	 */
	public boolean checkStock(ReservationEquipmentDto reservEquip, List<ReservationEquipmentDto> listReservEquipDto) {
		boolean available = true;
		for (LocalDate date = reservEquip.getDateStart().toLocalDateTime().toLocalDate(); date.isBefore(
				reservEquip.getDateEnd().toLocalDateTime().toLocalDate()) && available; date = date.plusDays(1)) {
			int somme = 0;
			for (ReservationEquipmentDto reservEquipDto : listReservEquipDto) {
				if (date.isAfter(reservEquipDto.getDateStart().toLocalDateTime().toLocalDate())
						&& date.isBefore(reservEquipDto.getDateEnd().toLocalDateTime().toLocalDate())) {
					somme += reservEquipDto.getQuantity();
				}
			}
			if (somme + reservEquip.getQuantity() > reservEquip.getEquipmentDto().getEquipmentStock()) {
				available = false;
			}
		}
		return available;
	}

	/**
	 * Method that deletes a {@link ReservationEquipmentDto} from a specific
	 * identifier
	 * 
	 * @param - {@link ReservationEquipmentDto} reservEquip
	 */
	@Override
	public boolean deleteEquipmentReservation(ReservationEquipmentDto reservEquip) {
		reserEquipRepo
				.deleteByReservEquipmentEntityPkEquipmentEquipmentId(reservEquip.getEquipmentDto().getEquipmentId());
		reserEquipRepo.deleteByReservEquipmentEntityPkReservationReservationId(
				reservEquip.getReservationDto().getReservationId());
		iReservation.deleteReservation(reservEquip.getReservationDto().getReservationId());
		return true;
	}
	
	/**
	 * Method that return a list of {@link ReservationEquipment} for a specific date
	 * 
	 * @Param - {@link Timestamp} dateStart
	 * @Return - the list of {@link ReservationEquipmentDto}
	 */
	@Override
	public List<ReservationEquipmentDto> getListReservEquipDate(Timestamp dateStart){
		return ReservationEquipmentParser.convertListToDTO(this.reserEquipRepo.findBydateStart(dateStart));
	}
	
	@Override
	public List<ReservationEquipmentDto> findReservationEquipmentByEquipmentDtoShopDtoIdShop(Integer idShop) {
		return ReservationEquipmentParser.convertListToDTO(this.reserEquipRepo.findReservationEquipmentByReservEquipmentEntityPkEquipmentShopShopId(idShop));
	}

	@Override
	public ReservationEquipmentDto findByReservationDtoReservationId(Integer idReserv) {
		return ReservationEquipmentParser.convertToDto(this.reserEquipRepo.findByReservEquipmentEntityPkReservationReservationId(idReserv));
	}

	@Override
	public ReservationEquipmentDto  findByReservationDtoReservationIdAndEquipmentDtoEquipmentId(Integer idEquipment, Integer idReserv) {
		return ReservationEquipmentParser.convertToDto(this.reserEquipRepo.findByReservEquipmentEntityPkReservationReservationIdAndReservEquipmentEntityPkEquipmentEquipmentId(idEquipment, idReserv));
	}
	
	@Override
	public void setReservationEquipmentIsGivenByReservationEquipment(Boolean isGiven, ReservationEquipmentDto reservationEquipmentDto) {
		ReservationEquipment reservationEquipment = this.reserEquipRepo.findByReservEquipmentEntityPkReservationReservationIdAndReservEquipmentEntityPkEquipmentEquipmentId(reservationEquipmentDto.getReservationDto().getReservationId(), reservationEquipmentDto.getEquipmentDto().getEquipmentId());
		reservationEquipment.setIsGiven(isGiven);
		reserEquipRepo.save(reservationEquipment);
	}
}

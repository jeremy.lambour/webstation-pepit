package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;

/**
 * Parser for {@link Ask} and {@link AskDTO}
 * 
 * @author p_lem
 *
 */
public class UserParser {

	private UserParser() {
		super();
	}

	/**
	 * Convert an {@link User} entity to an {@link UserDto}
	 * 
	 * @param {@link User} to set
	 * @return {@link UserDto} object
	 */
	public static UserDto convertToDto(User user) {
		UserDto userDTO;
		if (user != null) {

			userDTO = new UserDto();
			userDTO.setUsername(user.getUsername());
			userDTO.setEmail(user.getEmail());
			userDTO.setUserId(user.getUserId());
			userDTO.setFirstName(user.getFirstName());
			userDTO.setLastName(user.getLastName());
			userDTO.setAddress(user.getAddress());
			userDTO.setBirthDate(user.getBirthDate());
			userDTO.setAge(user.getAge());
			userDTO.setCity(user.getCity());
			userDTO.setPostalCode(user.getPostalCode());
			userDTO.setPhoneNumber(user.getPhoneNumber());
			if (user.getRole() != null) {
				userDTO.setUserRoleDto(AuthorityParser.convertToDto(user.getRole()));
			}
			if (user.getAgeRange() != null) {
				userDTO.setAgeRangeDto(AgeRangeParser.convertToDto(user.getAgeRange()));
			}
			userDTO.setEnabled(user.getEnabled());

			return userDTO;
		}
		return new UserDto();
	}

	/**
	 * Convert an {@link UserDto} to an {@link User} entity
	 * 
	 * @param {@link UserDto} to set
	 * @return {@link User} entity
	 */
	public static User convertToEntity(UserDto userDTO) {
		User user;
		if (userDTO != null) {
			user = new User();
			user.setUsername(userDTO.getUsername());
			user.setEmail(userDTO.getEmail());
			user.setPassword(userDTO.getPassword());
			user.setUserId(userDTO.getUserId());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setAddress(userDTO.getAddress());
			user.setBirthDate(userDTO.getBirthDate());
			user.setAge(userDTO.getAge());
			user.setCity(userDTO.getCity());
			user.setPostalCode(userDTO.getPostalCode());
			user.setPhoneNumber(userDTO.getPhoneNumber());
			if (userDTO.getUserRoleDto() != null) {
				user.setRole(AuthorityParser.convertToEntity(userDTO.getUserRoleDto()));
			}
			if (userDTO.getAgeRangeDto() != null) {
				user.setAgeRange(AgeRangeParser.convertToEntity(userDTO.getAgeRangeDto()));
			}
			user.setEnabled(userDTO.getEnabled());
			return user;
		}
		return new User();
	}

	/**
	 * Convert a list of {@link User} entity to a list of {@link UserDto}
	 * 
	 * @param listUser to set
	 * @return listUserDTO
	 */
	public static List<UserDto> convertListToDTO(List<User> listUser) {
		return listUser.stream().map(x -> convertToDto(x)).collect(Collectors.toList());

	}

	/**
	 * Convert a list of {@link UserDto} to a list of {@link User} entity
	 * 
	 * @param List<UserAuthentificationDTO> to set
	 * @return listUser
	 */
	public static List<User> convertListToEntity(List<UserDto> listAskDTO) {
		return listAskDTO.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

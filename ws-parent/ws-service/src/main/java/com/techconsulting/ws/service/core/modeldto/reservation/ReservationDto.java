package com.techconsulting.ws.service.core.modeldto.reservation;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;

/**
 * The class for reservation
 * 
 * @author Paul
 *
 */
public class ReservationDto {

	/**
	 * reservation identifier
	 */
	private Integer reservationId;

	/**
	 * number of persons in reservation
	 */
	private Integer nbPersons;

	/**
	 * the price of the insurance for loss equipment
	 */
	private Integer insuranceLossMaterial;

	/**
	 * the price when cancel a housing reservation
	 */
	private Integer houseCancelOpt;

	/**
	 * the price when cancel a equipment reservation
	 */
	private Integer equipmentCancelOpt;

	/**
	 * the price when cancel the reservation
	 */
	private Integer reservCancelopt;

	/**
	 * the reservation's user
	 */
	private UserDto user;

	/**
	 * reservation's constructor
	 */
	public ReservationDto() {
		super();
	}

	/**
	 * Getter for property reservationId
	 * 
	 * @return {@link Integer}
	 */
	public Integer getReservationId() {
		return reservationId;
	}

	/**
	 * Setter for property reservationId
	 * 
	 * @param reservationId
	 */
	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	/**
	 * Getter for property nbPersons
	 * 
	 * @return {@link Integer}
	 */
	public Integer getNbPersons() {
		return nbPersons;
	}

	/**
	 * Setter for property nbPersons
	 * 
	 * @param nbPersons
	 */
	public void setNbPersons(Integer nbPersons) {
		this.nbPersons = nbPersons;
	}

	/**
	 * Getter for property insuranceLossMaterial
	 * 
	 * @return {@link Integer}
	 */
	public Integer getInsuranceLossMaterial() {
		return insuranceLossMaterial;
	}

	/**
	 * Setter for property insuranceLossMaterial
	 * 
	 * @param insuranceLossMaterial
	 */
	public void setInsuranceLossMaterial(Integer insuranceLossMaterial) {
		this.insuranceLossMaterial = insuranceLossMaterial;
	}

	/**
	 * Getter for property houseCancelOpt
	 * 
	 * @return {@link Integer}
	 */
	public Integer getHouseCancelOpt() {
		return houseCancelOpt;
	}

	/**
	 * Setter for property houseCancelOpt
	 * 
	 * @param houseCancelOpt
	 */
	public void setHouseCancelOpt(Integer houseCancelOpt) {
		this.houseCancelOpt = houseCancelOpt;
	}

	/**
	 * Getter for property equipmentCancelOpt
	 * 
	 * @return {@link Integer}
	 */
	public Integer getEquipmentCancelOpt() {
		return equipmentCancelOpt;
	}

	/**
	 * Setter for property equipmentCancelOpt
	 * 
	 * @param equipmentCancelOpt
	 */
	public void setEquipmentCancelOpt(Integer equipmentCancelOpt) {
		this.equipmentCancelOpt = equipmentCancelOpt;
	}

	/**
	 * Getter for property reservCancelopt
	 * 
	 * @return {@link Integer}
	 */
	public Integer getReservCancelopt() {
		return reservCancelopt;
	}

	/**
	 * Setter for property reservCancelopt
	 * 
	 * @param reservCancelopt
	 */
	public void setReservCancelopt(Integer reservCancelopt) {
		this.reservCancelopt = reservCancelopt;
	}

	/**
	 * Getter for property {@link UserDto}
	 * 
	 * @return {@link UserDto}
	 */
	public UserDto getUser() {
		return user;
	}

	/**
	 * Setter for property {@link UserDto}
	 * 
	 * @param {@link UserDto}
	 */
	public void setUser(UserDto user) {
		this.user = user;
	}

}

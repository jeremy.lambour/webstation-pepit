package com.techconsulting.ws.service.core.service.accountcreation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.auth.Authority;
import com.techconsulting.ws.dao.repository.auth.AuthorityRepository;
import com.techconsulting.ws.service.core.modeldto.user.AuthorityDto;
import com.techconsulting.ws.service.core.parser.AuthorityParser;

/**
 * Authority service
 * 
 * @author Jeremy
 *
 */
@Service
public class AuthorityService implements IAuthorityService {

	/**
	 * {@link AuthorityRepository}
	 */
	@Autowired
	AuthorityRepository userRoleRepo;

	@Override
	@Transactional(readOnly = false)
	public AuthorityDto insertRole(AuthorityDto userRoleDto) {

		if (userRoleDto != null) {
			Authority userRole = userRoleRepo.save(AuthorityParser.convertToEntity(userRoleDto));
			return AuthorityParser.convertToDto(userRole);
		}
		return new AuthorityDto();
	}

	@Override
	@Transactional(readOnly = true)
	public AuthorityDto findByName(String authorityName) {
		AuthorityDto authorityDto = new AuthorityDto();
		if (authorityName != null && !authorityName.equals("")) {
			authorityDto = AuthorityParser.convertToDto(userRoleRepo.findByAuthorityName(authorityName));
		}
		return authorityDto;
	}

	@Override
	public List<AuthorityDto> findAll() {
		return AuthorityParser.convertListToDTO(userRoleRepo.findAll());
	}
}

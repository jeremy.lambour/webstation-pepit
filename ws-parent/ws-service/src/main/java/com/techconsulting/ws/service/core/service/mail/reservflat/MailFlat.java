package com.techconsulting.ws.service.core.service.mail.reservflat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.service.mail.IMailService;

@Service
public class MailFlat implements IMailFlat {

	@Autowired
	IMailService emailService;

	/**
	 * Method who send an email for an insertion of a reservation of flat
	 * 
	 * @param - {@link {@link ReservationFlatDto} reservFlatDto : object compose
	 *        with a reservation and a user
	 */
	@Override
	public void insertReservMail(ReservationFlatDto reservFlatDto) {
		emailService.sendEmail(reservFlatDto.getReservationDto().getUser().getEmail(), 0, 0, 5, false, "");
	}

}

package com.techconsulting.ws.service.core.modeldto.common;



public class ScheduleDto {

	/**
	 * The schedule's identifier
	 */
	private Integer scheduleId;

	/**
	 * The schedule's name
	 */
	private String scheduleDay;

	/**
	 * The schedule's open hour
	 */
	private String scheduleOpenHour;

	/**
	 * The schedule's close hour
	 */
	private String scheduleCloseHour;

	/**
	 * ScheduleDto's generic constructor
	 */
	public ScheduleDto() {
		super();
	}

	/**
	 * @return the scheduleId
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * @param scheduleId the scheduleId to set
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * @return the scheduleDay
	 */
	public String getScheduleDay() {
		return scheduleDay;
	}

	/**
	 * @param scheduleDay the scheduleDay to set
	 */
	public void setScheduleDay(String scheduleDay) {
		this.scheduleDay = scheduleDay;
	}

	/**
	 * @return the scheduleOpenHour
	 */
	public String getScheduleOpenHour() {
		return scheduleOpenHour;
	}

	/**
	 * @param scheduleOpenHour the scheduleOpenHour to set
	 */
	public void setScheduleOpenHour(String scheduleOpenHour) {
		this.scheduleOpenHour = scheduleOpenHour;
	}

	/**
	 * @return the scheduleCloseHour
	 */
	public String getScheduleCloseHour() {
		return scheduleCloseHour;
	}

	/**
	 * @param scheduleCloseHour the scheduleCloseHour to set
	 */
	public void setScheduleCloseHour(String scheduleCloseHour) {
		this.scheduleCloseHour = scheduleCloseHour;
	}

}

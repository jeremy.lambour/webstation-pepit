package com.techconsulting.ws.service.core.modeldto.skiingSchool;

import java.sql.Timestamp;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;

public class LessonDto {

	private static final long serialVersionUID = 1L;

	private Integer lessonId;

	
	private UserDto lessonMonitor;

	private SkiingSchoolDto lessonSkiingSchool;

	private String lessonLevel;
	
	private  String lessonSport;
	
	private  String lessonClub;
	
	private Timestamp lessonDate;
	
	private Timestamp lessonStartTime;
	
	private Timestamp lessonEndTime;
	
	private Long lessonDuration;
	
	private String lessonMeetingPlace;
	
	private Integer lessonPlanning;
	
	private Integer lessonNumberPlaces;

	public Integer getLessonId() {
		return lessonId;
	}

	public void setLessonId(Integer lessonId) {
		this.lessonId = lessonId;
	}

	public UserDto getLessonMonitor() {
		return lessonMonitor;
	}

	public void setLessonMonitor(UserDto lessonMonitor) {
		this.lessonMonitor = lessonMonitor;
	}

	public SkiingSchoolDto getLessonSkiingSchool() {
		return lessonSkiingSchool;
	}

	public String getLessonLevel() {
		return lessonLevel;
	}

	public void setLessonLevel(String lessonLevel) {
		this.lessonLevel = lessonLevel;
	}

	public String getLessonSport() {
		return lessonSport;
	}

	public void setLessonSport(String lessonSport) {
		this.lessonSport = lessonSport;
	}

	public String getLessonClub() {
		return lessonClub;
	}

	public void setLessonClub(String lessonClub) {
		this.lessonClub = lessonClub;
	}

	public Timestamp getLessonDate() {
		return lessonDate;
	}

	public void setLessonDate(Timestamp lessonDate) {
		this.lessonDate = lessonDate;
	}

	public Timestamp getLessonStartTime() {
		return lessonStartTime;
	}

	public void setLessonStartTime(Timestamp lessonStartTime) {
		this.lessonStartTime = lessonStartTime;
	}

	public Timestamp getLessonEndTime() {
		return lessonEndTime;
	}

	public void setLessonEndTime(Timestamp lessonEndTime) {
		this.lessonEndTime = lessonEndTime;
	}

	public Long getLessonDuration() {
		return lessonDuration;
	}

	public void setLessonDuration(Long lessonDuration) {
		this.lessonDuration = lessonDuration;
	}

	public String getLessonMeetingPlace() {
		return lessonMeetingPlace;
	}

	public void setLessonMeetingPlace(String lessonMeetingPlace) {
		this.lessonMeetingPlace = lessonMeetingPlace;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setLessonSkiingSchool(SkiingSchoolDto lessonSkiingSchool) {
		this.lessonSkiingSchool = lessonSkiingSchool;
	}

	public Integer getLessonPlanning() {
		return lessonPlanning;
	}

	public void setLessonPlanning(Integer lessonPlanningId) {
		this.lessonPlanning = lessonPlanningId;
	}

	public Integer getLessonNumberPlaces() {
		return lessonNumberPlaces;
	}

	public void setLessonNumberPlaces(Integer lessonNumberPlaces) {
		this.lessonNumberPlaces = lessonNumberPlaces;
	}
	
	
	
	

	
	
	
	
	

}

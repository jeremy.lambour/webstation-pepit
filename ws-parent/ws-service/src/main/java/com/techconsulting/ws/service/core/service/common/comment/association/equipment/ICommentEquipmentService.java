package com.techconsulting.ws.service.core.service.common.comment.association.equipment;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.common.comment.association.equipment.CommentEquipmentDTO;

public interface ICommentEquipmentService {
	public CommentEquipmentDTO insertCommentEquipment(CommentEquipmentDTO commentEquipmentDto);
	public CommentEquipmentDTO findCommentEquipmentById(Integer idCommentEquipment);
	public List<CommentEquipmentDTO> findByCommentEquipmentPkEquipment(Integer idEquipment);
	public boolean deleteByCommentEquipment(CommentEquipmentDTO commentEquipmentDto);
}

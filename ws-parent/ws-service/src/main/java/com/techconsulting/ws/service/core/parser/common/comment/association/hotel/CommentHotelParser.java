package com.techconsulting.ws.service.core.parser.common.comment.association.hotel;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.common.comment.association.hotel.CommentHotel;
import com.techconsulting.ws.dao.entity.common.comment.association.hotel.CommentHotelPK;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.hotel.CommentHotelDTO;
import com.techconsulting.ws.service.core.parser.HotelParser;
import com.techconsulting.ws.service.core.parser.common.comment.CommentParser;

public class CommentHotelParser {

	/**
	 * Constructor of CommentHotelParser
	 */
	private CommentHotelParser() {
		super();
	}
	
	/**
	 * Convert a {@link CommentHotel} entity to a {@link CommentHotelDTO}
	 * 
	 * @param {@link CommentHotel} to set
	 * @return {@link CommentHotelDTO} object
	 */
	public static CommentHotelDTO convertToDto(CommentHotel commentHotel) {
		CommentHotelDTO commentHotelDTO;
		if (commentHotel != null) {
			commentHotelDTO = new CommentHotelDTO();
			commentHotelDTO.setHotelDto(HotelParser.convertToDto(commentHotel.getCommentHotelPk().getHotel()));
			commentHotelDTO.setCommentDto(CommentParser.convertToDto(commentHotel.getCommentHotelPk().getComment()));
			return commentHotelDTO;
		}
		return new CommentHotelDTO();
	}
	
	/**
	 * Convert a {@link CommentHotelDTO} to a {@link CommentHotel} entity
	 * 
	 * @param {@link CommentHotelDTO} to set
	 * @return {@link CommentHotel}
	 */
	public static CommentHotel convertToEntity(CommentHotelDTO commentHotelDTO) {
		CommentHotel commentHotel;
		if (commentHotelDTO != null) {
			commentHotel = new CommentHotel();
			CommentHotelPK commentHotelPk = new CommentHotelPK();
			commentHotelPk.setComment(CommentParser.convertToEntity(commentHotelDTO.getCommentDto()));
			commentHotelPk.setHotel(HotelParser.convertToEntity(commentHotelDTO.getHotelDto()));
			commentHotel.setCommentHotelPk(commentHotelPk);
			return commentHotel;
		}
		return new CommentHotel();
	}
	
	/**
	 * Convert a list of {@link CommentHotel} to a list of {@link CommentHotelDTO}
	 * 
	 * @param CommentHotel to set
	 * @return CommentHotelDTO
	 */
	public static List<CommentHotelDTO> convertListToDTO(List<CommentHotel> listCommentHotel) {
		return listCommentHotel.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}
	
	/**
	 * Convert a list of {@link CommentHotelDTO} to a list of {@link CommentHotel}
	 * 
	 * @param CommentHotelDTO to set
	 * @return CommentHotel
	 */
	public static List<CommentHotel> convertListToEntity(List<CommentHotelDTO> listCommentHotelDto) {
		return listCommentHotelDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
	
}

package com.techconsulting.ws.service.core.service.common.comment;

import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;

public interface ICommentService {
	
	public CommentDTO insertComment(CommentDTO commentDto);
	
	public CommentDTO findCommentById(Integer idComment);
	
	public boolean deleteByCommentId(Integer idComment);
}

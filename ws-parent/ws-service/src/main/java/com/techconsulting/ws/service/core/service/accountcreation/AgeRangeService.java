package com.techconsulting.ws.service.core.service.accountcreation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.auth.AgeRange;
import com.techconsulting.ws.dao.repository.auth.AgeRangeRepository;
import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.parser.AgeRangeParser;

/**
 * Service to manage {@link AgeRange} and {@link AgeRangeDto}
 * 
 * @author p_lem
 *
 */
@Service
public class AgeRangeService implements IAgeRangeService {

	/**
	 * {@link AgeRangeRepository}
	 */
	@Autowired
	AgeRangeRepository ageRangeRepo;

	@Override
	@Transactional(readOnly = true)
	public AgeRangeDto findAgeRange(int id) {
		AgeRangeDto ageRangeDto = AgeRangeParser.convertToDto(ageRangeRepo.findOne(id));
		if (ageRangeDto != null) {
			return ageRangeDto;
		}
		return new AgeRangeDto();
	}
	
	/**
	 * Method that finds all the {@link FestivePeriodDto}
	 * 
	 * @Return - The list of {@link FestivePeriodDto}
	 */
	@Override
	public List<AgeRangeDto> findAll() {
		List<AgeRange> listAgeRange = ageRangeRepo.findAll();
		return AgeRangeParser.convertListToDTO(listAgeRange);
	}

}

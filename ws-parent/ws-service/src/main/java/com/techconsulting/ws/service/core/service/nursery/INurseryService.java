package com.techconsulting.ws.service.core.service.nursery;

import java.util.List;

import com.techconsulting.ws.dao.entity.nursery.Nursery;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryDto;


/**
 * {@link NurseryService} interface
 * @author Jeremy
 *
 */
public interface INurseryService {

	/**
	 * Get a {@link Nursery} by his identifier
	 * @param id
	 * @return {@link NurseryDto}
	 */
	public NurseryDto getNurseryById(Integer id);
	
	/**
	 * Get all {@link NurseryDto}
	 * @return List<NurseryDto>
	 */
	public List<NurseryDto> getAllNursery();
	
}

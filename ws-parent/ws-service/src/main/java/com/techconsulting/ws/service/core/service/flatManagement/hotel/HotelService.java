package com.techconsulting.ws.service.core.service.flatManagement.hotel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;
import com.techconsulting.ws.dao.repository.flatManagement.hotel.HotelRepository;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.parser.HotelParser;
import com.techconsulting.ws.service.core.parser.UserParser;

/**
 * Service for hotel
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class HotelService implements IHotelService{
	
	/**
	 * {@link HotelRepository}
	 */
	@Autowired
	HotelRepository hotelRepository;
	
	/**
	 * Method that inserts an {@link Hotel} in DataBase
	 * 
	 * @Param - {@link HotelDto} hotelDTO, the hotel we want to insert
	 * @Return - the {@link HotelDto} inserted
	 */
	@Override
	public HotelDto insertHotel(HotelDto hotelDTO) {
		HotelEntity hotel = HotelParser.convertToEntity(hotelDTO);
		return HotelParser.convertToDto(hotelRepository.save(hotel));
	}
	
	/**
	 * Method that finds all the {@link HotelDto}
	 * 
	 * @Return - The list of {@link HotelDto}
	 */
	@Override
	public List<HotelDto> findAll() {
		List<HotelEntity> listHotel = hotelRepository.findAll();
		return HotelParser.convertListToDTO(listHotel);
	}
	
	/**
	 * Method that finds an {@link HotelDto} with a specific id
	 * 
	 * @Param - Integer idHotel, we want to find the {@link HotelDto} with this id
	 * @Return - The {@link HotelDto}
	 */
	@Override
	public HotelDto findHotelById(Integer idHotel) {
		HotelEntity hotel = hotelRepository.findOne(idHotel);
		return HotelParser.convertToDto(hotel);
	}
	
	/**
	 * Method that sets an {@link HotelDto} name from its identifier
	 * 
	 * @Param - {@link HotelDto} hotelDto, we want to change the name of this
	 *        {@link HotelDto}
	 * @Param - String name, the new name for the {@link HotelDto}
	 * 
	 * @return - {@link HotelEntity} update hotel entity
	 */
	@Override
	public HotelEntity setHotelNameByHotel(HotelDto newHotelDto, String hotelName) {
		HotelEntity hotel = hotelRepository.findOne(newHotelDto.getHotelId());
		hotel.setHotelName(hotelName);
		hotelRepository.save(hotel);
		return hotel;
	}
	
	/**
	 * Method that sets an {@link HotelDto} address from its identifier
	 * 
	 * @Param - {@link HotelDto} hotelDto, we want to change the name of this
	 *        {@link HotelDto}
	 * @Param - String address, the new address for the {@link HotelDto}
	 * 
	 * @return - {@link HotelEntity} update hotel entity
	 */
	@Override
	public HotelEntity setHotelAddressByHotel(HotelDto newHotelDto, String address) {
		HotelEntity hotel = hotelRepository.findOne(newHotelDto.getHotelId());
		hotel.setAddress(address);
		hotelRepository.save(hotel);
		return hotel;
	}
	
	/**
	 * Method that sets an {@link HotelDto} hotelRoomsNumber from its identifier
	 * 
	 * @Param - {@link HotelDto} hotelDto, we want to change the name of this
	 *        {@link HotelDto}
	 * @Param - Integer hotelRoomsNumber, the new hotelRoomsNumber for the {@link HotelDto}
	 * 
	 * @return - {@link HotelEntity} update hotel entity
	 */
	@Override
	public HotelEntity setHotelRoomsNumberByHotel(HotelDto newHotelDto, Integer hotelRoomsNumber) {
		HotelEntity hotel = hotelRepository.findOne(newHotelDto.getHotelId());
		hotel.setHotelRoomsNumber(hotelRoomsNumber);
		hotelRepository.save(hotel);
		return hotel;
	}
	
	/**
	 * Method that sets an {@link HotelDto} phoneNumber from its identifier
	 * 
	 * @Param - {@link HotelDto} hotelDto, we want to change the name of this
	 *        {@link HotelDto}
	 * @Param - String phoneNumber, the new phoneNumber for the {@link HotelDto}
	 * 
	 * @return - {@link HotelEntity} update hotel entity
	 */
	@Override
	public HotelEntity setHotelPhoneNumberByHotel(HotelDto newHotelDto, String phoneNumber) {
		HotelEntity hotel = hotelRepository.findOne(newHotelDto.getHotelId());
		hotel.setPhoneNumber(phoneNumber);
		hotelRepository.save(hotel);
		return hotel;
	}
	
	/**
	 * Method that sets an {@link HotelDto} mailAddress from its identifier
	 * 
	 * @Param - {@link HotelDto} hotelDto, we want to change the name of this
	 *        {@link HotelDto}
	 * @Param - String mailAddress, the new mailAddress for the {@link HotelDto}
	 * 
	 * @return - {@link HotelEntity} update hotel entity
	 */
	@Override
	public HotelEntity setHotelMailAddressByHotel(HotelDto newHotelDto, String mailAddress) {
		HotelEntity hotel = hotelRepository.findOne(newHotelDto.getHotelId());
		hotel.setMailAddress(mailAddress);
		hotelRepository.save(hotel);
		return hotel;
	}
	
	/**
	 * Method that sets an {@link HotelDto} userDto from its identifier
	 * 
	 * @Param - {@link HotelDto} hotelDto, we want to change the name of this
	 * 
	 * @Param - {@link UserDto} userDto, the new userDto for the {@link HotelDto}
	 * 
	 * @return - {@link HotelEntity} update hotel entity
	 */
	@Override
	public HotelEntity setHotelUserDtoByHotel(HotelDto newHotelDto, UserDto userDto) {
		HotelEntity hotel = hotelRepository.findOne(newHotelDto.getHotelId());
		hotel.setUser(UserParser.convertToEntity(userDto));
		hotelRepository.save(hotel);
		return hotel;
	}
	
	/**
	 * Method that deletes a {@link HoteltDto} from a specific identifier
	 * 
	 * @Param - Integer idHotel, the identifier of the {@link HoteltDto} we want to
	 *        delete
	 * @return check boolean
	 */
	@Override
	public boolean deleteByHotelId(Integer idHotel) {
		hotelRepository.delete(idHotel);
		return true;
	}

}

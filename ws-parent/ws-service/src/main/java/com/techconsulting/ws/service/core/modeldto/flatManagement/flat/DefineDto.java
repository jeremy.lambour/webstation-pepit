package com.techconsulting.ws.service.core.modeldto.flatManagement.flat;

import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;

/**
 * Dto object for define
 * @author gaillardc
 *
 */
public class DefineDto {
	
	/**
	 * The {@link FlatDto}
	 */
	private FlatDto flatDto;

	/**
	 * The {@link FestivePeriodDto}
	 */
	private FestivePeriodDto festivePeriodDto;
	
	/**
	 * The flat's guidance
	 */
	private double flatPricePerNight;
	
	/**
	 * DefineDTO's generic constructor
	 */
	public DefineDto() {
		super();
	}

	/**
	 * @return the flatDto
	 */
	public FlatDto getFlatDto() {
		return flatDto;
	}

	/**
	 * @param flatDto the flatDto to set
	 */
	public void setFlatDto(FlatDto flatDto) {
		this.flatDto = flatDto;
	}

	/**
	 * @return the festivePeriodDto
	 */
	public FestivePeriodDto getFestivePeriodDto() {
		return festivePeriodDto;
	}

	/**
	 * @param festivePeriodDto the festivePeriodDto to set
	 */
	public void setFestivePeriodDto(FestivePeriodDto festivePeriodDto) {
		this.festivePeriodDto = festivePeriodDto;
	}

	/**
	 * @return the flatPricePerNight
	 */
	public double getFlatPricePerNight() {
		return flatPricePerNight;
	}

	/**
	 * @param flatPricePerNight the flatPricePerNight to set
	 */
	public void setFlatPricePerNight(double flatPricePerNight) {
		this.flatPricePerNight = flatPricePerNight;
	}
	
	

}

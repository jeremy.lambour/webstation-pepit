package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatCategoryEntity;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;

/**
 * Parser for {@link FlatCategoryEntity} and {@link FlatCategoryDto}
 *
 */
public class FlatCategoryParser {
	
	private FlatCategoryParser() {
		super();
	}
	
	/**
	 * Convert a {@link FlatCategoryEntity} entity to a {@link FlatCategoryDto}
	 * 
	 * @param {@link FlatCategoryEntity} to set
	 * @return {@link FlatCategoryDto} object
	 */
	public static FlatCategoryDto convertToDto(FlatCategoryEntity flatCategoryEntity) {
		FlatCategoryDto flatCategoryDto;
		if (flatCategoryEntity != null) {
			flatCategoryDto = new FlatCategoryDto();
			flatCategoryDto.setFlatCategoryId(flatCategoryEntity.getFlatCategoryId());
			flatCategoryDto.setFlatCategoryName(flatCategoryEntity.getFlatCategoryName());
			return flatCategoryDto;
		}
		return new FlatCategoryDto();
	}

	/**
	 * Convert a {@link FlatCategoryDto} to a {@link FlatCategoryEntity} entity
	 * 
	 * @param {@link FlatCategoryDto} to set
	 * @return {@link FlatCategoryEntity}
	 */
	public static FlatCategoryEntity convertToEntity(FlatCategoryDto flatCategoryDto) {
		FlatCategoryEntity flatCategoryEntity;
		if (flatCategoryDto != null) {
			flatCategoryEntity = new FlatCategoryEntity();
			flatCategoryEntity.setFlatCategoryId(flatCategoryDto.getFlatCategoryId());
			flatCategoryEntity.setFlatCategoryName(flatCategoryDto.getFlatCategoryName());
			return flatCategoryEntity;
		}
		return new FlatCategoryEntity();
	}

	/**
	 * Convert a list of {@link FlatCategoryEntity} to a list of {@link FlatCategoryDto}
	 * 
	 * @param listFlatCategoryEntity to set
	 * @return listFlatCategoryDto
	 */
	public static List<FlatCategoryDto> convertListToDTO(List<FlatCategoryEntity> listFlatCategoryEntity) {
		return listFlatCategoryEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link FlatCategoryDto} to a list of {@link FlatCategoryEntity}
	 * 
	 * @param listFlatCategoryDto to set
	 * @return listFlatCategoryEntity
	 */
	public static List<FlatCategoryEntity> convertListToEntity(List<FlatCategoryDto> listFlatCategoryDto) {
		return listFlatCategoryDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

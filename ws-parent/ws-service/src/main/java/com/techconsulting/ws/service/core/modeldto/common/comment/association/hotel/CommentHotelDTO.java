package com.techconsulting.ws.service.core.modeldto.common.comment.association.hotel;

import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;

public class CommentHotelDTO {
	/**
	 * the comment's equipment 
	 */
	private CommentDTO commentDto;
	
	/**
	 * the comment's equipment 
	 */
	private HotelDto hotelDto;

	/**
	 * Generic constructor of CommentHotelDTO
	 */
	public CommentHotelDTO() {
		super();
	}
	
	/**
	 * Getter of {@link CommentDTO}
	 * 
	 * @return {@link CommentDTO}
	 */
	public CommentDTO getCommentDto() {
		return commentDto;
	}
	
	/**
	 * Setter of {@link CommentDTO}
	 * 
	 * @param {@link CommentDTO}
	 */
	public void setCommentDto(CommentDTO commentDto) {
		this.commentDto = commentDto;
	}

	/**
	 * Getter of {@link HotelDto}
	 * 
	 * @return {@link HotelDto}
	 */
	public HotelDto getHotelDto() {
		return hotelDto;
	}
	
	/**
	 * Setter of {@link HotelDto}
	 * 
	 * @param {@link HotelDto}
	 */
	public void setHotelDto(HotelDto hotelDto) {
		this.hotelDto = hotelDto;
	}
}

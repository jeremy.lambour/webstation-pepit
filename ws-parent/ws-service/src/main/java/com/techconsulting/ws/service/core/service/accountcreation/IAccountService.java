package com.techconsulting.ws.service.core.service.accountcreation;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;

/**
 * Account Service interface
 * @author p_lem
 *
 */
public interface IAccountService {
	/**
	 * Method to insert a new user in database.
	 * 
	 * @param user : the {@link UserDto} to insert
	 * @return the new user insert in database
	 */
	 UserDto createAccount(UserDto user);
	 

	public UserDto updateUser(UserDto user);
}

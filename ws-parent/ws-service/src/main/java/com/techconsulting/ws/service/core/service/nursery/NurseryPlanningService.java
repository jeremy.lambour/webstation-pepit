package com.techconsulting.ws.service.core.service.nursery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.repository.nursery.NurseryPlanningRepository;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningDto;
import com.techconsulting.ws.service.core.parser.nursery.NurseryPlanningParser;

/**
 * {@link NurseryPlanningService}
 * @author Jeremy
 *
 */
@Service
@Transactional(readOnly = true)
public class NurseryPlanningService implements INurseryPlanningService {

	/**
	 * {@link NurseryPlanningRepository}
	 */
	@Autowired
	private NurseryPlanningRepository repository;

	/**
	 * {@link NurseryPlanningService}
	 */
	@Autowired
	private INurseryPlanningSlotService service;

	
	@Override
	public List<NurseryPlanningDto> getNurseryPlanningByNurseryIdAndSeasonId(Integer nurseryId,Integer seasonId){
		
		return NurseryPlanningParser.convertListToDTO(repository.findByNurseryNurseryIdAndSeasonSeasonId(nurseryId,seasonId));
	}

	/**
	 * 
	 */
	@Override
	@Transactional(readOnly = false)
	public NurseryPlanningDto createNurseryPlanning(NurseryPlanningDto nursery) {
		return NurseryPlanningParser.convertToDto(repository.save(NurseryPlanningParser.convertToEntity(nursery)));
	}

	@Override
	@Transactional(readOnly = false)
	public NurseryPlanningDto updateNurseryPlanning(NurseryPlanningDto nursery) {
		return NurseryPlanningParser.convertToDto(repository.save(NurseryPlanningParser.convertToEntity(nursery)));

	}

	@Override
	@Transactional(readOnly = false)
	public Boolean deleteNurseryPlanning(Integer id) {
		repository.delete(id);
		return (repository.findOne(id) == null ? true : false);

	}

	@Override
	public NurseryPlanningDto getNurseryPlanningById(Integer nursery) {
		NurseryPlanningDto dto = NurseryPlanningParser.convertToDto(repository.findOne(nursery));
		dto.setNurseryPlanningSlots(service.getNurseryPlanningSlotByNurseryPlanningId(dto.getNurseryPlanningId()));
		return dto;
	}

}

package com.techconsulting.ws.service.core.parser.reservation.equipment;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.reservation.Reservation;
import com.techconsulting.ws.dao.entity.reservation.equipment.ReservationEquipment;
import com.techconsulting.ws.dao.entity.reservation.equipment.ReservationEquipmentPK;
import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;
import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.parser.equipment.EquipmentParser;
import com.techconsulting.ws.service.core.parser.reservation.ReservationParser;

/**
 * Parser for {@link ReservationEquipment} and {@link ReservationEquipmentDto}
 * 
 * @author p_lem
 *
 */
public class ReservationEquipmentParser {
	
	/**
	 * Constructor of ReservationEquipmentParser
	 */
	private ReservationEquipmentParser() {
		super();
	}
	
	
	/**
	 * Convert a {@link ReservationEquipment} entity to a {@link ReservationEquipmentDto}
	 * 
	 * @param {@link ReservationEquipment} to set
	 * @return {@link ReservationEquipmentDto} object
	 */
	public static ReservationEquipmentDto convertToDto(ReservationEquipment reservEquipEntity) {
		ReservationEquipmentDto reservEquipDTO;
		if (reservEquipEntity != null) {
			reservEquipDTO = new ReservationEquipmentDto();
			reservEquipDTO.setEquipmentDto(EquipmentParser.convertToDto(reservEquipEntity.getReservEquipmentEntityPk().getEquipment()));
			reservEquipDTO.setReservationDto(ReservationParser.convertToDto(reservEquipEntity.getReservEquipmentEntityPk().getReservation()));
			reservEquipDTO.setDateStart(reservEquipEntity.getDateStart());
			reservEquipDTO.setDateEnd(reservEquipEntity.getDateEnd());
			reservEquipDTO.setQuantity(reservEquipEntity.getQuantity());
			reservEquipDTO.setPrice(reservEquipEntity.getPrice());
			reservEquipDTO.setIsGiven(reservEquipEntity.getIsGiven());
			return reservEquipDTO;
		}
		return new ReservationEquipmentDto();
	}

	/**
	 * Convert a {@link ReservationDto} to a {@link Reservation} entity
	 * 
	 * @param {@link ReservationDto} to set
	 * @return {@link Reservation}
	 */
	public static ReservationEquipment convertToEntity(ReservationEquipmentDto reservEquipDto) {
		ReservationEquipment reservEquip;
		ReservationEquipmentPK reservEquipPk;
		if (reservEquipDto != null) {
			reservEquip = new ReservationEquipment();
			reservEquipPk = new ReservationEquipmentPK();
			reservEquipPk.setEquipment(EquipmentParser.convertToEntity(reservEquipDto.getEquipmentDto()));
			reservEquipPk.setReservation(ReservationParser.convertToEntity(reservEquipDto.getReservationDto()));
			reservEquip.setReservEquipmentEntityPk(reservEquipPk);
			reservEquip.setDateStart(reservEquipDto.getDateStart());
			reservEquip.setDateEnd(reservEquipDto.getDateEnd());
			reservEquip.setQuantity(reservEquipDto.getQuantity());
			reservEquip.setPrice(reservEquipDto.getPrice());
			reservEquip.setIsGiven(reservEquipDto.getIsGiven());
			return reservEquip;
		}
		return new ReservationEquipment();
	}

	/**
	 * Convert a list of {@link ReservationEquipment} to a list of {@link ReservationEquipmentDto}
	 * 
	 * @param listReservationEquip to set
	 * @return listReservEquipDto
	 */
	public static List<ReservationEquipmentDto> convertListToDTO(List<ReservationEquipment> listReservationEquip) {
		return listReservationEquip.stream().map(x -> convertToDto(x)).collect(Collectors.toList());

	}
	
	/**
	 * Convert a list of {@link ReservationEquipmentDto} to a list of {@link ReservationEquipment}
	 * 
	 * @param listReservEquipDto to set
	 * @return listReservationEquip
	 */
	public static List<ReservationEquipment> convertListToEntity(List<ReservationEquipmentDto> listReservEquipDto) {
		return listReservEquipDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
	
}

package com.techconsulting.ws.service.core.parser.skiingSchool;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.skiingSchool.Lesson;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.LessonDto;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.PlanningDto;
import com.techconsulting.ws.service.core.parser.UserParser;

public class LessonParser {

	public static Lesson convertToEntity(LessonDto dto) {
		Lesson entity = new Lesson();
		if(dto != null) {
			entity.setLessonId(dto.getLessonId());
			entity.setClub(dto.getLessonClub());
			entity.setStartDateTime(dto.getLessonStartTime());
			entity.setDuration(dto.getLessonDuration());
			entity.setLessonLevel(dto.getLessonLevel());
			entity.setMeetingPlace(dto.getLessonMeetingPlace());
			entity.setSkiingSchool(SkiingSchoolParser.convertToEntity(dto.getLessonSkiingSchool()));
			entity.setMonitor(UserParser.convertToEntity(dto.getLessonMonitor()));
			entity.setEndDateTime(dto.getLessonEndTime());
			entity.setSport(dto.getLessonSport());
			entity.setDate(dto.getLessonDate());
			entity.setLessonNumberPlaces(dto.getLessonNumberPlaces());
		}
		return entity;
		
	}
	
	public static LessonDto convertToDto(Lesson entity) {
		LessonDto dto = new LessonDto();
		if(entity != null) {
			dto.setLessonId(entity.getLessonId());
			dto.setLessonClub(entity.getClub());
			dto.setLessonStartTime(entity.getStartDateTime());
			dto.setLessonDuration(entity.getDuration());
			dto.setLessonLevel(entity.getLessonLevel());
			dto.setLessonMeetingPlace(entity.getMeetingPlace());
			dto.setLessonSkiingSchool(SkiingSchoolParser.convertToDto(entity.getSkiingSchool()));
			dto.setLessonMonitor(UserParser.convertToDto(entity.getMonitor()));
			dto.setLessonEndTime(entity.getEndDateTime());
			dto.setLessonSport(entity.getSport());
			dto.setLessonDate(entity.getDate());
			dto.setLessonPlanning(entity.getLessonPlanning().getPlanningId());
			dto.setLessonNumberPlaces(entity.getLessonNumberPlaces());
		}
		return dto;
	}
	
	public static List<Lesson> toListEntity(List<LessonDto> dtos){
		List<Lesson> entities = new LinkedList<Lesson>();
		if(dtos != null) {
			entities =  dtos.stream().map(x-> convertToEntity(x)).collect(Collectors.toList());

		}
		return entities;
	}
	
	public static List<LessonDto> toListDto(List<Lesson> entities){
		List<LessonDto> dtos = new LinkedList<LessonDto>();
		if(entities != null) {
			dtos =  entities.stream().map(x-> convertToDto(x)).collect(Collectors.toList());

		}
		return dtos;	}
}

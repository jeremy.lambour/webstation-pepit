package com.techconsulting.ws.service.core.parser.skiingSchool;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.skiingSchool.SkiingSchool;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.SkiingSchoolDto;

public class SkiingSchoolParser {

	
	public static SkiingSchool convertToEntity(SkiingSchoolDto dto) {
		SkiingSchool entity = new SkiingSchool();
		if(dto != null) {
			entity.setSkiingSchoolId(dto.getSkiingSchoolId());
			entity.setPhoneNumber(dto.getPhoneNumber());
			entity.setSkiingSchoolName(dto.getSkiingSchoolName());
		}
		return entity;
		
	}
	
	public static SkiingSchoolDto convertToDto(SkiingSchool entity) {
		SkiingSchoolDto dto = new SkiingSchoolDto();
		if(entity != null) {
			dto.setSkiingSchoolId(entity.getSkiingSchoolId());
			dto.setSkiingSchoolName(entity.getSkiingSchoolName());
			dto.setPhoneNumber(entity.getPhoneNumber());
		}
		return dto;
	}
	
	public static List<SkiingSchool> toListEntity(List<SkiingSchoolDto> dtos){
		return dtos.stream().map(x-> convertToEntity(x)).collect(Collectors.toList());
	}
	
	public static List<SkiingSchoolDto> toListDto(List<SkiingSchool> entities){
		return entities.stream().map(x-> convertToDto(x)).collect(Collectors.toList());
	}
}

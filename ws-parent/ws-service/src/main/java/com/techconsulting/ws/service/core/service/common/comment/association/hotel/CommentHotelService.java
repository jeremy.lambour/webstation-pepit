package com.techconsulting.ws.service.core.service.common.comment.association.hotel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.comment.association.hotel.CommentHotel;
import com.techconsulting.ws.dao.repository.common.comment.association.flat.CommentHotelRepository;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.hotel.CommentHotelDTO;
import com.techconsulting.ws.service.core.parser.common.comment.association.hotel.CommentHotelParser;

@Service
@Transactional(readOnly = false)
public class CommentHotelService implements ICommentHotelService {
	@Autowired
	CommentHotelRepository commentHotelRepository;

	/**
	 * Method that inserts an {@link CommentHotel} in DataBase
	 * 
	 * @Param - {@link CommentHotelDTO} commentHotelDto, the commentHotel we want to
	 *        insert
	 * @Return - the {@link CommentHotelDTO} inserted
	 */
	@Override
	public CommentHotelDTO insertCommentHotel(CommentHotelDTO commentHotelDto) {
		CommentHotel commentHotel = CommentHotelParser.convertToEntity(commentHotelDto);
		return CommentHotelParser.convertToDto(commentHotelRepository.save(commentHotel));
	}

	/**
	 * Method that finds a {@link CommentHotelDTO} with a specific id
	 * 
	 * @Param - Integer idCommentHotel, we want to find the {@link CommentHotelDTO}
	 *        with this id
	 * @Return - The {@link CommentHotelDTO}
	 */
	@Override
	public CommentHotelDTO findCommentHotelById(Integer idCommentHotel) {
		CommentHotel commentHotel = commentHotelRepository.findOne(idCommentHotel);
		return CommentHotelParser.convertToDto(commentHotel);
	}

	/**
	 * Method that return a list of {@link CommentHotelDTO} for a specific user id
	 * 
	 * @Param - {@link Integer} idHotel
	 * @Return - the list of {@link CommentHotelDTO}
	 */
	@Override
	public List<CommentHotelDTO> findListCommentHotelByIdHotel(Integer idHotel) {
		List<CommentHotel> listCommentHotel = commentHotelRepository.findByCommentHotelPkHotelHotelId(idHotel);
		return CommentHotelParser.convertListToDTO(listCommentHotel);
	}

	/**
	 * Method that deletes a {@link CommentHotelDTO} from a specific identifier
	 * 
	 * @Param - Integer idCommentHotel, the identifier of the
	 *        {@link CommentHotelDTO} we want to delete
	 */
	@Override
	public boolean deleteByCommentHotel(CommentHotelDTO commentHotelDto) {
		commentHotelRepository
				.deleteByCommentHotelPkCommentCommentId(commentHotelDto.getCommentDto().getCommentId());
		commentHotelRepository.deleteByCommentHotelPkHotelHotelId(commentHotelDto.getHotelDto().getHotelId());
		return true;
	}
}

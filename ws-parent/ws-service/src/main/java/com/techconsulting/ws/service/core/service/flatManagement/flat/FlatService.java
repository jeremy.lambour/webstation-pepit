package com.techconsulting.ws.service.core.service.flatManagement.flat;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.dao.repository.flatManagement.flat.FlatRepository;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.parser.FlatCategoryParser;
import com.techconsulting.ws.service.core.parser.FlatParser;
import com.techconsulting.ws.service.core.parser.HotelParser;
import com.techconsulting.ws.service.core.parser.reservation.flat.ReservationFlatParser;

/**
 * Service for flat
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class FlatService implements IFlatService{
	
	/**
	 * {@link FlatRepository}
	 */
	@Autowired
	FlatRepository flatRepository;

	
	/**
	 * Method that inserts an {@link Flat} in DataBase
	 * 
	 * @Param - {@link FlatDto} flatDTO, the flat we want to insert
	 * @Return - the {@link FlatDto} inserted
	 */
	@Override
	public FlatDto insertFlat(FlatDto flatDTO) {
		FlatEntity flat = FlatParser.convertToEntity(flatDTO);
		return FlatParser.convertToDto(flatRepository.save(flat));
	}
	
	/**
	 * Method that finds a list of {@link FlatDto}s from an hotel identifier
	 * 
	 * @Param - {@link HotelDto} hotelDto, we want to find {@link FlatDto}s that have
	 *        this hotelDto
	 * @Return - A list of {@link FlatDto}s
	 */
	@Override
	public List<FlatDto> findByHotelDto(HotelDto hotelDto) {
		return FlatParser.convertListToDTO(flatRepository.findByHotelEntity(HotelParser.convertToEntity(hotelDto)));
	}
	
	/**
	 * Method that finds an {@link FlatDto} with a specific id
	 * 
	 * @Param - Integer idFlat, we want to find the {@link FlatDto} with this id
	 * @Return - The {@link FlatDto}
	 */
	@Override
	public FlatDto findFlatById(Integer idFlat) {
		FlatEntity flat = flatRepository.findOne(idFlat);
		return FlatParser.convertToDto(flat);
	}
	
	/**
	 * Method that finds a list of {@link FlatDTO} with a specific flatCategory and a
	 * specific hotel
	 * 
	 * @Param - {@link FlatCategoryDto} flatCategoryDto, we want to find the {@link FlatDTO}s
	 *        with this {@link FlatCategoryDto}
	 * @Param - {@link HotelDto} hotelDto, we want to find the {@link FlatDTO}s with this
	 *        hotel
	 * @Return - the list of {@link FlatDTO}s
	 */
	@Override
	public List<FlatDto> findByFlatCategoryDtoAndHotelDto(FlatCategoryDto flatCategoryDto, HotelDto hotelDto) {
		return flatRepository.findByFlatCategoryEntityAndHotelEntity(FlatCategoryParser.convertToEntity(flatCategoryDto), HotelParser.convertToEntity(hotelDto)).stream()
				.map(x -> FlatParser.convertToDto(x)).collect(Collectors.toList());
	}
	
	/**
	 * Method that sets an {@link FlatDTO} description from its identifier
	 * 
	 * @Param - {@link FlatDTO} flatDTO, we want to change the name of this
	 *        {@link FlatDTO}
	 * @Param - String description, the new description for the {@link FlatDTO}
	 */
	@Override
	public FlatEntity setFlatDescriptionByFlat(FlatDto flatDto, String description) {
		FlatEntity flat = flatRepository.findOne(flatDto.getFlatId());
		flat.setFlatDescription(description);
		flatRepository.save(flat);
		return flat;
	}
	
	/**
	 * Method that sets an {@link FlatDTO} guidance from its identifier
	 * 
	 * @Param - {@link FlatDTO} flatDTO, we want to change the name of this
	 *        {@link FlatDTO}
	 * @Param - String guidance, the new guidance for the {@link FlatDTO}
	 */
	@Override
	public FlatEntity setFlatGuidanceByFlat(FlatDto flatDto, String guidance) {
		FlatEntity flat = flatRepository.findOne(flatDto.getFlatId());
		flat.setFlatGuidance(guidance);
		flatRepository.save(flat);
		return flat;
	}
	
	/**
	 * Method that sets an {@link FlatDTO} capacity from its identifier
	 * 
	 * @Param - {@link FlatDTO} flatDTO, we want to change the name of this
	 *        {@link FlatDTO}
	 * @Param - Integer capacity, the new capacity for the {@link FlatDTO}
	 */
	@Override
	public FlatEntity setFlatCapacityByFlat(FlatDto flatDto, Integer capacity) {
		FlatEntity flat = flatRepository.findOne(flatDto.getFlatId());
		flat.setFlatCapacity(capacity);
		flatRepository.save(flat);
		return flat;
	}
	
	/**
	 * Method that deletes a {@link FlatDto} from a specific identifier
	 * 
	 * @Param - Integer idFlat, the identifier of the {@link FlatDto} we want to
	 *        delete
	 */
	@Override
	public boolean deleteByFlatId(Integer idFlat) {
		flatRepository.delete(idFlat);
		return true;
	}
	
	/**
	 * Method that finds a list of all the flats  {@link FlatDto}
	 * @return the list of all flats
	 */
	@Override
	public List<FlatDto> findAllFlat() {
		return FlatParser.convertListToDTO(flatRepository.findAll());
	}

}

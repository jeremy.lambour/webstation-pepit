package com.techconsulting.ws.service.core.service.skiingSchool;

import java.util.List;

import com.techconsulting.ws.dao.entity.skiingSchool.Lesson;
import com.techconsulting.ws.dao.entity.skiingSchool.Planning;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.LessonDto;

/**
 * {@link LessonService} interface 
 * @author ASUS
 *
 */
public interface ILessonService {

	/**
	 * Create a new {@link Lesson}
	 * @param lesson
	 * @return created {@link Lesson}
	 */
	public LessonDto createLesson(LessonDto lesson);
	
	/**
	 * Update a {@link Lesson}
	 * @param lesson
	 * @return updated {@link Lesson}
	 */
	public LessonDto updateLesson(LessonDto lesson);
	
	/**
	 * Delete {@link Lesson} with id give in parameter
	 * @param lessonId
	 * @return Boolean which represent deletion status
	 */
	public Boolean deleteLesson(Integer lessonId);
	
	/**
	 * Get {@link Lesson} by {@link Planning} identifier
	 * @param id
	 * @return List<LessonDto>
	 */
	public List<LessonDto> getLessonsByPLanningId(Integer id);
}

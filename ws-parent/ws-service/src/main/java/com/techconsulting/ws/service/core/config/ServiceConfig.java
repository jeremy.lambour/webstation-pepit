package com.techconsulting.ws.service.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.techconsulting.ws.dao.config.DataSourceConfig;
import com.techconsulting.ws.dao.config.EntityManagerConfig;
import com.techconsulting.ws.dao.config.TransactionManagerConfig;

/**
 * Service configuration
 * 
 * @author Jeremy
 *
 */
@Configuration
@ComponentScan(basePackages = "com.techconsulting.ws.core")
@Import({ DataSourceConfig.class, EntityManagerConfig.class, TransactionManagerConfig.class })
public class ServiceConfig {

	/**
	 * Define property sources place holder configurer
	 * 
	 * @return {@link PropertySourcesPlaceholderConfigurer}
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}

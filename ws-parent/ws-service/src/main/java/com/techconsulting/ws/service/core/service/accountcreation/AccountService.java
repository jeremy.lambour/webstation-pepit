package com.techconsulting.ws.service.core.service.accountcreation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.repository.auth.UserRepository;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.parser.UserParser;

/**
 * Service for account user.
 * 
 * @author p_lem
 *
 */
@Service
public class AccountService implements IAccountService {

	/**
	 * {@link UserRepository}
	 */
	@Autowired
	UserRepository userRepo;

	/**
	 * {@link AgeRangeService}
	 */
	@Autowired
	IAgeRangeService ageRangeService;

	@Autowired
	IAuthorityService authorityService;

	/**
	 * {@link PasswordEncoder}
	 */
	@Autowired
	private PasswordEncoder userPasswordEncoder;

	@Transactional(readOnly = false)
	public UserDto createAccount(UserDto userDTO) {
		UserDto newUser = new UserDto();
		AgeRangeDto ageRangeDto = ageRangeService.findAgeRange(1);

		if (userDTO != null) {
			userDTO.setUserRoleDto(authorityService.findByName(userDTO.getUserRoleDto().getAuthorityDtoName()));
			userDTO.setAgeRangeDto(ageRangeDto);
			userDTO.setEnabled(true);
			userDTO.setPassword(userPasswordEncoder.encode(userDTO.getPassword()));
			User user = UserParser.convertToEntity(userDTO);
			newUser = UserParser.convertToDto(userRepo.save(user));
		}
		return newUser;
	}
	
	@Override
	@Transactional(readOnly = false)
	public UserDto updateUser(UserDto user) {
		User userBd = this.userRepo.findOne(user.getUserId());
		user.setPassword(userBd.getPassword());
		int idAgeRange = computeAge(user.getAge());
		AgeRangeDto ageRangeDto = ageRangeService.findAgeRange(idAgeRange);
		user.setAgeRangeDto(ageRangeDto);
		return UserParser.convertToDto(this.userRepo.save(UserParser.convertToEntity(user)));
	}
	
	public int computeAge(int age) {
		if(age > 0 && age <= 5) {
			return 2;
		}else if(age > 5 && age < 15) {
			return 3;
		}else if(age > 15 && age < 65) {
			return 4;
		}else if(age > 65 && age < 75) {
			return 5;
		}else if(age > 75) {
			return 6;
		}
		return 0;
	}

}

package com.techconsulting.ws.service.core.service.mail.reservflat;

import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;

public interface IMailFlat {
	public void insertReservMail(ReservationFlatDto reservFlatDto);
}

package com.techconsulting.ws.service.core.service.flatManagement.hotel;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;

/**
 * The Interface of {@link HotelDto}
 */
public interface IHotelService {
	
	HotelDto insertHotel(HotelDto hotelDTO);
	List<HotelDto> findAll();
	HotelDto findHotelById(Integer idHotel);
	HotelEntity setHotelNameByHotel(HotelDto newHotelDto, String hotelName);
	HotelEntity setHotelAddressByHotel(HotelDto newHotelDto, String address);
	HotelEntity setHotelRoomsNumberByHotel(HotelDto newHotelDto, Integer hotelRoomsNumber);
	HotelEntity setHotelPhoneNumberByHotel(HotelDto newHotelDto, String phoneNumber);
	HotelEntity setHotelMailAddressByHotel(HotelDto newHotelDto, String mailAddress);
	HotelEntity setHotelUserDtoByHotel(HotelDto newHotelDto, UserDto userDto);
	boolean deleteByHotelId(Integer idHotel);

}

package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;

/**
 * Parser for {@link FlatEntity} and {@link FlatDto}
 *
 */
public class FlatParser {
	
	private FlatParser() {
		super();
	}
	
	/**
	 * Convert a {@link FlatEntity} entity to a {@link FlatDto}
	 * 
	 * @param {@link FlatEntity} to set
	 * @return {@link FlatDto} object
	 */
	public static FlatDto convertToDto(FlatEntity flatEntity) {
		FlatDto flatDto;
		if (flatEntity != null) {
			flatDto = new FlatDto();
			flatDto.setFlatCapacity(flatEntity.getFlatCapacity());
			flatDto.setFlatDescription(flatEntity.getFlatDescription());
			flatDto.setFlatGuidance(flatEntity.getFlatGuidance());
			flatDto.setFlatId(flatEntity.getFlatId());
			flatDto.setHotelDto(HotelParser.convertToDto(flatEntity.getHotelEntity()));
			flatDto.setFlatCategoryDto(FlatCategoryParser.convertToDto(flatEntity.getFlatCategoryEntity()));
			return flatDto;
		}
		return new FlatDto();
	}

	/**
	 * Convert a {@link FlatDto} to a {@link FlatEntity} entity
	 * 
	 * @param {@link FlatDto} to set
	 * @return {@link FlatEntity}
	 */
	public static FlatEntity convertToEntity(FlatDto flatDto) {
		FlatEntity flatEntity;
		if (flatDto != null) {
			flatEntity = new FlatEntity();
			flatEntity.setFlatCapacity(flatDto.getFlatCapacity());
			flatEntity.setFlatDescription(flatDto.getFlatDescription());
			flatEntity.setFlatGuidance(flatDto.getFlatGuidance());
			flatEntity.setFlatId(flatDto.getFlatId());
			flatEntity.setHotelEntity(HotelParser.convertToEntity(flatDto.getHotelDto()));
			flatEntity.setFlatCategoryEntity(FlatCategoryParser.convertToEntity(flatDto.getFlatCategoryDto()));
			return flatEntity;
		}
		return new FlatEntity();
	}

	/**
	 * Convert a list of {@link FlatEntity} to a list of {@link FlatDto}
	 * 
	 * @param listFlatEntity to set
	 * @return listFlatDto
	 */
	public static List<FlatDto> convertListToDTO(List<FlatEntity> listFlatEntity) {
		return listFlatEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link FlatDto} to a list of {@link FlatEntity}
	 * 
	 * @param listFlatDto to set
	 * @return listFlatEntity
	 */
	public static List<FlatEntity> convertListToEntity(List<FlatDto> listFlatDto) {
		return listFlatDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

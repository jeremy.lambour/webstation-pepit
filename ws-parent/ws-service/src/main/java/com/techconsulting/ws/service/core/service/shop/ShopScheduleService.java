package com.techconsulting.ws.service.core.service.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.shop.ShopScheduleEntity;
import com.techconsulting.ws.dao.repository.shop.ShopScheduleRepository;
import com.techconsulting.ws.service.core.modeldto.shop.ShopScheduleDto;
import com.techconsulting.ws.service.core.parser.ShopScheduleParser;

/**
 * Service for shopSchedule
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class ShopScheduleService implements IShopScheduleService {

	/**
	 * {@link ShopScheduleRepository}
	 */
	@Autowired
	ShopScheduleRepository shopScheduleRepository;

	/**
	 * Method that finds an {@link ShopScheduleDto} with a specific id
	 * 
	 * @Param - Integer idShopSchedule, we want to find the {@link ShopScheduleDto}
	 *        with this id
	 * @Return - The {@link ShopScheduleDto}
	 */
	@Override
	public List<ShopScheduleDto> findByShopScheduleIdShopEntityShopId(Integer idShopSchedule) {
		return ShopScheduleParser
				.convertListToDTO(shopScheduleRepository.findByShopScheduleIdShopEntityShopId(idShopSchedule));
	}

	/**
	 * Method that inserts an {@link ShopScheduleDto} in DataBase
	 * 
	 * @Param - {@link ShopScheduleDto} shopScheduleDto, the shopSchedule we want to
	 *        insert
	 * @Return - the {@link ShopScheduleDto} inserted
	 */
	@Override
	public ShopScheduleDto insertShopSchedule(ShopScheduleDto shopScheduleDto) {
		ShopScheduleEntity shopScheduleEntity = ShopScheduleParser.convertToEntity(shopScheduleDto);
		return ShopScheduleParser.convertToDto(shopScheduleRepository.save(shopScheduleEntity));
	}
	
	/**
	 * Method that deletes a {@link ShopScheduleDto} from a specific identifier
	 * 
	 * @Param - Integer idShop, the identifier of the {@link ShopScheduleDto} we want to
	 *        delete
	 */
	@Override
	public void deleteByShopScheduleIdShopEntityShopId(Integer idShop) {
		shopScheduleRepository.deleteByShopScheduleIdShopEntityShopId(idShop);
	}

}

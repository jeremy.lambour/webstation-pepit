package com.techconsulting.ws.service.core.service.nursery;

import java.util.List;

import com.techconsulting.ws.dao.entity.nursery.Nursery;
import com.techconsulting.ws.dao.entity.nursery.NurseryPlanning;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningDto;

public interface INurseryPlanningService {

	/**
	 * get list of {@link NurseryDto} with given nursery identifier and season identifier
	 * @param nurseryId
	 * @param seasonId
	 * @return List<NurseryPlanningDto>
	 */
	public List<NurseryPlanningDto> getNurseryPlanningByNurseryIdAndSeasonId(Integer nurseryId,Integer seasonId);
	
	/**
	 * create {@link NurseryPlanning}
	 * @param nursery
	 * @return created {@link NurseryPlanningDto}
	 */
	public NurseryPlanningDto createNurseryPlanning(NurseryPlanningDto nursery);
	
	/**
	 * update {@link NurseryPlanning}
	 * @param nursery
	 * @return {@link NurseryPlanningDto}
	 */
	public NurseryPlanningDto updateNurseryPlanning(NurseryPlanningDto nursery);
	
	/**
	 * delete {@link NurseryPlanning} with given identifier
	 * @param id 
	 * @return boolean which represent deleted status
	 */
	public Boolean deleteNurseryPlanning(Integer id);
	
	/**
	 * Get {@link NurseryPlanningDto} with {@link Nursery} identifier
	 * @param nursery
	 * @return
	 */
	public NurseryPlanningDto getNurseryPlanningById(Integer nursery);

}

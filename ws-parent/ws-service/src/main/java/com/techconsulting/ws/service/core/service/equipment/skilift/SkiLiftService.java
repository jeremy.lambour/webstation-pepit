package com.techconsulting.ws.service.core.service.equipment.skilift;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.SeasonEntity;
import com.techconsulting.ws.dao.entity.equipment.SkiLift;
import com.techconsulting.ws.dao.repository.equipment.SkiLiftRepository;
import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.parser.equipment.SkiLiftParser;

/**
 * Service for manage SkiLift
 * @author Paul
 *
 */
@Service
@Transactional(readOnly = false)
public class SkiLiftService implements ISkiLift {

	@Autowired
	SkiLiftRepository skiLiftRepo;
	
	/**
	 * Method that return all of {@link SkiLiftDto}
	 * 
	 * @Return - the list of {@link SkiLiftDto}
	 */
	@Override
	public List<SkiLiftDto> getListSkiLift() {
		return SkiLiftParser.convertListToDTO(this.skiLiftRepo.findAll());
	}
	
	/**
	 * Method that finds a list of {@link SkiLiftDto}
	 * 
	 * @Return - The list of {@link SkiLiftDto}
	 */
	@Override
	public List<SkiLiftDto> findSkiLiftDtoByAgeRangeAgeRangeIdAndDomainAndPassTypePassTypeId(int ageRangeIdInt, String domain, int passTypeIdInt) {
		List<SkiLift> listSkiLift = skiLiftRepo.findSkiLiftByAgeRangeAgeRangeIdAndDomainAndPassTypePassTypeId(ageRangeIdInt, domain, passTypeIdInt);
		return SkiLiftParser.convertListToDTO(listSkiLift);
	}
	
	/**
	 * Method that finds a list of {@link SkiLiftDto}
	 * 
	 * @Return - The list of {@link SkiLiftDto}
	 */
	@Override
	public List<SkiLiftDto> findSkiLiftDtoByAgeRangeAgeRangeIdAndPassTypePassTypeId(int ageRangeIdInt, int passTypeIdInt) {
		List<SkiLift> listSkiLift = skiLiftRepo.findSkiLiftByAgeRangeAgeRangeIdAndPassTypePassTypeId(ageRangeIdInt, passTypeIdInt);
		return SkiLiftParser.convertListToDTO(listSkiLift);
	}
	
	@Override
	public void setSkiLiftDtoPriceBySkiLift(SkiLiftDto newSkiLiftDto, float price) {
		SkiLift skiLift = skiLiftRepo.findOne(newSkiLiftDto.getSkiLiftId());
		skiLift.setPrice(price);
		skiLiftRepo.save(skiLift);
	}
}

package com.techconsulting.ws.service.core.parser.common.comment.association.equipment;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.common.comment.association.equipment.CommentEquipment;
import com.techconsulting.ws.dao.entity.common.comment.association.equipment.CommentEquipmentPK;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.equipment.CommentEquipmentDTO;
import com.techconsulting.ws.service.core.parser.common.comment.CommentParser;
import com.techconsulting.ws.service.core.parser.equipment.EquipmentParser;

public class CommentEquipmentParser {

	/**
	 * Constructor of CommentEquipmentParser
	 */
	private CommentEquipmentParser() {
		super();
	}
	
	/**
	 * Convert a {@link CommentEquipment} entity to a {@link CommentEquipmentDTO}
	 * 
	 * @param {@link CommentEquipment} to set
	 * @return {@link CommentEquipmentDTO} object
	 */
	public static CommentEquipmentDTO convertToDto(CommentEquipment commentEquip) {
		CommentEquipmentDTO commentEquipmentDTO;
		if (commentEquip != null) {
			commentEquipmentDTO = new CommentEquipmentDTO();
			commentEquipmentDTO.setCommentDto(CommentParser.convertToDto(commentEquip.getCommentEquipmentPk().getComment()));
			commentEquipmentDTO.setEquipmentDto(EquipmentParser.convertToDto(commentEquip.getCommentEquipmentPk().getEquipment()));;
			return commentEquipmentDTO;
		}
		return new CommentEquipmentDTO();
	}
	
	/**
	 * Convert a {@link CommentEquipmentDTO} to a {@link CommentEquipment} entity
	 * 
	 * @param {@link CommentEquipmentDTO} to set
	 * @return {@link CommentEquipment}
	 */
	public static CommentEquipment convertToEntity(CommentEquipmentDTO commentEquipmentDTO) {
		CommentEquipment commentEquip;
		if (commentEquipmentDTO != null) {
			commentEquip = new CommentEquipment();
			CommentEquipmentPK commentEquipmentPk = new CommentEquipmentPK();
			commentEquipmentPk.setEquipment(EquipmentParser.convertToEntity(commentEquipmentDTO.getEquipmentDto()));
			commentEquipmentPk.setComment(CommentParser.convertToEntity(commentEquipmentDTO.getCommentDto()));
			commentEquip.setCommentEquipmentPk(commentEquipmentPk);
			return commentEquip;
		}
		return new CommentEquipment();
	}
	
	/**
	 * Convert a list of {@link CommentEquipment} to a list of {@link CommentEquipmentDTO}
	 * 
	 * @param listCommentEquipment to set
	 * @return listCommentEquipmentDto
	 */
	public static List<CommentEquipmentDTO> convertListToDTO(List<CommentEquipment> listCommentEquipment) {
		return listCommentEquipment.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}
	
	/**
	 * Convert a list of {@link CommentEquipmentDTO} to a list of {@link CommentEquipment}
	 * 
	 * @param listCommentEquipmentDto to set
	 * @return listCommentEquipment
	 */
	public static List<CommentEquipment> convertListToEntity(List<CommentEquipmentDTO> listCommentEquipmentDto) {
		return listCommentEquipmentDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

package com.techconsulting.ws.service.core.modeldto.common;


public class PathDto {
	
	/**
	 * The path's path
	 */
	private String path;
	
	/**
	 * PathDto's generic constructor
	 */
	public PathDto() {
		super();
	}

	/**
	 * @return the pathDto's path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
}

package com.techconsulting.ws.service.core.parser.common.comment;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.common.comment.Comment;
import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.parser.UserParser;

public class CommentParser {
	
	/**
	 * Constructor of CommentParser
	 */
	private CommentParser() {
		super();
	}
	
	/**
	 * Convert a {@link Comment} entity to a {@link CommentDTO}
	 * 
	 * @param {@link Comment} to set
	 * @return {@link CommentDTO} object
	 */
	public static CommentDTO convertToDto(Comment comment) {
		CommentDTO commentDto;
		if (comment != null) {
			commentDto = new CommentDTO();
			commentDto.setCommentId(comment.getCommentId());
			commentDto.setContent(comment.getContent());
			commentDto.setFrom(UserParser.convertToDto(comment.getFrom()));
			commentDto.setNote(comment.getNote());
			return commentDto;
		}
		return new CommentDTO();
	}
	
	/**
	 * Convert a {@link CommentDTO} to a {@link Comment} entity
	 * 
	 * @param {@link CommentDTO} to set
	 * @return {@link Comment}
	 */
	public static Comment convertToEntity(CommentDTO commentDto) {
		Comment comment;
		if (commentDto != null) {
			comment = new Comment();
			comment.setCommentId(commentDto.getCommentId());
			comment.setContent(commentDto.getContent());
			comment.setFrom(UserParser.convertToEntity(commentDto.getFrom()));
			comment.setNote(commentDto.getNote());
			return comment;
		}
		return new Comment();
	}
	
	/**
	 * Convert a list of {@link Comment} to a list of {@link CommentDTO}
	 * 
	 * @param Comment to set
	 * @return CommentDTO
	 */
	public static List<CommentDTO> convertListToDTO(List<Comment> listComment) {
		return listComment.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}
	
	/**
	 * Convert a list of {@link CommentDTO} to a list of {@link Comment}
	 * 
	 * @param CommentDTO to set
	 * @return Comment
	 */
	public static List<Comment> convertListToEntity(List<CommentDTO> listCommentDto) {
		return listCommentDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

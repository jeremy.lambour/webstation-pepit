package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.common.SeasonEntity;
import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;

/**
 * Parser for {@link SeasonEntity} and {@link SeasonDto}
 *
 */
public class SeasonParser {
	private SeasonParser() {
		super();
	}

	/**
	 * Convert a {@link SeasonEntity} entity to a {@link SeasonDto}
	 * 
	 * @param {@link SeasonEntity} to set
	 * @return {@link SeasonDto} object
	 */
	public static SeasonDto convertToDto(SeasonEntity seasonEntity) {
		SeasonDto seasonDto;
		if (seasonEntity != null) {
			seasonDto = new SeasonDto();
			seasonDto.setSeasonBeginning(seasonEntity.getSeasonBeginning());
			seasonDto.setSeasonEnd(seasonEntity.getSeasonEnd());
			seasonDto.setSeasonId(seasonEntity.getSeasonId());
			seasonDto.setSeasonName(seasonEntity.getSeasonName());
			seasonDto.setActive(seasonEntity.getActive());
			return seasonDto;
		}
		return new SeasonDto();
	}

	/**
	 * Convert a {@link SeasonDto} to a {@link SeasonEntity} entity
	 * 
	 * @param {@link SeasonDto} to set
	 * @return {@link SeasonEntity}
	 */
	public static SeasonEntity convertToEntity(SeasonDto seasonDto) {
		SeasonEntity seasonEntity;
		if (seasonDto != null) {
			seasonEntity = new SeasonEntity();
			seasonEntity.setSeasonBeginning(seasonDto.getSeasonBeginning());
			seasonEntity.setSeasonEnd(seasonDto.getSeasonEnd());
			seasonEntity.setSeasonId(seasonDto.getSeasonId());
			seasonEntity.setSeasonName(seasonDto.getSeasonName());
			seasonEntity.setActive(seasonDto.getActive());
			return seasonEntity;
		}
		return new SeasonEntity();
	}

	/**
	 * Convert a list of {@link SeasonEntity} to a list of {@link SeasonDto}
	 * 
	 * @param listSeasonEntity to set
	 * @return listSeasonDto
	 */
	public static List<SeasonDto> convertListToDTO(List<SeasonEntity> listSeasonEntity) {
		return listSeasonEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link SeasonDto} to a list of {@link SeasonEntity}
	 * 
	 * @param listSeasonDto to set
	 * @return listSeasonEntity
	 */
	public static List<SeasonEntity> convertListToEntity(List<SeasonDto> listSeasonDto) {
		return listSeasonDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

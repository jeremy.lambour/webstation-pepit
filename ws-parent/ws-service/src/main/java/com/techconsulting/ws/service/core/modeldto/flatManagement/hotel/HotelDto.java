package com.techconsulting.ws.service.core.modeldto.flatManagement.hotel;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;

/**
 * Dto object for hotel
 * @author gaillardc
 *
 */
public class HotelDto {
	
	/**
	 * The hotel's identifier
	 */
	private Integer hotelId;

	/**
	 * The hotel's name
	 */
	private String hotelName;

	/**
	 * The hotel's rooms number
	 */
	private Integer hotelRoomsNumber;

	/**
	 * The hotel's phone number
	 */
	private String phoneNumber;
	
	/**
	 * The hotel's mail_address
	 */
	private String mailAddress;
	
	/**
	 * The hotel's address
	 */
	private String address;
	
	/**
	 * The {@link UserDto}
	 */
	private UserDto userDto;
	
	/**
	 * HotelDTO's generic constructor
	 */
	public HotelDto() {
		super();
	}

	/**
	 * @return the hotelId
	 */
	public Integer getHotelId() {
		return hotelId;
	}

	/**
	 * @param hotelId the hotelId to set
	 */
	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}

	/**
	 * @return the hotelName
	 */
	public String getHotelName() {
		return hotelName;
	}

	/**
	 * @param hotelName the hotelName to set
	 */
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	/**
	 * @return the hotelRoomsNumber
	 */
	public Integer getHotelRoomsNumber() {
		return hotelRoomsNumber;
	}

	/**
	 * @param hotelRoomsNumber the hotelRoomsNumber to set
	 */
	public void setHotelRoomsNumber(Integer hotelRoomsNumber) {
		this.hotelRoomsNumber = hotelRoomsNumber;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the mailAdress
	 */
	public String getMailAddress() {
		return mailAddress;
	}

	/**
	 * @param mailAdress the mailAdress to set
	 */
	public void setMailAddress(String mailAdress) {
		this.mailAddress = mailAdress;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the userDto
	 */
	public UserDto getUserDto() {
		return userDto;
	}

	/**
	 * @param userDto the userDto to set
	 */
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	
	

}

package com.techconsulting.ws.service.core.service.common;

import org.springframework.web.multipart.MultipartFile;

public interface IUploadService {
	
	String saveImageIdea(MultipartFile image);
	byte[] loadImgFile(String path);

}

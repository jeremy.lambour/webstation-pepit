package com.techconsulting.ws.service.core.parser.common;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.common.PassType;
import com.techconsulting.ws.service.core.modeldto.common.PassTypeDto;

public class PassTypeParser {
	/**
	 * Constructor of PassTypeParser
	 */
	private PassTypeParser() {
		super();
	}

	/**
	 * Convert a {@link PassType} entity to a {@link PassTypeDto}
	 * 
	 * @param {@link PassType} to set
	 * @return {@link PassTypeDto} object
	 */
	public static PassTypeDto convertToDto(PassType passType) {
		PassTypeDto passTypeDto;
		if (passType != null) {
			passTypeDto = new PassTypeDto();
			passTypeDto.setPassTypeId(passType.getPassTypeId());
			passTypeDto.setPassTypeName(passType.getPassTypeName());
			return passTypeDto;
		}
		return new PassTypeDto();
	}

	/**
	 * Convert a {@link PassTypeDto} to a {@link PassType} entity
	 * 
	 * @param {@link PassTypeDto} to set
	 * @return {@link PassType}
	 */
	public static PassType convertToEntity(PassTypeDto passTypeDto) {
		PassType passType;
		if (passTypeDto != null) {
			passType = new PassType();
			passType.setPassTypeId(passTypeDto.getPassTypeId());
			passType.setPassTypeName(passTypeDto.getPassTypeName());
			return passType;
		}
		return new PassType();
	}

	/**
	 * Convert a list of {@link PassType} to a list of {@link PassTypeDto}
	 * 
	 * @param listPassType to set
	 * @return listPassTypeDto
	 */
	public static List<PassTypeDto> convertListToDTO(List<PassType> listPassType) {
		return listPassType.stream().map(x -> convertToDto(x)).collect(Collectors.toList());

	}
	
	/**
	 * Convert a list of {@link PassTypeDto} to a list of {@link PassType}
	 * 
	 * @param listPassTypeDto to set
	 * @return listPassType
	 */
	public static List<PassType> convertListToEntity(List<PassTypeDto> listPassTypeDto) {
		return listPassTypeDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

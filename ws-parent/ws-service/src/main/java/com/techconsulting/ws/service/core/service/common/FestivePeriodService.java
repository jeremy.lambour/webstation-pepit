package com.techconsulting.ws.service.core.service.common;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.FestivePeriodEntity;
import com.techconsulting.ws.dao.repository.common.FestivePeriodRepository;
import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.parser.FestivePeriodParser;

/**
 * Service to manage {@link FestivePeriod} and {@link FestivePeriodDto}
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class FestivePeriodService implements IFestivePeriodService{
	
	/**
	 * {@link FestivePeriodRepository}
	 */
	@Autowired
	FestivePeriodRepository festivePeriodRepository;
	
	/**
	 * Method that finds all the {@link FestivePeriodDto}
	 * 
	 * @Return - The list of {@link FestivePeriodDto}
	 */
	@Override
	public List<FestivePeriodDto> findAll() {
		List<FestivePeriodEntity> listFestivePeriod = festivePeriodRepository.findAll();
		return FestivePeriodParser.convertListToDTO(listFestivePeriod);
	}
	
	
	/**
	 * Method that finds a {@link FestivePeriodDto} with a specific name
	 * 
	 * @Param - String name, we want to find the {@link FestivePeriodDto} with this name
	 * @Return - The {@link FestivePeriodDto}
	 */
	@Override
	public FestivePeriodDto findByFestivePeriodName(String name) {
		FestivePeriodEntity festivePeriodEntity = festivePeriodRepository.findByFestivePeriodName(name);
		return FestivePeriodParser.convertToDto(festivePeriodEntity);
	}
	
	/**
	 * Method that finds an {@link FestivePeriodDto} with a specific id
	 * 
	 * @Param - Integer festivePeriodId, we want to find the {@link FestivePeriodDto} with
	 *        this id
	 * @Return - The {@link FestivePeriodDto}
	 */
	@Override
	public FestivePeriodDto findFestivePeriodById(Integer festivePeriodId) {
		FestivePeriodEntity festivePeriodEntity = festivePeriodRepository.findOne(festivePeriodId);
		return FestivePeriodParser.convertToDto(festivePeriodEntity);
	}
	
	/**
	 * Method that sets an {@link FestivePeriodDto} beginning's date
	 * 
	 * @Param - {@link FestivePeriodDto} festivePeriodDto, we want to change the beginning's date of this
	 *        {@link FestivePeriodDto}
	 * @Param - Date beginning's date, the new beginning's date for the {@link FestivePeriodDto}
	 */
	@Override
	public void setFestivePeriodFestivePeriodBeginningBySeason(FestivePeriodDto festivePeriodDto, Date festivePeriodBeginning) {
		FestivePeriodEntity festivePeriodEntity = festivePeriodRepository.findOne(festivePeriodDto.getFestivePeriodId());
		festivePeriodEntity.setFestivePeriodBeginning(festivePeriodBeginning);
		festivePeriodRepository.save(festivePeriodEntity);
	}
	
	/**
	 * Method that sets an {@link FestivePeriodDto} beginning's date
	 * 
	 * @Param - {@link FestivePeriodDto} festivePeriodDto, we want to change the beginning's date of this
	 *        {@link FestivePeriodDto}
	 * @Param - Date beginning's date, the new beginning's date for the {@link FestivePeriodDto}
	 */
	@Override
	public void setFestivePeriodFestivePeriodEndBySeason(FestivePeriodDto festivePeriodDto, Date festivePeriodEnd) {
		FestivePeriodEntity festivePeriodEntity = festivePeriodRepository.findOne(festivePeriodDto.getFestivePeriodId());
		festivePeriodEntity.setFestivePeriodEnd(festivePeriodEnd);
		festivePeriodRepository.save(festivePeriodEntity);
	}
	
}

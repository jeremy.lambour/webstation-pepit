package com.techconsulting.ws.service.core.modeldto.flatManagement.flat;

/**
 * Dto object for flatCategory
 * @author gaillardc
 *
 */
public class FlatCategoryDto {
	
	/**
	 * The flatCategory's identifier
	 */
	private Integer flatCategoryId;

	/**
	 * The flatCategory's name
	 */
	private String flatCategoryName;
	
	public FlatCategoryDto() {
		super();
	}

	/**
	 * @return the flatCategoryId
	 */
	public Integer getFlatCategoryId() {
		return flatCategoryId;
	}

	/**
	 * @param flatCategoryId the flatCategoryId to set
	 */
	public void setFlatCategoryId(Integer flatCategoryId) {
		this.flatCategoryId = flatCategoryId;
	}

	/**
	 * @return the flatCategoryName
	 */
	public String getFlatCategoryName() {
		return flatCategoryName;
	}

	/**
	 * @param flatCategoryName the flatCategoryName to set
	 */
	public void setFlatCategoryName(String flatCategoryName) {
		this.flatCategoryName = flatCategoryName;
	}
	
	

}

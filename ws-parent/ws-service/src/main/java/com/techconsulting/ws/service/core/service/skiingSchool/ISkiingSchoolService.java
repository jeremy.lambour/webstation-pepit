package com.techconsulting.ws.service.core.service.skiingSchool;

import java.util.List;

import com.techconsulting.ws.dao.entity.skiingSchool.SkiingSchool;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.SkiingSchoolDto;

/**
 * {@link SkiingSchoolService} Inteface
 * @author ASUS
 *
 */
public interface ISkiingSchoolService {

	/**
	 * Create a new {@link SkiingSchool} entity
	 * @param skiingSchool
	 * @return created {@link SkiingSchool}
	 */
	public SkiingSchoolDto createSkiingSchool(SkiingSchoolDto skiingSchool);
	
	/**
	 * Update a {@link SkiingSchool} entity
	 * @param updatedSkiingSchool
	 * @return updated {@link SkiingSchool}
	 */
	public SkiingSchoolDto updateSkiingSchool(SkiingSchoolDto updatedSkiingSchool);
	
	/**
	 * Delete an existing {@link SkiingSchool} 
	 * @param skiingSchoolId
	 * @return boolean which represent deletion status 
	 */
	public Boolean deleteSkiingSchool(Integer skiingSchoolId);
	
	/**
	 * Get all {@link SkiingSchool}
	 * @return  List<SkiingSchoolDto>
	 */
	public List<SkiingSchoolDto> findAllSkiingSchool();
}

package com.techconsulting.ws.service.core.parser.skiingSchool;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.skiingSchool.Planning;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.PlanningDto;
import com.techconsulting.ws.service.core.parser.SeasonParser;
import com.techconsulting.ws.service.core.parser.UserParser;

public class PlanningParser {

	public static Planning convertToEntity(PlanningDto dto) {
		Planning entity = new Planning();
		if(dto != null) {
		  entity.setPlanningBeginning(dto.getPlanningBeginning());
		  entity.setPlanningEnd(dto.getPlanningEnd());
		  entity.setPlanningCreator(UserParser.convertToEntity(dto.getPlanningCreator()));
		  entity.setPlanningMonitor(UserParser.convertToEntity(dto.getPlanningMonitor()));
		  entity.setPlanningId(dto.getPlanningId());
		  entity.setPlanningName(dto.getPlanningName());
		  entity.setPlanningSeason(SeasonParser.convertToEntity(dto.getPlanningSeason()));
		}
		return entity;
		
	}
	
	public static PlanningDto convertToDto(Planning entity) {
		PlanningDto dto = new PlanningDto();
		if(entity != null) {
			dto.setPlanningBeginning(entity.getPlanningBeginning());
			dto.setPlanningEnd(entity.getPlanningEnd());
			dto.setPlanningCreator(UserParser.convertToDto(entity.getPlanningCreator()));
			dto.setPlanningMonitor(UserParser.convertToDto(entity.getPlanningMonitor()));
			dto.setPlanningId(entity.getPlanningId());
			dto.setPlanningName(entity.getPlanningName());
			dto.setPlanningSeason(SeasonParser.convertToDto(entity.getPlanningSeason()));
		}
		return dto;
	}
	
	public static List<Planning> toListEntity(List<PlanningDto> dtos){
		return dtos.stream().map(x-> convertToEntity(x)).collect(Collectors.toList());
	}
	
	public static List<PlanningDto> toListDto(List<Planning> entities){
		return entities.stream().map(x-> convertToDto(x)).collect(Collectors.toList());
	}
}

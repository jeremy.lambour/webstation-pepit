package com.techconsulting.ws.service.core.modeldto.user;

/**
 * The class for role of the user
 * 
 * @author Paul
 *
 */
public class AuthorityDto {

	/**
	 * the {@link UserDto}
	 */
	private Integer authorityDtoId;

	/**
	 * the {@link UserAuthorityDto}
	 */
	private String authorityDtoName;

	/**
	 * Constructor of user role dto
	 * 
	 * @return the id role
	 */
	public AuthorityDto() {
		super();
	}

	/**
	 * Getter of the {@link UserDto} who add role
	 * 
	 * @return the {@link UserDto}
	 */
	public Integer getAuthorityDtoId() {
		return authorityDtoId;
	}

	/**
	 * Setter of the {@link UserDto}
	 * 
	 * @param the {@link UserDto} to set
	 */
	public void setAuthorityIdDto(Integer authorityIdDTO) {
		this.authorityDtoId = authorityIdDTO;
	}

	/**
	 * Getter of the {@link UserAuthorityDto}
	 * 
	 * @return the {@link UserAuthorityDto}
	 */
	public String getAuthorityDtoName() {
		return this.authorityDtoName;
	}

	/**
	 * Setter of the {@link UserAuthorityDto}
	 * 
	 * @param the {@link UserAuthorityDto} to set
	 */
	public void setAuthorityDtoName(String userAuthorityDto) {
		this.authorityDtoName = userAuthorityDto;
	}
}

package com.techconsulting.ws.service.core.service.mail.reservskilift;

import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;

public interface IMailSkilift {
	public void insertReservMail(ReservationLiftDto reservLiftDto);

	public void deleteReservMail(ReservationLiftDto reservLiftDto);
}

package com.techconsulting.ws.service.core.service.common;

import java.util.Date;
import java.util.List;

import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;

/**
 * The Interface of {@link FestivePerioDto}
 */
public interface IFestivePeriodService {
	List<FestivePeriodDto> findAll();
	FestivePeriodDto findByFestivePeriodName(String name);
	FestivePeriodDto findFestivePeriodById(Integer festivePeriodId);
	void setFestivePeriodFestivePeriodBeginningBySeason(FestivePeriodDto festivePeriodDto, Date festivePeriodBeginning);
	void setFestivePeriodFestivePeriodEndBySeason(FestivePeriodDto festivePeriodDto, Date festivePeriodEnd);
}

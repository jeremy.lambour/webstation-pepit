package com.techconsulting.ws.service.core.modeldto.shop;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;

/**
 * Dto object for shop
 * 
 * @author gaillardc
 *
 */
public class ShopDto {
	/**
	 * The shop's identifier
	 */
	private Integer shopId;

	/**
	 * The shop's name
	 */
	private String shopName;

	/**
	 * The shop's address
	 */
	private String shopAddress;

	/**
	 * The shop's phone number
	 */
	private String phoneNumber;

	/**
	 * The shop's latitude
	 */
	private double shopLatitude;

	/**
	 * The shop's longitude
	 */
	private double shopLongitude;

	/**
	 * The shop's manager
	 */
	private UserDto userDTO;

	/**
	 * ShopDTO's generic constructor
	 */
	public ShopDto() {
		super();
	}

	/**
	 * @return the shop_id
	 */
	public Integer getShopId() {
		return shopId;
	}

	/**
	 * @param shop_id the shop_id to set
	 */
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the shop_name
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shop_name the shop_name to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the shop_address
	 */
	public String getShopAddress() {
		return shopAddress;
	}

	/**
	 * @param shop_address the shop_address to set
	 */
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	/**
	 * @return the phone_number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phone_number the phone_number to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the shop_latitude
	 */
	public double getShopLatitude() {
		return shopLatitude;
	}

	/**
	 * @param shop_latitude the shop_latitude to set
	 */
	public void setShopLatitude(double shopLatitude) {
		this.shopLatitude = shopLatitude;
	}

	/**
	 * @return the shop_longitude
	 */
	public double getShopLongitude() {
		return shopLongitude;
	}

	/**
	 * @param shop_longitude the shop_longitude to set
	 */
	public void setShopLongitude(double shopLongitude) {
		this.shopLongitude = shopLongitude;
	}

	/**
	 * @return the userDTO
	 */
	public UserDto getUserDTO() {
		return userDTO;
	}

	/**
	 * @param userDTO the userDTO to set
	 */
	public void setUserDTO(UserDto userDTO) {
		this.userDTO = userDTO;
	}

}

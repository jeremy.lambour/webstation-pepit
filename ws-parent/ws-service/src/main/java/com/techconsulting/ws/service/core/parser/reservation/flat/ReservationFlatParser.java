package com.techconsulting.ws.service.core.parser.reservation.flat;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.reservation.flat.ReservationFlat;
import com.techconsulting.ws.dao.entity.reservation.flat.ReservationFlatId;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.parser.FlatParser;
import com.techconsulting.ws.service.core.parser.reservation.ReservationParser;

/**
 * Parser for {@link ReservationFlat} and {@link ReservationFlatDto}
 *
 */
public class ReservationFlatParser {
	
	private ReservationFlatParser() {
		super();
	}
	
	/**
	 * Convert a {@link ReservationFlat} entity to a {@link ReservationFlatDto}
	 * 
	 * @param reservationFlat
	 *            to set
	 * @return reservationFlatDto object
	 */
	public static ReservationFlatDto convertToDto(ReservationFlat reservationFlat) {
		ReservationFlatDto reservationFlatDto;
		if (reservationFlat != null) {
			reservationFlatDto = new ReservationFlatDto();
			reservationFlatDto.setPrice(reservationFlat.getReservationFlatId().getPrice());
			reservationFlatDto.setEndDate(reservationFlat.getReservationFlatId().getEndDate());
			reservationFlatDto.setStartDate(reservationFlat.getReservationFlatId().getStartDate());
			reservationFlatDto.setFlatDto(FlatParser.convertToDto(reservationFlat.getReservationFlatId().getFlat()));
			reservationFlatDto.setReservationDto(ReservationParser.convertToDto(reservationFlat.getReservationFlatId().getReservation()));
			return reservationFlatDto;
		}
		return new ReservationFlatDto();
	}

	/**
	 * Convert an {@link ReservationFlatDto} to an {@link ReservationFlat} entity
	 * 
	 * @param reservationFlatDto
	 *            to set
	 * @return reservationFlat
	 */
	public static ReservationFlat convertToEntity(ReservationFlatDto reservationFlatDto) {
		ReservationFlat reservationFlat;
		if (reservationFlatDto != null) {
			reservationFlat = new ReservationFlat();
			ReservationFlatId reservationFlatId = new ReservationFlatId();
			reservationFlatId.setPrice(reservationFlatDto.getPrice());
			reservationFlatId.setEndDate(reservationFlatDto.getEndDate());
			reservationFlatId.setStartDate(reservationFlatDto.getStartDate());
			reservationFlatId.setReservation(ReservationParser.convertToEntity(reservationFlatDto.getReservationDto()));
			reservationFlatId.setFlat(FlatParser.convertToEntity(reservationFlatDto.getFlatDto()));
			reservationFlat.setReservationFlatId(reservationFlatId);
			return reservationFlat;
		}
		return new ReservationFlat();
	}

	/**
	 * Convert a list of {@link ReservationFlat} entity to a list of {@link ReservationFlatDto}
	 * 
	 * @param listReservationFlat
	 *            to set
	 * @return listReservationFlatDto
	 */
	public static List<ReservationFlatDto> convertListToDTO(List<ReservationFlat> listReservationFlat) {
		return listReservationFlat.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link ReservationFlatDto} to a list of {@link ReservationFlat} entity
	 * 
	 * @param listReservationFlatDto
	 *            to set
	 * @return listReservationFlat
	 */
	public static List<ReservationFlat> convertListToEntity(List<ReservationFlatDto> listReservationFlatDto) {
		return listReservationFlatDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

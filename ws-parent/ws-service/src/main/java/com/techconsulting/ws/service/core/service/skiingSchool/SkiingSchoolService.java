package com.techconsulting.ws.service.core.service.skiingSchool;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.repository.skiingSchool.SkiingSchoolRepository;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.SkiingSchoolDto;
import com.techconsulting.ws.service.core.parser.skiingSchool.SkiingSchoolParser;

@Service
@Transactional(readOnly = true)
public class SkiingSchoolService implements ISkiingSchoolService {

	@Autowired
	private SkiingSchoolRepository repository;
	
	@Override
	@Transactional(readOnly = false)
	public SkiingSchoolDto createSkiingSchool(SkiingSchoolDto skiingSchool) {
		return SkiingSchoolParser.convertToDto(repository.save(SkiingSchoolParser.convertToEntity(skiingSchool)));
	}

	@Override
	@Transactional(readOnly = false)
	public SkiingSchoolDto updateSkiingSchool(SkiingSchoolDto updatedSkiingSchool) {
		return SkiingSchoolParser.convertToDto(repository.save(SkiingSchoolParser.convertToEntity(updatedSkiingSchool)));

	}

	@Override
	@Transactional(readOnly = false)
	public Boolean deleteSkiingSchool(Integer skiingSchoolId) {
       repository.delete(skiingSchoolId);
       return (repository.findOne(skiingSchoolId) == null ? true : false); 
	}

	@Override
	public List<SkiingSchoolDto> findAllSkiingSchool() {
		return SkiingSchoolParser.toListDto(repository.findAll());
	}

}

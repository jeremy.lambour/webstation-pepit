package com.techconsulting.ws.service.core.service.common;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.SeasonEntity;
import com.techconsulting.ws.dao.repository.common.SeasonRepository;
import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;
import com.techconsulting.ws.service.core.parser.SeasonParser;

/**
 * Service to manage {@link Season} and {@link SeasonDto}
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class SeasonService implements ISeasonService{
	
	/**
	 * {@link SeasonRepository}
	 */
	@Autowired
	SeasonRepository seasonRepository;
	
	/**
	 * Method that finds an {@link SeasonDto} with a specific id
	 * 
	 * @Param - Integer idSeason, we want to find the {@link SeasonDto} with
	 *        this id
	 * @Return - The {@link SeasonDto}
	 */
	@Override
	public SeasonDto findSeasonById(Integer idSeason) {
		SeasonEntity seasonEntity = seasonRepository.findOne(idSeason);
		return SeasonParser.convertToDto(seasonEntity);
	}
	
	/**
	 * Method that sets an {@link SeasonDto} active status from its identifier
	 * 
	 * @Param - {@link SeasonDto} seasonDto, we want to change the active status of this
	 *        {@link SeasonDto}
	 * @Param - Boolean active, the new active status for the {@link SeasonDto}
	 */
	@Override
	public void setSeasonActiveBySeason(SeasonDto seasonDto, Boolean active) {
		SeasonEntity season = seasonRepository.findOne(seasonDto.getSeasonId());
		season.setActive(active);
		seasonRepository.save(season);
	}
	
	@Override
	public void setSeasonSeasonBeginningBySeason(SeasonDto newSeasonDto, Date seasonBeginning) {
		SeasonEntity seasonEntity = seasonRepository.findOne(newSeasonDto.getSeasonId());
		seasonEntity.setSeasonBeginning(seasonBeginning);
		seasonRepository.save(seasonEntity);
	}
	
	@Override
	public void setSeasonSeasonEndBySeason(SeasonDto newSeasonDto, Date seasonEnd) {
		SeasonEntity seasonEntity = seasonRepository.findOne(newSeasonDto.getSeasonId());
		seasonEntity.setSeasonEnd(seasonEnd);
		seasonRepository.save(seasonEntity);
	}
	
	@Override
	public List<SeasonDto> getAllSeason() {
		
		return SeasonParser.convertListToDTO(seasonRepository.findAll());
	}



}

package com.techconsulting.ws.service.core.service.common;

import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;

/**
 * The Interface of {@link ScheduleDto}
 */
public interface IScheduleService {
	ScheduleDto insertSchedule(ScheduleDto scheduleDto);

	ScheduleDto findScheduleById(Integer idSchedule);
	
	void setScheduleOpenHourBySchedule(ScheduleDto scheduleDto, String openHour);
	
	void setScheduleCloseHourBySchedule(ScheduleDto scheduleDto, String closeHour);
	void deleteByScheduleId(Integer idSchedule);

}

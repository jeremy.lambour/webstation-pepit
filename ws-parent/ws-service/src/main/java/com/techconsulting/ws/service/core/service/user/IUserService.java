package com.techconsulting.ws.service.core.service.user;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;

/**
 * User service interface
 * 
 * @author Jeremy
 *
 */
public interface IUserService extends UserDetailsService {

	public UserDto findUserByUsername(String username);

	public List<UserDto> findAllUsers();
	
	public List<UserDto> findAllByAuthority(String authorityName);
	
	public UserDto findById(Integer id);
}

package com.techconsulting.ws.service.core.modeldto.flatManagement.flat;

/**
 * Dto object for flatAvailability
 * @author gaillardc
 *
 */
public class FlatAvailabilityDto {
	
	/**
	 * The flatAvailability's identifier
	 */
	private Integer flatAvailabilityId;

	/**
	 * The flatAvailability's date availability
	 */
	private String dateAvailability;

	/**
	 * The flatAvailability's availability
	 */
	private Boolean availability;
	
	/**
	 * The {@link FlatDto}
	 */
	private FlatDto flatDto;
	
	/**
	 * FlatAvailabilityDTO's generic constructor
	 */
	public FlatAvailabilityDto() {
		super();
	}

	/**
	 * @return the flatAvailabilityId
	 */
	public Integer getFlatAvailabilityId() {
		return flatAvailabilityId;
	}

	/**
	 * @param flatAvailabilityId the flatAvailabilityId to set
	 */
	public void setFlatAvailabilityId(Integer flatAvailabilityId) {
		this.flatAvailabilityId = flatAvailabilityId;
	}

	/**
	 * @return the dateAvailability
	 */
	public String getDateAvailability() {
		return dateAvailability;
	}

	/**
	 * @param dateAvailability the dateAvailability to set
	 */
	public void setDateAvailability(String dateAvailability) {
		this.dateAvailability = dateAvailability;
	}

	/**
	 * @return the availability
	 */
	public Boolean getAvailability() {
		return availability;
	}

	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}

	/**
	 * @return the flatDto
	 */
	public FlatDto getFlatDto() {
		return flatDto;
	}

	/**
	 * @param flatDto the flatDto to set
	 */
	public void setFlatDto(FlatDto flatDto) {
		this.flatDto = flatDto;
	}
	
	

}

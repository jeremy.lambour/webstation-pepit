package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.common.ScheduleEntity;
import com.techconsulting.ws.dao.entity.shop.ShopEntity;
import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;

/**
 * Parser for {@link ScheduleEntity} and {@link ScheduleDto}
 *
 */
public class ScheduleParser {
	
	private ScheduleParser() {
		super();
	}

	/**
	 * Convert a {@link ScheduleEntity} entity to a {@link ScheduleDto}
	 * 
	 * @param {@link ScheduleEntity} to set
	 * @return {@link ScheduleDto} object
	 */
	public static ScheduleDto convertToDto(ScheduleEntity scheduleEntity) {
		ScheduleDto scheduleDto;
		if (scheduleEntity != null) {
			scheduleDto = new ScheduleDto();
			scheduleDto.setScheduleId(scheduleEntity.getScheduleId());
			scheduleDto.setScheduleDay(scheduleEntity.getScheduleDay());
			scheduleDto.setScheduleOpenHour(scheduleEntity.getScheduleOpenHour());
			scheduleDto.setScheduleCloseHour(scheduleEntity.getScheduleCloseHour());
			return scheduleDto;
		}
		return new ScheduleDto();
	}

	/**
	 * Convert a {@link ScheduleDto} to a {@link ScheduleEntity}
	 * 
	 * @param {@link ScheduleDto} to set
	 * @return {@link ScheduleEntity}
	 */
	public static ScheduleEntity convertToEntity(ScheduleDto scheduleDto) {
		ScheduleEntity scheduleEntity;
		if (scheduleDto != null) {
			scheduleEntity = new ScheduleEntity();
			scheduleEntity.setScheduleId(scheduleDto.getScheduleId());
			scheduleEntity.setScheduleDay(scheduleDto.getScheduleDay());
			scheduleEntity.setScheduleOpenHour(scheduleDto.getScheduleOpenHour());
			scheduleEntity.setScheduleCloseHour(scheduleDto.getScheduleCloseHour());
			return scheduleEntity;
		}
		return new ScheduleEntity();
	}

	/**
	 * Convert a list of {@link ShopEntity} to a list of {@link ScheduleDto}
	 * 
	 * @param listScheduleEntity to set
	 * @return listScheduleDto
	 */
	public static List<ScheduleDto> convertListToDTO(List<ScheduleEntity> listScheduleEntity) {
		return listScheduleEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link ScheduleDto} to a list of {@link ScheduleEntity}
	 * 
	 * @param listScheduleDto to set
	 * @return listScheduleEntity
	 */
	public static List<ScheduleEntity> convertListToEntity(List<ScheduleDto> listScheduleDto) {
		return listScheduleDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

package com.techconsulting.ws.service.core.service.flatManagement.flat;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.DefineDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;

/**
 * The Interface of {@link DefineDto}
 */
public interface IDefineService {
	
	DefineDto findByFestivePeriodDtoAndFlatDto(FestivePeriodDto festivePeriodDto, FlatDto flatDto);
	List<DefineDto> findDefineDtoByFlatDtoFlatId(Integer idFlat);
	void setDefineFlatPricePerNightByDefine(DefineDto newDefineDto, double flatPricePerNight);

}

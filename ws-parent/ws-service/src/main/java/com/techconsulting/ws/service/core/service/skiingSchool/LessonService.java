package com.techconsulting.ws.service.core.service.skiingSchool;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.skiingSchool.Lesson;
import com.techconsulting.ws.dao.repository.skiingSchool.LessonRepository;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.LessonDto;
import com.techconsulting.ws.service.core.parser.skiingSchool.LessonParser;
import com.techconsulting.ws.service.core.parser.skiingSchool.PlanningParser;

@Service
@Transactional(readOnly = false,noRollbackFor=Exception.class)
public class LessonService implements ILessonService {

	@Autowired
	private LessonRepository repository;
	
	@Autowired
	private IPlanningService planningService;

	@Override
	public LessonDto createLesson(LessonDto lesson) {
		Lesson created = LessonParser.convertToEntity(lesson);
		created.setLessonPlanning(PlanningParser.convertToEntity(planningService.getPlanningById(lesson.getLessonPlanning())));
		return LessonParser.convertToDto(repository.save(created));
	}

	@Override
	public LessonDto updateLesson(LessonDto lesson) {
		Lesson updated = LessonParser.convertToEntity(lesson);
		updated.setLessonPlanning(PlanningParser.convertToEntity(planningService.getPlanningById(lesson.getLessonPlanning())));
		return LessonParser.convertToDto(repository.save(updated));

	}

	@Transactional(readOnly = false)
	@Override
	public Boolean deleteLesson(Integer lessonId) {
		repository.delete(lessonId);
		return (repository.findOne(lessonId) == null ? true : false); 

	}

	@Override
	public List<LessonDto> getLessonsByPLanningId(Integer id) {
		return LessonParser.toListDto(repository.findByLessonPlanningPlanningId(id));
	}

}

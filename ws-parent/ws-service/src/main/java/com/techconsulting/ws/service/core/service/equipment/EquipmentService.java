package com.techconsulting.ws.service.core.service.equipment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.equipment.Equipment;
import com.techconsulting.ws.dao.repository.equipment.EquipmentRepository;
import com.techconsulting.ws.dao.repository.shop.ShopRepository;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.parser.equipment.EquipmentParser;

@Service
public class EquipmentService implements IEquipmentService {

	@Autowired
	EquipmentRepository equipmentRepository;

	@Autowired
	ShopRepository shopRepository;

	/**
	 * Method that inserts an {@link Equipment} in DataBase
	 * 
	 * @Param - {@link EquipmentDto} equipmentDTO, the equipment we want to insert
	 * @Return - the {@link EquipmentDto} inserted
	 */

	@Override
	@Transactional(readOnly = false)
	public EquipmentDto insertEquipment(EquipmentDto equipmentDTO) {
		Equipment equipment = EquipmentParser.convertToEntity(equipmentDTO);
		return EquipmentParser.convertToDto(equipmentRepository.save(equipment));
	}

	/**
	 * Method that finds an {@link EquipmentDto} with a specific id
	 * 
	 * @Param - Integer idEquipment, we want to find the {@link EquipmentDto} with
	 *        this id
	 * @Return - The {@link EquipmentDto}
	 */
	@Override
	public EquipmentDto findEquipmentById(Integer idEquipment) {
		Equipment equipment = equipmentRepository.findOne(idEquipment);
		return EquipmentParser.convertToDto(equipment);
	}
	
	/**
	 * Method that finds a list of {@link EquipmentDto} with a specific shop's id
	 * 
	 * @Param - Integer idShop, we want to find the list of {@link EquipmentDto} with
	 *        this id
	 * @Return - The {@link List<EquipmentDto>}
	 */
	@Override
	public List<EquipmentDto> findEquipmentByShopId(Integer idShop){
		return EquipmentParser.convertListToDTO(equipmentRepository.findByShopShopId(idShop));
	}

	/**
	 * Method that finds all the {@link EquipmentDto}
	 * 
	 * @Return - The list of {@link EquipmentDto}
	 */

	@Override
	public List<EquipmentDto> findAll() {
		return EquipmentParser.convertListToDTO(equipmentRepository.findAll());
	}

	/**
	 * Method that sets an {@link EquipmentDto} equipment from its identifier
	 * 
	 * @Param - {@link EquipmentDto} equipmentDTO, we want to change the attributes
	 *        of this {@link EquipmentDto}
	 * @Param - Equipment newEquipment, the new attributes for the
	 *        {@link EquipmentDto}
	 */

	@Override
	public void setEquipmentByEquipment(EquipmentDto equipmentDTO, Equipment newEquipment) {
		Equipment equipment = equipmentRepository.findOne(equipmentDTO.getEquipmentId());
		equipment.setEquipmentName(newEquipment.getEquipmentName());
		equipment.setEquipmentDescription(newEquipment.getEquipmentDescription());
		equipment.setEquipmentState(newEquipment.getEquipmentState());
		equipment.setEquipmentStock(newEquipment.getEquipmentStock());
		equipment.setEquipmentSize(newEquipment.getEquipmentSize());
		equipment.setEquipmentOut(newEquipment.getEquipmentOut());
		equipment.setPriceLocation(newEquipment.getPriceLocation());
		equipment.setShop(newEquipment.getShop());
		equipmentRepository.save(equipment);
	}

	/**
	 * Method that deletes a {@link EquipmentDto} from a specific identifier
	 * 
	 * @Param - Integer idEquipment, the identifier of the {@link EquipmentDto} we
	 *        want to delete
	 */

	@Override
	public void deleteByEquipmentId(Integer idEquipment) {
		equipmentRepository.delete(idEquipment);
	}
	
	
}

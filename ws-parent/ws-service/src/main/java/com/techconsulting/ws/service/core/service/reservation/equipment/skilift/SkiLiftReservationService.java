package com.techconsulting.ws.service.core.service.reservation.equipment.skilift;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.reservation.lift.ReservationLift;
import com.techconsulting.ws.dao.repository.reservation.lift.ReservationSkiLiftRepository;
import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.parser.reservation.lift.ReservationLiftParser;

/**
 * Service for manage Reservation of SkiLift
 * 
 * @author Paul
 *
 */
@Service
@Transactional(readOnly = false)
public class SkiLiftReservationService implements ISkiLiftReservationService {

	@Autowired
	ReservationSkiLiftRepository reservSkiLiftRepo;

	/**
	 * Method that inserts an {@link ReservationLiftDto} in DataBase
	 * 
	 * @Param - {@link ReservationLiftDto} reservSkiLiftDto
	 * @Return - the {@link ReservationLiftDto} inserted
	 */
	@Override
	public ReservationLiftDto insertReservSkiLift(ReservationLiftDto reservSkiLiftDto) {
		ReservationLift reservSkiLift = ReservationLiftParser.convertToEntity(reservSkiLiftDto);
		return ReservationLiftParser.convertToDto(reservSkiLiftRepo.save(reservSkiLift));
	}

	/**
	 * Method that return a list of {@link ReservationLiftDto} for a specific user
	 * id
	 * 
	 * @Param - {@link Integer} idUser
	 * @Return - the list of {@link ReservationLiftDto}
	 */
	@Override
	public List<ReservationLiftDto> getListReservSkiLiftUser(int idUser) {
		return ReservationLiftParser.convertListToDTO(this.reservSkiLiftRepo.findByReservLiftPkUserUserId(idUser));
	}

	/**
	 * Method that deletes a {@link ReservationLiftDto} from a specific identifier
	 * 
	 * @param - {@link ReservationLiftDto} reservLift
	 */
	@Override
	public boolean deleteSkiLiftReservation(ReservationLiftDto reservLift) {
		reservSkiLiftRepo.deleteByReservLiftPkSkiLiftSkiLiftId(reservLift.getSkiLiftDto().getSkiLiftId());
		reservSkiLiftRepo.deleteByReservLiftPkUserUserId(reservLift.getUserDto().getUserId());
		return true;
	}

}


package com.techconsulting.ws.service.core.modeldto.equipment;

import com.techconsulting.ws.dao.entity.shop.ShopEntity;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;

/**
 * The class for an equipment
 * 
 * @author Paul
 *
 */
public class EquipmentDto {

	/**
	 * Equipment identifier
	 */
	private Integer equipmentId;

	/**
	 * Equipment's name
	 */
	private String equipmentName;

	/**
	 * Equipment's description
	 */
	private String equipmentDescription;

	/**
	 * Equipment's state
	 */
	private String equipmentState;

	/**
	 * Equipment's stock
	 */
	private Integer equipmentStock;

	/**
	 * Number of removed equipment from the stock
	 */
	private Integer equipmentOut;

	/**
	 * Equipment's size
	 */
	private String equipmentSize;
	
	/**
	 * Equipment's price location
	 */
	private Integer priceLocation;

	/**
	 * the equipment's shop
	 */
	private ShopDto shop;

	public EquipmentDto() {
		super();
	}

	/**
	 * Getter for property equipmentId
	 * 
	 * @return {@link Integer}
	 */
	public Integer getEquipmentId() {
		return equipmentId;
	}

	/**
	 * Setter for property equipmentId
	 * 
	 * @param {@link Integer}
	 */
	public void setEquipmentId(Integer equipmentId) {
		this.equipmentId = equipmentId;
	}

	/**
	 * Getter for property equipmentName
	 * 
	 * @return {@link String}
	 */
	public String getEquipmentName() {
		return equipmentName;
	}

	/**
	 * Setter for property equipmentName
	 * 
	 * @param {@link String}
	 */
	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	/**
	 * Getter for property equipmentDescription
	 * 
	 * @return {@link String}
	 */
	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	/**
	 * Setter for property equipmentDescription
	 * 
	 * @param {@link String}
	 */
	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}

	/**
	 * Getter for property equipmentState
	 * 
	 * @return {@link String}
	 */
	public String getEquipmentState() {
		return equipmentState;
	}

	/**
	 * Setter for property equipmentState
	 * 
	 * @param {@link String}
	 */
	public void setEquipmentState(String equipmentState) {
		this.equipmentState = equipmentState;
	}

	/**
	 * Getter for property equipmentStock
	 * 
	 * @return {@link Integer}
	 */
	public Integer getEquipmentStock() {
		return equipmentStock;
	}

	/**
	 * Setter for property equipmentStock
	 * 
	 * @param {@link Integer}
	 */
	public void setEquipmentStock(Integer equipmentStock) {
		this.equipmentStock = equipmentStock;
	}

	/**
	 * Getter for property equipmentOut
	 * 
	 * @return {@link Integer}
	 */
	public Integer getEquipmentOut() {
		return equipmentOut;
	}

	/**
	 * Setter for property equipmentOut
	 * 
	 * @param {@link Integer}
	 */
	public void setEquipmentOut(Integer equipmentOut) {
		this.equipmentOut = equipmentOut;
	}

	/**
	 * Getter for property equipmentSize
	 * 
	 * @return {@link String}
	 */
	public String getEquipmentSize() {
		return equipmentSize;
	}

	/**
	 * Setter for property equipmentSize
	 * 
	 * @param {@link String}
	 */
	public void setEquipmentSize(String equipmentSize) {
		this.equipmentSize = equipmentSize;
	}
	
	
	/**
	 * Getter for property priceLocation
	 * @return {@link Integer}
	 */
	public Integer getPriceLocation() {
		return priceLocation;
	}
	
	/**
	 * Setter for property priceLocation
	 * @param {@link Integer}
	 */
	public void setPriceLocation(Integer priceLocation) {
		this.priceLocation = priceLocation;
	}

	/**
	 * Getter for property shop of an equipment
	 * 
	 * @return {@link ShopEntity}
	 */
	public ShopDto getShop() {
		return shop;
	}

	/**
	 * Setter for property shop of an equipment
	 * 
	 * @param {@link ShopEntity}
	 */
	public void setShop(ShopDto shop) {
		this.shop = shop;
	}
}

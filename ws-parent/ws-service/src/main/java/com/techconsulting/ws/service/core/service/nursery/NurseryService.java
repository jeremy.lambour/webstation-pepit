package com.techconsulting.ws.service.core.service.nursery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.repository.nursery.NurseryRepository;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryDto;
import com.techconsulting.ws.service.core.parser.nursery.NurseryParser;

@Service
@Transactional(readOnly=true)
public class NurseryService implements INurseryService {

	@Autowired
	private NurseryRepository repository;
	
	@Override
	public NurseryDto getNurseryById(Integer id) {
		return NurseryParser.convertToDto(repository.findOne(id));
	}

	@Override
	public List<NurseryDto> getAllNursery() {
		return NurseryParser.convertListToDTO(repository.findAll());
	}

}

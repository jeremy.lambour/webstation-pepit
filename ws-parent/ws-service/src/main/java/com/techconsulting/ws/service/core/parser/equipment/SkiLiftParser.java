package com.techconsulting.ws.service.core.parser.equipment;

import java.util.List;

import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.equipment.SkiLift;
import com.techconsulting.ws.dao.entity.reservation.lift.ReservationLift;
import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.parser.AgeRangeParser;
import com.techconsulting.ws.service.core.parser.common.PassTypeParser;


/**
 * Parser for {@link SkiLift} and {@link SkiLiftDto}
 * 
 * @author p_lem
 *
 */
public class SkiLiftParser {
	/**
	 * Constructor of SkiLiftParser
	 */
	private SkiLiftParser() {
		super();
	}

	/**
	 * Convert a {@link SkiLift} entity to a {@link SkiLiftDto}
	 * 
	 * @param {@link SkiLift} to set
	 * @return {@link SkiLiftDto} object
	 */
	public static SkiLiftDto convertToDto(SkiLift skiLift) {
		SkiLiftDto skiLiftDto;
		if (skiLift != null) {
			skiLiftDto = new SkiLiftDto();
			skiLiftDto.setSkiLiftId(skiLift.getSkiLiftId());
			skiLiftDto.setAgeRange(AgeRangeParser.convertToDto(skiLift.getAgeRange()));
			skiLiftDto.setPassType(PassTypeParser.convertToDto(skiLift.getPassType()));
			skiLiftDto.setDomain(skiLift.getDomain());
			skiLiftDto.setDuration(skiLift.getDuration());
			skiLiftDto.setPrice(skiLift.getPrice());
			return skiLiftDto;
		}
		return new SkiLiftDto();
	}

	/**
	 * Convert a {@link SkiLiftDto} to a {@link SkiLift} entity
	 * 
	 * @param {@link SkiLiftDto} to set
	 * @return {@link SkiLift}
	 */
	public static SkiLift convertToEntity(SkiLiftDto skiLiftDto) {
		SkiLift skiLift;
		if (skiLiftDto != null) {
			skiLift = new SkiLift();
			skiLift.setSkiLiftId(skiLiftDto.getSkiLiftId());
			skiLift.setAgeRange(AgeRangeParser.convertToEntity(skiLiftDto.getAgeRange()));
			skiLift.setPassType(PassTypeParser.convertToEntity(skiLiftDto.getPassType()));
			skiLift.setDomain(skiLiftDto.getDomain());
			skiLift.setDuration(skiLiftDto.getDuration());
			skiLift.setPrice(skiLiftDto.getPrice());
			return skiLift;
		}
		return new SkiLift();
	}

	/**
	 * Convert a list of {@link SkiLift} to a list of {@link SkiLiftDto}
	 * 
	 * @param SkiLift to set
	 * @return SkiLiftDto
	 */
	public static List<SkiLiftDto> convertListToDTO(List<SkiLift> listSkiLift) {
		return listSkiLift.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}
	
	/**
	 * Convert a list of {@link ReservationLiftDto} to a list of {@link ReservationLift}
	 * 
	 * @param listReservDto to set
	 * @return listReservation
	 */
	public static List<SkiLift> convertListToEntity(List<SkiLiftDto> listSkiLiftDto) {
		return listSkiLiftDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

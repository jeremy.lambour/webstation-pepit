package com.techconsulting.ws.service.core.service.accountcreation;

import java.util.List;

import com.techconsulting.ws.dao.entity.auth.Authority;
import com.techconsulting.ws.service.core.modeldto.user.AuthorityDto;

/**
 * Authority Service interface
 * 
 * @author Jeremy
 *
 */
public interface IAuthorityService {
	/**
	 * Insert new {@link Authority}
	 * 
	 * @param userRoleDto {@link AuthorityDto}
	 * @return created {@link AuthorityDto}
	 */
	AuthorityDto insertRole(AuthorityDto userRoleDto);

	/**
	 * Find the {@link Authority} by this name
	 * 
	 * @param authorityName : name of the authority.
	 * @return {@link AuthorityDto}
	 */
	AuthorityDto findByName(String authorityName);
	
	/**
	 * Find all possible authority in database
	 * @return {@link AuthorityDto} list
	 */
	List<AuthorityDto> findAll();

}

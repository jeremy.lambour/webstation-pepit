package com.techconsulting.ws.service.core.parser.reservation.lift;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.reservation.lift.ReservationLift;
import com.techconsulting.ws.dao.entity.reservation.lift.ReservationLiftPK;
import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.parser.UserParser;
import com.techconsulting.ws.service.core.parser.equipment.SkiLiftParser;

/**
 * Parser for {@link ReservationLift} and {@link ReservationLiftDto}
 * 
 * @author p_lem
 *
 */
public class ReservationLiftParser {
	/**
	 * Constructor of ReservationLiftParser
	 */
	private ReservationLiftParser() {
		super();
	}

	/**
	 * Convert a {@link ReservationLift} entity to a {@link ReservationLiftDto}
	 * 
	 * @param {@link ReservationLift} to set
	 * @return {@link ReservationLiftDto} object
	 */
	public static ReservationLiftDto convertToDto(ReservationLift reservEntity) {
		ReservationLiftDto reservLiftDTO;
		if (reservEntity != null) {
			reservLiftDTO = new ReservationLiftDto();
			reservLiftDTO.setUserDto(UserParser.convertToDto(reservEntity.getReservLiftPk().getUser()));
			reservLiftDTO.setSkiLiftDto(SkiLiftParser.convertToDto(reservEntity.getReservLiftPk().getSkiLift()));
			reservLiftDTO.setNameReserv(reservEntity.getReservLiftPk().getNameReserv());
			reservLiftDTO.setDateStart(reservEntity.getDateStart());
			reservLiftDTO.setDateEnd(reservEntity.getDateEnd());
			reservLiftDTO.setCaution(reservEntity.getCaution());
			reservLiftDTO.setInsuranceSnow(reservEntity.getInsuranceSnow());
			reservLiftDTO.setPrice(reservEntity.getPrice());
			return reservLiftDTO;
		}
		return new ReservationLiftDto();
	}

	/**
	 * Convert a {@link ReservationLiftDto} to a {@link ReservationLift} entity
	 * 
	 * @param {@link ReservationLiftDto} to set
	 * @return {@link ReservationLift}
	 */
	public static ReservationLift convertToEntity(ReservationLiftDto reservLiftDto) {
		ReservationLift reservLift;
		if (reservLiftDto != null) {
			ReservationLiftPK reservLiftPk;
			reservLiftPk = new ReservationLiftPK();
			reservLift= new ReservationLift();
			reservLiftPk.setUser(UserParser.convertToEntity(reservLiftDto.getUserDto()));
			reservLiftPk.setSkiLift(SkiLiftParser.convertToEntity(reservLiftDto.getSkiLiftDto()));
			reservLiftPk.setNameReserv(reservLiftDto.getNameReserv());
			reservLift.setReservLiftPk(reservLiftPk);
			reservLift.setDateStart(reservLiftDto.getDateStart());
			reservLift.setDateEnd(reservLiftDto.getDateEnd());
			reservLift.setCaution(reservLiftDto.getCaution());
			reservLift.setInsuranceSnow(reservLiftDto.getInsuranceSnow());
			reservLift.setPrice(reservLiftDto.getPrice());
			return reservLift;
		}
		return new ReservationLift();
	}

	/**
	 * Convert a list of {@link ReservationLift} to a list of {@link ReservationLiftDto}
	 * 
	 * @param listReservationLift to set
	 * @return listReservLiftDto
	 */
	public static List<ReservationLiftDto> convertListToDTO(List<ReservationLift> listReservationLift) {
		return listReservationLift.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}
	
	/**
	 * Convert a list of {@link ReservationLiftDto} to a list of {@link ReservationLift}
	 * 
	 * @param listReservDto to set
	 * @return listReservation
	 */
	public static List<ReservationLift> convertListToEntity(List<ReservationLiftDto> listReservLiftDto) {
		return listReservLiftDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

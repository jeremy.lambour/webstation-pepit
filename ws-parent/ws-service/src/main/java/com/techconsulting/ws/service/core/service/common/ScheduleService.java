package com.techconsulting.ws.service.core.service.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.ScheduleEntity;
import com.techconsulting.ws.dao.repository.common.ScheduleRepository;
import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;
import com.techconsulting.ws.service.core.parser.ScheduleParser;

/**
 * Service to manage {@link Schedule} and {@link ScheduleDto}
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class ScheduleService implements IScheduleService {

	/**
	 * {@link ScheduleRepository}
	 */
	@Autowired
	ScheduleRepository scheduleRepository;

	/**
	 * Method that inserts an {@link Schedule} in DataBase
	 * 
	 * @Param - {@link ScheduleDto} scheduleDto, the schedule we want to insert
	 * @Return - the {@link ScheduleDto} inserted
	 */
	@Override
	public ScheduleDto insertSchedule(ScheduleDto scheduleDto) {
		ScheduleEntity schedule = ScheduleParser.convertToEntity(scheduleDto);
		return ScheduleParser.convertToDto(scheduleRepository.save(schedule));
	}

	/**
	 * Method that finds an {@link ScheduleDto} with a specific id
	 * 
	 * @Param - Integer idSchedule, we want to find the {@link ScheduleDto} with
	 *        this id
	 * @Return - The {@link ScheduleDto}
	 */
	@Override
	public ScheduleDto findScheduleById(Integer idSchedule) {
		ScheduleEntity scheduleEntity = scheduleRepository.findOne(idSchedule);
		return ScheduleParser.convertToDto(scheduleEntity);
	}
	
	/**
	 * Method that sets an {@link ScheduleDto} open_hour from its identifier
	 * 
	 * @Param - {@link ScheduleDto} scheduleDto, we want to change the open_hour of this
	 *        {@link ScheduleDto}
	 * @Param - String open_hour, the new open_hour for the {@link ScheduleDto}
	 */
	@Override
	public void setScheduleOpenHourBySchedule(ScheduleDto scheduleDto, String openHour) {
		ScheduleEntity schedule = scheduleRepository.findOne(scheduleDto.getScheduleId());
		schedule.setScheduleOpenHour(openHour);
		scheduleRepository.save(schedule);
	}
	
	/**
	 * Method that sets an {@link ScheduleDto} close_hour from its identifier
	 * 
	 * @Param - {@link ScheduleDto} scheduleDto, we want to change the close_hour of this
	 *        {@link ScheduleDto}
	 * @Param - String close_hour, the new close_hour for the {@link ScheduleDto}
	 */
	@Override
	public void setScheduleCloseHourBySchedule(ScheduleDto scheduleDto, String closeHour) {
		ScheduleEntity schedule = scheduleRepository.findOne(scheduleDto.getScheduleId());
		schedule.setScheduleCloseHour(closeHour);
		scheduleRepository.save(schedule);
	}
	
	/**
	 * Method that deletes a {@link ScheduleDto} from a specific identifier
	 * 
	 * @Param - Integer idSchedule, the identifier of the {@link ScheduleDto} we want to
	 *        delete
	 */
	@Override
	public void deleteByScheduleId(Integer idSchedule) {
		scheduleRepository.deleteByScheduleId(idSchedule);
	}

}

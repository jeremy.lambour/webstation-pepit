package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.auth.Authority;
import com.techconsulting.ws.service.core.modeldto.user.AuthorityDto;

/**
 * Parser for {@link Authority} and {@link AuthorityDto}
 * 
 * @author p_lem
 *
 */
public class AuthorityParser {

	private AuthorityParser() {
		super();
	}

	/**
	 * Convert an {@link UserRole} entity to an {@link AuthorityDto}
	 * 
	 * @param {@link UserRole} to set
	 * @return {@link AuthorityDto} object
	 */
	public static AuthorityDto convertToDto(Authority userRole) {
		AuthorityDto userRoleDto;
		if (userRole != null) {
			userRoleDto = new AuthorityDto();
			userRoleDto.setAuthorityIdDto(userRole.getAuthorityId());
			userRoleDto.setAuthorityDtoName(userRole.getAuthorityName());
			return userRoleDto;
		}
		return new AuthorityDto();
	}

	/**
	 * Convert an {@link AuthorityDto} to an {@link UserRole} entity
	 * 
	 * @param {@link AuthorityDto} to set
	 * @return {@link UserRole} entity
	 */
	public static Authority convertToEntity(AuthorityDto userRoleDto) {
		Authority userRole;
		if (userRoleDto != null) {
			userRole = new Authority();
			userRole.setAuthorityId(userRoleDto.getAuthorityDtoId());
			userRole.setAuthorityName(userRoleDto.getAuthorityDtoName());
			return userRole;
		}
		return new Authority();
	}

	/**
	 * Convert a list of {@link UserRole} entity to a list of {@link AuthorityDto}
	 * 
	 * @param listUserRole to set
	 * @return listUserRoleDto
	 */
	public static List<AuthorityDto> convertListToDTO(List<Authority> listUserRole) {
		return listUserRole.stream().map(x -> convertToDto(x)).collect(Collectors.toList());

	}

	/**
	 * Convert a list of {@link AuthorityDto} to a list of {@link UserRole} entity
	 * 
	 * @param List<UserRoleDto> to set
	 * @return listUserRole
	 */
	public static List<Authority> convertListToEntity(List<AuthorityDto> listUserRoleDto) {
		return listUserRoleDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

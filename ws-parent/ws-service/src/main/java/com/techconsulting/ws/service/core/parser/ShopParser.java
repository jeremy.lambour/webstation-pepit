package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.entity.shop.ShopEntity;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;

/**
 * Parser for {@link ShopEntity} and {@link ShopDto}
 *
 */
public class ShopParser {

	private ShopParser() {
		super();
	}

	/**
	 * Convert a {@link ShopEntity} entity to a {@link ShopDto}
	 * 
	 * @param {@link ShopEntity} to set
	 * @return {@link ShopDto} object
	 */
	public static ShopDto convertToDto(ShopEntity shopEntity) {
		ShopDto shopDTO;
		if (shopEntity != null) {
			shopDTO = new ShopDto();
			shopDTO.setPhoneNumber(shopEntity.getPhoneNumber());
			shopDTO.setShopAddress(shopEntity.getShopAddress());
			shopDTO.setShopId(shopEntity.getShopId());
			shopDTO.setShopLatitude(shopEntity.getShopLatitude());
			shopDTO.setShopLongitude(shopEntity.getShopLongitude());
			shopDTO.setShopName(shopEntity.getShopName());
			if (shopEntity.getUser() != null) {
				UserDto userDto = new UserDto();
				userDto.setUserId(1);
				shopDTO.setUserDTO(userDto);
			} else {
				UserDto userDto = new UserDto();
				userDto.setUserId(2);
				shopDTO.setUserDTO(userDto);
			}
			return shopDTO;
		}
		return new ShopDto();
	}

	/**
	 * Convert a {@link ShopDto} to a {@link ShopEntity} entity
	 * 
	 * @param {@link ShopDto} to set
	 * @return {@link ShopEntity}
	 */
	public static ShopEntity convertToEntity(ShopDto shopDTO) {
		ShopEntity shopEntity;
		if (shopDTO != null) {
			shopEntity = new ShopEntity();
			shopEntity.setPhoneNumber(shopDTO.getPhoneNumber());
			shopEntity.setShopAddress(shopDTO.getShopAddress());
			shopEntity.setShopId(shopDTO.getShopId());
			shopEntity.setShopLatitude(shopDTO.getShopLatitude());
			shopEntity.setShopLongitude(shopDTO.getShopLongitude());
			shopEntity.setShopName(shopDTO.getShopName());
			if (shopDTO.getUserDTO() != null) {
				shopEntity.setUser(UserParser.convertToEntity(shopDTO.getUserDTO()));
			} else {
				User user = new User();
				user.setUserId(2);
				shopEntity.setUser(user);
			}
			return shopEntity;
		}
		return new ShopEntity();
	}

	/**
	 * Convert a list of {@link ShopEntity} to a list of {@link ShopDto}
	 * 
	 * @param listShopEntity to set
	 * @return listShopDTO listUser
	 */
	public static List<ShopDto> convertListToDTO(List<ShopEntity> listShopEntity) {
		return listShopEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link ShopDto} to a list of {@link ShopEntity}
	 * 
	 * @param listShopDTO to set
	 * @return listShopEntity
	 */
	public static List<ShopEntity> convertListToEntity(List<ShopDto> listShopDTO) {
		return listShopDTO.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

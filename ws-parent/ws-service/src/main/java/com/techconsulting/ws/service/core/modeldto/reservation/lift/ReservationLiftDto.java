package com.techconsulting.ws.service.core.modeldto.reservation.lift;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;

public class ReservationLiftDto {

	/**
	 * The reservation
	 */
	private SkiLiftDto skiLiftDto;

	/**
	 * The reservation's user
	 */
	private UserDto userDto;

	/**
	 * Reservation's name
	 */
	private String nameReserv;

	/**
	 * the date who start the reservation
	 */
	private Timestamp dateStart;

	/**
	 * the date who stop the reservation
	 */
	private Timestamp dateEnd;

	/**
	 * caution of the reservation
	 */
	private float caution;

	/**
	 * Inusurance for no snowing day
	 */
	private float insuranceSnow;

	/**
	 * price of the reservation
	 */
	private float price;

	/**
	 * Generic constructor
	 */
	public ReservationLiftDto() {
		super();
	}

	/**
	 * Getter of the reservation's ski Lift
	 * 
	 * @return {@link SkiLiftDto}
	 */
	public SkiLiftDto getSkiLiftDto() {
		return skiLiftDto;
	}

	/**
	 * Setter of the reservation's ski Lift
	 * 
	 * @param {@link SkiLiftDto}
	 */
	public void setSkiLiftDto(SkiLiftDto skiLift) {
		this.skiLiftDto = skiLift;
	}

	/**
	 * Getter of the reservation's user
	 * 
	 * @return {@link UserDto}
	 */
	public UserDto getUserDto() {
		return userDto;
	}

	/**
	 * Setter of the reservation's user
	 * 
	 * @param {@link UserDto}
	 */
	public void setUserDto(UserDto user) {
		this.userDto = user;
	}

	/**
	 * Getter of the reservation's name
	 * 
	 * @return {@link String}
	 */
	public String getNameReserv() {
		return nameReserv;
	}

	/**
	 * Setter of the reservation's name
	 * 
	 * @param {@link String}
	 */
	public void setNameReserv(String nameReserv) {
		this.nameReserv = nameReserv;
	}

	/**
	 * Getter for property dateStart
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateStart() {
		return dateStart;
	}

	/**
	 * Setter for property dateStart
	 * 
	 * @param a Timestamp
	 */
	public void setDateStart(Timestamp dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for property dateEnd
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateEnd() {
		return dateEnd;
	}

	/**
	 * Setter for property dateEnd
	 * 
	 * @param a Timestamp
	 */
	public void setDateEnd(Timestamp dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * Getter for property caution
	 * 
	 * @return {@link float}
	 */
	public float getCaution() {
		return caution;
	}

	/**
	 * Setter for property caution
	 * 
	 * @param {@link float}
	 */
	public void setCaution(float caution) {
		this.caution = caution;
	}

	/**
	 * Getter for property insurance Snow
	 * 
	 * @return {@link float}
	 */
	public float getInsuranceSnow() {
		return insuranceSnow;
	}

	/**
	 * Setter for property insurance Snow
	 * 
	 * @param {@link float}
	 */
	public void setInsuranceSnow(float insuranceSnow) {
		this.insuranceSnow = insuranceSnow;
	}

	/**
	 * Getter for property price
	 * 
	 * @return {@link float}
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * Setter for property price
	 * 
	 * @param {@link float}
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * Getter of informations about reservation's ski lift
	 * 
	 * @return String - the informations
	 */
	public String getInfos() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		StringBuilder sB = new StringBuilder();
		sB.append("Nom de réservation :" + this.nameReserv + "\n");
		sB.append("Prix : " + this.price + "\n");
		sB.append("Date de début :" + formatter.format(new Date(dateStart.getTime())) + "\n");
		sB.append("Date de fin :" + formatter.format(new Date(dateEnd.getTime())) + "\n");
		return sB.toString();
	}

}

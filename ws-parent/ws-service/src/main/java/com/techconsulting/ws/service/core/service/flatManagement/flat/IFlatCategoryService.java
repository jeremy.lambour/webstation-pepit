package com.techconsulting.ws.service.core.service.flatManagement.flat;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;

/**
 * The Interface of {@link FlatCategoryDto}
 */
public interface IFlatCategoryService {
	
	FlatCategoryDto insertFlatCategory(FlatCategoryDto flatCategoryDto);
	List<FlatCategoryDto> findAll();
	FlatCategoryDto findFlatCategoryById(Integer idFlatCategory);

}

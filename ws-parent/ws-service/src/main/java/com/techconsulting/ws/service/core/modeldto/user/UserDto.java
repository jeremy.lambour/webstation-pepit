package com.techconsulting.ws.service.core.modeldto.user;

import java.sql.Timestamp;

/**
 * The user authentification class
 * @author p_lem
 *
 */
public class UserDto {
	/**
	 * user id
	 */
	private Integer userId;
	
	/**
	 * user's username
	 */
	private String username;
	
	/**
	 * user's password
	 */
	private String password;
	
	/**
	 * The user's role
	 */
	private AuthorityDto userRoleDto;

	/**
	 * user's first name
	 */
	private String firstName;

	/**
	 * user's last name
	 */
	private String lastName;

	/**
	 * user's birthdate
	 */
	private Timestamp birthDate;
	
	/**
	 * user's age
	 */
	private int age;
	
	/**
	 * user's age range
	 */
	private AgeRangeDto ageRangeDto;

	/**
	 * user's phone number
	 */
	private String phoneNumber;

	/**
	 * user's address
	 */
	private String address;

	/**
	 * user's email
	 */
	private String email;

	/**
	 * user's city
	 */
	private String city;
	
	/**
	 * city's postal code
	 */
	private int postalCode;

	/**
	 * boolean to enable user's account
	 */
	private boolean isEnabled;
	
	/**
	 * The UserAuthentificationDto constructor
	 */
	public UserDto() {
		super();
	}
	
	/**
	 * Getter of the user's id
	 * @return the user's id
	 */
	public Integer getUserId() {
		return userId;
	}
	
	/**
	 * Setter of user's id
	 * @param userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	/**
	 * Getter of the username
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Setter of user's username
	 * @param username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * Getter of the user's password
	 * @return the user's password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter of the user's password
	 * @param the user's password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Getter of {@link AuthorityDto}
	 * @return the {@link AuthorityDto}
	 */
	public AuthorityDto getUserRoleDto() {
		return userRoleDto;
	}

	/**
	 * Setter of {@link AuthorityDto}
	 * @param the {@link AuthorityDto} to set
	 */
	public void setUserRoleDto(AuthorityDto userRoleDto) {
		this.userRoleDto = userRoleDto;
	}

	/**
	 * Getter of the user's first name
	 * @return the user's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter of user's first name
	 * @param first name to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter of the user's last name
	 * @return the user's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter of user's last name
	 * @param last name to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Getter of the user's birthdate
	 * @return the user's birthdate
	 */
	public Timestamp getBirthDate() {
		return birthDate;
	}

	/**
	 * Setter of user's birthdate
	 * @param birthdate to set
	 */
	public void setBirthDate(Timestamp birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Getter of the user's age
	 * @return the user's age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Setter of user's age
	 * @param age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	public AgeRangeDto getAgeRangeDto() {
		return ageRangeDto;
	}
	
	/**
	 * Setter of user's age
	 * @param age to set
	 */
	public void setAgeRangeDto(AgeRangeDto ageRange) {
		this.ageRangeDto = ageRange;
	}

	/**
	 * Getter of the user's phone number
	 * @return the user's phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Setter of user's phone number
	 * @param phone number to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Getter of the user's address
	 * @return the user's address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Setter of user's address
	 * @param address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Getter of the user's email
	 * @return the user's email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter of the user's email
	 * @param the user's email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter of the user's city
	 * @return the user's city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Setter of the user's city
	 * @param the user's city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Getter of the user's postal code
	 * @return the user's postal code
	 */
	public int getPostalCode() {
		return postalCode;
	}

	/**
	 * Setter of the user's postal code
	 * @param the user's postal code to set
	 */
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Getter of the user's isEnabled
	 * @return the user's isEnabled
	 */
	public boolean getEnabled() {
		return isEnabled;
	}

	/**
	 * Setter of the user's isEnabled
	 * @param the user's isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

}

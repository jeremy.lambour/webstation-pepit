package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.common.FestivePeriodEntity;
import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;

/**
 * Parser for {@link FestivePeriodEntity} and {@link FestivePeriodDto}
 *
 */
public class FestivePeriodParser {
	
	private FestivePeriodParser() {
		super();
	}
	
	/**
	 * Convert a {@link FestivePeriodEntity} entity to a {@link FestivePeriodDto}
	 * 
	 * @param {@link FestivePeriodEntity} to set
	 * @return {@link FestivePeriodDto} object
	 */
	public static FestivePeriodDto convertToDto(FestivePeriodEntity festivePeriodEntity) {
		FestivePeriodDto festivePeriodDto;
		if (festivePeriodEntity != null) {
			festivePeriodDto = new FestivePeriodDto();
			festivePeriodDto.setFestivePeriodBeginning(festivePeriodEntity.getFestivePeriodBeginning());
			festivePeriodDto.setFestivePeriodEnd(festivePeriodEntity.getFestivePeriodEnd());
			festivePeriodDto.setFestivePeriodId(festivePeriodEntity.getFestivePeriodId());
			festivePeriodDto.setFestivePeriodName(festivePeriodEntity.getFestivePeriodName());
			festivePeriodDto.setSeasonDto(SeasonParser.convertToDto(festivePeriodEntity.getSeasonEntity()));
			return festivePeriodDto;
		}
		return new FestivePeriodDto();
	}

	/**
	 * Convert a {@link FestivePeriodDto} to a {@link FestivePeriodEntity} entity
	 * 
	 * @param {@link FestivePeriodDto} to set
	 * @return {@link FestivePeriodEntity}
	 */
	public static FestivePeriodEntity convertToEntity(FestivePeriodDto festivePeriodDto) {
		FestivePeriodEntity festivePeriodEntity;
		if (festivePeriodDto != null) {
			festivePeriodEntity = new FestivePeriodEntity();
			festivePeriodEntity.setFestivePeriodBeginning(festivePeriodDto.getFestivePeriodBeginning());
			festivePeriodEntity.setFestivePeriodEnd(festivePeriodDto.getFestivePeriodEnd());
			festivePeriodEntity.setFestivePeriodId(festivePeriodDto.getFestivePeriodId());
			festivePeriodEntity.setFestivePeriodName(festivePeriodDto.getFestivePeriodName());
			festivePeriodEntity.setSeasonEntity(SeasonParser.convertToEntity(festivePeriodDto.getSeasonDto()));
			return festivePeriodEntity;
		}
		return new FestivePeriodEntity();
	}

	/**
	 * Convert a list of {@link FestivePeriodEntity} to a list of {@link FestivePeriodDto}
	 * 
	 * @param listFestivePeriodEntity to set
	 * @return listFestivePeriodDto listUser
	 */
	public static List<FestivePeriodDto> convertListToDTO(List<FestivePeriodEntity> listFestivePeriodEntity) {
		return listFestivePeriodEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link FestivePeriodDto} to a list of {@link FestivePeriodEntity}
	 * 
	 * @param listFestivePeriodDto to set
	 * @return listFestivePeriodEntity
	 */
	public static List<FestivePeriodEntity> convertListToEntity(List<FestivePeriodDto> listFestivePeriodDto) {
		return listFestivePeriodDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

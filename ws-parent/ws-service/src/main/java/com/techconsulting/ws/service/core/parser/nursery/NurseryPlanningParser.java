package com.techconsulting.ws.service.core.parser.nursery;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.nursery.NurseryPlanning;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningDto;
import com.techconsulting.ws.service.core.parser.SeasonParser;

public class NurseryPlanningParser {

	/**
	 * Convert an {@link NurseryPlanning} entity to an {@link NurseryPlanningDto}
	 * 
	 * @param {@link NurseryPlanning} to set
	 * @return {@link NurseryPlanningDto} object
	 */
	public static NurseryPlanningDto convertToDto(NurseryPlanning nurseryPlanning) {
		NurseryPlanningDto nurseryPlanningDto;
		if (nurseryPlanning != null) {
			nurseryPlanningDto = new NurseryPlanningDto();
			nurseryPlanningDto.setNurseryPlanningId(nurseryPlanning.getNurseryPlanningId());
			nurseryPlanningDto.setNurseryPlanningName(nurseryPlanning.getNurseryPlanningName());
			nurseryPlanningDto.setNurseryPlanningBeginning(nurseryPlanning.getNurseryPlanningBeginning());
			nurseryPlanningDto.setNurseryPlanningEnd(nurseryPlanning.getNurseryPlanningEnd());
			nurseryPlanningDto.setNursery(NurseryParser.convertToDto(nurseryPlanning.getNursery()));
			nurseryPlanningDto.setSeason(SeasonParser.convertToDto(nurseryPlanning.getSeason()));
			return nurseryPlanningDto;
		}
		return new NurseryPlanningDto();
	}

	/**
	 * Convert an {@link NurseryPlanningDto} to an {@link NurseryPlanning} entity
	 * 
	 * @param {@link NurseryPlanningDto} to set
	 * @return {@link NurseryPlanning} entity
	 */
	public static NurseryPlanning convertToEntity(NurseryPlanningDto nurseryPlanningDto) {
		NurseryPlanning nurseryPlanning;
		if (nurseryPlanningDto != null) {
			nurseryPlanning = new NurseryPlanning();
			nurseryPlanning.setNurseryPlanningId(nurseryPlanningDto.getNurseryPlanningId());
			nurseryPlanning.setNurseryPlanningName(nurseryPlanningDto.getNurseryPlanningName());
			nurseryPlanning.setNurseryPlanningBeginning(nurseryPlanningDto.getNurseryPlanningBeginning());
			nurseryPlanning.setNurseryPlanningEnd(nurseryPlanningDto.getNurseryPlanningEnd());
			nurseryPlanning.setNursery(NurseryParser.convertToEntity(nurseryPlanningDto.getNursery()));
			nurseryPlanning.setSeason(SeasonParser.convertToEntity(nurseryPlanningDto.getSeason()));
			return nurseryPlanning;
		}
		return new NurseryPlanning();
	}

	/**
	 * Convert a list of {@link NurseryPlanning} entity to a list of {@link NurseryPlanningDto}
	 * 
	 * @param list <NurseryPlanning> to set
	 * @return list <NurseryPlanningDto>
	 */
	public static List<NurseryPlanningDto> convertListToDTO(List<NurseryPlanning> listNurseryPlanning) {
		return listNurseryPlanning.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link NurseryPlanningDto} to a list of {@link NurseryPlanning} entity
	 * 
	 * @param List<NurseryPlanningDto> to set
	 * @return list<NurseryPlanning>
	 */
	public static List<NurseryPlanning> convertListToEntity(List<NurseryPlanningDto> listNurseryPlanningDto) {
		return listNurseryPlanningDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

package com.techconsulting.ws.service.core.service.skiingSchool;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.skiingSchool.Planning;
import com.techconsulting.ws.dao.repository.skiingSchool.PlanningRepository;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.LessonDto;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.PlanningDto;
import com.techconsulting.ws.service.core.parser.SeasonParser;
import com.techconsulting.ws.service.core.parser.skiingSchool.PlanningParser;
import com.techconsulting.ws.service.core.service.common.ISeasonService;

@Service
@Transactional(readOnly=false)
public class PlanningService implements IPlanningService{

	@Autowired
	private PlanningRepository repository;
	
    @Autowired
    private ILessonService lessonService;
    
    @Autowired
    private ISeasonService seasonService;
	
	@Transactional(readOnly=false)
	@Override
	public PlanningDto createPlanning(PlanningDto planning) {
		Planning p = PlanningParser.convertToEntity(planning);
		p.setPlanningSeason(SeasonParser.convertToEntity(seasonService.findSeasonById(p.getPlanningSeason().getSeasonId())));
		return PlanningParser.convertToDto(repository.save(p));

	}

	@Override
	@Transactional(readOnly=false)
	public PlanningDto updatePlanning(PlanningDto planning) {
		PlanningDto planninDto =  PlanningParser.convertToDto(repository.save(PlanningParser.convertToEntity(planning)));
		planninDto.setPlanningSeason(seasonService.findSeasonById(planninDto.getPlanningSeason().getSeasonId()));
		return planninDto;
	}

	@Override
	@Transactional(readOnly=false)
	public Boolean deleteplanning(Integer planningId) {
		List<LessonDto> lessons = lessonService.getLessonsByPLanningId(planningId);
		for(LessonDto lesson : lessons) {
			lessonService.deleteLesson(lesson.getLessonId());
		}
		repository.delete(planningId);
		return (repository.findOne(planningId) == null ? true : false); 

	}

	@Override
	public List<PlanningDto> getAllPlanningByMonitorAnSeasonId(Integer monitorId,Integer seasonId) {
		List<PlanningDto> plannings = PlanningParser.toListDto(repository.findByPlanningMonitorUserIdAndPlanningSeasonSeasonId(monitorId, seasonId));
		return plannings;
	}

	@Override
	public PlanningDto getPlanningById(Integer id) {
		PlanningDto planning = PlanningParser.convertToDto(repository.findOne(id));
		planning.setLessons(lessonService.getLessonsByPLanningId(id));
		return planning;
	}

}

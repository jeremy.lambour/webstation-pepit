package com.techconsulting.ws.service.core.service.common.comment.association.equipment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.comment.association.equipment.CommentEquipment;
import com.techconsulting.ws.dao.repository.common.comment.association.equipment.CommentEquipmentRepository;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.equipment.CommentEquipmentDTO;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.parser.common.comment.association.equipment.CommentEquipmentParser;

@Service
@Transactional(readOnly = false)
public class CommentEquipmentService implements ICommentEquipmentService {

	@Autowired
	CommentEquipmentRepository commentEquipmentRepository;

	/**
	 * Method that inserts an {@link CommentEquipmentDTO} in DataBase
	 * 
	 * @Param - {@link CommentEquipmentDTO} commentEquipmentDto, the
	 *        commentEquipment we want to insert
	 * @Return - the {@link CommentEquipmentDTO} inserted
	 */
	@Override
	public CommentEquipmentDTO insertCommentEquipment(CommentEquipmentDTO commentEquipmentDto) {
		CommentEquipment commentEquipment = CommentEquipmentParser.convertToEntity(commentEquipmentDto);
		return CommentEquipmentParser.convertToDto(commentEquipmentRepository.save(commentEquipment));
	}

	/**
	 * Method that finds a {@link CommentEquipmentDTO} with a specific id
	 * 
	 * @Param - Integer idCommentEquipment, we want to find the
	 *        {@link CommentEquipmentDTO} with this id
	 * @Return - The {@link CommentEquipmentDTO}
	 */
	@Override
	public CommentEquipmentDTO findCommentEquipmentById(Integer idCommentEquipment) {
		CommentEquipment commentEquipment = commentEquipmentRepository.findOne(idCommentEquipment);
		return CommentEquipmentParser.convertToDto(commentEquipment);
	}

	/**
	 * Method that return a list of {@link CommentEquipmentDTO} for a specific
	 * equipmentDto
	 * 
	 * @Param - {@link EquipmentDto} equipmentDto
	 * @Return - the list of {@link CommentEquipmentDTO}
	 */
	@Override
	public List<CommentEquipmentDTO> findByCommentEquipmentPkEquipment(Integer idEquipment) {
		List<CommentEquipment> listCommentEquipment = commentEquipmentRepository
				.findByCommentEquipmentPkEquipmentEquipmentId(idEquipment);
		return CommentEquipmentParser.convertListToDTO(listCommentEquipment);
	}

	/**
	 * Method that deletes a {@link CommentEquipmentDTO} from a specific identifier
	 * 
	 * @Param - Integer idCommentEquipment, the identifier of the
	 *        {@link CommentEquipmentDTO} we want to delete
	 */
	@Override
	public boolean deleteByCommentEquipment(CommentEquipmentDTO commentEquipmentDto) {
		commentEquipmentRepository
				.deleteByCommentEquipmentPkCommentCommentId(commentEquipmentDto.getCommentDto().getCommentId());
		commentEquipmentRepository
				.deleteByCommentEquipmentPkEquipmentEquipmentId(commentEquipmentDto.getEquipmentDto().getEquipmentId());
		return true;
	}

}

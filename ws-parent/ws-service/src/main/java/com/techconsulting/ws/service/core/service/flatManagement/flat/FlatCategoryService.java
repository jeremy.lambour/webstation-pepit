package com.techconsulting.ws.service.core.service.flatManagement.flat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatCategoryEntity;
import com.techconsulting.ws.dao.repository.flatManagement.flat.FlatCategoryRepository;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.parser.FlatCategoryParser;

/**
 * Service for flatCategory
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class FlatCategoryService implements IFlatCategoryService{
	
	/**
	 * {@link FlatCategoryRepository}
	 */
	@Autowired
	FlatCategoryRepository flatCategoryRepository;

	
	/**
	 * Method that inserts an {@link FlatCategoryEntity} in DataBase
	 * 
	 * @Param - {@link FlatCategoryDto} flatCategoryDTO, the flat category we want to insert
	 * @Return - the {@link FlatCategoryDto} inserted
	 */
	@Override
	public FlatCategoryDto insertFlatCategory(FlatCategoryDto flatCategoryDto) {
		FlatCategoryEntity flatCategory = FlatCategoryParser.convertToEntity(flatCategoryDto);
		return FlatCategoryParser.convertToDto(flatCategoryRepository.save(flatCategory));
	}
	
	/**
	 * Method that finds all the {@link FlatCategoryDto}
	 * 
	 * @Return - The list of {@link FlatCategoryDto}
	 */
	@Override
	public List<FlatCategoryDto> findAll() {
		List<FlatCategoryEntity> listFlatCategory = flatCategoryRepository.findAll();
		return FlatCategoryParser.convertListToDTO(listFlatCategory);
	}
	
	/**
	 * Method that finds an {@link FlatCategoryDto} with a specific id
	 * 
	 * @Param - Integer idFlatCategory, we want to find the {@link FlatCategoryDto} with this id
	 * @Return - The {@link FlatCategoryDto}
	 */
	@Override
	public FlatCategoryDto findFlatCategoryById(Integer idFlatCategory) {
		FlatCategoryEntity flatCategory = flatCategoryRepository.findOne(idFlatCategory);
		return FlatCategoryParser.convertToDto(flatCategory);
	}

}

package com.techconsulting.ws.service.core.modeldto.skiingSchool;

public class SkiingSchoolDto {

	
	private	Integer skiingSchoolId;
	
	
	private String skiingSchoolName;

	
	private String phoneNumber;


	public Integer getSkiingSchoolId() {
		return skiingSchoolId;
	}


	public void setSkiingSchoolId(Integer skiingSchoolId) {
		this.skiingSchoolId = skiingSchoolId;
	}


	public String getSkiingSchoolName() {
		return skiingSchoolName;
	}


	public void setSkiingSchoolName(String skiingSchoolName) {
		this.skiingSchoolName = skiingSchoolName;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
}

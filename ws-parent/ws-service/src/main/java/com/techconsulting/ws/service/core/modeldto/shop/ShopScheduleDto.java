package com.techconsulting.ws.service.core.modeldto.shop;

import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;

/**
 * Dto object for shopSchedule
 * @author gaillardc
 *
 */
public class ShopScheduleDto {

	/**
	 * The {@link ShopDto}
	 */
	private ShopDto shopDto;

	/**
	 * The {@link ScheduleDto}
	 */
	private ScheduleDto scheduleDto;

	/**
	 * ShopScheduleDto's generic constructor
	 */
	public ShopScheduleDto() {
		super();
	}

	/**
	 * @return the shopDto
	 */
	public ShopDto getShopDto() {
		return shopDto;
	}

	/**
	 * @param shopDto the shopDto to set
	 */
	public void setShopDto(ShopDto shopDto) {
		this.shopDto = shopDto;
	}

	/**
	 * @return the scheduleDto
	 */
	public ScheduleDto getScheduleDto() {
		return scheduleDto;
	}

	/**
	 * @param scheduleDto the scheduleDto to set
	 */
	public void setScheduleDto(ScheduleDto scheduleDto) {
		this.scheduleDto = scheduleDto;
	}

}

package com.techconsulting.ws.service.core.service.common.comment.association.hotel;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.common.comment.association.hotel.CommentHotelDTO;

public interface ICommentHotelService  {
	public CommentHotelDTO insertCommentHotel(CommentHotelDTO commentHotelDto);
	public CommentHotelDTO findCommentHotelById(Integer idCommentHotel);
	public List<CommentHotelDTO> findListCommentHotelByIdHotel(Integer idHotel);
	public boolean deleteByCommentHotel(CommentHotelDTO commentHotelDto);
}

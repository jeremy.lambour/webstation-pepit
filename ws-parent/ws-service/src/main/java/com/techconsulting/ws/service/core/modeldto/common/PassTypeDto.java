package com.techconsulting.ws.service.core.modeldto.common;


public class PassTypeDto {
	/**
	 * The pass type's identifier
	 */
	private Integer passTypeId;

	/**
	 * Pass type's name
	 */
	private String passTypeName;

	/**
	 * Generic Constructor
	 */
	public PassTypeDto() {
		super();
	}

	/**
	 * Getter for property passTypeId
	 * @return {@link Integer}
	 */
	public Integer getPassTypeId() {
		return passTypeId;
	}

	/**
	 * Setter for property passTypeId
	 * @param {@link Integer}
	 */
	public void setPassTypeId(Integer passTypeId) {
		this.passTypeId = passTypeId;
	}

	/**
	 * Getter for property pass type's Name
	 * @return {@link String}
	 */
	public String getPassTypeName() {
		return passTypeName;
	}

	/**
	 * Setter for property pass type's Name
	 * @param {@link String}
	 */
	public void setPassTypeName(String passTypeName) {
		this.passTypeName = passTypeName;
	}
}

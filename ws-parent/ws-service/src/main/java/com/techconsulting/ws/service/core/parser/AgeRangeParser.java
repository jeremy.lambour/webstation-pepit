package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.auth.AgeRange;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;

/**
 * Parser for convert ageRange Object
 * 
 * @author Jeremy
 *
 */
public class AgeRangeParser {

	private AgeRangeParser() {

	}

	/**
	 * Convert an {@link AgeRange} entity to an {@link AgeRangeDto}
	 * 
	 * @param {@link AgeRange} to set
	 * @return {@link AgeRangeDto} object
	 */
	public static AgeRangeDto convertToDto(AgeRange ageRange) {
		AgeRangeDto ageRangeDto;
		if (ageRange != null) {
			ageRangeDto = new AgeRangeDto();
			ageRangeDto.setAgeRangeId(ageRange.getAgeRangeId());
			ageRangeDto.setAgeRangeName(ageRange.getAgeRangeName());
			return ageRangeDto;
		}
		return new AgeRangeDto();
	}

	/**
	 * Convert an {@link AgeRangeDto} to an {@link AgeRange} entity
	 * 
	 * @param {@link AgeRangeDto} to set
	 * @return {@link AgeRange} entity
	 */
	public static AgeRange convertToEntity(AgeRangeDto ageRangeDto) {
		AgeRange ageRange;
		if (ageRangeDto != null) {
			ageRange = new AgeRange();
			ageRange.setAgeRangeId(ageRangeDto.getAgeRangeId());
			ageRange.setAgeRangeName(ageRangeDto.getAgeRangeName());
			return ageRange;
		}
		return new AgeRange();
	}

	/**
	 * Convert a list of {@link AgeRange} entity to a list of {@link AgeRangeDto}
	 * 
	 * @param listAgeRange to set
	 * @return listAgeRangeDto
	 */
	public static List<AgeRangeDto> convertListToDTO(List<AgeRange> listAgeRange) {
		return listAgeRange.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link AgeRangeDto} to a list of {@link AgeRange} entity
	 * 
	 * @param List<UserRoleDto> to set
	 * @return listUserRole
	 */
	public static List<AgeRange> convertListToEntity(List<AgeRangeDto> listAgeRangeDto) {
		return listAgeRangeDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

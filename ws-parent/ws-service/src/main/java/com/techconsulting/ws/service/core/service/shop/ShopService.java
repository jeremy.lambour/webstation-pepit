package com.techconsulting.ws.service.core.service.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.shop.ShopEntity;
import com.techconsulting.ws.dao.repository.auth.AgeRangeRepository;
import com.techconsulting.ws.dao.repository.auth.UserRepository;
import com.techconsulting.ws.dao.repository.shop.ShopRepository;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.parser.ShopParser;
import com.techconsulting.ws.service.core.parser.UserParser;

/**
 * Service for shop
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class ShopService implements IShopService {

	/**
	 * {@link ShopRepository}
	 */
	@Autowired
	ShopRepository shopRepository;

	/**
	 * {@link UserRepository}
	 */
	@Autowired
	UserRepository userRepository;

	/**
	 * {@link AgeRangeRepository}
	 */
	@Autowired
	AgeRangeRepository ageRangeRepository;

	/**
	 * Method that inserts an {@link Shop} in DataBase
	 * 
	 * @Param - {@link ShopDto} shopDTO, the shop we want to insert
	 * @Return - the {@link ShopDto} inserted
	 */
	@Override
	public ShopDto insertShop(ShopDto shopDTO) {
		ShopEntity shop = ShopParser.convertToEntity(shopDTO);
		return ShopParser.convertToDto(shopRepository.save(shop));
	}

	/**
	 * Method that finds an {@link ShopDto} with a specific id
	 * 
	 * @Param - Integer idShop, we want to find the {@link ShopDto} with this id
	 * @Return - The {@link ShopDto}
	 */
	@Override
	public ShopDto findShopById(Integer idShop) {
		ShopEntity shop = shopRepository.findOne(idShop);
		return ShopParser.convertToDto(shop);
	}

	/**
	 * Method that finds all the {@link ShopDto}
	 * 
	 * @Return - The list of {@link ShopDto}
	 */
	@Override
	public List<ShopDto> findAll() {
		List<ShopEntity> listShop = shopRepository.findAll();
		return ShopParser.convertListToDTO(listShop);
	}

	/**
	 * Method that sets an {@link ShopDto} name from its identifier
	 * 
	 * @Param - {@link ShopDto} shopDTO, we want to change the name of this
	 *        {@link ShopDto}
	 * @Param - String name, the new name for the {@link ShopDto}
	 */
	@Override
	public ShopEntity setShopNameByShop(ShopDto shopDTO, String name) {
		ShopEntity shop = shopRepository.findOne(shopDTO.getShopId());
		shop.setShopName(name);
		shopRepository.save(shop);
		return shop;
	}

	/**
	 * Method that sets an {@link ShopDto} name from its identifier
	 * 
	 * @Param - {@link ShopDto} shopDTO, we want to change the address of this
	 *        {@link ShopDto}
	 * @Param - String address, the new address for the {@link ShopDto}
	 */
	@Override
	public ShopEntity setShopAddressByShop(ShopDto shopDTO, String address) {
		ShopEntity shop = shopRepository.findOne(shopDTO.getShopId());
		shop.setShopAddress(address);
		shopRepository.save(shop);
		return shop;
	}

	/**
	 * Method that sets an {@link ShopDto} phoneNumber from its identifier
	 * 
	 * @Param - {@link ShopDto} shopDTO, we want to change the phoneNumber of this
	 *        {@link ShopDto}
	 * @Param - String phoneNumber, the new phoneNumber for the {@link ShopDto}
	 */
	@Override
	public ShopEntity setShopPhoneNumberByShop(ShopDto shopDTO, String phoneNumber) {
		ShopEntity shop = shopRepository.findOne(shopDTO.getShopId());
		shop.setPhoneNumber(phoneNumber);
		shopRepository.save(shop);
		return shop;
	}

	/**
	 * Method that sets an {@link ShopDto} userDto from its identifier
	 * 
	 * @Param - {@link ShopDto} shopDTO, we want to change the userDto of this
	 *        {@link ShopDto}
	 * @Param - {@link UserDto} userDto, the new userDto for the {@link ShopDto}
	 */
	@Override
	public ShopEntity setShopUserDtoByShop(ShopDto shopDTO, UserDto userDto) {
		ShopEntity shop = shopRepository.findOne(shopDTO.getShopId());
		shop.setUser(UserParser.convertToEntity(userDto));
		shopRepository.save(shop);
		return shop;
	}

	/**
	 * Method that deletes a {@link ShopDto} from a specific identifier
	 * 
	 * @Param - Integer idShop, the identifier of the {@link ShopDto} we want to
	 *        delete
	 */
	@Override
	public boolean deleteByShopId(Integer idShop) {
		shopRepository.delete(idShop);
		return true;
	}

	@Override
	public List<ShopDto> findShopByUserDtoUserId(Integer idUser) {
		List<ShopEntity> listShop = shopRepository.findShopByUserUserId(idUser);
		return ShopParser.convertListToDTO(listShop);
	}

}

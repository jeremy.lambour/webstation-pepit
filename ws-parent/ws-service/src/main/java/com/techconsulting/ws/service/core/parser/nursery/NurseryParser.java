package com.techconsulting.ws.service.core.parser.nursery;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.nursery.Nursery;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryDto;

public class NurseryParser {

	/**
	 * Convert an {@link Nursery} entity to an {@link NurseryDto}
	 * 
	 * @param {@link Nursery} to set
	 * @return {@link NurseryDto} object
	 */
	public static NurseryDto convertToDto(Nursery nursery) {
		NurseryDto nurseryDto;
		if (nursery != null) {
			nurseryDto = new NurseryDto();
			nurseryDto.setNurseryId(nursery.getNurseryId());
			nurseryDto.setNurseryName(nursery.getNurseryName());
			nurseryDto.setNurseryPhoneNumber(nursery.getNurseryPhoneNumber());
			return nurseryDto;
		}
		return new NurseryDto();
	}

	/**
	 * Convert an {@link NurseryDto} to an {@link Nursery} entity
	 * 
	 * @param {@link NurseryDto} to set
	 * @return {@link Nursery} entity
	 */
	public static Nursery convertToEntity(NurseryDto nurseryDto) {
		Nursery nursery;
		if (nurseryDto != null) {
			nursery = new Nursery();
			nursery.setNurseryId(nurseryDto.getNurseryId());
			nursery.setNurseryName(nurseryDto.getNurseryName());
			nursery.setNurseryPhoneNumber(nurseryDto.getNurseryPhoneNumber());
			return nursery;
		}
		return new Nursery();
	}

	/**
	 * Convert a list of {@link Nursery} entity to a list of {@link NurseryDto}
	 * 
	 * @param list <Nursery> to set
	 * @return list <NurseryDto>
	 */
	public static List<NurseryDto> convertListToDTO(List<Nursery> listNursery) {
		return listNursery.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link NurseryDto} to a list of {@link Nursery} entity
	 * 
	 * @param List<NurseryDto> to set
	 * @return list<Nursery>
	 */
	public static List<Nursery> convertListToEntity(List<NurseryDto> listNurseryDto) {
		return listNurseryDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

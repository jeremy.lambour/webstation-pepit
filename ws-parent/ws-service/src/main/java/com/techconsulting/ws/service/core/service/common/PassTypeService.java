package com.techconsulting.ws.service.core.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.PassType;
import com.techconsulting.ws.dao.repository.common.FestivePeriodRepository;
import com.techconsulting.ws.dao.repository.common.PassTypeRepository;
import com.techconsulting.ws.service.core.modeldto.common.PassTypeDto;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.parser.AgeRangeParser;
import com.techconsulting.ws.service.core.parser.common.PassTypeParser;

/**
 * Service to manage {@link PassType} and {@link PassTypeDto}
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class PassTypeService implements IPassTypeService{
	
	/**
	 * {@link FestivePeriodRepository}
	 */
	@Autowired
	PassTypeRepository passTypeRepository;
	
	/**
	 * Method that finds all the {@link PassTypeDto}
	 * 
	 * @Return - The list of {@link PassTypeDto}
	 */
	@Override
	public List<PassTypeDto> findAll() {
		List<PassType> listPassType = passTypeRepository.findAll();
		return PassTypeParser.convertListToDTO(listPassType);
	}
	
	@Override
	@Transactional(readOnly = true)
	public PassTypeDto findPassType(int id) {
		PassTypeDto passTypeDto = PassTypeParser.convertToDto(passTypeRepository.findOne(id));
		if (passTypeDto != null) {
			return passTypeDto;
		}
		return new PassTypeDto();
	}
	
}

package com.techconsulting.ws.service.core.modeldto.common;

public class ImageDto {
	
	/**
	 * The imageDto's image
	 */
	private byte[] imgByte;
	
	/**
	 * ImageDto's generic constructor
	 */
	public ImageDto() {
		super();
	}

	/**
	 * @return the imageDto's image
	 */
	public byte[] getImgByte() {
		return imgByte;
	}

	/**
	 * @param image the image to set
	 */
	public void setImgByte(byte[] imgByte) {
		this.imgByte = imgByte;
	}

}

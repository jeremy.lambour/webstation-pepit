package com.techconsulting.ws.service.core.service.equipment;

import java.util.List;

import com.techconsulting.ws.dao.entity.equipment.Equipment;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;



public interface IEquipmentService {
	
	EquipmentDto insertEquipment(EquipmentDto equipmentDTO);
	EquipmentDto findEquipmentById(Integer idEquipment);
	public List<EquipmentDto> findEquipmentByShopId(Integer idShop);
	List<EquipmentDto> findAll();
	void setEquipmentByEquipment(EquipmentDto equipmentDTO,Equipment equipment);
	void deleteByEquipmentId(Integer idEquipment);
	
	
}

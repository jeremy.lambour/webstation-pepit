package com.techconsulting.ws.service.core.service.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public interface IMailService {
	public boolean sendSimpleEmail(String to, String subject, String text);

	public MimeMessage templateMail(String to, int subjectId, int stateId, int objectId, boolean isFooter,
			String footer) throws MessagingException;

	public boolean sendEmail(String to, int subjectId, int stateId, int objectId, boolean isFooter, String footer);

}

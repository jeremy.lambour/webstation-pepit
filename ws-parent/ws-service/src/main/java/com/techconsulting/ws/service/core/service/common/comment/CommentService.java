package com.techconsulting.ws.service.core.service.common.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.comment.Comment;
import com.techconsulting.ws.dao.repository.common.comment.CommentRepository;
import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.parser.common.comment.CommentParser;

@Service
@Transactional(readOnly = false)
public class CommentService implements ICommentService{

	@Autowired
	CommentRepository commentRepository;
	
	/**
	 * Method that inserts an {@link Comment} in DataBase
	 * 
	 * @Param - {@link CommentDTO} commentDto, the comment we want to insert
	 * @Return - the {@link CommentDTO} inserted
	 */

	@Override
	public CommentDTO insertComment(CommentDTO commentDto) {
		Comment comment = CommentParser.convertToEntity(commentDto);
		return CommentParser.convertToDto(commentRepository.save(comment));
	}
	
	/**
	 * Method that finds an {@link CommentDTO} with a specific id
	 * 
	 * @Param - Integer idComment, we want to find the {@link CommentDTO} with
	 *        this id
	 * @Return - The {@link CommentDTO}
	 */
	@Override
	public CommentDTO findCommentById(Integer idComment) {
		Comment comment = commentRepository.findOne(idComment);
		return CommentParser.convertToDto(comment);
	}
	
	/**
	 * Method that deletes a {@link CommentDTO} from a specific identifier
	 * 
	 * @Param - Integer idComment, the identifier of the {@link CommentDTO} we
	 *        want to delete
	 */
	@Override
	public boolean deleteByCommentId(Integer idComment) {
		commentRepository.delete(idComment);
		return true;
	}
}

package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;

/**
 * Parser for {@link HotelEntity} and {@link HotelDto}
 *
 */
public class HotelParser {
	
	private HotelParser() {
		super();
	}
	
	/**
	 * Convert a {@link HotelEntity} entity to a {@link HotelDto}
	 * 
	 * @param {@link HotelEntity} to set
	 * @return {@link HotelDto} object
	 */
	public static HotelDto convertToDto(HotelEntity hotelEntity) {
		HotelDto hotelDto;
		if (hotelEntity != null) {
			hotelDto = new HotelDto();
			hotelDto.setAddress(hotelEntity.getAddress());
			hotelDto.setHotelId(hotelEntity.getHotelId());
			hotelDto.setHotelName(hotelEntity.getHotelName());
			hotelDto.setHotelRoomsNumber(hotelEntity.getHotelRoomsNumber());
			hotelDto.setMailAddress(hotelEntity.getMailAddress());
			hotelDto.setPhoneNumber(hotelEntity.getPhoneNumber());
			if (hotelEntity.getUser() != null) {
				hotelDto.setUserDto(UserParser.convertToDto(hotelEntity.getUser()));
			} else {
				UserDto userDto = new UserDto();
				userDto.setUserId(2);
				hotelDto.setUserDto(userDto);
			}
			return hotelDto;
		}
		return new HotelDto();
	}

	/**
	 * Convert a {@link HotelDto} to a {@link HotelEntity} entity
	 * 
	 * @param {@link HotelDto} to set
	 * @return {@link HotelEntity}
	 */
	public static HotelEntity convertToEntity(HotelDto hotelDto) {
		HotelEntity hotelEntity;
		if (hotelDto != null) {
			hotelEntity = new HotelEntity();
			hotelEntity.setAddress(hotelDto.getAddress());
			hotelEntity.setHotelId(hotelDto.getHotelId());
			hotelEntity.setHotelName(hotelDto.getHotelName());
			hotelEntity.setHotelRoomsNumber(hotelDto.getHotelRoomsNumber());
			hotelEntity.setMailAddress(hotelDto.getMailAddress());
			hotelEntity.setPhoneNumber(hotelDto.getPhoneNumber());
			if (hotelDto.getUserDto() != null) {
				hotelEntity.setUser(UserParser.convertToEntity(hotelDto.getUserDto()));
			} else {
				User user = new User();
				user.setUserId(2);
				hotelEntity.setUser(user);
			}
			return hotelEntity;
		}
		return new 	HotelEntity();
	}

	/**
	 * Convert a list of {@link HotelEntity} to a list of {@link HotelDto}
	 * 
	 * @param listHotelEntity to set
	 * @return listHotelDto
	 */
	public static List<HotelDto> convertListToDTO(List<HotelEntity> listHotelEntity) {
		return listHotelEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link HotelDto} to a list of {@link HotelEntity}
	 * 
	 * @param listHotelDto to set
	 * @return listHotelEntity
	 */
	public static List<HotelEntity> convertListToEntity(List<HotelDto> listHotelDto) {
		return listHotelDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

package com.techconsulting.ws.service.core.service.shop;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.dao.entity.shop.ShopEntity;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;

/**
 * The Interface of {@link ShopDto}
 */
public interface IShopService {
	ShopDto insertShop(ShopDto shopDTO);
	ShopDto findShopById(Integer idShop);
	List<ShopDto> findAll();
	ShopEntity setShopNameByShop(ShopDto shopDTO, String name);
	ShopEntity setShopAddressByShop(ShopDto shopDTO, String address);
	ShopEntity setShopPhoneNumberByShop(ShopDto shopDTO, String phoneNumber);
	ShopEntity setShopUserDtoByShop(ShopDto shopDTO, UserDto userDto);
	boolean deleteByShopId(Integer idShop);
	List<ShopDto> findShopByUserDtoUserId(Integer idUser);
}

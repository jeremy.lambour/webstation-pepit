package com.techconsulting.ws.service.core.service.mail.reservskilift;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.service.mail.IMailService;

@Service
public class MailSkilift implements IMailSkilift {

	@Autowired
	IMailService emailService;

	/**
	 * Method who send an email for an insertion of a reservation of skilift
	 * 
	 * @param - {@link {@link ReservationLiftDto} reservSkilift : object compose
	 *        with a reservation and a user
	 */
	@Override
	public void insertReservMail(ReservationLiftDto reservSkiLiftDto) {
		emailService.sendEmail(reservSkiLiftDto.getUserDto().getEmail(), 0, 0, 1, true, reservSkiLiftDto.getInfos());
	}

	/**
	 * Method who send an email for a delete of a reservation of skilift
	 * 
	 * @param - {@link {@link ReservationLiftDto} reservSkilift : object compose
	 *        with a reservation and a user
	 */
	@Override
	public void deleteReservMail(ReservationLiftDto reservSkiLiftDto) {
		emailService.sendEmail(reservSkiLiftDto.getUserDto().getEmail(), 0, 1, 1, true, reservSkiLiftDto.getInfos());
	}

}

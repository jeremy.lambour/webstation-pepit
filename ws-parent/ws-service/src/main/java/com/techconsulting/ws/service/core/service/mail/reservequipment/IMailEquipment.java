package com.techconsulting.ws.service.core.service.mail.reservequipment;

import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;

public interface IMailEquipment {

	public boolean insertReservMail(ReservationEquipmentDto reservEquipDto);

	public boolean deleteReservMail(ReservationEquipmentDto reservEquipDto);

}

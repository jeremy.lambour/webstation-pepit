package com.techconsulting.ws.service.core.service.mail.reservequipment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.service.mail.IMailService;

@Service
public class MailEquipment implements IMailEquipment {

	@Autowired
	IMailService emailService;

	/**
	 * Method who send an email for an insertion of a reservation of an equipment
	 * 
	 * @param - {@link {@link ReservationEquipmentDto} reservEquipDto : object
	 *        compose with a reservation and a user
	 *        
	 * @return {@link boolean}
	 */
	public boolean insertReservMail(ReservationEquipmentDto reservEquipDto) {
		emailService.sendEmail(reservEquipDto.getReservationDto().getUser().getEmail(), 0, 0, 0, true,
				reservEquipDto.getInfos());
		return true;
	}

	/**
	 * Method who send an email for an delete of a reservation of an equipment
	 * 
	 * @param - {@link ReservationEquipmentDto} reservSkilift : object
	 *        compose with a reservation and a user
	 *    
	 * @return {@link boolean}
	 */
	public boolean deleteReservMail(ReservationEquipmentDto reservEquipDto) {
		emailService.sendEmail(reservEquipDto.getReservationDto().getUser().getEmail(), 1, 1, 0, true,
				reservEquipDto.getInfos());
		return true;
	}

}

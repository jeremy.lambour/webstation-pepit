package com.techconsulting.ws.service.core.service.reservation.flat;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;

public interface IReservationFlat {
	List<ReservationFlatDto> findByReservationFlatIdFlatDto(FlatDto flatDto);
	
	ReservationFlatDto insertReservationFlat(ReservationFlatDto rFlat);
	
	List<ReservationFlatDto> findByReservationFlatIdReservationDtoUserUserId(Integer userId);
	
	List<ReservationFlatDto> findAll();

}

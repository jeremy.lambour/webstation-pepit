package com.techconsulting.ws.service.core.parser.nursery;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.nursery.NurseryPlanningSlot;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningSlotDto;

public class NurseryPlanningSlotParser {

	/**
	 * Convert an {@link NurseryPlanningSlot} entity to an {@link NurseryPlanningSlotDto}
	 * 
	 * @param {@link NurseryPlanningSlot} to set
	 * @return {@link NurseryPlanningSlotDto} object
	 */
	public static NurseryPlanningSlotDto convertToDto(NurseryPlanningSlot nurseryPlanningSlot) {
		NurseryPlanningSlotDto nurseryPlanningSlotDto;
		if (nurseryPlanningSlot != null) {
			nurseryPlanningSlotDto = new NurseryPlanningSlotDto();
			nurseryPlanningSlotDto.setNurseryPlanningSlotId(nurseryPlanningSlot.getNurseryPlanningSlotId());
			nurseryPlanningSlotDto.setDate(nurseryPlanningSlot.getDate());
			nurseryPlanningSlotDto.setDuration(nurseryPlanningSlot.getDuration());
			nurseryPlanningSlotDto.setEndTime(nurseryPlanningSlot.getEndTime());
			nurseryPlanningSlotDto.setNurseryPlanning(nurseryPlanningSlot.getNurseryPlanning().getNurseryPlanningId());
			nurseryPlanningSlotDto.setStartTime(nurseryPlanningSlot.getStartTime());
			nurseryPlanningSlotDto.setNurserySlotNumberPlaces(nurseryPlanningSlot.getNurserySlotNumberPlaces());
			return nurseryPlanningSlotDto;
		}
		return new NurseryPlanningSlotDto();
	}

	/**
	 * Convert an {@link NurseryPlanningSlotDto} to an {@link NurseryPlanningSlot} entity
	 * 
	 * @param {@link NurseryPlanningSlotDto} to set
	 * @return {@link NurseryPlanningSlot} entity
	 */
	public static NurseryPlanningSlot convertToEntity(NurseryPlanningSlotDto nurseryPlanningSlotDto) {
		NurseryPlanningSlot nurseryPlanningSlot;
		if (nurseryPlanningSlotDto != null) {
			nurseryPlanningSlot = new NurseryPlanningSlot();
			nurseryPlanningSlot.setNurseryPlanningSlotId(nurseryPlanningSlotDto.getNurseryPlanningSlotId());
			nurseryPlanningSlot.setDate(nurseryPlanningSlotDto.getDate());
			nurseryPlanningSlot.setDuration(nurseryPlanningSlotDto.getDuration());
			nurseryPlanningSlot.setEndTime(nurseryPlanningSlotDto.getEndTime());
			nurseryPlanningSlot.setStartTime(nurseryPlanningSlotDto.getStartTime());
			nurseryPlanningSlot.setNurserySlotNumberPlaces(nurseryPlanningSlotDto.getNurserySlotNumberPlaces());
			return nurseryPlanningSlot;
		}
		return new NurseryPlanningSlot();
	}

	/**
	 * Convert a list of {@link NurseryPlanningSlot} entity to a list of {@link NurseryPlanningSlotDto}
	 * 
	 * @param list <NurseryPlanningSlot> to set
	 * @return list <NurseryPlanningSlotDto>
	 */
	public static List<NurseryPlanningSlotDto> convertListToDTO(List<NurseryPlanningSlot> listNurseryPlanningSlot) {
		return listNurseryPlanningSlot.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link NurseryPlanningSlotDto} to a list of {@link NurseryPlanningSlot} entity
	 * 
	 * @param List<NurseryPlanningSlot> to set
	 * @return list<NurseryPlanningSlotDto>
	 */
	public static List<NurseryPlanningSlot> convertListToEntity(List<NurseryPlanningSlotDto> listNurseryPlanningSlotDto) {
		return listNurseryPlanningSlotDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

package com.techconsulting.ws.service.core.service.shop;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.shop.ShopScheduleDto;

/**
 * The Interface of {@link ShopScheduleDto}
 */
public interface IShopScheduleService {
	List<ShopScheduleDto> findByShopScheduleIdShopEntityShopId(Integer idShop);

	ShopScheduleDto insertShopSchedule(ShopScheduleDto shopScheduleDto);
	
	void deleteByShopScheduleIdShopEntityShopId(Integer idShop);
}

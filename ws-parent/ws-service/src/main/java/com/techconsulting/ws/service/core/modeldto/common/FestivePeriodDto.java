package com.techconsulting.ws.service.core.modeldto.common;

import java.util.Date;

import com.techconsulting.ws.dao.entity.common.SeasonEntity;

public class FestivePeriodDto {
	
	/**
	 * The festivePeriod's identifier
	 */
	private Integer festivePeriodId;

	/**
	 * The festivePeriod's name
	 */
	private String festivePeriodName;

	/**
	 * The festivePeriod's beginning
	 */
	private Date festivePeriodBeginning;

	/**
	 * The festivePeriod's end
	 */
	private Date festivePeriodEnd;
	
	/**
	 * The {@link SeasonEntity}
	 */
	private SeasonDto seasonDto;
	
	/**
	 * FestivePeriodDto's generic constructor
	 */
	public FestivePeriodDto() {
		super();
	}

	/**
	 * @return the festivePeriodId
	 */
	public Integer getFestivePeriodId() {
		return festivePeriodId;
	}

	/**
	 * @param festivePeriodId the festivePeriodId to set
	 */
	public void setFestivePeriodId(Integer festivePeriodId) {
		this.festivePeriodId = festivePeriodId;
	}

	/**
	 * @return the festivePeriodName
	 */
	public String getFestivePeriodName() {
		return festivePeriodName;
	}

	/**
	 * @param festivePeriodName the festivePeriodName to set
	 */
	public void setFestivePeriodName(String festivePeriodName) {
		this.festivePeriodName = festivePeriodName;
	}

	/**
	 * @return the festivePeriodBeginning
	 */
	public Date getFestivePeriodBeginning() {
		return festivePeriodBeginning;
	}

	/**
	 * @param festivePeriodBeginning the festivePeriodBeginning to set
	 */
	public void setFestivePeriodBeginning(Date festivePeriodBeginning) {
		this.festivePeriodBeginning = festivePeriodBeginning;
	}

	/**
	 * @return the festivePeriodEnd
	 */
	public Date getFestivePeriodEnd() {
		return festivePeriodEnd;
	}

	/**
	 * @param festivePeriodEnd the festivePeriodEnd to set
	 */
	public void setFestivePeriodEnd(Date festivePeriodEnd) {
		this.festivePeriodEnd = festivePeriodEnd;
	}

	/**
	 * @return the seasonDto
	 */
	public SeasonDto getSeasonDto() {
		return seasonDto;
	}

	/**
	 * @param seasonDto the seasonDto to set
	 */
	public void setSeasonDto(SeasonDto seasonDto) {
		this.seasonDto = seasonDto;
	}
	
	

}

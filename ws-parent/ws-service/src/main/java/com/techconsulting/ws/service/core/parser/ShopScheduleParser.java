package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.shop.ShopScheduleEntity;
import com.techconsulting.ws.dao.entity.shop.ShopScheduleIdEntity;
import com.techconsulting.ws.service.core.modeldto.shop.ShopScheduleDto;

/**
 * Parser for {@link ShopScheduleEntity} and {@link ShopScheduleDto}
 *
 */
public class ShopScheduleParser {
	
	private ShopScheduleParser() {
		super();
	}

	/**
	 * Convert a {@link ShopScheduleEntity} to a {@link ShopScheduleDto}
	 * 
	 * @param shopScheduleEntity to set
	 * @return shopScheduleDto object
	 */
	public static ShopScheduleDto convertToDto(ShopScheduleEntity shopScheduleEntity) {
		ShopScheduleDto shopScheduleDto;
		if (shopScheduleEntity != null) {
			shopScheduleDto = new ShopScheduleDto();
			shopScheduleDto.setShopDto(ShopParser.convertToDto(shopScheduleEntity.getShopScheduleId().getShopEntity()));
			shopScheduleDto.setScheduleDto(
					ScheduleParser.convertToDto(shopScheduleEntity.getShopScheduleId().getScheduleEntity()));
			return shopScheduleDto;
		}
		return new ShopScheduleDto();
	}

	/**
	 * Convert an {@link ShopScheduleDto} to an {@link ShopScheduleEntity}
	 * 
	 * @param ShopScheduleDto to set
	 * @return shopScheduleEntity
	 */
	public static ShopScheduleEntity convertToEntity(ShopScheduleDto shopScheduleDto) {
		ShopScheduleEntity shopScheduleEntity;
		if (shopScheduleDto != null) {
			shopScheduleEntity = new ShopScheduleEntity();
			ShopScheduleIdEntity shopScheduleIdEntity = new ShopScheduleIdEntity();
			shopScheduleIdEntity.setShopEntity(ShopParser.convertToEntity(shopScheduleDto.getShopDto()));
			shopScheduleIdEntity.setScheduleEntity(ScheduleParser.convertToEntity(shopScheduleDto.getScheduleDto()));
			shopScheduleEntity.setShopScheduleId(shopScheduleIdEntity);
			return shopScheduleEntity;
		}
		return new ShopScheduleEntity();
	}

	/**
	 * Convert a list of {@link ShopScheduleEntity} to a list of
	 * {@link ShopScheduleDto}
	 * 
	 * @param listShopScheduleEntity to set
	 * @return listShopScheduleDto
	 */
	public static List<ShopScheduleDto> convertListToDTO(List<ShopScheduleEntity> listShopScheduleEntity) {
		return listShopScheduleEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link ShopScheduleDto} to a list of
	 * {@link ShopScheduleEntity}
	 * 
	 * @param listShopScheduleDto to set
	 * @return listShopScheduleEntity
	 */
	public static List<ShopScheduleEntity> convertListToEntity(List<ShopScheduleDto> listShopScheduleDto) {
		return listShopScheduleDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}
}

package com.techconsulting.ws.service.core.modeldto.common;

import java.util.Date;

public class SeasonDto {
	
	/**
	 * The season's identifier
	 */
	private Integer seasonId;

	/**
	 * The season's name
	 */
	private String seasonName;

	/**
	 * The season's beginning
	 */
	private Date seasonBeginning;

	/**
	 * The season's end
	 */
	private Date seasonEnd;
	
	/**
	 * The season's active status
	 */
	private Boolean active;
	

	/**
	 * SeasonDto's generic constructor
	 */
	public SeasonDto() {
		super();
	}

	/**
	 * @return the seasonId
	 */
	public Integer getSeasonId() {
		return seasonId;
	}

	/**
	 * @param seasonId the seasonId to set
	 */
	public void setSeasonId(Integer seasonId) {
		this.seasonId = seasonId;
	}

	/**
	 * @return the seasonName
	 */
	public String getSeasonName() {
		return seasonName;
	}

	/**
	 * @param seasonName the seasonName to set
	 */
	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}

	/**
	 * @return the seasonBeginning
	 */
	public Date getSeasonBeginning() {
		return seasonBeginning;
	}

	/**
	 * @param seasonBeginning the seasonBeginning to set
	 */
	public void setSeasonBeginning(Date seasonBeginning) {
		this.seasonBeginning = seasonBeginning;
	}

	/**
	 * @return the seasonEnd
	 */
	public Date getSeasonEnd() {
		return seasonEnd;
	}

	/**
	 * @param seasonEnd the seasonEnd to set
	 */
	public void setSeasonEnd(Date seasonEnd) {
		this.seasonEnd = seasonEnd;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}

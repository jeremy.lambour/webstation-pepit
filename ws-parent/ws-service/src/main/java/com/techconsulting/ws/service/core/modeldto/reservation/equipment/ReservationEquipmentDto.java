package com.techconsulting.ws.service.core.modeldto.reservation.equipment;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;

/**
 * The class for reservation of an equipment
 * 
 * @author Paul
 *
 */
public class ReservationEquipmentDto {

	/**
	 * the equipment reserved
	 */
	private EquipmentDto equipmentDto;

	/**
	 * The reservation
	 */
	private ReservationDto reservationDto;

	/**
	 * the date who start the reservation
	 */
	private Timestamp dateStart;

	/**
	 * the date who stop the reservation
	 */
	private Timestamp dateEnd;

	/**
	 * Equipment quantity reserved
	 */
	private int quantity;

	/**
	 * Reservation's price
	 */
	private Integer price;
	
	/**
	 * Reservation's isGiven status
	 */
	private Boolean isGiven;

	/**
	 * Generic constructor of ReservationEquipmentDto
	 */
	public ReservationEquipmentDto() {
		super();
	}

	/**
	 * Getter of {@link EquipmentDto}
	 * 
	 * @return {@link EquipmentDto}
	 */
	public EquipmentDto getEquipmentDto() {
		return equipmentDto;
	}

	/**
	 * Setter of {@link EquipmentDto}
	 * 
	 * @param {@link EquipmentDto}
	 */
	public void setEquipmentDto(EquipmentDto equipmentDto) {
		this.equipmentDto = equipmentDto;
	}

	/**
	 * Getter of {@link ReservationDto}
	 * 
	 * @return {@link ReservationDto}
	 */
	public ReservationDto getReservationDto() {
		return reservationDto;
	}

	/**
	 * Setter of {@link ReservationDto}
	 * 
	 * @param {@link ReservationDto}
	 */
	public void setReservationDto(ReservationDto reservationDto) {
		this.reservationDto = reservationDto;
	}

	/**
	 * Getter for property dateStart
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateStart() {
		return dateStart;
	}

	/**
	 * Setter for property dateStart
	 * 
	 * @param a Timestamp
	 */
	public void setDateStart(Timestamp dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for property dateEnd
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateEnd() {
		return dateEnd;
	}

	/**
	 * Setter for property dateEnd
	 * 
	 * @param a Timestamp
	 */
	public void setDateEnd(Timestamp dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * Getter for property quantity
	 * 
	 * @return a Integer
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Setter for property quantity
	 * 
	 * @param a Integer
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * Getter for property price
	 * 
	 * @return a Integer
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * Setter for property price
	 * 
	 * @param a Integer
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * Getter of informations about reservation's equipment
	 * 
	 * @return String - the informations
	 */
	public String getInfos() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		StringBuilder sB = new StringBuilder();
		sB.append("Quantité :" + this.quantity + "\n");
		sB.append("Prix : " + this.price + "\n");
		sB.append("Date de début :" + formatter.format(new Date(dateStart.getTime())) + "\n");
		sB.append("Date de fin :" + formatter.format(new Date(dateEnd.getTime())) + "\n");
		return sB.toString();
	}
    /*
     * Getter method for isGiven
	 * @return the isGiven
	 */
	public Boolean getIsGiven() {
		return isGiven;
	}

	/**
	 * @param isGiven the isGiven to set
	 */
	public void setIsGiven(Boolean isGiven) {
		this.isGiven = isGiven;
	}
	
	

}

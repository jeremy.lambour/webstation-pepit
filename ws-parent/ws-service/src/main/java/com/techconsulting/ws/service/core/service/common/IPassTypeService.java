package com.techconsulting.ws.service.core.service.common;

import java.util.List;
import com.techconsulting.ws.service.core.modeldto.common.PassTypeDto;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;

/**
 * The Interface of {@link PassTypeDto}
 */
public interface IPassTypeService {
	List<PassTypeDto> findAll();
	PassTypeDto findPassType(int id);
}

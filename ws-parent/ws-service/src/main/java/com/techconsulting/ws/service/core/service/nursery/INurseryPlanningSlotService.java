package com.techconsulting.ws.service.core.service.nursery;

import java.util.List;

import com.techconsulting.ws.dao.entity.nursery.NurseryPlanning;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningSlotDto;

/**
 * {@link NurseryPlanningService} declaration
 * @author Jeremy
 *
 */
public interface INurseryPlanningSlotService {
	
	/**
	 * Create a new {@link NurseryPlanningSlotDto} 
	 * @param dto {@link NurseryPlanningSlotDto} to create
	 * @return created {@link NurseryPlanningSlotDto}
	 */
	public NurseryPlanningSlotDto createNurseryPlanningSlot(NurseryPlanningSlotDto dto);
	
	/**
	 * Update {@link NurseryPlanningSlotDto} 
	 * @param dto {@link NurseryPlanningSlotDto} to update
	 * @return Updated {@link NurseryPlanningSlotDto}
	 */
	public NurseryPlanningSlotDto updateNurseryPlanningSlot(NurseryPlanningSlotDto dto);
	
	/**
	 * Delete {@link NurseryPlanningSlotDto} targeted by id give in parameter
	 * @param id
	 * @return boolean which represent deletion status
	 */
	public Boolean deleteNurseryPlanningSlot(Integer id);
	
	/**
	 * Get {@link NurseryPlanningSlotDto} with {@link NurseryPlanning} identifier
	 * @param id
	 * @return List<NurseryPlanningSlotDto>
	 */
	public List<NurseryPlanningSlotDto> getNurseryPlanningSlotByNurseryPlanningId(Integer id);


}


package com.techconsulting.ws.service.core.parser.reservation;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.reservation.Reservation;
import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;
import com.techconsulting.ws.service.core.parser.UserParser;

/**
 * Parser for a reservation
 * 
 * @author Paul
 *
 */
public class ReservationParser {

	/**
	 * Constructor of ReservationParser
	 */
	private ReservationParser() {
		super();
	}

	/**
	 * Convert a {@link Reservation} entity to a {@link ReservationDto}
	 * 
	 * @param {@link Reservation} to set
	 * @return {@link ReservationDto} object
	 */
	public static ReservationDto convertToDto(Reservation reservEntity) {
		ReservationDto reservDTO;
		if (reservEntity != null) {
			reservDTO = new ReservationDto();
			reservDTO.setReservationId(reservEntity.getReservationId());
			reservDTO.setNbPersons(reservEntity.getNbPersons());
			reservDTO.setReservCancelopt(reservEntity.getReservCancelopt());
			reservDTO.setEquipmentCancelOpt(reservEntity.getEquipmentCancelOpt());
			reservDTO.setInsuranceLossMaterial(reservEntity.getInsuranceLossMaterial());
			reservDTO.setHouseCancelOpt(reservEntity.getHouseCancelOpt());
			reservDTO.setUser(UserParser.convertToDto(reservEntity.getUser()));
			return reservDTO;
		}
		return new ReservationDto();
	}

	/**
	 * Convert a {@link ReservationDto} to a {@link Reservation} entity
	 * 
	 * @param {@link ReservationDto} to set
	 * @return {@link Reservation}
	 */
	public static Reservation convertToEntity(ReservationDto reservDto) {
		Reservation reserv;
		if (reservDto != null) {
			reserv = new Reservation();
			reserv.setReservationId(reservDto.getReservationId());
			reserv.setNbPersons(reservDto.getNbPersons());
			reserv.setReservCancelopt(reservDto.getReservCancelopt());
			reserv.setEquipmentCancelOpt(reservDto.getEquipmentCancelOpt());
			reserv.setInsuranceLossMaterial(reservDto.getInsuranceLossMaterial());
			reserv.setHouseCancelOpt(reservDto.getHouseCancelOpt());
			reserv.setUser(UserParser.convertToEntity(reservDto.getUser()));
			return reserv;
		}
		return new Reservation();
	}

	/**
	 * Convert a list of {@link Reservation} to a list of {@link ReservationDto}
	 * 
	 * @param listReservation to set
	 * @return listReservDto
	 */
	public static List<ReservationDto> convertListToDTO(List<Reservation> listReservation) {
		List<ReservationDto> listReservDto = listReservation.stream().map(x -> convertToDto(x))
				.collect(Collectors.toList());
		return listReservDto;
	}

	/**
	 * Convert a list of {@link ReservationDto} to a list of {@link Reservation}
	 * 
	 * @param listReservDto to set
	 * @return listReservation
	 */
	public static List<Reservation> convertListToEntity(List<ReservationDto> listReservDto) {
		List<Reservation> listReservation = listReservDto.stream().map(x -> convertToEntity(x))
				.collect(Collectors.toList());
		return listReservation;
	}
}

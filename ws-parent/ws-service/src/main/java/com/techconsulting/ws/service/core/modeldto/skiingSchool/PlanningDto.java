package com.techconsulting.ws.service.core.modeldto.skiingSchool;

import java.sql.Timestamp;
import java.util.List;

import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;

public class PlanningDto {

	private Integer PlanningId;
	
	
	private UserDto planningCreator;
	

	private List<LessonDto>lessons;
	
	private SeasonDto planningSeason;
	private UserDto planningMonitor;
	
	private String planningName;
	
	private Timestamp planningBeginning;
	
	private Timestamp planningEnd;

	public Integer getPlanningId() {
		return PlanningId;
	}

	public void setPlanningId(Integer planningId) {
		PlanningId = planningId;
	}

	public UserDto getPlanningCreator() {
		return planningCreator;
	}

	public void setPlanningCreator(UserDto userId) {
		this.planningCreator = userId;
	}

	public UserDto getPlanningMonitor() {
		return planningMonitor;
	}

	public void setPlanningMonitor(UserDto useUserId) {
		this.planningMonitor = useUserId;
	}

	public String getPlanningName() {
		return planningName;
	}

	public void setPlanningName(String planningName) {
		this.planningName = planningName;
	}

	public Timestamp getPlanningBeginning() {
		return planningBeginning;
	}

	public void setPlanningBeginning(Timestamp planningBeginning) {
		this.planningBeginning = planningBeginning;
	}

	public Timestamp getPlanningEnd() {
		return planningEnd;
	}

	public void setPlanningEnd(Timestamp planningEnd) {
		this.planningEnd = planningEnd;
	}

	public List<LessonDto> getLessons() {
		return lessons;
	}

	public void setLessons(List<LessonDto> lessons) {
		this.lessons = lessons;
	}

	public SeasonDto getPlanningSeason() {
		return planningSeason;
	}

	public void setPlanningSeason(SeasonDto planningSeason) {
		this.planningSeason = planningSeason;
	}
	
	
	
	
	
	
}

package com.techconsulting.ws.service.core.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.repository.auth.UserRepository;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.parser.UserParser;

/**
 * Service for manage user
 * 
 * @author Jeremy
 *
 */
@Service
public class UserService implements IUserService {

	/**
	 * {@link UserRepository}
	 */
	@Autowired
	UserRepository repository;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) {
		User user = repository.findByUsername(username);
		if (user != null) {
			return user;
		} else {
			throw new UsernameNotFoundException("user " + username + "does not exist");
		}
	}

	@Override
	@Transactional(readOnly = true)
	public UserDto findUserByUsername(String username) {
		User user = repository.findByUsername(username);
		if (user != null) {
			return UserParser.convertToDto(user);
		} else {
			return null;
		}
	}

	@Override
	public List<UserDto> findAllUsers() {
		return UserParser.convertListToDTO(repository.findAll());
	}

	@Override
	public List<UserDto> findAllByAuthority(String authorityName) {
		return UserParser.convertListToDTO(repository.findByRoleAuthorityName(authorityName));
	}

	@Override
	@Transactional(readOnly = true)
	public UserDto findById(Integer id) {
		return UserParser.convertToDto(repository.findOne(id));
	}
	
}

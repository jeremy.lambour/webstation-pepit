package com.techconsulting.ws.service.core.parser;

import java.util.List;
import java.util.stream.Collectors;

import com.techconsulting.ws.dao.entity.flatManagement.flat.DefineEntity;
import com.techconsulting.ws.dao.entity.flatManagement.flat.DefineIdEntity;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.DefineDto;

/**
 * Parser for {@link DefineEntity} and {@link DefineDto}
 *
 */
public class DefineParser {
	
	private DefineParser() {
		super();
	}
	
	/**
	 * Convert a {@link DefineEntity} entity to a {@link DefineDto}
	 * 
	 * @param defineEntity
	 *            to set
	 * @return defineDto object
	 */
	public static DefineDto convertToDto(DefineEntity defineEntity) {
		DefineDto defineDto;
		if (defineEntity != null) {
			defineDto = new DefineDto();
			defineDto.setFlatPricePerNight(defineEntity.getFlatPricePerNight());
			defineDto.setFestivePeriodDto(FestivePeriodParser.convertToDto(defineEntity.getDefineIdEntity().getFestivePeriodEntity()));
			defineDto.setFlatDto(FlatParser.convertToDto(defineEntity.getDefineIdEntity().getFlat()));
			return defineDto;
		}
		return new DefineDto();
	}

	/**
	 * Convert an {@link DefineDto} to an {@link DefineEntity} entity
	 * 
	 * @param defineDto
	 *            to set
	 * @return defineEntity
	 */
	public static DefineEntity convertToEntity(DefineDto defineDto) {
		DefineEntity defineEntity;
		if (defineDto != null) {
			defineEntity = new DefineEntity();
			DefineIdEntity defineIdEntity = new DefineIdEntity();
			defineIdEntity.setFestivePeriodEntity(FestivePeriodParser.convertToEntity(defineDto.getFestivePeriodDto()));
			defineIdEntity.setFlat(FlatParser.convertToEntity(defineDto.getFlatDto()));
			defineEntity.setDefineIdEntity(defineIdEntity);
			return defineEntity;
		}
		return new DefineEntity();
	}

	/**
	 * Convert a list of {@link DefineEntity} entity to a list of {@link DefineDto}
	 * 
	 * @param listDefineEntity
	 *            to set
	 * @return listDefineDto
	 */
	public static List<DefineDto> convertListToDTO(List<DefineEntity> listDefineEntity) {
		return listDefineEntity.stream().map(x -> convertToDto(x)).collect(Collectors.toList());
	}

	/**
	 * Convert a list of {@link DefineDto} to a list of {@link DefineEntity} entity
	 * 
	 * @param listDefineDto
	 *            to set
	 * @return listDefineEntity
	 */
	public static List<DefineEntity> convertListToEntity(List<DefineDto> listDefineDto) {
		return listDefineDto.stream().map(x -> convertToEntity(x)).collect(Collectors.toList());
	}

}

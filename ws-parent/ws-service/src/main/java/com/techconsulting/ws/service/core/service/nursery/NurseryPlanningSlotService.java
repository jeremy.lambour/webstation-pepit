package com.techconsulting.ws.service.core.service.nursery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.nursery.NurseryPlanningSlot;
import com.techconsulting.ws.dao.repository.nursery.NurseryPlanningSlotRepository;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningSlotDto;
import com.techconsulting.ws.service.core.parser.nursery.NurseryPlanningParser;
import com.techconsulting.ws.service.core.parser.nursery.NurseryPlanningSlotParser;

@Service
@Transactional(readOnly = true,noRollbackFor=Exception.class)
public class NurseryPlanningSlotService implements INurseryPlanningSlotService{

	@Autowired
	private NurseryPlanningSlotRepository repository;
	
	@Autowired
	private INurseryPlanningService service;
	
	@Override
	@Transactional(readOnly=false)
	public NurseryPlanningSlotDto createNurseryPlanningSlot(NurseryPlanningSlotDto dto) {
		NurseryPlanningSlot entity = NurseryPlanningSlotParser.convertToEntity(dto);
		entity.setNurseryPlanning(NurseryPlanningParser.convertToEntity(service.getNurseryPlanningById(dto.getNurseryPlanning())));
		return NurseryPlanningSlotParser.convertToDto(repository.save(entity));
	}

	@Override
	@Transactional(readOnly=false)

	public NurseryPlanningSlotDto updateNurseryPlanningSlot(NurseryPlanningSlotDto dto) {
		NurseryPlanningSlot entity = NurseryPlanningSlotParser.convertToEntity(dto);
		entity.setNurseryPlanning(NurseryPlanningParser.convertToEntity(service.getNurseryPlanningById(dto.getNurseryPlanning())));
		return NurseryPlanningSlotParser.convertToDto(repository.save(entity));	}

	@Override
	@Transactional(readOnly=false)

	public Boolean deleteNurseryPlanningSlot(Integer id) {
		repository.delete(id);
		return (repository.findOne(id) == null ? true : false);
	}

	@Override
	public List<NurseryPlanningSlotDto> getNurseryPlanningSlotByNurseryPlanningId(Integer id) {
	
		return NurseryPlanningSlotParser.convertListToDTO(repository.findByNurseryPlanningNurseryPlanningId(id));
	}

}

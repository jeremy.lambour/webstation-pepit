package com.techconsulting.ws.service.core.modeldto.equipment;

import com.techconsulting.ws.service.core.modeldto.common.PassTypeDto;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;

public class SkiLiftDto {
	
	/**
	 * SkiLift identifier
	 */
	private Integer skiLiftId;
	
	/**
	 * Reservation's duration
	 */
	private String duration;
	
	/**
	 * Domain's name
	 */
	private String domain;
	
	/**
	 * Reservation's price
	 */
	private float price;
	
	/**
	 * Pass's type
	 */
	private PassTypeDto passType;
	
	/**
	 * Reservation's ageRange
	 */
	private AgeRangeDto ageRange;

	/**
	 * Generic constructor
	 */
	public SkiLiftDto() {
		super();
	}

	/**
	 * Getter of the identifier of ski lift
	 * @return {@link Integer}
	 */
	public Integer getSkiLiftId() {
		return skiLiftId;
	}

	/**
	 * Setter of the identifier of ski lift
	 * @param {@link Integer}
	 */
	public void setSkiLiftId(Integer skiLiftId) {
		this.skiLiftId = skiLiftId;
	}

	/**
	 * Getter of property duration
	 * @return {@link String}
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * Setter of property duration
	 * @param {@link String}
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * Getter of the property domain
	 * @return {@link String}
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * Setter of the property domain
	 * @param {@link String}
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * Getter of the property price reservation
	 * @return {@link float}
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * Setter of the property price
	 * @param {@link float}
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * Getter of the pass type
	 * @return {@link PassTypeDto}
	 */
	public PassTypeDto getPassType() {
		return passType;
	}

	/**
	 * Setter of the pass type
	 * @param {@link PassTypeDto}
	 */
	public void setPassType(PassTypeDto passType) {
		this.passType = passType;
	}

	/**
	 * Getter of the age range
	 * @return {@link AgeRangeDto}
	 */
	public AgeRangeDto getAgeRange() {
		return ageRange;
	}

	/**
	 * Setter of the age range
	 * @param {@link AgeRangeDto}
	 */
	public void setAgeRange(AgeRangeDto ageRange) {
		this.ageRange = ageRange;
	}
	
	
}

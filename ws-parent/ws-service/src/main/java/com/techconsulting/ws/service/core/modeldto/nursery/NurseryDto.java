package com.techconsulting.ws.service.core.modeldto.nursery;

public class NurseryDto {

	private Integer nurseryId;

	private String nurseryName;

	private String nurseryPhoneNumber;

	public Integer getNurseryId() {
		return nurseryId;
	}

	public String getNurseryName() {
		return nurseryName;
	}

	public String getNurseryPhoneNumber() {
		return nurseryPhoneNumber;
	}

	public void setNurseryId(Integer nurseryId) {
		this.nurseryId = nurseryId;
	}

	public void setNurseryName(String nurseryName) {
		this.nurseryName = nurseryName;
	}

	public void setNurseryPhoneNumber(String nurseryPhoneNumber) {
		this.nurseryPhoneNumber = nurseryPhoneNumber;
	}
	
	
}

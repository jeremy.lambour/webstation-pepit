package com.techconsulting.ws.service.core.modeldto.flatManagement.flat;

import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;

/**
 * Dto object for flat
 * @author gaillardc
 *
 */
public class FlatDto {
	
	/**
	 * The flat's identifier
	 */
	private Integer flatId;

	/**
	 * The flat's description
	 */
	private String flatDescription;

	/**
	 * The flat's guidance
	 */
	private String flatGuidance;

	/**
	 * The flat's capacity
	 */
	private Integer flatCapacity;
	
	/**
	 * The {@link HotelDto}
	 */
	private HotelDto hotelDto;
	
	/**
	 * The {@link FlatCategoryDto}
	 */
	private FlatCategoryDto flatCategoryDto;
	
	/**
	 * FlatDTO's generic constructor
	 */
	public FlatDto() {
		super();
	}

	/**
	 * @return the flatId
	 */
	public Integer getFlatId() {
		return flatId;
	}

	/**
	 * @param flatId the flatId to set
	 */
	public void setFlatId(Integer flatId) {
		this.flatId = flatId;
	}

	/**
	 * @return the flatDescription
	 */
	public String getFlatDescription() {
		return flatDescription;
	}

	/**
	 * @param flatDescription the flatDescription to set
	 */
	public void setFlatDescription(String flatDescription) {
		this.flatDescription = flatDescription;
	}

	/**
	 * @return the flatGuidance
	 */
	public String getFlatGuidance() {
		return flatGuidance;
	}

	/**
	 * @param flatGuidance the flatGuidance to set
	 */
	public void setFlatGuidance(String flatGuidance) {
		this.flatGuidance = flatGuidance;
	}

	/**
	 * @return the flatCapacity
	 */
	public Integer getFlatCapacity() {
		return flatCapacity;
	}

	/**
	 * @param flatCapacity the flatCapacity to set
	 */
	public void setFlatCapacity(Integer flatCapacity) {
		this.flatCapacity = flatCapacity;
	}

	/**
	 * @return the hotelDto
	 */
	public HotelDto getHotelDto() {
		return hotelDto;
	}

	/**
	 * @param hotelDto the hotelDto to set
	 */
	public void setHotelDto(HotelDto hotelDto) {
		this.hotelDto = hotelDto;
	}
	
	/**
	 * @return the flatCategoryDto
	 */
	public FlatCategoryDto getFlatCategoryDto() {
		return flatCategoryDto;
	}

	/**
	 * @param flatCategoryDto the flatCategoryDto to set
	 */
	public void setFlatCategoryDto(FlatCategoryDto flatCategoryDto) {
		this.flatCategoryDto = flatCategoryDto;
	}
	
	

}

package com.techconsulting.ws.service.core.service.reservation.equipment;

import java.sql.Timestamp;
import java.util.List;

import com.techconsulting.ws.dao.entity.reservation.equipment.ReservationEquipment;
import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;

/**
 * The Interface of {@link ReservationEquipment}
 */
public interface IEquipmentReservationService {

	public ReservationEquipmentDto insertReservEquip(ReservationEquipmentDto reservEquip);
	
	public List<ReservationEquipmentDto> getListReservEquipUser(int idUser);

	public boolean checkAvailability(ReservationEquipmentDto reservEquip);

	public boolean deleteEquipmentReservation(ReservationEquipmentDto reservEquip);
	
	List<ReservationEquipmentDto> getListReservEquipDate(Timestamp dateStart);
	
	List<ReservationEquipmentDto> findReservationEquipmentByEquipmentDtoShopDtoIdShop(Integer idShop);
	
	ReservationEquipmentDto findByReservationDtoReservationId(Integer idReserv);
	ReservationEquipmentDto  findByReservationDtoReservationIdAndEquipmentDtoEquipmentId(Integer idReserv, Integer idEquipment);
	void setReservationEquipmentIsGivenByReservationEquipment(Boolean isGiven, ReservationEquipmentDto reservationEquipmentDto);
}

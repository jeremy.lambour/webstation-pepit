package com.techconsulting.ws.service.core.modeldto.user;

/**
 * Dto object for ageRange
 * 
 * @author Jeremy
 *
 */
public class AgeRangeDto {

	/**
	 * age range identifier
	 */
	private Integer ageRangeId;

	/**
	 * age range name
	 */
	private String ageRangeName;

	/**
	 * age range dto constructor
	 */
	public AgeRangeDto() {
		super();
	}

	/**
	 * getter for property ageRangeId
	 * 
	 * @return integer represent age range identifier
	 */
	public Integer getAgeRangeId() {
		return ageRangeId;
	}

	/**
	 * setter for property ageRangeId
	 * 
	 * @param ageRangeId
	 */
	public void setAgeRangeId(Integer ageRangeId) {
		this.ageRangeId = ageRangeId;
	}

	/**
	 * getter for property ageRangeName
	 * 
	 * @return string which represent ageRangeName
	 */
	public String getAgeRangeName() {
		return ageRangeName;
	}

	/**
	 * setter for property ageRangeName
	 * 
	 * @param ageRangeName
	 */
	public void setAgeRangeName(String ageRangeName) {
		this.ageRangeName = ageRangeName;
	}
}

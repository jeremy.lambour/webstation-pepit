package com.techconsulting.ws.service.core.service.skiingSchool;

import java.util.List;

import com.techconsulting.ws.dao.entity.skiingSchool.Planning;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.PlanningDto;

/**
 * {@link PlanningService} interface
 * @author ASUS
 *
 */
public interface IPlanningService {

	/**
	 * Create a new {@link Planning}
	 * @param planning
	 * @return created {@link PlanningDto}
	 */
	public PlanningDto createPlanning(PlanningDto planning);
	
	/**
	 * Update an existing {@link Planning}
	 * @param planning
	 * @return Updated {@link Planning}
	 */
	public PlanningDto updatePlanning(PlanningDto planning);
	
	/**
	 * delete an existing {@link Planning}
	 * @param planningId
	 * @return boolean which correspond to deletion status
	 */
	public Boolean deleteplanning(Integer planningId);
	
	/**
	 * Get {@link Planning} by monitor and season Identifier
	 * @param monitorId
	 * @param seasonId
	 * @return List<PlanningDto>
	 */
	public List<PlanningDto> getAllPlanningByMonitorAnSeasonId(Integer monitorId,Integer seasonId);
	
	/**
	 * Get {@link Planning} by his identifier
	 * @param id
	 * @return {@link PlanningDto}
	 */
	public PlanningDto getPlanningById(Integer id);
	
}

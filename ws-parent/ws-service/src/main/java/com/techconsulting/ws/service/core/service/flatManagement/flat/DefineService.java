package com.techconsulting.ws.service.core.service.flatManagement.flat;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.common.SeasonEntity;
import com.techconsulting.ws.dao.entity.flatManagement.flat.DefineEntity;
import com.techconsulting.ws.dao.repository.flatManagement.flat.DefineRepository;
import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.DefineDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.parser.DefineParser;
import com.techconsulting.ws.service.core.parser.FestivePeriodParser;
import com.techconsulting.ws.service.core.parser.FlatParser;

/**
 * Service for define
 * @author gaillardc
 *
 */
@Service
@Transactional(readOnly = false)
public class DefineService implements IDefineService{
	
	/**
	 * {@link DefineRepository}
	 */
	@Autowired
	DefineRepository defineRepository;
	
	
	/**
	 * Method that finds a {@link DefineDTO} with a specific flat and a
	 * specific festivePeriod to get the price
	 * 
	 * @Param - {@link FestivePeriodDto} festivePeriodDto, we want to find the {@link DefineDTO}
	 *        with this {@link FestivePeriodDto}
	 * @Param - {@link FlatDto} flatDto, we want to find the {@link DefineDTO} with this
	 *        flatDto
	 * @Return - the {@link DefineDTO}
	 */
	@Override
	public DefineDto findByFestivePeriodDtoAndFlatDto(FestivePeriodDto festivePeriodDto, FlatDto flatDto) {
		return DefineParser.convertToDto(defineRepository.findByDefineIdEntityFestivePeriodEntityAndDefineIdEntityFlat(FestivePeriodParser.convertToEntity(festivePeriodDto), FlatParser.convertToEntity(flatDto)));
	}
	
	@Override
	public List<DefineDto> findDefineDtoByFlatDtoFlatId(Integer idFlat) {
		List<DefineEntity> listDefineEntity = defineRepository.findDefineEntityByDefineIdEntityFlatFlatId(idFlat);
		return DefineParser.convertListToDTO(listDefineEntity);
	}
	
	@Override
	public void setDefineFlatPricePerNightByDefine(DefineDto newDefineDto, double flatPricePerNight) {
		DefineEntity defineEntity = defineRepository.findByDefineIdEntityFestivePeriodEntityAndDefineIdEntityFlat(FestivePeriodParser.convertToEntity(newDefineDto.getFestivePeriodDto()), FlatParser.convertToEntity(newDefineDto.getFlatDto()));
		defineEntity.setFlatPricePerNight(flatPricePerNight);
		defineRepository.save(defineEntity);
	}
}

package com.techconsulting.ws.service.core.service.common;

import java.util.Date;
import java.util.List;

import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;

/**
 * The Interface of {@link SeasonDto}
 */
public interface ISeasonService {
	SeasonDto findSeasonById(Integer idSeason);
	void setSeasonActiveBySeason(SeasonDto seasonDto, Boolean active);
	void setSeasonSeasonBeginningBySeason(SeasonDto newSeasonDto, Date seasonBeginning);
	void setSeasonSeasonEndBySeason(SeasonDto newSeasonDto, Date seasonEnd);
	public List<SeasonDto> getAllSeason();

}

package com.techconsulting.ws.service.core.service.equipment.skilift;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;

public interface ISkiLift {
	
	public List<SkiLiftDto> getListSkiLift();
	List<SkiLiftDto> findSkiLiftDtoByAgeRangeAgeRangeIdAndDomainAndPassTypePassTypeId(int ageRangeIdInt, String domain, int passTypeIdInt);
	List<SkiLiftDto> findSkiLiftDtoByAgeRangeAgeRangeIdAndPassTypePassTypeId(int ageRangeIdInt, int passTypeIdInt);
	void setSkiLiftDtoPriceBySkiLift(SkiLiftDto newSkiLiftDto, float price);

}

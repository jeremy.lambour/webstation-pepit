package com.techconsulting.ws.service.core.service.accountcreation;

import java.util.List;

import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;

/**
 * AgeRange service interface
 * @author p_lem
 *
 */
public interface IAgeRangeService {
	/**
	 * find age range with his id
	 * 
	 * @param id of the ageRange
	 * @return an {@link AgeRangeDto}
	 */
	 AgeRangeDto findAgeRange(int id);
	 
	 List<AgeRangeDto> findAll();
}

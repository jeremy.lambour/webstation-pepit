package com.techconsulting.ws.service.core.service.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 * Class for mail service
 * 
 * @author paullemoine
 *
 */
@Component
@ComponentScan("com.techconsulting.ws.service")
@PropertySource("classpath:mail.properties")
public class MailService implements IMailService {

	private final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

	@Autowired
	Environment env;

	@Autowired
	public JavaMailSender emailSender;

	/**
	 * Method to send a simple message
	 * 
	 * @param - String to
	 * @param - String subject : subject of the email
	 * @param - String text : text of the email
	 * 
	 * @return {@link boolean}
	 */
	public boolean sendSimpleEmail(String to, String subject, String text) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(subject);
		message.setText(text);
		emailSender.send(message);
		return true;
	}

	/**
	 * Method to send an complex email with a template
	 * 
	 * @param - String to : person who receive the email
	 * @param - int subjectId : the id of the email's template
	 * @param - int stateId : the state of the email's template
	 * @param - int objectId : the object of the email's template
	 * @param - boolean isFooter : describe if there is a footer or not
	 * @param - String Footer : the footer the email's template
	 * 
	 * @return {@link boolean}
	 */
	public boolean sendEmail(String to, int subjectId, int stateId, int objectId, boolean isFooter, String footer) {
		try {
			emailSender.send(templateMail(to, subjectId, stateId, objectId, isFooter, footer));
		} catch (MailException | MessagingException mE) {
			LOGGER.debug(mE.getMessage());
		}
		return true;
	}

	/**
	 * Method to generate a template email
	 * 
	 * @param - String to : person who receive the email
	 * @param - int subjectId : the id of the email's template
	 * @param - int stateId : the state of the email's template
	 * @param - int objectId : the object of the email's template
	 * @param - boolean isFooter : describe if there is a footer or not
	 * @param - String Footer : the footer the email's template
	 * 
	 */
	public MimeMessage templateMail(String to, int subjectId, int stateId, int objectId, boolean isFooter,
			String footer) throws MessagingException {
		String subjectEnd = env.getProperty("mail.subject.end");
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper;

		helper = new MimeMessageHelper(message, true);
		helper.setTo(to);

		StringBuilder subject = new StringBuilder();
		subject.append(env.getProperty("mail.subject.start"));
		switch (subjectId) {
		case 0:
			subject.append(env.getProperty("mail.subject.start.1"));
			subject.append(subjectEnd);
			break;
		case 1:
			subject.append(env.getProperty("mail.subject.start.2"));
			subject.append(subjectEnd);
			break;
		case 2:
			subject.append(env.getProperty("mail.subject.start.3"));
			subject.append(subjectEnd);
			break;
		default:
			subject.append("");
			break;
		}
		helper.setSubject(subject.toString());
		// @formatter:off
		StringBuilder content = new StringBuilder();
		content.append(env.getProperty("mail.head"));
		content.append(env.getProperty("mail.content"));
		switch (stateId) {
		case 0:
			content.append(env.getProperty("mail.state.1"));
			break;
		case 1:
			content.append(env.getProperty("mail.state.2"));
			break;
		case 2:
			content.append(env.getProperty("mail.state.3"));
			break;
		default:
			content.append("");
			break;
		}
		content.append(env.getProperty("mail.content.2"));
		switch (objectId) {
		case 0:
			content.append(env.getProperty("mail.object.1"));
			break;
		case 1:
			content.append(env.getProperty("mail.object.2"));
			break;
		case 2:
			content.append(env.getProperty("mail.object.3"));
			break;
		case 3:
			content.append(env.getProperty("mail.object.4"));
			break;
		case 4:
			content.append(env.getProperty("mail.object.5"));
			break;
		default:
			content.append("");
		}
		content.append(env.getProperty("mail.content.3"));
		if (isFooter) {
			content.append(env.getProperty("mail.footer"));
			content.append(footer);
		}
		// @formatter:on
		content.append(env.getProperty("mail.signature"));
		helper.setText(content.toString());

		return message;
	}
}
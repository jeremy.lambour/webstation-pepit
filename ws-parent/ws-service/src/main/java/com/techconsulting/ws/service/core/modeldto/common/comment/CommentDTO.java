package com.techconsulting.ws.service.core.modeldto.common.comment;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;

public class CommentDTO {
	/**
	 * The comment's identifier
	 */
	private Integer commentId;
	/**
	 * The comment's content
	 */
	private String content;
	/**
	 * The user who send the comment
	 */
	private UserDto from;
	/**
	 * The comment's note
	 */
	private int note;
	
	/**
	 * CommentDto's constructor
	 */
	public CommentDTO() {
		super();
	}

	/**
	 * Getter for property commentId
	 * 
	 * @return {@link Integer}
	 */
	public Integer getCommentId() {
		return commentId;
	}

	/**
	 * Setter for property commentId
	 * 
	 * @param commentId
	 */
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	/**
	 * Getter for property content
	 * 
	 * @return {@link String}
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Setter for property String content
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Getter for property from
	 * 
	 * @return {@link UserDto}
	 */
	public UserDto getFrom() {
		return from;
	}
	
	/**
	 * Setter for property from
	 * 
	 * @param {@link UserDto}
	 */
	public void setFrom(UserDto from) {
		this.from = from;
	}

	/**
	 * Getter for property note
	 * 
	 * @return {@link int}
	 */
	public int getNote() {
		return note;
	}

	/**
	 * Setter for property note
	 * 
	 * @param {@link int}
	 */
	public void setNote(int note) {
		this.note = note;
	}
}

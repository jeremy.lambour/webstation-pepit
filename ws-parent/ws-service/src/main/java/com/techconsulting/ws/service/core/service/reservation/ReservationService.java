package com.techconsulting.ws.service.core.service.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techconsulting.ws.dao.entity.reservation.Reservation;
import com.techconsulting.ws.dao.repository.reservation.ReservationRepository;
import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;
import com.techconsulting.ws.service.core.parser.reservation.ReservationParser;

/**
 * Service for manage Reservation
 * @author Paul
 *
 */
@Service
@Transactional(readOnly = false)
public class ReservationService implements IReservation{
	
	@Autowired
	ReservationRepository reservationRepo;
	
	/**
	 * Method that inserts an {@link Reservation} in DataBase
	 * 
	 * @Param - {@link ReservationDto} reservDTO, the ask we want to insert
	 * @Return - the {@link ReservationDto} inserted
	 */
	@Override
	public ReservationDto insertReservation(ReservationDto reservDTO) {
		Reservation reserv = ReservationParser.convertToEntity(reservDTO);
		return ReservationParser.convertToDto(reservationRepo.save(reserv));
	}
	
	@Override
	public boolean deleteReservation(int idReserv) {
		reservationRepo.delete(idReserv);
		return true;
	}
}

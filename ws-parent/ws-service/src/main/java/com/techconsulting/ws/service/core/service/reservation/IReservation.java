package com.techconsulting.ws.service.core.service.reservation;

import com.techconsulting.ws.service.core.modeldto.reservation.ReservationDto;

public interface IReservation {

	public ReservationDto insertReservation(ReservationDto reservDTO);

	public boolean deleteReservation(int idReserv);
}

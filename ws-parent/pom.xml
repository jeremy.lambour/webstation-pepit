<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.techconsulting</groupId>
	<artifactId>ws-parent</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>pom</packaging>

	<name>ws-parent</name>
	<url>http://maven.apache.org</url>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven-compiler-plugin.version}</version>
				<configuration>
					<source>${jdk.version}</source>
					<target>${jdk.version}</target>
					<compilerArgument>-Xlint:all</compilerArgument>
					<showWarnings>true</showWarnings>
					<showDeprecation>true</showDeprecation>
				</configuration>
			</plugin>
              <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M1</version>
                <configuration>
                  <properties>
                    <property>
                      <name>listener</name>
                      <value>org.sonar.java.jacoco.JUnitListener</value>
                    </property>
                  </properties>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonarsource.scanner.maven</groupId>
                <artifactId>sonar-maven-plugin</artifactId>
                <version>${maven-sonar-plugin}</version>
            </plugin>
	                    <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.1</version>
                <configuration>
                    <destFile>${sonar.jacoco.reportPaths}</destFile>
                    <append>true</append>
                </configuration>
                <executions>
                    <execution>
                        <id>agent</id>
                        <goals>
                          <goal>prepare-agent</goal>
                        </goals>
                      </execution>
                </executions>
            </plugin>
	        
		</plugins>
	</build>
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<logback.version>1.2.2</logback.version>
		<sl4j.version>1.7.25</sl4j.version>
		<mockito.version>2.0.2-beta</mockito.version>
		<spring-framework.version>4.3.15.RELEASE</spring-framework.version>
		<jdk.version>1.8</jdk.version>
		<maven-war-plugin.version>3.0.0</maven-war-plugin.version>
		<maven-compiler-plugin.version>3.3</maven-compiler-plugin.version>
		<javax-servlet.version>3.1.0</javax-servlet.version>
		<spring-security.version>4.2.0.RELEASE</spring-security.version>
		<spring-security-oauth2.version>2.3.4.RELEASE</spring-security-oauth2.version>
		<spring-security-jwt.version>1.0.9.RELEASE</spring-security-jwt.version>
		<main.basedir>${project.basedir}/../</main.basedir>
		<maven-sonar-plugin>3.5.0.1254</maven-sonar-plugin>
    	<sonar.dynamicAnalysis>reuseReports</sonar.dynamicAnalysis>
		<jacoco-maven-plugin.version>0.8.2</jacoco-maven-plugin.version>
		<sonar.sourceEncoding>UTF-8</sonar.sourceEncoding>
		  
		 <!-- sonar JACOCO properties -->
        <sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
        <sonar.dynamicAnalysis>reuseReports</sonar.dynamicAnalysis>
        <sonar.jacoco.reportPaths>${project.reporting.outputDirectory}/jacoco-it.exec</sonar.jacoco.reportPaths>
        <sonar.language>java</sonar.language>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>com.techconsulting</groupId>
				<artifactId>ws-dao</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>com.techconsulting</groupId>
				<artifactId>ws-service</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.data</groupId>
				<artifactId>spring-data-releasetrain</artifactId>
				<version>Ingalls-SR11</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

		</dependencies>
	</dependencyManagement>
	<repositories>
		<repository>
			<id>spring-libs-release</id>
			<name>Spring Releases</name>
			<url>https://repo.spring.io/libs-release</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>

	<dependencies>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>${logback.version}</version>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${sl4j.version}</version>
		</dependency>

		<!-- TEST -->
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>${mockito.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/junit/junit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat</artifactId>
			<version>9.0.6</version>
			<type>pom</type>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.projectlombok/lombok-maven -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok-maven</artifactId>
			<version>1.16.20.0</version>
			<type>pom</type>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-entitymanager</artifactId>
			<version>5.3.6.Final</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-config -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${spring-security.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-web -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>${spring-security.version}</version>
		</dependency>
		
		<dependency>
            <groupId>org.jacoco</groupId>
            <artifactId>org.jacoco.agent</artifactId>
            <version>0.8.1</version>
            <classifier>runtime</classifier>
        </dependency>
        
        <dependency>
          <groupId>org.codehaus.sonar-plugins.java</groupId>
          <artifactId>sonar-jacoco-listeners</artifactId>
          <version>1.2</version>
          <scope>test</scope>
        </dependency>
	</dependencies>
	<modules>
		<module>ws-rest</module>
		<module>ws-service</module>
		<module>ws-dao</module>
	</modules>

</project>

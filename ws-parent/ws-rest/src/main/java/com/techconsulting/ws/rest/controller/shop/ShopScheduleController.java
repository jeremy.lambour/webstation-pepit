package com.techconsulting.ws.rest.controller.shop;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopScheduleDto;
import com.techconsulting.ws.service.core.service.common.IScheduleService;
import com.techconsulting.ws.service.core.service.shop.IShopScheduleService;
import com.techconsulting.ws.service.core.service.shop.IShopService;

/**
 * The Controller of the {@link ShopSchedule} class
 */
@RestController
@RequestMapping(value = "/api/ShopSchedule")
public class ShopScheduleController {

	static final Logger LOGGER = LoggerFactory.getLogger(ShopScheduleController.class);

	@Autowired
	IShopScheduleService iShopSchedule;

	@Autowired
	IShopService iShop;

	@Autowired
	IScheduleService iSchedule;

	/**
	 * Method that inserts a {@link ShopScheduleDto} for a specific {@link Schedule}
	 * and a specific {@link Shop}
	 * 
	 * @param scheduleMondayId, the {@link ScheduleDto}'s id
	 * @param shopId, the {@link ShopDto}'s id
	 * @return the shopScheduleDto inserted
	 */
	@RequestMapping(value = "/insertShopSchedule/{scheduleMondayId}/{shopId}", method = RequestMethod.GET)
	public ResponseEntity<ShopScheduleDto> insertShopSchedule(@PathVariable int scheduleMondayId,
			@PathVariable int shopId) {
		ShopScheduleDto shopScheduleDto = new ShopScheduleDto();
		shopScheduleDto.setScheduleDto(iSchedule.findScheduleById(scheduleMondayId));
		shopScheduleDto.setShopDto(iShop.findShopById(shopId));
		iShopSchedule.insertShopSchedule(shopScheduleDto);
		return new ResponseEntity<>(shopScheduleDto, HttpStatus.OK);
	}

	/**
	 * Method that finds a list of {@link ShopScheduleDto} from a specific shop
	 * identifier
	 * 
	 * @param shopId the {@link ShopDto}'s id
	 * @return the list of {@link ShopScheduleDto}
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getShopSchedule", method = RequestMethod.GET)
	public ResponseEntity<List<ScheduleDto>> getShopSchedule(@RequestParam(value = "idShop") Integer idShop) {

		List<ScheduleDto> listScheduleDto = new ArrayList<>();

		List<ShopScheduleDto> listShopScheduleDto = iShopSchedule.findByShopScheduleIdShopEntityShopId(idShop);

		for (int i = 0; i < listShopScheduleDto.size(); i++) {
			listScheduleDto.add(listShopScheduleDto.get(i).getScheduleDto());
		}

		return new ResponseEntity<>(listScheduleDto, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes a {@link ShopScheduleDto} by an identifier
	 * 
	 * @Param - Integer idShop, the shop we want to delete
	 * @return the deleted {@link ShopScheduleDto}
	 */
	@RequestMapping(value = "/deleteShopschedule", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ShopScheduleDto> deleteShopschedule(@RequestBody Integer idShop) {
		ShopScheduleDto shopScheduleDto = new ShopScheduleDto();
		List<ShopScheduleDto> listShopScheduleDto = iShopSchedule.findByShopScheduleIdShopEntityShopId(idShop);
		if(!listShopScheduleDto.isEmpty()) {
		shopScheduleDto = listShopScheduleDto.get(0);}
		iShopSchedule.deleteByShopScheduleIdShopEntityShopId(idShop);

		return new ResponseEntity<>(shopScheduleDto, HttpStatus.OK);
	}

}

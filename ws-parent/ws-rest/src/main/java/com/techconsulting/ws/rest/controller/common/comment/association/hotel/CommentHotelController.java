package com.techconsulting.ws.rest.controller.common.comment.association.hotel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.hotel.CommentHotelDTO;
import com.techconsulting.ws.service.core.service.common.comment.ICommentService;
import com.techconsulting.ws.service.core.service.common.comment.association.hotel.ICommentHotelService;

@RestController
@RequestMapping(value = "/api/CommentHotel")
public class CommentHotelController {
	@Autowired
	ICommentHotelService iCommentHotelServ;
	
	@Autowired
	ICommentService iCommentService;
	
	/**
	 * Method that insert the {@link CommentHotelDTO} in parameters
	 * 
	 * @Param - {@link CommentHotelDTO} commentEquipmentDTO, the commentEquipmentDTO we want to insert
	 * @Return - the {@link CommentHotelDTO} inserted
	 */
	@PostMapping(value = "/")
	public ResponseEntity<CommentHotelDTO> insertCommentHotel(@RequestBody CommentHotelDTO commentHotelDTO) {
		CommentDTO c = iCommentService.insertComment(commentHotelDTO.getCommentDto());
		 commentHotelDTO.setCommentDto(c);
		CommentHotelDTO newCommentHotelDto = iCommentHotelServ.insertCommentHotel(commentHotelDTO);
		return new ResponseEntity<>(newCommentHotelDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a list of {@link CommentHotelDTO}
	 * 
	 * @param idHotel the {@link Hotel}'s id
	 * @return the list of {@link CommentHotelDTO}
	 */
	@GetMapping(value = "/getCommentHotel")
	public ResponseEntity<List<CommentHotelDTO>> getListCommentHotel(@RequestParam(value = "idHotel") Integer idHotel) {
		List<CommentHotelDTO> listCommentHotelDto = iCommentHotelServ.findListCommentHotelByIdHotel(idHotel);
		return new ResponseEntity<>(listCommentHotelDto, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes a {@link CommentHotelDTO} by an identifier
	 * 
	 * @return the check boolean
	 */
	@PostMapping(value = "/deleteCommentHotel")
	public ResponseEntity<Boolean> deleteCommentHotel(@RequestBody CommentHotelDTO commentHotelDto) {
		iCommentHotelServ.deleteByCommentHotel(commentHotelDto);
		return new ResponseEntity<>(true, HttpStatus.OK);
	}
	
}

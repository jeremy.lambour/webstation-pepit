package com.techconsulting.ws.rest.controller.nursery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.nursery.NurseryDto;
import com.techconsulting.ws.service.core.service.nursery.INurseryService;
import com.techconsulting.ws.service.core.service.nursery.NurseryService;

/**
 * {@link NurseryDto} endpoint
 * @author Jeremy
 *
 */
@RestController
@RequestMapping(value = "/api/nursery")
public class NurseryController {

	/**
	 * {@link NurseryService}
	 */
	@Autowired
	private INurseryService service;
	
	/**
	 * Get all {@link NurseryDto}
	 * @return ResponseEntity<List<NurseryDto>>
	 */
	@GetMapping(value="/")
	public ResponseEntity<List<NurseryDto>> getAllNursery(){
		try {
			return new ResponseEntity<List<NurseryDto>>(service.getAllNursery(), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Get a {@link NurseryDto} by his id
	 * @param id
	 * @return ResponseEntity<NurseryDto>
	 */
	@GetMapping(value="/{id}")
	public ResponseEntity<NurseryDto> getNurseryById(@PathVariable Integer id){
		try {
			return new ResponseEntity<NurseryDto>(service.getNurseryById(id), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

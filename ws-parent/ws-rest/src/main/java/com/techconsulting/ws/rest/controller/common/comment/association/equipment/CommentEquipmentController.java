package com.techconsulting.ws.rest.controller.common.comment.association.equipment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.dao.entity.equipment.Equipment;
import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.equipment.CommentEquipmentDTO;
import com.techconsulting.ws.service.core.service.common.comment.ICommentService;
import com.techconsulting.ws.service.core.service.common.comment.association.equipment.ICommentEquipmentService;

@RestController
@RequestMapping(value = "/api/CommentEquipment")
public class CommentEquipmentController {
	
	@Autowired
	ICommentEquipmentService iCommentEquipServ;
	
	@Autowired
	ICommentService iCommentService;
	
	/**
	 * Method that insert the {@link CommentEquipmentDTO} in parameters
	 * 
	 * @Param - {@link CommentEquipmentDTO} commentEquipmentDTO, the commentEquipmentDTO we want to insert
	 * @Return - the {@link CommentEquipmentDTO} inserted
	 */
	@PostMapping(value = "/")
	public ResponseEntity<CommentEquipmentDTO> insertCommentEquipmentDto(@RequestBody CommentEquipmentDTO commentEquipmentDTO) {
		CommentDTO c = iCommentService.insertComment(commentEquipmentDTO.getCommentDto());
		commentEquipmentDTO.setCommentDto(c);
		CommentEquipmentDTO newCommentEquipmentDto = iCommentEquipServ.insertCommentEquipment(commentEquipmentDTO);
		return new ResponseEntity<>(newCommentEquipmentDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a list of {@link CommentEquipmentDTO}
	 * 
	 * @param idEquip the {@link Equipment}'s id
	 * @return the list of {@link CommentEquipmentDTO}
	 */
	@GetMapping(value = "/getCommentEquipment")
	public ResponseEntity<List<CommentEquipmentDTO>> getListCommentEquipmentDTO(@RequestParam(value = "idEquip") Integer idEquip) {
		List<CommentEquipmentDTO> listCommentEquip = iCommentEquipServ.findByCommentEquipmentPkEquipment(idEquip);
		return new ResponseEntity<>(listCommentEquip, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes a {@link CommentEquipmentDTO} by an identifier
	 * 
	 * @return the check boolean
	 */
	@PostMapping(value = "/deleteCommentEquipment")
	public ResponseEntity<Boolean> deleteCommentEquipmentDTO(@RequestBody CommentEquipmentDTO commentEquipDTO) {
		iCommentEquipServ.deleteByCommentEquipment(commentEquipDTO);
		return new ResponseEntity<>(true, HttpStatus.OK);
	}
	 
}

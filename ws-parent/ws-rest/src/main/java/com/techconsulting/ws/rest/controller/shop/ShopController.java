package com.techconsulting.ws.rest.controller.shop;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.service.shop.IShopService;

/**
 * The Controller of the {@link Shop} class
 */
@RestController
@RequestMapping(value = "/api/Shop")
public class ShopController {
	static final Logger LOGGER = LoggerFactory.getLogger(ShopController.class);

	@Autowired
	IShopService iShop;

	/**
	 * Method that insert the {@link ShopDto} in parameters
	 * 
	 * @Param - {@link ShopDto} shop, the shop we want to insert
	 * @Return - the {@link ShopDto} inserted
	 */
	@RequestMapping(value = "/insertShop", method = RequestMethod.POST)
	public ResponseEntity<ShopDto> insertShop(@RequestBody ShopDto shopDTO) {
		ShopDto newShop = iShop.insertShop(shopDTO);
		return new ResponseEntity<>(newShop, HttpStatus.OK);
	}

	/**
	 * Method that updates a {@link ShopDto}
	 * 
	 * @param shop the {@link ShopDto}
	 * @return the update {@link ShopDto}
	 */
	@RequestMapping(value = "/updateShop", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ShopDto> updateShop(@RequestBody ShopDto shopDto) {

		ShopDto newShopDto = iShop.findShopById(shopDto.getShopId());

		iShop.setShopNameByShop(newShopDto, shopDto.getShopName());
		iShop.setShopAddressByShop(newShopDto, shopDto.getShopAddress());
		iShop.setShopPhoneNumberByShop(newShopDto, shopDto.getPhoneNumber());
		if (shopDto.getUserDTO() != null) {
			iShop.setShopUserDtoByShop(newShopDto, shopDto.getUserDTO());
		}

		return new ResponseEntity<>(newShopDto, HttpStatus.OK);
	}

	/**
	 * Method that finds a {@link ShopDto}
	 * 
	 * @param shopId the {@link ShopDto}'s id
	 * @return the {@link ShopDto}
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getShop", method = RequestMethod.GET)
	public ResponseEntity<ShopDto> getShop(@RequestParam(value = "idShop") Integer idShop) {
		ShopDto shopDto = iShop.findShopById(idShop);
		return new ResponseEntity<>(shopDto, HttpStatus.OK);
	}

	/**
	 * Method that deletes a {@link ShopDto} by an identifier
	 * 
	 * @Param - Integer idShop, the shop we want to delete
	 * @return the deleted {@link ShopDto}
	 */
	@RequestMapping(value = "/deleteShop", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ShopDto> deleteShop(@RequestBody Integer idShop) {

		ShopDto shopDto = iShop.findShopById(idShop);
		iShop.deleteByShopId(shopDto.getShopId());

		return new ResponseEntity<>(shopDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds all the {@link ShopDto}
	 * 
	 * @return the list of {@link ShopDto}
	 */
	@RequestMapping(value = "/getListShopToDisplay", method = RequestMethod.GET)
	public ResponseEntity<List<ShopDto>> getListShopToDisplay(){
		List<ShopDto> listShopDto = iShop.findAll();
		return new ResponseEntity<>(listShopDto, HttpStatus.OK);
	}
	

}

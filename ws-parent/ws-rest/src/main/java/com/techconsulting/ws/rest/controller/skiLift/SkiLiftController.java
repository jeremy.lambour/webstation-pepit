package com.techconsulting.ws.rest.controller.skiLift;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;
import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;
import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopScheduleDto;
import com.techconsulting.ws.service.core.service.accountcreation.IAgeRangeService;
import com.techconsulting.ws.service.core.service.common.IPassTypeService;
import com.techconsulting.ws.service.core.service.equipment.skilift.ISkiLift;

/**
 * The Controller of the {@link PassType} class
 */
@RestController
@RequestMapping(value = "/api/SkiLift")
public class SkiLiftController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(SkiLiftController.class);

	@Autowired
	ISkiLift iSkiLift;
	
	@Autowired
	IAgeRangeService iAgeRange;
	
	@Autowired
	IPassTypeService iPassType;
	
	/**
	 * Method that inserts a {@link ShopScheduleDto} for a specific {@link Schedule}
	 * and a specific {@link Shop}
	 * 
	 * @param scheduleMondayId, the {@link ScheduleDto}'s id
	 * @param shopId, the {@link ShopDto}'s id
	 * @return the shopScheduleDto inserted
	 */
	@RequestMapping(value = "/findSkiLift/{ageRangeId}/{domain}/{passTypeId}", method = RequestMethod.GET)
	public ResponseEntity<List<SkiLiftDto>> findSkiLift(@PathVariable String ageRangeId,
			@PathVariable String domain, @PathVariable String passTypeId) {
		int ageRangeIdInt = Integer.parseInt(ageRangeId);
		int passTypeIdInt = Integer.parseInt(passTypeId);
		List<SkiLiftDto> listSkiLiftDto = iSkiLift.findSkiLiftDtoByAgeRangeAgeRangeIdAndDomainAndPassTypePassTypeId(ageRangeIdInt, domain, passTypeIdInt);
		return new ResponseEntity<>(listSkiLiftDto, HttpStatus.OK);
	}
	
	/**
	 * Method that inserts a {@link ShopScheduleDto} for a specific {@link Schedule}
	 * and a specific {@link Shop}
	 * 
	 * @param scheduleMondayId, the {@link ScheduleDto}'s id
	 * @param shopId, the {@link ShopDto}'s id
	 * @return the shopScheduleDto inserted
	 */
	@RequestMapping(value = "/findSkiLiftNordique/{ageRangeId}/{passTypeId}", method = RequestMethod.GET)
	public ResponseEntity<List<SkiLiftDto>> findSkiLiftNordique(@PathVariable String ageRangeId, @PathVariable String passTypeId) {
		int ageRangeIdInt = Integer.parseInt(ageRangeId);
		int passTypeIdInt = Integer.parseInt(passTypeId);
		List<SkiLiftDto> listSkiLiftDto = iSkiLift.findSkiLiftDtoByAgeRangeAgeRangeIdAndPassTypePassTypeId(ageRangeIdInt, passTypeIdInt);
		return new ResponseEntity<>(listSkiLiftDto, HttpStatus.OK);
	}
	
	/**
	 * Method that updates a {@link SkiLiftDto}
	 * 
	 * @param SkiLiftDto the {@link SkiLiftDto}
	 * @return the updated {@link SkiLiftDto}
	 */
	@PostMapping(value="/updateSkiLift")
	@ResponseBody
	public ResponseEntity<SkiLiftDto> updateSkiLift(@RequestBody SkiLiftDto skiLiftDto) {
		List<SkiLiftDto> newSkiLiftDto = new ArrayList<>();
		if( !skiLiftDto.getDomain().equals("")) {
			newSkiLiftDto = iSkiLift.findSkiLiftDtoByAgeRangeAgeRangeIdAndDomainAndPassTypePassTypeId(skiLiftDto.getAgeRange().getAgeRangeId(), skiLiftDto.getDomain(), skiLiftDto.getPassType().getPassTypeId());
		} else {
			newSkiLiftDto = iSkiLift.findSkiLiftDtoByAgeRangeAgeRangeIdAndPassTypePassTypeId(skiLiftDto.getAgeRange().getAgeRangeId(), skiLiftDto.getPassType().getPassTypeId());
		}
		
		for(int i = 0; i < newSkiLiftDto.size(); i++) {
			if(newSkiLiftDto.get(i).getDuration().equals(skiLiftDto.getDuration())) {
				iSkiLift.setSkiLiftDtoPriceBySkiLift(newSkiLiftDto.get(i), skiLiftDto.getPrice());
			}
		}

		return new ResponseEntity<>(new SkiLiftDto(), HttpStatus.OK);
	}
	
	

}

package com.techconsulting.ws.rest.controller.mail.skilift;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.service.mail.reservskilift.IMailSkilift;
import com.techconsulting.ws.service.core.service.mail.reservskilift.MailSkilift;

/**
 * The Controller of the {@link MailSkilift} class
 */
@RestController
@RequestMapping(value = "/api/MailSkilift")
public class MailSkiliftController {

	@Autowired
	IMailSkilift iMailSkilift;

	/**
	 * Controller method who call the service of mail to send an email when the user
	 * make a reservation on a skilift
	 * 
	 * @param {@link ReservationLiftDto} reservLiftDto : object with a skilift's
	 *        reservation and a user
	 */
	@PostMapping(value = "/")
	public void insertReservSkilift(@RequestBody ReservationLiftDto reservLiftDto) {
		iMailSkilift.insertReservMail(reservLiftDto);
	}

	/**
	 * Controller method who call the service of mail to send an email when the user
	 * delete his reservation on a skilift
	 * 
	 * @param {@link ReservationLiftDto} reservLiftDto : object with a skilift's
	 *        reservation and a user
	 */
	@PostMapping(value = "/delete")
	public void deleteReservSkilift(@RequestBody ReservationLiftDto reservLiftDto) {
		iMailSkilift.deleteReservMail(reservLiftDto);
	}

}

package com.techconsulting.ws.rest.controller.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;
import com.techconsulting.ws.service.core.service.common.IScheduleService;

/**
 * The Controller of the {@link Schedule} class
 */
@RestController
@RequestMapping(value = "/api/Schedule")
public class ScheduleController {

	static final Logger LOGGER = LoggerFactory.getLogger(ScheduleController.class);

	@Autowired
	IScheduleService iSchedule;

	/**
	 * Method that insert the {@link ScheduleDto} in parameters
	 * 
	 * @Param - {@link ScheduleDto} schedule, the schedule we want to insert
	 * @Return - the {@link ScheduleDto} inserted
	 */
	@RequestMapping(value = "/insertSchedule", method = RequestMethod.POST)
	public ResponseEntity<ScheduleDto> insertSchedule(@RequestBody ScheduleDto scheduleDto) {
		ScheduleDto newSchedule = iSchedule.insertSchedule(scheduleDto);
		return new ResponseEntity<>(newSchedule, HttpStatus.OK);
	}
	
	/**
	 * Method that updates a {@link ScheduleDto}
	 * 
	 * @param scheduleDto the {@link ScheduleDto}
	 * @return the update {@link ScheduleDto}
	 */
	@RequestMapping(value = "/updateSchedule", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ScheduleDto> updateSchedule(@RequestBody ScheduleDto scheduleDto) {

		ScheduleDto newScheduleDto = iSchedule.findScheduleById(scheduleDto.getScheduleId());

		iSchedule.setScheduleOpenHourBySchedule(newScheduleDto, scheduleDto.getScheduleOpenHour());
		iSchedule.setScheduleCloseHourBySchedule(newScheduleDto, scheduleDto.getScheduleCloseHour());

		return new ResponseEntity<>(newScheduleDto, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes a {@link ScheduleDto} by an identifier
	 * 
	 * @Param - Integer idSchedule, the schedule we want to delete
	 * @return the deleted {@link ScheduleDto}
	 */
	@RequestMapping(value = "/deleteSchedule", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ScheduleDto> deleteSchedule(@RequestBody Integer idSchedule) {

		ScheduleDto scheduleDto = iSchedule.findScheduleById(idSchedule);
		iSchedule.deleteByScheduleId(scheduleDto.getScheduleId());

		return new ResponseEntity<>(scheduleDto, HttpStatus.OK);
	}

}

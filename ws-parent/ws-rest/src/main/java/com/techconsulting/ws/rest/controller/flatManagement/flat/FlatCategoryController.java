package com.techconsulting.ws.rest.controller.flatManagement.flat;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IFlatCategoryService;

/**
 * The Controller of the {@link FlatCategoryEntity} class
 */
@RestController
@RequestMapping(value = "/api/FlatCategory")
public class FlatCategoryController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(FlatController.class);

	@Autowired
	IFlatCategoryService iFlatCategory;
	
	/**
	 * Method that insert the {@link FlatCategoryDto} in parameters
	 * 
	 * @Param - {@link FlatCategoryDto} flatCategoryDto, the flat category we want to insert
	 * @Return - the {@link FlatCategoryDto} inserted
	 */
	
	@RequestMapping(value = "/insertFlatCategory", method = RequestMethod.POST)
	public ResponseEntity<FlatCategoryDto> insertFlatCategory(@RequestBody FlatCategoryDto flatCategoryDto) {
		FlatCategoryDto newFlatCategory = iFlatCategory.insertFlatCategory(flatCategoryDto);
		return new ResponseEntity<>(newFlatCategory, HttpStatus.OK);
	}
	
	/**
	 * Method that finds all the {@link FlatCategoryDto}
	 * 
	 * @return a list of {@link FlatCategoryDto}
	 */
	@RequestMapping(value = "/getFlatCategories", method = RequestMethod.GET)
	public ResponseEntity<List<FlatCategoryDto>> getFlatCategories() {
		List<FlatCategoryDto> listFlatCategoryDto = iFlatCategory.findAll();
		return new ResponseEntity<>(listFlatCategoryDto, HttpStatus.OK);
	}

}

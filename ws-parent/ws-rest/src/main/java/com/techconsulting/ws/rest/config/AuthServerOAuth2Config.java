package com.techconsulting.ws.rest.config;



import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import com.techconsulting.ws.service.core.service.user.IUserService;
/**
 * Config use by spring to create an Auth2config for authServer
 * @author Jeremy
 *
 */
@Configuration
@EnableAuthorizationServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import(SecurityConfig.class)
public class AuthServerOAuth2Config extends AuthorizationServerConfigurerAdapter {

	/**
	 * {@link DataSource}
	 */
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	
	/**
	 * {@link AuthenticationManager}
	 */
	@Autowired
	private AuthenticationManager authentificationManager;
	
	/**
	 * {@link IUserService}
	 */
	@Autowired
	private IUserService userDetailsService;
	
	/**
	 * {@link BCryptPasswordEncoder}
	 */
	@Autowired
	private BCryptPasswordEncoder oauthClientPasswordEncoder;
	
	/**
	 * Create bean for tokenStore used by spring OAuth2
	 * @return {@link TokenStore}
	 */
	@Bean
	public TokenStore tokenStore() {
		return new JdbcTokenStore(dataSource);
	}
	
	/**
	 * Create {@link OAuth2AccessDeniedHandler}
	 * @return {@link OAuth2AccessDeniedHandler}
	 */
	@Bean
	public OAuth2AccessDeniedHandler oauthAccessDeniedHandler() {
		return new OAuth2AccessDeniedHandler();
	}
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()").passwordEncoder(oauthClientPasswordEncoder);
	}
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.jdbc(dataSource);
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints.tokenStore(tokenStore()).authenticationManager(authentificationManager).userDetailsService(userDetailsService);
	}
	
	
}

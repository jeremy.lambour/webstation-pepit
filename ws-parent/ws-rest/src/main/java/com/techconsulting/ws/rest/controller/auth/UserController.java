package com.techconsulting.ws.rest.controller.auth;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.rest.controller.common.AgeRangeController;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.user.IUserService;
import com.techconsulting.ws.service.core.service.user.UserService;

/**
 * Controller use to make actions for user
 * 
 * @author Jeremy
 *
 */
@RestController
@RequestMapping("/api/user")
public class UserController {
	static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	/**
	 * {@link UserService}
	 */
	@Autowired
	IUserService service;

	/**
	 * Method that find a {@link UserDto} in database
	 * 
	 * @Param - {@link String} user's login
	 * @Return - the {@link UserDto} find
	 */
	@GetMapping(value = "/")
	public ResponseEntity<UserDto> findByUsername(@RequestParam(value = "login") String login) {
		UserDto userDTO;
		LOGGER.debug(login);
		if (login != null) {
			userDTO = service.findUserByUsername(login);
			return new ResponseEntity<>(userDTO, HttpStatus.OK);
		}
		return new ResponseEntity<>(new UserDto(), HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<UserDto>> findAllUsers(){
		List<UserDto> users = service.findAllUsers();
		return new ResponseEntity<List<UserDto>>(users, HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value="/userAuthority")
	public ResponseEntity<List<UserDto>> findAllUserByAuthority(@RequestParam("authorityName") String authorityName){
		try {
			return new ResponseEntity<>(service.findAllByAuthority(authorityName), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value="/find")
	public ResponseEntity<UserDto> findUserById(@RequestParam("userId") Integer id){
		try {
			return new ResponseEntity<>(service.findById(id), HttpStatus.ACCEPTED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

}

package com.techconsulting.ws.rest.controller.skiingSchool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.skiingSchool.LessonDto;
import com.techconsulting.ws.service.core.service.skiingSchool.ILessonService;
import com.techconsulting.ws.service.core.service.skiingSchool.LessonService;

/**
 * {@link LessonController}
 * @author Jeremy
 *
 */
@RestController
@RequestMapping(value = "/api/lesson")
public class LessonController {

	/**
	 * {@link LessonService}
	 */
	@Autowired
	public ILessonService service;

	/**
	 * Path to create a new {@link LessonDto}
	 * @param body
	 * @return ResponseEntity<LessonDto>
	 */
	@PostMapping(value = "/")
	public ResponseEntity<LessonDto> createLesson(@RequestBody LessonDto body) {
		try {
			return new ResponseEntity<>(service.createLesson(body), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to update an existing {@link LessonDto}
	 * @param body
	 * @return ResponseEntity<LessonDto>
	 */
	@PutMapping(value = "/")
	public ResponseEntity<LessonDto> updateLesson(@RequestBody LessonDto body) {
		try {
			return new ResponseEntity<>(service.updateLesson(body), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to delete an existing {@link LessonDto}
	 * @param id
	 * @return ResponseEntity<Boolean>
	 */
	@DeleteMapping(value="/")
	public ResponseEntity<Boolean> deleteLesson(@RequestParam("id") Integer id){
		try {
			return new ResponseEntity<Boolean>(service.deleteLesson(id), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
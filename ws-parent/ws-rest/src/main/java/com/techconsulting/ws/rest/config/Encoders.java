package com.techconsulting.ws.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Define encoders use to encrypt password
 * @author Jeremy
 *
 */
@Configuration
public class Encoders {
	
	/**
	 * Encoders use by oauth client to encode client password
	 * @return {@link PasswordEncoder}
	 */
	@Bean
	public PasswordEncoder oauthClientPasswordEncoder() {
		return new BCryptPasswordEncoder(4);
	}
	
	/**
	 * Encoder use by application to encode register user password
	 * @return {@link PasswordEncoder}
	 */
	@Bean
	public PasswordEncoder userPasswordEncoder() {
		return new BCryptPasswordEncoder(8);
	}

}

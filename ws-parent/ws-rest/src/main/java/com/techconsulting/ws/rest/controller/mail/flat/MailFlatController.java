package com.techconsulting.ws.rest.controller.mail.flat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.service.mail.reservflat.IMailFlat;
import com.techconsulting.ws.service.core.service.mail.reservflat.MailFlat;

/**
 * The Controller of the {@link MailFlat} class
 */
@RestController
@RequestMapping(value = "/api/MailFlat")
public class MailFlatController {

	@Autowired
	IMailFlat iMailFlat;

	/**
	 * Controller method who call the service of mail to send an email when the user
	 * make a reservation on a flat
	 * 
	 * @param {@link ReservationFlatDto} reservFlatDto : object with a flat's
	 *        reservation and a user
	 */
	@PostMapping(value = "/")
	public void sendMail(@RequestBody ReservationFlatDto reservFlatDto) {
		iMailFlat.insertReservMail(reservFlatDto);
	}

}
package com.techconsulting.ws.rest.controller.reservation;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.dao.repository.auth.UserRepository;
import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.DefineDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.service.common.IFestivePeriodService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IDefineService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IFlatService;
import com.techconsulting.ws.service.core.service.reservation.IReservation;
import com.techconsulting.ws.service.core.service.reservation.flat.IReservationFlat;

/**
 * Controller use to create a reservation between flat and user
 * 
 * @author Clara
 *
 */
@RestController
@RequestMapping(value = "/api/ReservationFlat")
public class ReservationFlatController {

	@Autowired
	IReservationFlat iReservationFlat;
	
	@Autowired
	IFlatService iFlatService;
	
	@Autowired
	IReservation iReservation;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	IFestivePeriodService iFestivePeriodService;
	
	@Autowired
	IDefineService iDefineService;
	
	/**
	 * Method that finds if a {@link FlatDto} is available for specifics dates
	 * 
	 * @param arrivingDate, the first date
	 * @param leavingDate, the second date
	 * @param flat, the flat we want to know the availability
	 * @return true if available, else false
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/getAvailabilityFlat/{arrivingDate}/{leavingDate}/{flatId}", method = RequestMethod.GET)
	public ResponseEntity<FlatDto> getAvailabilityFlat(@PathVariable String arrivingDate,
			@PathVariable String leavingDate, @PathVariable Integer flatId) throws ParseException {
		FlatDto flatDto = iFlatService.findFlatById(flatId);
		List<FlatDto> listFlatDto = iFlatService.findByFlatCategoryDtoAndHotelDto(flatDto.getFlatCategoryDto(), flatDto.getHotelDto());
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateArrivingDate = formatter.parse(arrivingDate);
		Date dateLeavingDate = formatter.parse(leavingDate);
		
		boolean availabilityGlobal = false;
		boolean availability = true;
		
		for(int j = 0; j < listFlatDto.size() && !availabilityGlobal; j++) {
			List<ReservationFlatDto> listReservation = iReservationFlat.findByReservationFlatIdFlatDto(listFlatDto.get(j));
			if(listReservation.isEmpty()) {
				availabilityGlobal = true;
				flatDto = listFlatDto.get(j);
			} else {
				availability = checkAvailibility(listReservation, dateArrivingDate, dateLeavingDate);
			}
		}
		
		if(availabilityGlobal && availability) {
			return new ResponseEntity<>(flatDto, HttpStatus.OK);
		} else {
			flatDto = new FlatDto();
			return new ResponseEntity<>(flatDto, HttpStatus.OK);
		}
	}
	
	public boolean checkAvailibility(List<ReservationFlatDto> listReservation, Date dateArrivingDate, Date dateLeavingDate) throws ParseException{
		boolean availability = true;
		for(int i = 0; i < listReservation.size() && availability; i++) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date dateStartDate = formatter.parse(listReservation.get(i).getStartDate().toString());
			Date dateEndDate = formatter.parse(listReservation.get(i).getEndDate().toString());
			if(dateStartDate.after(dateArrivingDate) && dateEndDate.before(dateLeavingDate)) {
				availability = false;
			}
			if(dateStartDate.before(dateArrivingDate) && dateEndDate.before(dateLeavingDate) && dateEndDate.after(dateArrivingDate)) {
				availability = false;
			}
			if(dateStartDate.after(dateArrivingDate) && dateStartDate.after(dateLeavingDate) && dateEndDate.after(dateLeavingDate)) {
				availability = false;
			}
		}
		return availability;
	}
	
	/**
	 * Method that finds if a {@link FlatDto} is available for specifics dates
	 * 
	 * @param arrivingDate, the first date
	 * @param leavingDate, the second date
	 * @param flat, the flat we want to know the availability
	 * @return true if available, else false
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/insertReservationFlat", method = RequestMethod.POST)
	public ResponseEntity<ReservationFlatDto> insertReservationFlat(@RequestBody ReservationFlatDto rFlat) {
		ReservationFlatDto newReservationFlatDto = iReservationFlat.insertReservationFlat(rFlat);
		return new ResponseEntity<>(newReservationFlatDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a price for a {@link FlatDto} at specifics dates
	 * 
	 * @param id, the flat we want to know the price
	 * @param arrivingDate, the first date
	 * @param leavingDate, the second date
	 * @return the price
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/getPriceFlatForDates/{id}/{startDate}/{endDate}", method = RequestMethod.GET)
	public ResponseEntity<Double> getPriceFlatForDates(@PathVariable String id,
			@PathVariable String startDate, @PathVariable String endDate) throws ParseException {
		FlatDto flatDto = iFlatService.findFlatById(Integer.parseInt(id));
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateArrivingDate = formatter.parse(startDate);
		Date dateLeavingDate = formatter.parse(endDate);
		
		int nbDays = dateLeavingDate.getDate() - dateArrivingDate.getDate();
		
		DefineDto defineDto;
		
		Double price;
		
		if(nbDays % 7 != 0) {
			FestivePeriodDto festivePeriodDto = iFestivePeriodService.findByFestivePeriodName("Tarif nuitée");
			defineDto = iDefineService.findByFestivePeriodDtoAndFlatDto(festivePeriodDto, flatDto);
			price = nbDays * defineDto.getFlatPricePerNight();
		} else {
			List<FestivePeriodDto> listFestivePeriodDto = iFestivePeriodService.findAll();
			FestivePeriodDto festivePeriodDto = checkFestivePeriod(listFestivePeriodDto, dateArrivingDate, dateLeavingDate);
			defineDto = iDefineService.findByFestivePeriodDtoAndFlatDto(festivePeriodDto, flatDto);
			
			nbDays = nbDays / 7;
			price = nbDays * defineDto.getFlatPricePerNight();
		}
		
		return new ResponseEntity<>(price, HttpStatus.OK);

	}
	
	public FestivePeriodDto checkFestivePeriod(List<FestivePeriodDto> listFestivePeriodDto, Date dateArrivingDate, Date dateLeavingDate) {
		FestivePeriodDto festivePeriodDto = new FestivePeriodDto();
		boolean found =false;
		for(int i = 0; i < listFestivePeriodDto.size() && !found; i++) {
			if(listFestivePeriodDto.get(i).getFestivePeriodBeginning() != null && listFestivePeriodDto.get(i).getFestivePeriodEnd() != null) {
				Date periodBeginning = listFestivePeriodDto.get(i).getFestivePeriodBeginning();
				Date periodEnd = listFestivePeriodDto.get(i).getFestivePeriodEnd();
				if( periodBeginning.before(dateArrivingDate) && periodEnd.after(dateLeavingDate) ||
					periodBeginning.before(dateArrivingDate) && periodEnd.after(dateArrivingDate) && periodEnd.before(dateLeavingDate) ||
				    periodBeginning.compareTo(dateArrivingDate) == 0 && periodEnd.compareTo(dateLeavingDate) == 0) {
					festivePeriodDto = listFestivePeriodDto.get(i);
					found = true;
				}
			}
		}
		return festivePeriodDto;
	}
	
	/**
	 * Method that finds a price for a {@link FlatDto} at specifics dates
	 * 
	 * @param id, the flat we want to know the price
	 * @param arrivingDate, the first date
	 * @param leavingDate, the second date
	 * @return the price
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/getReservationsFlatUser/{userId}", method = RequestMethod.GET)
	public ResponseEntity<List<ReservationFlatDto>> getReservationsFlatUser(@PathVariable Integer userId) {
		List<ReservationFlatDto> listReservationFlatDto = iReservationFlat.findByReservationFlatIdReservationDtoUserUserId(userId);
		return new ResponseEntity<>(listReservationFlatDto, HttpStatus.OK);

	}
	
	/**
	 * Method that finds a list of all the reserved flats  {@link ReservationFlatDto}
	 * @return the list of all the reserved flats
	 */
	@GetMapping(value = "/") 
	public ResponseEntity<List<ReservationFlatDto>> getAllReservedFlat () {
		List<ReservationFlatDto> listReservationFlatDto = iReservationFlat.findAll();
		return new ResponseEntity<>(listReservationFlatDto, HttpStatus.OK);
	}
	

	

}


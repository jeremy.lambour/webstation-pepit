package com.techconsulting.ws.rest.controller.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.user.AuthorityDto;
import com.techconsulting.ws.service.core.service.accountcreation.IAuthorityService;

@RestController
@RequestMapping("/api/authority")
public class AuthorityController {

	@Autowired
	private IAuthorityService service;
	
	
	@GetMapping("/")
	public ResponseEntity<List<AuthorityDto>> getAllAuthorities(){
		try {
			return new ResponseEntity<>(service.findAll(), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

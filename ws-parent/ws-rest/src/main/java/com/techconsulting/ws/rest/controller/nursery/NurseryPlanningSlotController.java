package com.techconsulting.ws.rest.controller.nursery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningSlotDto;
import com.techconsulting.ws.service.core.service.nursery.INurseryPlanningSlotService;
import com.techconsulting.ws.service.core.service.nursery.NurseryPlanningSlotService;

/**
 *  {@link NurseryPlanningSlotController}
 * @author Jeremy
 *
 */
@RestController
@RequestMapping("/api/nurseryPlanningSlot")
public class NurseryPlanningSlotController {

	/**
	 * {@link NurseryPlanningSlotService}
	 */
	@Autowired
	private INurseryPlanningSlotService service;
	
	
	/**
	 * Path to create a new {@link NurseryPlanningSlotDto}
	 * @param body
	 * @return ResponseEntity<NurseryPlanningSlotDto>
	 */
	@PostMapping(value = "/")
	public ResponseEntity<NurseryPlanningSlotDto> createNurseryPlanningSlot(@RequestBody NurseryPlanningSlotDto body) {
		try {
			return new ResponseEntity<>(service.createNurseryPlanningSlot(body), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to update an existing {@link NurseryPlanningSlotDto}
	 * @param planning
	 * @return ResponseEntity<NurseryPlanningSlotDto>
	 */
	@PutMapping(value = "/")
	public ResponseEntity<NurseryPlanningSlotDto> updateNurseryPlanningSlot(@RequestBody NurseryPlanningSlotDto planning) {
		try {
			return new ResponseEntity<>(service.createNurseryPlanningSlot(planning), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to delete an existing {@link NurseryPlanningSlotDto}
	 * @param planning
	 * @return ResponseEntity<Boolean>
	 */
	@DeleteMapping(value="/")
	public ResponseEntity<Boolean> deleteNurseryPlanningSlot(@RequestParam("id") Integer id){
		try {
			return new ResponseEntity<Boolean>(service.deleteNurseryPlanningSlot(id), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

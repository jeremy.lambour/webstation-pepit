package com.techconsulting.ws.rest.controller.skiingSchool;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.dao.entity.skiingSchool.SkiingSchool;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.SkiingSchoolDto;
import com.techconsulting.ws.service.core.service.skiingSchool.ISkiingSchoolService;
import com.techconsulting.ws.service.core.service.skiingSchool.SkiingSchoolService;

/**
 * {@link SkiingSchoolController}
 * @author Jeremy
 *
 */
@RestController
@RequestMapping(value = "/api/SkiingSchool")
public class SkiingSchoolController {

	/**
	 * {@link SkiingSchoolService}
	 */
	@Autowired
	public ISkiingSchoolService service;

	/**
	 * Path to create a new {@link SkiingSchool}
	 * @param body
	 * @return ResponseEntity<SkiingSchoolDto>
	 */
	@PostMapping(value = "/")
	public ResponseEntity<SkiingSchoolDto> createSkiingSchool(@RequestBody SkiingSchoolDto body) {
		try {
			return new ResponseEntity<>(service.createSkiingSchool(body), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to update an existing {@link SkiingSchool}
	 * @param body
	 * @return ResponseEntity<SkiingSchoolDto>
	 */
	@PutMapping(value = "/")
	public ResponseEntity<SkiingSchoolDto> updateSkiingSchool(@RequestBody SkiingSchoolDto body) {
		try {
			return new ResponseEntity<>(service.updateSkiingSchool(body), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to delete an existing {@link SkiingSchool}
	 * @param skiingSchoolId
	 * @return ResponseEntity<Boolean>
	 */
	@DeleteMapping(value="/")
	public ResponseEntity<Boolean> deleteSkiingSchool(@RequestParam Integer skiingSchoolId){
		try {
			return new ResponseEntity<Boolean>(service.deleteSkiingSchool(skiingSchoolId), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Path to find all {@link SkiingSchool}
	 * @return ResponseEntity<List<SkiingSchoolDto>>
	 */
	@GetMapping(value="/")
	public ResponseEntity<List<SkiingSchoolDto>> findAllSkiingSchool(){
		try {
			return new ResponseEntity<List<SkiingSchoolDto>>(service.findAllSkiingSchool(), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

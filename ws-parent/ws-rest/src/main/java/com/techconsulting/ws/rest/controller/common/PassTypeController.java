package com.techconsulting.ws.rest.controller.common;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.PassTypeDto;
import com.techconsulting.ws.service.core.service.common.IPassTypeService;

/**
 * The Controller of the {@link PassType} class
 */
@RestController
@RequestMapping(value = "/api/PassType")
public class PassTypeController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(PassTypeController.class);

	@Autowired
	IPassTypeService iPassType;
	
	/**
	 * Method that gets a {@link PassTypeDto}
	 * 
	 * @param PassTypeDto id the {@link PassTypeDto}
	 * @return the {@link PassTypeDto}
	 */
	@GetMapping(value="/findAll")
	@ResponseBody
	public ResponseEntity<List<PassTypeDto>> findAll() {
		List<PassTypeDto> listPassTypeDto = iPassType.findAll();
		return new ResponseEntity<>(listPassTypeDto, HttpStatus.OK);
	}
	
	

}

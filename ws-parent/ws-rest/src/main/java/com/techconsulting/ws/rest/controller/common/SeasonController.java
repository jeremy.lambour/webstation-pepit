package com.techconsulting.ws.rest.controller.common;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;
import com.techconsulting.ws.service.core.service.common.ISeasonService;

/**
 * The Controller of the {@link Schedule} class
 */
@RestController
@RequestMapping(value = "/api/Season")
public class SeasonController {

	static final Logger LOGGER = LoggerFactory.getLogger(SeasonController.class);

	@Autowired
	ISeasonService iSeason;

	/**
	 * Method that updates a {@link SeasonDto}
	 * 
	 * @param seasonDto the {@link SeasonDto}
	 * @return the update {@link SeasonDto}
	 */
	@PostMapping(value = "/updateSeason")
	@ResponseBody
	public ResponseEntity<SeasonDto> updateSeason(@RequestBody SeasonDto season) {

		SeasonDto newSeasonDto = iSeason.findSeasonById(season.getSeasonId());

		SeasonDto seasonInterWinter = iSeason.findSeasonById(1);
		SeasonDto seasonWinter = iSeason.findSeasonById(2);
		SeasonDto seasonInterSummer = iSeason.findSeasonById(3);
		SeasonDto seasonSummer = iSeason.findSeasonById(4);

		if (season.getSeasonId() == 2) {
			iSeason.setSeasonActiveBySeason(seasonWinter, !season.getActive());
			iSeason.setSeasonActiveBySeason(seasonInterWinter, !season.getActive());
			iSeason.setSeasonActiveBySeason(seasonInterSummer, season.getActive());
			iSeason.setSeasonActiveBySeason(seasonSummer, season.getActive());
		} else if (season.getSeasonId() == 4) {
			iSeason.setSeasonActiveBySeason(seasonWinter, season.getActive());
			iSeason.setSeasonActiveBySeason(seasonInterWinter, season.getActive());
			iSeason.setSeasonActiveBySeason(seasonInterSummer, !season.getActive());
			iSeason.setSeasonActiveBySeason(seasonSummer, !season.getActive());
		}

		return new ResponseEntity<>(newSeasonDto, HttpStatus.OK);
	}

	/**
	 * Method that gets a {@link SeasonDto}
	 * 
	 * @param seasonDto the {@link SeasonDto}
	 * @return the {@link SeasonDto}
	 */
	@GetMapping(value = "/getSeason")
	@ResponseBody
	public ResponseEntity<SeasonDto> getSeason(@RequestParam(value = "seasonId") Integer seasonId) {
		SeasonDto newSeasonDto = iSeason.findSeasonById(seasonId);
		return new ResponseEntity<>(newSeasonDto, HttpStatus.OK);
	}

	/**
	 * Method that updates the beginning and end dates of a {@link SeasonDto}
	 * 
	 * @param seasonDto the {@link SeasonDto}
	 * @return the updated {@link SeasonDto}
	 */
	@PostMapping(value = "/updateDatesSeason")
	@ResponseBody
	public ResponseEntity<SeasonDto> updateDatesSeason(@RequestBody SeasonDto season) {
		SeasonDto newSeasonDto = iSeason.findSeasonById(season.getSeasonId());
		iSeason.setSeasonSeasonBeginningBySeason(newSeasonDto, season.getSeasonBeginning());
		iSeason.setSeasonSeasonEndBySeason(newSeasonDto, season.getSeasonEnd());
		return new ResponseEntity<>(newSeasonDto, HttpStatus.OK);
	}

	/**
	 * Method that return the active's {@link SeasonDto}
	 * 
	 * @return the list of active's {@link SeasonDto}
	 */
	@GetMapping(value = "/getActiveSeason")
	@ResponseBody
	public ResponseEntity<List<SeasonDto>> getActiveSeason() {
		SeasonDto seasonInterWinter = iSeason.findSeasonById(1);
		SeasonDto seasonWinter = iSeason.findSeasonById(2);
		SeasonDto seasonInterSummer = iSeason.findSeasonById(3);
		SeasonDto seasonSummer = iSeason.findSeasonById(4);

		List<SeasonDto> listSeason = new ArrayList<>();
		listSeason.add(seasonInterWinter);
		listSeason.add(seasonWinter);
		listSeason.add(seasonInterSummer);
		listSeason.add(seasonSummer);

		List<SeasonDto> listSeasonActive = new ArrayList<>();

		for (SeasonDto s : listSeason) {
			if (s.getActive()) {
				listSeasonActive.add(s);
			}
		}
		return new ResponseEntity<>(listSeasonActive, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<SeasonDto>> getAllSeasons() {
		try {
			return new ResponseEntity<>(iSeason.getAllSeason(), HttpStatus.ACCEPTED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/{seasonId}")
	public ResponseEntity<SeasonDto> getAllSeasons(@PathVariable Integer seasonId) {
		try {
			return new ResponseEntity<>(iSeason.findSeasonById(seasonId), HttpStatus.ACCEPTED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

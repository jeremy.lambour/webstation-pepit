package com.techconsulting.ws.rest.controller.flatManagement.flat;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.DefineDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.service.common.IFestivePeriodService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IDefineService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IFlatCategoryService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IFlatService;

/**
 * The Controller of the {@link Define} class
 */
@RestController
@RequestMapping(value = "/api/Define")
public class DefineController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(DefineController.class);

	@Autowired
	IDefineService iDefine;
	
	@Autowired
	IFlatService iFlat;
	
	@Autowired
	IFlatCategoryService iFlatCategory;
	
	@Autowired
	IFestivePeriodService iFestivePeriodService;
	
	/**
	 * Method that finds a {@link DefineDto} from a {@link FlatDto} identifier in parameter
	 * 
	 * @param flatId the {@link FlatDto}'s id
	 * @return the {@link DefineDto}
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getDefine", method = RequestMethod.GET)
	public ResponseEntity<List<DefineDto>> getDefine(@RequestParam(value = "idFlat") Integer idFlat) {
		List<DefineDto> listDefineDto = iDefine.findDefineDtoByFlatDtoFlatId(idFlat);
		return new ResponseEntity<>(listDefineDto, HttpStatus.OK);
	}
	
	/**
	 * Method that updates a list of {@link DefineDto}
	 * 
	 * @param defineDto the {@link DefineDto}
	 * @return the first updated {@link DefineDto} in the same category
	 */
	@RequestMapping(value = "/updateDefine", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<DefineDto> updateDefine(@RequestBody DefineDto defineDto) {
		FlatCategoryDto flatCategoryDto = iFlatCategory.findFlatCategoryById(defineDto.getFlatDto().getFlatCategoryDto().getFlatCategoryId());
		List<FlatDto> listFlatDto = iFlat.findByFlatCategoryDtoAndHotelDto(flatCategoryDto, defineDto.getFlatDto().getHotelDto());
		FestivePeriodDto festivePeriodDto = iFestivePeriodService.findFestivePeriodById(defineDto.getFestivePeriodDto().getFestivePeriodId());
		for(int i =0;i<listFlatDto.size(); i++) {
			List<DefineDto> listDefineDto = iDefine.findDefineDtoByFlatDtoFlatId(listFlatDto.get(i).getFlatId());
			for(int j = 0; j < listDefineDto.size(); j++) {
				DefineDto newDefineDto = iDefine.findByFestivePeriodDtoAndFlatDto(festivePeriodDto, listFlatDto.get(i));
					iDefine.setDefineFlatPricePerNightByDefine(newDefineDto, defineDto.getFlatPricePerNight());
			}
		}

		return new ResponseEntity<>(defineDto, HttpStatus.OK);
	}
	
}

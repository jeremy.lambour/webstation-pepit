package com.techconsulting.ws.rest.controller.common;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.service.accountcreation.IAgeRangeService;

/**
 * The Controller of the {@link PassType} class
 */
@RestController
@RequestMapping(value = "/api/AgeRange")
public class AgeRangeController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(AgeRangeController.class);

	@Autowired
	IAgeRangeService iAgeRange;
	
	/**
	 * Method that gets a {@link AgeRangeDto}
	 * 
	 * @param AgeRangeDto id the {@link AgeRangeDto}
	 * @return the {@link AgeRangeDto}
	 */
	@GetMapping(value="/findAll")
	@ResponseBody
	public ResponseEntity<List<AgeRangeDto>> findAll() {
		List<AgeRangeDto> listPassTypeDto = iAgeRange.findAll();
		return new ResponseEntity<>(listPassTypeDto, HttpStatus.OK);
	}
	
	

}

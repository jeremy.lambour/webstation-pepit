package com.techconsulting.ws.rest.controller.equipment;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.dao.entity.equipment.Equipment;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.parser.equipment.EquipmentParser;
import com.techconsulting.ws.service.core.service.equipment.IEquipmentService;
import com.techconsulting.ws.service.core.service.shop.IShopService;

/**
 * The Controller of the {@link Equipment} class
 */
@RestController
@RequestMapping(value = "/api/Equipment")
public class EquipmentController {
	static final Logger LOGGER = LoggerFactory.getLogger(EquipmentController.class);

	@Autowired
	IEquipmentService iEquipment;

	@Autowired
	IShopService ishop;

	/**
	 * Method that insert the {@link EquipmentDTO} in parameters
	 * 
	 * @Param - {@link EquipmentDTO} equipment, the equipment we want to insert
	 * @Return - the {@link EquipmentDTO} inserted
	 */
	@PostMapping(value = "/insertEquipment")
	public ResponseEntity<EquipmentDto> insertEquipment(@RequestBody EquipmentDto equipmentDTO) {
		ShopDto shopDto = ishop.findShopById(equipmentDTO.getShop().getShopId());
		equipmentDTO.setShop(shopDto);
		EquipmentDto newEquipment = iEquipment.insertEquipment(equipmentDTO);
		return new ResponseEntity<>(newEquipment, HttpStatus.OK);
	}

	/**
	 * Method that updates a {@link EquipmentDto}
	 * 
	 * @param equipment the {@link EquipmentDto}
	 * @return the update {@link EquipmentDto}
	 */
	@PostMapping(value = "/updateEquipment")
	@ResponseBody
	public ResponseEntity<EquipmentDto> updateEquipment(@RequestBody EquipmentDto equipmentDto) {
		iEquipment.setEquipmentByEquipment(equipmentDto, EquipmentParser.convertToEntity(equipmentDto));
		EquipmentDto newEquipmentDto = iEquipment.findEquipmentById(equipmentDto.getEquipmentId());
		return new ResponseEntity<>(newEquipmentDto, HttpStatus.OK);
	}

	/**
	 * Method that finds a {@link EquipmentDto}
	 * 
	 * @param equipmentId the {@link EquipmentDto}'s id
	 * @return the {@link EquipmentDto}
	 * @throws ParseException
	 */
	@GetMapping(value = "/getEquipment")
	public ResponseEntity<EquipmentDto> getEquipment(@RequestParam(value = "idEquipment") Integer idEquipment) {
		EquipmentDto equipmentDto = iEquipment.findEquipmentById(idEquipment);
		return new ResponseEntity<>(equipmentDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link EquipmentDto} by id shop
	 * 
	 * @param equipmentId the {@link EquipmentDto}'s id
	 * @return the {@link EquipmentDto}
	 * @throws ParseException
	 */
	@GetMapping(value = "/getEquipmentByShopId")
	public ResponseEntity<List<EquipmentDto>> getEquipmentByShopId(@RequestParam(value = "ShopId") Integer shopId) {
		List<EquipmentDto> listEquipmentDto = iEquipment.findEquipmentByShopId(shopId);
		return new ResponseEntity<>(listEquipmentDto, HttpStatus.OK);
	}

	/**
	 * Method that deletes a {@link EquipmentDto} by an identifier
	 * 
	 * @Param - Integer idEquipment, the equipment we want to delete
	 * @return the deleted {@link EquipmentDto}
	 */
	@PostMapping(value = "/deleteEquipment")
	@ResponseBody
	public ResponseEntity<EquipmentDto> deleteEquipment(@RequestBody Integer idEquipment) {

		EquipmentDto equipmentDto = iEquipment.findEquipmentById(idEquipment);
		iEquipment.deleteByEquipmentId(equipmentDto.getEquipmentId());

		return new ResponseEntity<>(equipmentDto, HttpStatus.OK);
	}

	/**
	 * Method that finds all the {@link EquipmentDto}
	 * 
	 * @return the list of {@link EquipmentDto}
	 */
	@GetMapping(value = "/getListEquipmentToDisplay")
	public ResponseEntity<List<EquipmentDto>> getListEquipmentToDisplay() {
		List<EquipmentDto> listEquipmentDto = iEquipment.findAll();
		return new ResponseEntity<>(listEquipmentDto, HttpStatus.OK);
	}
	
	

}

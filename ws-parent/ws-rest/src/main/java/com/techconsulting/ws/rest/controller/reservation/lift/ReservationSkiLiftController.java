package com.techconsulting.ws.rest.controller.reservation.lift;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.service.accountcreation.IAgeRangeService;
import com.techconsulting.ws.service.core.service.equipment.skilift.ISkiLift;
import com.techconsulting.ws.service.core.service.reservation.equipment.skilift.ISkiLiftReservationService;

/**
 * Controller use to create a reservation of a skilift
 * 
 * @author Paul
 *
 */
@RestController
@RequestMapping(value = "/api/reservSkiLift")
public class ReservationSkiLiftController {

	/**
	 * The skilift interface who provide the skilift service
	 */
	@Autowired
	ISkiLift iSkiLift;

	/**
	 * The skilift reservation interface who provide the skilift reservation service
	 */
	@Autowired
	ISkiLiftReservationService iReservationSkiLift;

	/**
	 * The age range interface who provide the age range service
	 */
	@Autowired
	IAgeRangeService iAgeRangeService;

	/**
	 * Method that insert the {@link ReservationEquipmentDto} in parameters
	 * 
	 * @Param - {@link ReservationEquipmentDto} reservEquipDTO, the reservEquipDTO
	 *        we want to insert
	 * @Return - the {@link ReservationEquipmentDto} inserted
	 */
	@PostMapping(value = "/")
	public ResponseEntity<ReservationLiftDto> insertReservSkiLift(@RequestBody ReservationLiftDto reservLift) {
		ReservationLiftDto newReservationSkiLiftDto = iReservationSkiLift.insertReservSkiLift(reservLift);
		return new ResponseEntity<>(newReservationSkiLiftDto, HttpStatus.OK);
	}

	/**
	 * Method that finds a {@link ReservationLiftDto} by user id
	 * 
	 * @param user id
	 * @return the list of {@link ReservationLiftDto}
	 */
	@GetMapping(value = "/getReservLift")
	public ResponseEntity<List<ReservationLiftDto>> getReservLift(@RequestParam(value = "idUser") int idUser) {
		List<ReservationLiftDto> listSkiLift = iReservationSkiLift.getListReservSkiLiftUser(idUser);
		return new ResponseEntity<>(listSkiLift, HttpStatus.OK);
	}

	/**
	 * Method that deletes a {@link ReservationEquipmentDto} by an identifier
	 * 
	 * @return the check boolean
	 */
	@PostMapping(value = "/deleteReservLift")
	public ResponseEntity<Boolean> deleteReservation(@RequestBody ReservationLiftDto reservLiftDTO) {
		iReservationSkiLift.deleteSkiLiftReservation(reservLiftDTO);
		return new ResponseEntity<>(true, HttpStatus.OK);
	}

	/**
	 * Method that finds all a {@link SkiLiftDto}
	 * 
	 * @return the {@link ShopDto}
	 */
	@GetMapping(value = "/all")
	public ResponseEntity<List<SkiLiftDto>> getAll() {
		List<SkiLiftDto> listSkiLift = iSkiLift.getListSkiLift();
		return new ResponseEntity<>(listSkiLift, HttpStatus.OK);
	}

	/**
	 * Method that finds all a {@link AgeRangeDto}
	 * 
	 * @return the list of {@link AgeRangeDto}
	 */
	@GetMapping(value = "/ageRange")
	public ResponseEntity<AgeRangeDto> getAgeRange(@RequestParam(value = "idAge") int ageRangeId) {
		AgeRangeDto ageRange = iAgeRangeService.findAgeRange(ageRangeId);
		return new ResponseEntity<>(ageRange, HttpStatus.OK);
	}

}

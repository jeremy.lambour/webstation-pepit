package com.techconsulting.ws.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.techconsulting.ws.service.core.service.user.IUserService;
import com.techconsulting.ws.service.core.service.user.UserService;

/**
 * Config file for security
 * @author Jeremy
 *
 */
@Configuration
@EnableWebSecurity
@Import(Encoders.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * {@link UserService}
	 */
	@Autowired
	IUserService userService;

	/**
	 * {@link PasswordEncoder}
	 */
	@Autowired
    private PasswordEncoder userPasswordEncoder;
	
	/**
	 * define an authentificationManager Bean
	 */
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManager();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(userPasswordEncoder);
	}


}

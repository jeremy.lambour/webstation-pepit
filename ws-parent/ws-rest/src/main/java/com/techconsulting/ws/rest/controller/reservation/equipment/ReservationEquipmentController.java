package com.techconsulting.ws.rest.controller.reservation.equipment;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;

import com.techconsulting.ws.service.core.service.reservation.equipment.IEquipmentReservationService;
import com.techconsulting.ws.service.core.service.shop.IShopService;

/**
 * Controller use to create a reservation between equipment and user
 * 
 * @author Paul
 *
 */
@RestController
@RequestMapping(value = "/api/reservEquipment")
public class ReservationEquipmentController {

	@Autowired
	IEquipmentReservationService iEquipReservServ;
	
	@Autowired
	IShopService iShopService;

	/**
	 * Method that insert the {@link ReservationEquipmentDto} in parameters
	 * 
	 * @Param - {@link ReservationEquipmentDto} reservEquipDTO, the reservEquipDTO we want to insert
	 * @Return - the {@link ReservationEquipmentDto} inserted
	 */
	@PostMapping(value = "/")
	public ResponseEntity<ReservationEquipmentDto> insertReservEquipment(@RequestBody ReservationEquipmentDto reservEquipDTO) {
		ReservationEquipmentDto newReservationEquipmentDto = iEquipReservServ.insertReservEquip(reservEquipDTO);
		return new ResponseEntity<>(newReservationEquipmentDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link ShopDto}
	 * 
	 * @param shopId the {@link ShopDto}'s id
	 * @return the {@link ShopDto}
	 * @throws ParseException
	 */
	@GetMapping(value = "/getReservationEquipmentUser")
	public ResponseEntity<List<ReservationEquipmentDto>> getListReservationEquipment(@RequestParam(value = "idUser") Integer idUser) {
		List<ReservationEquipmentDto> listReservEquip = iEquipReservServ.getListReservEquipUser(idUser);
		return new ResponseEntity<>(listReservEquip, HttpStatus.OK);
	}

	/**
	 * Method that check the availibility of an equipment
	 * @param - {@link ReservationEquipmentDto}reservEquipDTO
	 * @return - {@link boolean} the availibility 
	 */
	@PostMapping(value = "/checkAvailibity")
	public ResponseEntity<Boolean> checkAvailibity(@RequestBody ReservationEquipmentDto reservEquipDTO) {
		Boolean availibity = iEquipReservServ.checkAvailability(reservEquipDTO);
		return new ResponseEntity<>(availibity, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes a {@link ReservationEquipmentDto} by an identifier
	 * 
	 * @return the check boolean
	 */
	@PostMapping(value = "/deleteReservation")
	public ResponseEntity<Boolean> deleteReservation(@RequestBody ReservationEquipmentDto reservEquipDTO) {
		iEquipReservServ.deleteEquipmentReservation(reservEquipDTO);
		return new ResponseEntity<>(true, HttpStatus.OK);
	}
	
	@GetMapping(value = "/getListReservByDay")
	public ResponseEntity<List<ReservationEquipmentDto>> getListReservByDay(@RequestParam(value = "dateStart") String dateStart) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date newDate = formatter.parse(dateStart);
		Timestamp ts = new Timestamp(newDate.getTime());
		
		List<ReservationEquipmentDto> listReservEquipDay = iEquipReservServ.getListReservEquipDate(ts);
		return new ResponseEntity<>(listReservEquipDay, HttpStatus.OK);
	}
	
	@GetMapping(value = "/getClientsByShopManager")
	public ResponseEntity<List<UserDto>> getClientsByShopManager(@RequestParam(value = "idUser") Integer idUser) {
		List<UserDto> listUserDto = new ArrayList<>();
		List<ShopDto> listShopDto = iShopService.findShopByUserDtoUserId(idUser);
		List<ReservationEquipmentDto> listReservEquip = new ArrayList<>();
		Boolean found = false;
		for(int i = 0; i < listShopDto.size(); i++) {
			listReservEquip.addAll(iEquipReservServ.findReservationEquipmentByEquipmentDtoShopDtoIdShop(listShopDto.get(i).getShopId()));
		}
		for(int j = 0; j < listReservEquip.size(); j++) {
			for(int h = 0; h < listUserDto.size(); h++) {
				if(listReservEquip.get(j).getReservationDto().getUser().getUserId() == listUserDto.get(h).getUserId()) {
					found = true;
				}
			}
			if(!found) {
				listUserDto.add(listReservEquip.get(j).getReservationDto().getUser());
			}
			found = false;
		}
		return new ResponseEntity<>(listUserDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link ShopDto}
	 * 
	 * @param shopId the {@link ShopDto}'s id
	 * @return the {@link ShopDto}
	 * @throws ParseException
	 */
	@GetMapping(value = "/getReservationEquipmentUserDateAfterNow")
	public ResponseEntity<List<ReservationEquipmentDto>> getReservationEquipmentUserDateAfterNow(@RequestParam(value = "idUser") Integer idUser) {
		List<ReservationEquipmentDto> listReservEquip = iEquipReservServ.getListReservEquipUser(idUser);
		List<ReservationEquipmentDto> listReservEquipFinal = new ArrayList<>();
		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		for(int i = 0 ; i<listReservEquip.size();i++) {
			Timestamp tempo = listReservEquip.get(i).getDateStart();
			tempo.setDate(tempo.getDate()+1);
			if(tempo.after(t)) {
				listReservEquipFinal.add(listReservEquip.get(i));
			}
		}
		return new ResponseEntity<>(listReservEquipFinal, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link ShopDto}
	 * 
	 * @param shopId the {@link ShopDto}'s id
	 * @return the {@link ShopDto}
	 * @throws ParseException
	 */
	@GetMapping(value = "/findReservEquipmentByIdReserv")
	public ResponseEntity<ReservationEquipmentDto> findReservEquipmentByIdReserv(@RequestParam(value = "idReserv") Integer idReserv) {
		ReservationEquipmentDto reservEquip = iEquipReservServ.findByReservationDtoReservationId(idReserv);
		return new ResponseEntity<>(reservEquip, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link ShopDto}
	 * 
	 * @param shopId the {@link ShopDto}'s id
	 * @return the {@link ShopDto}
	 * @throws ParseException
	 */
	@GetMapping(value = "/getReservationEquipmentUserToTakeBack")
	public ResponseEntity<List<ReservationEquipmentDto>> getReservationEquipmentUserToTakeBack(@RequestParam(value = "idUser") Integer idUser) {
		List<ReservationEquipmentDto> listReservEquip = iEquipReservServ.getListReservEquipUser(idUser);
		List<ReservationEquipmentDto> listReservEquipFinal = new ArrayList<>();
		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		t.setHours(0);
		t.setMinutes(0);
		t.setSeconds(0);
		t.setNanos(0);
		for(int i = 0 ; i<listReservEquip.size();i++) {
			Timestamp tempo = listReservEquip.get(i).getDateEnd();
			tempo.setDate(tempo.getDate()+1);
			if(t.before(tempo)) {
				listReservEquip.get(i).getDateEnd().setDate(listReservEquip.get(i).getDateEnd().getDate()-1);
				listReservEquipFinal.add(listReservEquip.get(i));
			}
		}
		return new ResponseEntity<>(listReservEquipFinal, HttpStatus.OK);
	}
	
	/**
	 * Method that updates a {@link ReservationEquipmentDto}
	 * 
	 * @param reservationEquipmentDto the {@link ReservationEquipmentDto}
	 * @return the updated {@link ReservationEquipmentDto}
	 */
	@RequestMapping(value = "/updateIsGiven/{isGiven}/{idEquipment}/{idReserv}", method = RequestMethod.GET)
	public ResponseEntity<ReservationEquipmentDto> updateIsGiven(@PathVariable Boolean isGiven,
			@PathVariable Integer idEquipment, @PathVariable Integer idReserv) throws ParseException {
		ReservationEquipmentDto reservationEquipmentDto = iEquipReservServ.findByReservationDtoReservationIdAndEquipmentDtoEquipmentId(idReserv, idEquipment);
		iEquipReservServ.setReservationEquipmentIsGivenByReservationEquipment(isGiven, reservationEquipmentDto);
		ReservationEquipmentDto newReservationEquipmentDto = iEquipReservServ.findByReservationDtoReservationIdAndEquipmentDtoEquipmentId(idReserv, idEquipment);
		return new ResponseEntity<>(newReservationEquipmentDto, HttpStatus.OK);
	}

}

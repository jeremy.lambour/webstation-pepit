package com.techconsulting.ws.rest.controller.nursery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.nursery.NurseryDto;
import com.techconsulting.ws.service.core.modeldto.nursery.NurseryPlanningDto;
import com.techconsulting.ws.service.core.service.nursery.INurseryPlanningService;
import com.techconsulting.ws.service.core.service.nursery.NurseryPlanningService;

/**
 * {@link NurseryPlanningDto} endpoint
 * @author Jeremy
 *
 */
@RestController
@RequestMapping(value = "/api/nurseryPlanning")
public class NurseryPlanningController {

	/**
	 * {@link NurseryPlanningService}
	 */
	@Autowired
	private INurseryPlanningService service;
	
	/**
	 * Path to create a new {@link NurseryPlanningDto}
	 * @param body
	 * @return {@link ResponseEntity} with created {@link NurseryPlanningDto}
	 */
	@PostMapping(value = "/")
	public ResponseEntity<NurseryPlanningDto> createNurseryPlanning(@RequestBody NurseryPlanningDto body) {
		try {
			return new ResponseEntity<>(service.createNurseryPlanning(body), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to update a  {@link NurseryPlanningDto}
	 * @param planning
	 * @return {@link ResponseEntity} with updated {@link NurseryPlanningDto}
	 */
	@PutMapping(value = "/")
	public ResponseEntity<NurseryPlanningDto> updateNurseryPlanning(@RequestBody NurseryPlanningDto planning) {
		try {
			return new ResponseEntity<>(service.updateNurseryPlanning(planning), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to delete a  {@link NurseryPlanningDto}
	 * @param id
	 * @return {@link ResponseEntity} with created Boolean see {@link NurseryPlanningService} for more information about boolean signification
	 */
	@DeleteMapping(value="/")
	public ResponseEntity<Boolean> deletePlanning(@RequestParam("id") Integer id){
		try {
			return new ResponseEntity<Boolean>(service.deleteNurseryPlanning(id), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Path to get {@link NurseryPlanningDto} with season and nursery id
	 * @param nurseryId
	 * @param seasonId
	 * @return ResponseEntity<List<NurseryPlanningDto>>
	 */
	@GetMapping(value="/")
	public ResponseEntity<List<NurseryPlanningDto>> getNurseryPlanningByNurseryIdAndSeasonId(@RequestParam("nurseryId") Integer nurseryId, @RequestParam("seasonId") Integer seasonId){
		try {
			return new ResponseEntity<List<NurseryPlanningDto>>(service.getNurseryPlanningByNurseryIdAndSeasonId(nurseryId, seasonId), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Get {@link NurseryDto} by identifier
	 * @param id
	 * @return ResponseEntity<NurseryPlanningDto>
	 */
	@GetMapping(value="/{id}")
	public ResponseEntity<NurseryPlanningDto> getNurseryById(@PathVariable Integer id){
		try {
			return new ResponseEntity<NurseryPlanningDto>(service.getNurseryPlanningById(id), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

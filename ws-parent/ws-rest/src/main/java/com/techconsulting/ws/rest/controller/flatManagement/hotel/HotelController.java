package com.techconsulting.ws.rest.controller.flatManagement.hotel;


import java.text.ParseException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IFlatService;
import com.techconsulting.ws.service.core.service.flatManagement.hotel.IHotelService;

/**
 * The Controller of the {@link Hotel} class
 */
@RestController
@RequestMapping(value = "/api/Hotel")
public class HotelController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(HotelController.class);

	@Autowired
	IHotelService iHotel;
	
	@Autowired
	IFlatService iFlat;

	/**
	 * Method that insert the {@link HotelDto} in parameters
	 * 
	 * @Param - {@link HotelDto} hotel, the hotel we want to insert
	 * @Return - the {@link HotelDto} inserted
	 */
	
	@RequestMapping(value = "/insertHotel", method = RequestMethod.POST)
	public ResponseEntity<HotelDto> insertHotel(@RequestBody HotelDto hotelDTO) {
		HotelDto newHotel = iHotel.insertHotel(hotelDTO);
		return new ResponseEntity<>(newHotel, HttpStatus.OK);
	}
	
	/**
	 * Method that finds all the {@link HotelDto}
	 * 
	 * @return the list of {@link HotelDto}
	 */
	@RequestMapping(value = "/getListHotelToDisplay", method = RequestMethod.GET)
	public ResponseEntity<List<HotelDto>> getListHotelToDisplay(){
		List<HotelDto> listHotelDto = iHotel.findAll();
		return new ResponseEntity<>(listHotelDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link HotelDto}
	 * 
	 * @param hotelId the {@link HotelDto}'s id
	 * @return the {@link HotelDto}
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getHotel", method = RequestMethod.GET)
	public ResponseEntity<HotelDto> getHotel(@RequestParam(value = "idHotel") Integer idHotel) {
		HotelDto hotelDto = iHotel.findHotelById(idHotel);
		return new ResponseEntity<>(hotelDto, HttpStatus.OK);
	}
	
	/**
	 * Method that updates a {@link HotelDto}
	 * 
	 * @param hotel the {@link HotelDto}
	 * @return the update {@link HotelDto}
	 */
	@RequestMapping(value = "/updateHotel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<HotelDto> updateHotel(@RequestBody HotelDto hotelDto) {

		HotelDto newHotelDto = iHotel.findHotelById(hotelDto.getHotelId());

		iHotel.setHotelNameByHotel(newHotelDto, hotelDto.getHotelName());
		iHotel.setHotelAddressByHotel(newHotelDto, hotelDto.getAddress());
		iHotel.setHotelRoomsNumberByHotel(newHotelDto, hotelDto.getHotelRoomsNumber());
		iHotel.setHotelPhoneNumberByHotel(newHotelDto, hotelDto.getPhoneNumber());
		iHotel.setHotelMailAddressByHotel(newHotelDto, hotelDto.getMailAddress());
		if (hotelDto.getUserDto() != null) {
			iHotel.setHotelUserDtoByHotel(newHotelDto, hotelDto.getUserDto());
		}

		return new ResponseEntity<>(newHotelDto, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes the flat related to an {@link HotelDto} by an identifier
	 * 
	 * @Param - Integer idHotel, the hotel we want to delete flats
	 * @return the deleted {@link FlatDto}
	 */
	@RequestMapping(value = "/deleteHotelFlat", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<FlatDto>> deleteHotelFlat(@RequestBody Integer idHotel) {

		HotelDto hotelDto = iHotel.findHotelById(idHotel);
		
		List<FlatDto> listFlatDto = iFlat.findByHotelDto(hotelDto);
		
		for(int i =0;i<listFlatDto.size(); i++) {
			iFlat.deleteByFlatId(listFlatDto.get(i).getFlatId());
		}

		return new ResponseEntity<>(listFlatDto, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes the {@link HotelDto} by an identifier
	 * 
	 * @Param - Integer idHotel, the hotel we want to delete flats
	 * @return the deleted {@link HotelDto}
	 */
	@RequestMapping(value = "/deleteHotel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<HotelDto> deleteHotel(@RequestBody Integer idHotel) {

		HotelDto hotelDto = iHotel.findHotelById(idHotel);
		iHotel.deleteByHotelId(idHotel);
		return new ResponseEntity<>(hotelDto, HttpStatus.OK);
	}
	
	

}

package com.techconsulting.ws.rest.controller.skiingSchool;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.dao.entity.skiingSchool.Planning;
import com.techconsulting.ws.service.core.modeldto.skiingSchool.PlanningDto;
import com.techconsulting.ws.service.core.service.skiingSchool.IPlanningService;
import com.techconsulting.ws.service.core.service.skiingSchool.PlanningService;

/**
 * {@link PlanningController}
 * @author Jeremy
 *
 */
@RestController
@RequestMapping(value = "/api/planning")
public class PlanningController {

	/**
	 * {@link PlanningService}
	 */
	@Autowired
	public IPlanningService service;

	/**
	 * Path to create a new {@link Planning}
	 * @param body
	 * @return ResponseEntity<PlanningDto>
	 */
	@PostMapping(value = "/")
	public ResponseEntity<PlanningDto> createPlanning(@RequestBody PlanningDto body) {
		try {
			return new ResponseEntity<>(service.createPlanning(body), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Path to update an existing {@link Planning}
	 * @param planning
	 * @return ResponseEntity<PlanningDto>
	 */
	@PutMapping(value = "/")
	public ResponseEntity<PlanningDto> updatePlanning(@RequestBody PlanningDto planning) {
		try {
			return new ResponseEntity<>(service.updatePlanning(planning), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Delete an existing {@link Planning}
	 * @param id
	 * @return ResponseEntity<Boolean>
	 */
	@DeleteMapping(value="/")
	public ResponseEntity<Boolean> deletePlanning(@RequestParam("id") Integer id){
		try {
			return new ResponseEntity<Boolean>(service.deleteplanning(id), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value="/")
	public ResponseEntity<List<PlanningDto>> getAllPlanningByMonitor(@RequestParam("monitorId") Integer monitorId, @RequestParam("seasonId") Integer seasonId){
		try {
			return new ResponseEntity<List<PlanningDto>>(service.getAllPlanningByMonitorAnSeasonId(monitorId, seasonId), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping(value="/{id}")
	public ResponseEntity<PlanningDto> getPlanningById(@PathVariable Integer id){
		try {
			return new ResponseEntity<PlanningDto>(service.getPlanningById(id), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
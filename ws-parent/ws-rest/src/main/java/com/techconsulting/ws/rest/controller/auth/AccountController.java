package com.techconsulting.ws.rest.controller.auth;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.accountcreation.AccountService;
import com.techconsulting.ws.service.core.service.accountcreation.AuthorityService;
import com.techconsulting.ws.service.core.service.accountcreation.IAccountService;
import com.techconsulting.ws.service.core.service.accountcreation.IAuthorityService;

/**
 * Controller use to register new user
 * 
 * @author Jeremy
 *
 */
@RestController
@RequestMapping(value = "/account")
public class AccountController {

	/**
	 * {@link AccountService}
	 */
	@Autowired
	IAccountService iAccountService;

	/**
	 * {@link AuthorityService}
	 */
	@Autowired
	IAuthorityService iUserRoleService;

	/**
	 * Method that insert the {@link UserDto} in database
	 * 
	 * @Param - {@link UserDto} user, the user we want to insert
	 * @Return - the new {@link UserDto} inserted
	 */
	@PostMapping(value = "/")
	public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDTO) {
		UserDto newUserDTO = new UserDto();
		if (userDTO != null) {
			userDTO.setPassword(new String(Base64.getDecoder().decode(userDTO.getPassword())));
			userDTO.setPostalCode(0);
			newUserDTO = iAccountService.createAccount(userDTO);
			return new ResponseEntity<>(newUserDTO, HttpStatus.OK);
		}
		return new ResponseEntity<>(newUserDTO, HttpStatus.NO_CONTENT);
	}

	/**
	 * Method that update an user in database.
	 * 
	 * @param - {@link UserDto} user : the user to update.
	 * @return - the updated {@link UserDto}
	 */
	@PostMapping(value = "/update")
	public ResponseEntity<UserDto> updateUser(@RequestBody UserDto user) {
		UserDto updateUser = this.iAccountService.updateUser(user);
		return new ResponseEntity<>(updateUser, HttpStatus.OK);
	}

}

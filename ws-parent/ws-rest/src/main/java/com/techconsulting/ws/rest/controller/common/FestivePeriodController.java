package com.techconsulting.ws.rest.controller.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.common.SeasonDto;
import com.techconsulting.ws.service.core.service.common.IFestivePeriodService;

/**
 * The Controller of the {@link FestivePeriod} class
 */
@RestController
@RequestMapping(value = "/api/FestivePeriod")
public class FestivePeriodController {

	static final Logger LOGGER = LoggerFactory.getLogger(FestivePeriodController.class);

	@Autowired
	IFestivePeriodService iFestivePeriod;
	
	/**
	 * Method that gets a {@link FestivePeriodDto}
	 * 
	 * @param FestivePeriodDto id the {@link FestivePeriodDto}
	 * @return the {@link FestivePeriodDto}
	 */
	@GetMapping(value="/getFestivePeriod")
	@ResponseBody
	public ResponseEntity<FestivePeriodDto> getFestivePeriod(@RequestParam(value = "festivePeriodId") Integer festivePeriodId) {
		FestivePeriodDto newFestivePeriodDto = iFestivePeriod.findFestivePeriodById(festivePeriodId);
		return new ResponseEntity<>(newFestivePeriodDto, HttpStatus.OK);
	}
	
	/**
	 * Method that updates a {@link FestivePeriodDto}
	 * 
	 * @param festivePeriodDto the {@link FestivePeriodDto}
	 * @return the update {@link FestivePeriodDto}
	 */
	@PostMapping(value="/updateFestivePeriod")
	@ResponseBody
	public ResponseEntity<FestivePeriodDto> updateFestivePeriod(@RequestBody FestivePeriodDto festivePeriod) {
		FestivePeriodDto newFestivePeriodDto = iFestivePeriod.findFestivePeriodById(festivePeriod.getFestivePeriodId());
		iFestivePeriod.setFestivePeriodFestivePeriodBeginningBySeason(newFestivePeriodDto, festivePeriod.getFestivePeriodBeginning());
		iFestivePeriod.setFestivePeriodFestivePeriodEndBySeason(newFestivePeriodDto, festivePeriod.getFestivePeriodEnd());
		return new ResponseEntity<>(newFestivePeriodDto, HttpStatus.OK);
	}

}

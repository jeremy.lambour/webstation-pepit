package com.techconsulting.ws.rest.controller.mail.equipment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.service.mail.reservequipment.IMailEquipment;
import com.techconsulting.ws.service.core.service.mail.reservequipment.MailEquipment;

/**
 * The Controller of the {@link MailEquipment} class
 */
@RestController
@RequestMapping(value = "/api/MailEquipment")
public class MailEquipmentController {

	@Autowired
	IMailEquipment iMailEquip;

	/**
	 * Controller method who call the service of mail to send an email when the user
	 * make a reservation on an equipment
	 * 
	 * @param {@link ReservationEquipmentDto} reservEquipDto : object with a
	 *        equipment's reservation and a user
	 */
	@PostMapping(value = "/")
	public void insertReservMail(@RequestBody ReservationEquipmentDto reservEquipDto) {
		iMailEquip.insertReservMail(reservEquipDto);
	}

	/**
	 * Controller method who call the service of mail to send an email when the user
	 * delete his reservation on an equipment
	 * 
	 * @param {@link ReservationEquipmentDto} reservEquipDto : object with a
	 *        equipment's reservation and a user
	 */
	@PostMapping(value = "/delete")
	public void deleteReservMail(@RequestBody ReservationEquipmentDto reservEquipDto) {
		iMailEquip.deleteReservMail(reservEquipDto);
	}

}

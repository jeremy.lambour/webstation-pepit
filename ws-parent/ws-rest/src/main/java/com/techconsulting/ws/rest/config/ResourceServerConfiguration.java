package com.techconsulting.ws.rest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * Config and enable resources server 
 * @author Jeremy
 *
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	/**
	 * resource server identifier
	 */
	private static final String RESOURCE_ID = "resource-server-rest-api";
	/**
	 * pattern use for oauth2 read scope
	 */
	private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
	/**
	 * pattern use for oauth2 read/write scope
	 */
	private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
	
	/**
	 * pattern which define default secured route
	 */
	private static final String SECURED_PATTERN = "/api/**";

	@Override
	public void configure(ResourceServerSecurityConfigurer ressources) {
		ressources.resourceId(RESOURCE_ID);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.requestMatchers().and().authorizeRequests()
		        .antMatchers("/account/**").permitAll()
				.antMatchers("/oauth/token").permitAll()
				.antMatchers("/api/Season/getActiveSeason").permitAll()
				.antMatchers(HttpMethod.POST, SECURED_PATTERN).access(SECURED_WRITE_SCOPE)
				.antMatchers(HttpMethod.PUT, SECURED_PATTERN).access(SECURED_WRITE_SCOPE)
				.antMatchers(HttpMethod.DELETE, SECURED_PATTERN).access(SECURED_WRITE_SCOPE)
				.anyRequest().access(SECURED_READ_SCOPE);
	}
	

}

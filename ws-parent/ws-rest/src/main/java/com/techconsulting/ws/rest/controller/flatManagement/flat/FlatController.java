package com.techconsulting.ws.rest.controller.flatManagement.flat;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IFlatCategoryService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.IFlatService;
import com.techconsulting.ws.service.core.service.flatManagement.hotel.IHotelService;

/**
 * The Controller of the {@link Flat} class
 */
@RestController
@RequestMapping(value = "/api/Flat")
public class FlatController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(FlatController.class);

	@Autowired
	IFlatService iFlat;
	
	@Autowired
	IHotelService iHotel;
	
	@Autowired
	IFlatCategoryService iFlatCategory;

	/**
	 * Method that insert the {@link FlatDto} in parameters
	 * 
	 * @Param - {@link FlatDto} flat, the flat we want to insert
	 * @Return - the {@link flatDto} inserted
	 */
	
	@RequestMapping(value = "/insertFlat", method = RequestMethod.POST)
	public ResponseEntity<FlatDto> insertFlat(@RequestBody FlatDto flatDTO) {
		if(flatDTO.getFlatCategoryDto().getFlatCategoryId() != null) {
			FlatCategoryDto flatCategoryDto = iFlatCategory.findFlatCategoryById(flatDTO.getFlatCategoryDto().getFlatCategoryId());
			flatDTO.setFlatCategoryDto(flatCategoryDto);
		}
		
		FlatDto newFlat = iFlat.insertFlat(flatDTO);
		return new ResponseEntity<>(newFlat, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a list of {@link FlatDto} related to the {@link HotelDto} identifier in parameters
	 * 
	 * @param hotelId the {@link HotelDto}'s id
	 * @return the list of flat
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getListFlatToDisplay", method = RequestMethod.GET)
	public ResponseEntity<List<FlatDto>> getListFlatToDisplay(@RequestParam(value = "idHotel") Integer idHotel) {
		HotelDto hotelDto = iHotel.findHotelById(idHotel);
		List<FlatDto> listFlatDto = iFlat.findByHotelDto(hotelDto);
		return new ResponseEntity<>(listFlatDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a list of all the flats  {@link FlatDto}
	 * @return the list of all flats
	 */
	@GetMapping(value = "/") 
	public ResponseEntity<List<FlatDto>> getAllFlat() {
		List<FlatDto> listFlatDto = iFlat.findAllFlat();
		return new ResponseEntity<>(listFlatDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link FlatDto}
	 * 
	 * @param flatId the {@link FlatDto}'s id
	 * @return the {@link FlatDto}
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getFlat", method = RequestMethod.GET)
	public ResponseEntity<FlatDto> getFlat(@RequestParam(value = "idFlat") Integer idFlat) {
		FlatDto flatDto = iFlat.findFlatById(idFlat);
		return new ResponseEntity<>(flatDto, HttpStatus.OK);
	}
	
	/**
	 * Method that finds a {@link FlatDto}
	 * 
	 * @param flatId the {@link FlatDto}'s id in a string
	 * @return the {@link FlatDto}
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getFlatString", method = RequestMethod.GET)
	public ResponseEntity<FlatDto> getFlatString(@RequestParam(value = "i") String idFlat) {
		FlatDto flatDto = iFlat.findFlatById(Integer.parseInt(idFlat));
		return new ResponseEntity<>(flatDto, HttpStatus.OK);
	}
	
	/**
	 * Method that updates a {@link FlatDto}
	 * 
	 * @param flatDto the {@link FlatDto}
	 * @return the list of updated {@link FlatDto} in the same category
	 */
	@RequestMapping(value = "/updateFlat", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<FlatDto>> updateFlat(@RequestBody FlatDto flatDto) {

		List<FlatDto> listFlatDto = iFlat.findByFlatCategoryDtoAndHotelDto(flatDto.getFlatCategoryDto(), flatDto.getHotelDto());
		
		for(int i =0;i<listFlatDto.size(); i++) {
			iFlat.setFlatCapacityByFlat(listFlatDto.get(i), flatDto.getFlatCapacity());
			iFlat.setFlatDescriptionByFlat(listFlatDto.get(i), flatDto.getFlatDescription());
			iFlat.setFlatGuidanceByFlat(listFlatDto.get(i), flatDto.getFlatGuidance());
		}

		return new ResponseEntity<>(listFlatDto, HttpStatus.OK);
	}
	
	/**
	 * Method that deletes a {@link FlatDto} by an identifier
	 * 
	 * @Param - Integer idFlat, the flat we want to delete
	 * @return the deleted {@link FlatDto}
	 */
	@RequestMapping(value = "/deleteFlat", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<FlatDto> deleteFlat(@RequestBody Integer idFlat) {

		FlatDto flatDto = iFlat.findFlatById(idFlat);
		
		List<FlatDto> listFlatDto = iFlat.findByFlatCategoryDtoAndHotelDto(flatDto.getFlatCategoryDto(), flatDto.getHotelDto());
		
		for(int i =0;i<listFlatDto.size(); i++) {
			iFlat.deleteByFlatId(listFlatDto.get(i).getFlatId());
		}

		return new ResponseEntity<>(flatDto, HttpStatus.OK);
	}
	
	

}

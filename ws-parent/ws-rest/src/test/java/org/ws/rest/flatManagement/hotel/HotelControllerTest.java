package org.ws.rest.flatManagement.hotel;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.flatManagement.hotel.HotelController;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.FlatService;
import com.techconsulting.ws.service.core.service.flatManagement.hotel.HotelService;

/**
 * Test's Class for the {@link HotelController}
 */
public class HotelControllerTest {

	@InjectMocks
	HotelController hotelController;

	@Mock
	private HotelService mockHotelService;
	
	@Mock
	private FlatService mockFlatService;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testGetListHotelToDisplay. It tests the method getListHotelToDisplay of the {@link HotelController} class.
	 * It should return a list of {@link HotelDto}
	 */
	@Test
	public void testGetListHotelToDisplay() {
		List<HotelDto> listHotelDto = new ArrayList<>();
		Mockito.when(mockHotelService.findAll()).thenReturn(listHotelDto);

		assertEquals(HttpStatus.OK, hotelController.getListHotelToDisplay().getStatusCode());
	}
	
	/**
	 * Method testGetHotel. It tests the method getHotel of the {@link HotelController}
	 * class. It should return an {@link HotelDto}
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testGetHotel() throws ParseException {
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));

		Mockito.when(mockHotelService.findHotelById(Mockito.anyInt())).thenReturn(hotelDto);

		assertEquals(HttpStatus.OK, hotelController.getHotel(Mockito.anyInt()).getStatusCode());
	}
	
	/**
	 * Method testUpdateHotel. It tests the method updateHotel of the
	 * {@link HotelController} class. It should return an {@link HotelDto}
	 */

	@Test
	public void testUpdateHotel() {

		UserDto userDto = new UserDto();
		userDto.setUserId(Integer.valueOf(1));
		
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		hotelDto.setUserDto(userDto);

		Mockito.when(mockHotelService.findHotelById(Mockito.anyInt())).thenReturn(hotelDto);

		assertEquals(HttpStatus.OK, hotelController.updateHotel(hotelDto).getStatusCode());
	}
	
	/**
	 * Method testDeleteHotelFlat. It tests the method deleteHotelFlat of the
	 * {@link HotelController} class. It should return a list of {@link FlatDto}
	 */

	@Test
	public void testDeleteHotelFlat() {

		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(Integer.valueOf(1));
		flatDto.setHotelDto(hotelDto);
		
		List<FlatDto> listFlatDto = new ArrayList<>();
		listFlatDto.add(flatDto);

		Mockito.when(mockHotelService.findHotelById(Mockito.anyInt())).thenReturn(hotelDto);
		Mockito.when(mockFlatService.findByHotelDto(Mockito.any())).thenReturn(listFlatDto);

		assertEquals(HttpStatus.OK, hotelController.deleteHotelFlat(Mockito.anyInt()).getStatusCode());
	}
	
	/**
	 * Method testDeleteHotel. It tests the method deleteHotel of the
	 * {@link HotelController} class. It should return a {@link HotelDto}
	 */

	@Test
	public void testDeleteHotel() {

		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		

		Mockito.when(mockHotelService.findHotelById(Mockito.anyInt())).thenReturn(hotelDto);

		assertEquals(HttpStatus.OK, hotelController.deleteHotel(hotelDto.getHotelId()).getStatusCode());
	}
	
	/**
	 * Method testInsertHotel. It tests the method insertHotel
	 * of the {@link HotelController} class.
	 */
	@Test
	public void testInsertHotel() {
		HotelDto hotelDto = new HotelDto();
		Mockito.when(mockHotelService.insertHotel(Mockito.any())).thenReturn(hotelDto);

		assertEquals(HttpStatus.OK,
				hotelController.insertHotel(hotelDto).getStatusCode());
	}
}

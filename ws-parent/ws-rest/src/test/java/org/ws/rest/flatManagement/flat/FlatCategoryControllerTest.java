package org.ws.rest.flatManagement.flat;


import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.flatManagement.flat.FlatCategoryController;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.FlatCategoryService;


/**
 * Test's Class for the {@link FlatCategoryController}
 */
public class FlatCategoryControllerTest {

	@InjectMocks
	FlatCategoryController flatCategoryController;

	@Mock
	private FlatCategoryService mockFlatCategoryService;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testGetFlatCategories. It tests the method getFlatCategories of the {@link FlatCategoryController}
	 * class. It should return a list of {@link FlatCategoryDto}
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testinsertFlatCategory() throws ParseException {
		List<FlatCategoryDto> listFlatCategory = new ArrayList<>();

		Mockito.when(mockFlatCategoryService.findAll()).thenReturn(listFlatCategory);

		assertEquals(HttpStatus.OK, flatCategoryController.getFlatCategories().getStatusCode());
	}
	
	/**
	 * Method testInsertFlatCategory. It tests the method insertFlatCategory
	 * of the {@link FlatCategoryController} class.
	 */
	@Test
	public void testInsertFlatCategory() {
		FlatCategoryDto flatCategoryDto = new FlatCategoryDto();
		Mockito.when(mockFlatCategoryService.insertFlatCategory(Mockito.any())).thenReturn(flatCategoryDto);

		assertEquals(HttpStatus.OK,
				flatCategoryController.insertFlatCategory(flatCategoryDto).getStatusCode());
	}

}

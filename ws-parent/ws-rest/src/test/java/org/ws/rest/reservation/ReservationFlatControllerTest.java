package org.ws.rest.reservation;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.reservation.ReservationFlatController;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.service.common.FestivePeriodService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.DefineService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.FlatService;
import com.techconsulting.ws.service.core.service.reservation.flat.ReservationFlatService;


/**
 * Test's Class for the {@link ReservationFlatController}
 */
public class ReservationFlatControllerTest {

	@InjectMocks
	ReservationFlatController reservationFlatController;

	@Mock
	private ReservationFlatService mockReservationFlatService;
	
	@Mock
	private FlatService mockFlatService;
	
	@Mock
	private DefineService mockDefineService;
	
	@Mock
	private FestivePeriodService mockFestivePeriodService;
	
	@Mock
	private SimpleDateFormat formatter;
	

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	
	/**
	 * Method testGetReservationsFlatUser. It tests the method getReservationsFlatUser of the {@link ReservationFlatController} class.
	 * It should return a list of {@link ReservationFlatDto}
	 */
	@Test
	public void testGetReservationsFlatUser() {
		List<ReservationFlatDto> listReservationFlatDto = new ArrayList<>();
		Mockito.when(mockReservationFlatService.findByReservationFlatIdReservationDtoUserUserId(Mockito.anyInt())).thenReturn(listReservationFlatDto);
		assertEquals(HttpStatus.OK, reservationFlatController.getReservationsFlatUser(Mockito.anyInt()).getStatusCode());
	}

}

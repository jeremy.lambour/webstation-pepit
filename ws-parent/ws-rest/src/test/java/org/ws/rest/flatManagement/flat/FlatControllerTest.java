package org.ws.rest.flatManagement.flat;


import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.flatManagement.flat.FlatController;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatCategoryDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.flat.FlatDto;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.service.flatManagement.flat.FlatCategoryService;
import com.techconsulting.ws.service.core.service.flatManagement.flat.FlatService;
import com.techconsulting.ws.service.core.service.flatManagement.hotel.HotelService;

/**
 * Test's Class for the {@link FlatController}
 */
public class FlatControllerTest {

	@InjectMocks
	FlatController flatController;

	@Mock
	private FlatService mockFlatService;
	
	@Mock
	private HotelService mockHotelService;

	@Mock
	private FlatCategoryService mockFlatCategoryService;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testGetListFlatToDisplay. It tests the method getListFlatToDisplay of the {@link FlatController} class.
	 * It should return a list of {@link FlatDto}
	 */
	@Test
	public void testGetListFlatToDisplay() {
		List<FlatDto> listFlatDto = new ArrayList<>();
		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		Mockito.when(mockHotelService.findHotelById(Mockito.anyInt())).thenReturn(hotelDto);
		Mockito.when(mockFlatService.findByHotelDto(Mockito.any())).thenReturn(listFlatDto);
		assertEquals(HttpStatus.OK, flatController.getListFlatToDisplay(Mockito.any()).getStatusCode());
	}
	
	/**
	 * Method testGetFlat. It tests the method getFlat of the {@link FlatController}
	 * class. It should return an {@link FlatDto}
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testGetFlat() throws ParseException {
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(Integer.valueOf(1));

		Mockito.when(mockFlatService.findFlatById(Mockito.anyInt())).thenReturn(flatDto);

		assertEquals(HttpStatus.OK, flatController.getFlat(Mockito.anyInt()).getStatusCode());
	}
	

	
	/**
	 * Method testUpdateFlat. It tests the method updateFlat of the
	 * {@link FlatController} class. It should return an {@link FlatDto}
	 */

	@Test
	public void testUpdateFlat() {

		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(Integer.valueOf(1));
		flatDto.setHotelDto(hotelDto);
		
		List<FlatDto> listFlatDto = new ArrayList<>();
		listFlatDto.add(flatDto);

		Mockito.when(mockFlatService.findByFlatCategoryDtoAndHotelDto(Mockito.any(),Mockito.any())).thenReturn(listFlatDto);

		assertEquals(HttpStatus.OK, flatController.updateFlat(flatDto).getStatusCode());
	}
	
	/**
	 * Method testDeleteFlat. It tests the method deleteFlat of the
	 * {@link FlatController} class. It should return a {@link FlatDto}
	 */

	@Test
	public void testDeleteFlat() {

		HotelDto hotelDto = new HotelDto();
		hotelDto.setHotelId(Integer.valueOf(1));
		
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(Integer.valueOf(1));
		flatDto.setHotelDto(hotelDto);
		
		List<FlatDto> listFlatDto = new ArrayList<>();
		listFlatDto.add(flatDto);

		Mockito.when(mockFlatService.findFlatById(Mockito.anyInt())).thenReturn(flatDto);
		Mockito.when(mockFlatService.findByFlatCategoryDtoAndHotelDto(Mockito.any(),Mockito.any())).thenReturn(listFlatDto);

		assertEquals(HttpStatus.OK, flatController.deleteFlat(flatDto.getFlatId()).getStatusCode());
	}
	
	/**
	 * Method testInsertFlat. It tests the method insertFlat
	 * of the {@link FlatController} class.
	 */
	@Test
	public void testInsertFlat() {
		FlatDto flatDto = new FlatDto();
		FlatCategoryDto flatCategoryDto = new FlatCategoryDto();
		flatCategoryDto.setFlatCategoryId(Integer.valueOf(1));
		flatDto.setFlatCategoryDto(flatCategoryDto);
		Mockito.when(mockFlatCategoryService.findFlatCategoryById(Mockito.anyInt())).thenReturn(flatCategoryDto);
		Mockito.when(mockFlatService.insertFlat(Mockito.any())).thenReturn(flatDto);

		assertEquals(HttpStatus.OK,
				flatController.insertFlat(flatDto).getStatusCode());
	}
	
	/**
	 * Method testGetFlatString. It tests the method getFlatString of the {@link FlatController}
	 * class. It should return an {@link FlatDto}
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testGetFlatString() throws ParseException {
		FlatDto flatDto = new FlatDto();
		flatDto.setFlatId(Integer.valueOf(1));

		Mockito.when(mockFlatService.findFlatById(Mockito.anyInt())).thenReturn(flatDto);

		assertEquals(HttpStatus.OK, flatController.getFlatString("1").getStatusCode());
	}

}

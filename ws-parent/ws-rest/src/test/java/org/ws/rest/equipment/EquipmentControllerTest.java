package org.ws.rest.equipment;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import com.techconsulting.ws.rest.controller.equipment.EquipmentController;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.service.equipment.EquipmentService;
import com.techconsulting.ws.service.core.service.shop.ShopService;


/**
 * Test's Class for the {@link EquipmentController}
 */
public class EquipmentControllerTest {

	@InjectMocks
	EquipmentController equipmentController;

	@Mock
	private EquipmentService mockEquipmentService;
	
	@Mock
	private ShopService mockShopService;

	/**
	 * Initialisation of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testUpdateEquipment. It tests the method updateEquipment of the
	 * {@link EquipmentController} class. It should return an {@link EquipmentDto}
	 */

	@Test
	public void testUpdateEquipment() {

		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(22));

		EquipmentDto equipmentDto = new EquipmentDto();
		equipmentDto.setEquipmentId(Integer.valueOf(22));
		equipmentDto.setShop(shopDto);

		Mockito.when(mockEquipmentService.findEquipmentById(Mockito.anyInt())).thenReturn(equipmentDto);

		assertEquals(HttpStatus.OK, equipmentController.updateEquipment(equipmentDto).getStatusCode());
	}

	/**
	 * Method testGetEquipment. It tests the method getEquipment of the {@link EquipmentController}
	 * class. It should return an {@link EquipmentDto}
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testGetEquipment() throws ParseException {
		EquipmentDto equipmentDto = new EquipmentDto();
		equipmentDto.setEquipmentId(Integer.valueOf(5));

		Mockito.when(mockEquipmentService.findEquipmentById(Mockito.anyInt())).thenReturn(equipmentDto);

		assertEquals(HttpStatus.OK, equipmentController.getEquipment(Integer.valueOf(5)).getStatusCode());
	}
	
	/**
	 * Method testGetListEquipmentToDisplay. It tests the method getListEquipmentToDisplay of the {@link EquipmentController} class.
	 * It should return a list of {@link EquipmentDto}
	 */
	@Test
	public void testGetListEquipmentToDisplay() {
		List<EquipmentDto> listEquipmentDto = new ArrayList<>();
		Mockito.when(mockEquipmentService.findAll()).thenReturn(listEquipmentDto);

		assertEquals(HttpStatus.OK, equipmentController.getListEquipmentToDisplay().getStatusCode());
	}
	
	/**
	 * Method testInsertShop. It tests the method insertShop
	 * of the {@link EquipmentController} class.
	 */
	@Test
	public void testInsertEquipment() {
		EquipmentDto equipmentDto = new EquipmentDto();
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(1));
		equipmentDto.setShop(shopDto);
		Mockito.when(mockShopService.findShopById(Mockito.anyInt())).thenReturn(shopDto);
		Mockito.when(mockEquipmentService.insertEquipment(Mockito.any())).thenReturn(equipmentDto);

		assertEquals(HttpStatus.OK,
				equipmentController.insertEquipment(equipmentDto).getStatusCode());
	}
	
	/**
	 * Method testGetEquipmentByShopId. It tests the method getEquipmentByShopId of the {@link EquipmentController}
	 * class. It should return an {@link EquipmentDto}
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testGetEquipmentByShopId() throws ParseException {
		List<EquipmentDto> listEquipmentDto = new ArrayList<>();

		Mockito.when(mockEquipmentService.findEquipmentByShopId(Mockito.anyInt())).thenReturn(listEquipmentDto);

		assertEquals(HttpStatus.OK, equipmentController.getEquipmentByShopId(Integer.valueOf(1)).getStatusCode());
	}
	
	/**
	 * Method testDeleteEquipment. It tests the method deleteEquipment of the
	 * {@link EquipmentController} class.
	 */
	@Test
	public void testDeleteEquipment() {
		EquipmentDto equipmentDto = new EquipmentDto();
		Mockito.when(mockEquipmentService.findEquipmentById(Mockito.anyInt())).thenReturn(equipmentDto);
		assertEquals(HttpStatus.OK, equipmentController.deleteEquipment(Integer.valueOf(1)).getStatusCode());
	}
	

}

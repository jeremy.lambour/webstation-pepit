package com.techconsulting.ws.rest.controller.common;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.common.ScheduleController;
import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;
import com.techconsulting.ws.service.core.service.common.ScheduleService;

/**
 * Test's Class for the {@link ScheduleController}
 */
public class ScheduleControllerTest {

	@InjectMocks
	ScheduleController scheduleController;

	@Mock
	private ScheduleService mockScheduleService;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertSchedule. It tests the method insertSchedule of the
	 * {@link ScheduleController} class. It should return a {@link ScheduleDTO}
	 */
	@Test
	public void testInsertSchedule() {
		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScheduleCloseHour("");
		scheduleDto.setScheduleDay("");
		scheduleDto.setScheduleId(Integer.valueOf(1));
		scheduleDto.setScheduleOpenHour("");
		
		Mockito.when(mockScheduleService.insertSchedule(Mockito.any())).thenReturn(scheduleDto);

		assertEquals(scheduleController.insertSchedule(scheduleDto).getStatusCode(), HttpStatus.OK);
	}
	
	/**
	 * Method testUpdateSchedule. It tests the method updateSchedule of the
	 * {@link ScheduleController} class. It should return a {@link ScheduleDto}
	 */
	@Test
	public void testUpdateSchedule() {
		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScheduleId(Integer.valueOf(1));
		scheduleDto.setScheduleCloseHour("close");
		scheduleDto.setScheduleOpenHour("open");
		scheduleDto.setScheduleDay("day");

		Mockito.when(mockScheduleService.findScheduleById(Mockito.anyInt())).thenReturn(scheduleDto);

		assertEquals(HttpStatus.OK, scheduleController.updateSchedule(scheduleDto).getStatusCode());
	}
	
	/**
	 * Method testUpdateSchedule. It tests the method updateSchedule of the
	 * {@link ScheduleController} class. It should return a {@link ScheduleDto}
	 */
	@Test
	public void testDeleteSchedule() {
		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScheduleId(Integer.valueOf(1));
		scheduleDto.setScheduleCloseHour("close");
		scheduleDto.setScheduleOpenHour("open");
		scheduleDto.setScheduleDay("day");

		Mockito.when(mockScheduleService.findScheduleById(Mockito.anyInt())).thenReturn(scheduleDto);

		assertEquals(HttpStatus.OK, scheduleController.deleteSchedule(Mockito.anyInt()).getStatusCode());
	}

}

package com.techconsulting.ws.rest.controller.shop;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.shop.ShopController;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.service.shop.ShopService;

/**
 * Test's Class for the {@link ShopController}
 */
public class ShopControllerTest {

	@InjectMocks
	ShopController shopController;

	@Mock
	private ShopService mockShopService;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testUpdateShop. It tests the method updateShop of the
	 * {@link ShopController} class. It should return an {@link ShopDto}
	 */

	@Test
	public void testUpdateShop() {

		UserDto userDto = new UserDto();
		userDto.setUserId(Integer.valueOf(1));

		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(26));
		shopDto.setUserDTO(userDto);

		Mockito.when(mockShopService.findShopById(Mockito.anyInt())).thenReturn(shopDto);

		assertEquals(HttpStatus.OK, shopController.updateShop(shopDto).getStatusCode());
	}

	/**
	 * Method testGetShop. It tests the method getShop of the {@link ShopController}
	 * class. It should return an {@link ShopDto}
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testGetShop() throws ParseException {
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(26));

		Mockito.when(mockShopService.findShopById(Mockito.anyInt())).thenReturn(shopDto);

		assertEquals(HttpStatus.OK, shopController.getShop(Integer.valueOf(26)).getStatusCode());
	}
	
	/**
	 * Method testGetListShopToDisplay. It tests the method getListShopToDisplay of the {@link ShopController} class.
	 * It should return a list of {@link ShopDto}
	 */
	@Test
	public void testGetListShopToDisplay() {
		List<ShopDto> listShopDto = new ArrayList<>();
		Mockito.when(mockShopService.findAll()).thenReturn(listShopDto);

		assertEquals(HttpStatus.OK, shopController.getListShopToDisplay().getStatusCode());
	}
	
	/**
	 * Method testInsertShop. It tests the method insertShop
	 * of the {@link ShopController} class.
	 */
	@Test
	public void testInsertShop() {
		ShopDto shopDto = new ShopDto();
		Mockito.when(mockShopService.insertShop(shopDto)).thenReturn(shopDto);

		assertEquals(HttpStatus.OK,
				shopController.insertShop(shopDto).getStatusCode());
	}
	
	/**
	 * Method testDeleteShop. It tests the method deleteShop of the
	 * {@link ShopController} class.
	 */
	@Test
	public void testDeleteShop() {
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(26));

		Mockito.when(mockShopService.findShopById(Mockito.anyInt())).thenReturn(shopDto);
		assertEquals(HttpStatus.OK, shopController.deleteShop(Mockito.anyInt()).getStatusCode());
	}
	

}

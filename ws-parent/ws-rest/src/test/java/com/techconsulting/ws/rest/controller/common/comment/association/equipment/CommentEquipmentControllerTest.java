package com.techconsulting.ws.rest.controller.common.comment.association.equipment;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.equipment.CommentEquipmentDTO;
import com.techconsulting.ws.service.core.modeldto.equipment.EquipmentDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.common.comment.ICommentService;
import com.techconsulting.ws.service.core.service.common.comment.association.equipment.ICommentEquipmentService;

/**
 * Test's Class for the {@link CommentEquipmentController}
 */
public class CommentEquipmentControllerTest {

	@InjectMocks
	CommentEquipmentController commentEquipController;
	
	@Mock
	private ICommentEquipmentService mockCommentEquipService;
	
	@Mock
	ICommentService mockCommentService;
	
	CommentEquipmentDTO commentEquipTestDto;
	
	CommentDTO commentTestDto;
	
	EquipmentDto equipmentTestDto;
	
	UserDto userTestDto;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Init the model use by the test
	 */
	@Before
	public void initModel() {
		this.userTestDto = new UserDto();
		this.userTestDto.setUserId(0);
		
		
		this.commentTestDto = new CommentDTO();
		this.commentTestDto.setCommentId(0);
		this.commentTestDto.setContent("");
		this.commentTestDto.setFrom(userTestDto);
		
		this.equipmentTestDto = new EquipmentDto();
		this.equipmentTestDto.setEquipmentId(0);	
		
		this.commentEquipTestDto = new CommentEquipmentDTO();
		this.commentEquipTestDto.setCommentDto(this.commentTestDto);
		this.commentEquipTestDto.setEquipmentDto(this.equipmentTestDto);
	}
	
	/**
	 * Method testInsertCommentEquipment. It tests the method insertCommentEquipment of the
	 * {@link CommentEquipmentController} class. It should return a {@link CommentEquipmentDto}
	 */
	@Test
	public void testInsertCommentEquipment() {
		Mockito.when(mockCommentService.insertComment(this.commentTestDto)).thenReturn(this.commentTestDto);
		Mockito.when(mockCommentEquipService.insertCommentEquipment(this.commentEquipTestDto)).thenReturn(this.commentEquipTestDto);
		assertEquals(HttpStatus.OK, commentEquipController.insertCommentEquipmentDto(this.commentEquipTestDto).getStatusCode());
	}
	
	/**
	 * Method testGetListCommentEquipmentDTO. It tests the method getListCommentEquipmentDTO of the
	 * {@link CommentEquipmentController} class. It should return a list of {@link CommentEquipmentDto}
	 */
	@Test
	public void testGetListCommentEquipmentDTO() {
		Mockito.when(mockCommentEquipService.findByCommentEquipmentPkEquipment(this.commentEquipTestDto.getEquipmentDto().getEquipmentId())).thenReturn(new ArrayList<CommentEquipmentDTO>());
		assertEquals(HttpStatus.OK, commentEquipController.getListCommentEquipmentDTO(this.commentEquipTestDto.getEquipmentDto().getEquipmentId()).getStatusCode());
	}
	
	/**
	 * Method testDeleteCommentEquipmentDTO. It tests the method deleteCommentEquipmentDTO of the
	 * {@link CommentEquipmentController} class. It should return a {@link boolean} if the element is delete.
	 */
	@Test
	public void testDeleteCommentEquipmentDTO() {
		Mockito.when(mockCommentEquipService.deleteByCommentEquipment(this.commentEquipTestDto)).thenReturn(Mockito.anyBoolean());
		assertEquals(HttpStatus.OK, commentEquipController.deleteCommentEquipmentDTO(this.commentEquipTestDto).getStatusCode());
	}
	
}

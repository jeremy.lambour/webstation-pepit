package com.techconsulting.ws.rest.controller.auth;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.user.UserService;


/**
 * Test's Class for the {@link UserController}
 */
public class UserControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	UserService iUserServ;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testFindByUsername. It tests the method
	 * findByUsername of the {@link UserController}
	 * class.
	 */
	@Test
	public void testFindByUsername() {
		UserDto userDto = new UserDto();
		Mockito.when(iUserServ.findUserByUsername(Mockito.anyString())).thenReturn(userDto);
		assertEquals(HttpStatus.OK, userController.findByUsername("").getStatusCode());
	}

}

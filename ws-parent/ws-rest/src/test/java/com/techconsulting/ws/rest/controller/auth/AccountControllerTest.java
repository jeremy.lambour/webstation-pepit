package com.techconsulting.ws.rest.controller.auth;

import static org.junit.Assert.assertEquals;

import java.util.Base64;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.accountcreation.IAccountService;

public class AccountControllerTest {

	@InjectMocks
	AccountController accountController;

	@Mock
	private IAccountService mockAccountService;

	@Mock
	private Base64 mocBase64;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testCreateAccount This method tests the method createAccount of the
	 * {@link AccountController} class.
	 */
	@Test
	public void testCreateAccount() {
		UserDto newUserDTO = new UserDto();
		newUserDTO.setPassword(Base64.getEncoder().encodeToString("".getBytes()));
		Mockito.when(mockAccountService.createAccount(Mockito.any(UserDto.class))).thenReturn(newUserDTO);
		assertEquals(HttpStatus.OK, accountController.createUser(newUserDTO).getStatusCode());
		assertEquals(HttpStatus.NO_CONTENT, accountController.createUser(null).getStatusCode());
	}
	
	/**
	 * Method testUpdateUser This method tests the method updateUser of the
	 * {@link AccountController} class.
	 */
	@Test
	public void testUpdateUser() {
		UserDto newUserDTO = new UserDto();
		Mockito.when(mockAccountService.updateUser(Mockito.any())).thenReturn(newUserDTO);
		assertEquals(HttpStatus.OK, accountController.updateUser(newUserDTO).getStatusCode());
	}

}

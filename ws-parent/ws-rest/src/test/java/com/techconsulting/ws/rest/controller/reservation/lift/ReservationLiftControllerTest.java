package com.techconsulting.ws.rest.controller.reservation.lift;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.reservation.equipment.ReservationEquipmentController;
import com.techconsulting.ws.service.core.modeldto.equipment.SkiLiftDto;
import com.techconsulting.ws.service.core.modeldto.reservation.lift.ReservationLiftDto;
import com.techconsulting.ws.service.core.modeldto.user.AgeRangeDto;
import com.techconsulting.ws.service.core.service.accountcreation.AgeRangeService;
import com.techconsulting.ws.service.core.service.equipment.skilift.SkiLiftService;
import com.techconsulting.ws.service.core.service.reservation.equipment.skilift.SkiLiftReservationService;

/**
 * Test's Class for the {@link ReservationEquipmentController}
 */
public class ReservationLiftControllerTest {

	@InjectMocks
	ReservationSkiLiftController reservliftController;

	@Mock
	SkiLiftService mockISkiLift;

	@Mock
	SkiLiftReservationService mockIReservationSkiLift;

	@Mock
	AgeRangeService mockIAgeRangeService;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testInsertReservSkiLift. It tests the method insertReservSkiLift
	 * of the {@link ReservationSkiLiftController} class.
	 */
	@Test
	public void testInsertReservSkiLift() {
		ReservationLiftDto reservationSkiLiftDto = new ReservationLiftDto();
		Mockito.when(mockIReservationSkiLift.insertReservSkiLift(Mockito.any(ReservationLiftDto.class))).thenReturn(reservationSkiLiftDto);
		assertEquals(HttpStatus.OK, reservliftController.insertReservSkiLift(reservationSkiLiftDto).getStatusCode());
	}
	
	/**
	 * Method testGetReservLift. It tests the method getReservLift
	 * of the {@link ReservationSkiLiftController} class.
	 */
	@Test
	public void testGetReservLift() {
		List<ReservationLiftDto> reservationlistSkiLift = new ArrayList<>();
		Mockito.when(mockIReservationSkiLift.getListReservSkiLiftUser(Mockito.anyInt())).thenReturn(reservationlistSkiLift);
		assertEquals(HttpStatus.OK, reservliftController.getReservLift(1).getStatusCode());
	}
	
	/**
	 * Method testDeleteReservation. It tests the method deleteReservation
	 * of the {@link ReservationSkiLiftController} class.
	 */
	@Test
	public void testDeleteReservation() {
		ReservationLiftDto reservationSkiLiftDto = new ReservationLiftDto();
		assertEquals(HttpStatus.OK, reservliftController.deleteReservation(reservationSkiLiftDto).getStatusCode());
	}
	
	/**
	 * Method testGetAll. It tests the method getAll
	 * of the {@link ReservationSkiLiftController} class.
	 */
	@Test
	public void testGetAll() {
		List<SkiLiftDto> listSkiLiftDto = new ArrayList<>();
		Mockito.when(mockISkiLift.getListSkiLift()).thenReturn(listSkiLiftDto);
		assertEquals(HttpStatus.OK, reservliftController.getAll().getStatusCode());
	}
	
	/**
	 * Method testGetAgeRange. It tests the method getAgeRange
	 * of the {@link ReservationSkiLiftController} class.
	 */
	@Test
	public void testGetAgeRange() {
		AgeRangeDto ageRange = new AgeRangeDto();
		Mockito.when(mockIAgeRangeService.findAgeRange(Mockito.anyInt())).thenReturn(ageRange);
		assertEquals(HttpStatus.OK, reservliftController.getAgeRange(1).getStatusCode());
	}

}

package com.techconsulting.ws.rest.controller.shop;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.rest.controller.shop.ShopScheduleController;
import com.techconsulting.ws.service.core.modeldto.common.ScheduleDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopDto;
import com.techconsulting.ws.service.core.modeldto.shop.ShopScheduleDto;
import com.techconsulting.ws.service.core.service.common.ScheduleService;
import com.techconsulting.ws.service.core.service.shop.ShopScheduleService;
import com.techconsulting.ws.service.core.service.shop.ShopService;

/**
 * Test's Class for the {@link ShopScheduleController}
 */
public class ShopScheduleControllerTest {
	
	@InjectMocks
	ShopScheduleController shopScheduleController;

	@Mock
	private ShopScheduleService mockShopScheduleService;
	
	@Mock
	private ScheduleService mockScheduleService;
	
	@Mock
	private ShopService mockShopService;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testInsertShopSchedule. It tests the method insertShopSchedule of the
	 * {@link ShopScheduleController} class. It should return a {@link ShopScheduleDTO}
	 */
	@Test
	public void testInsertShopSchedule() {
		ShopScheduleDto shopScheduleDto = new ShopScheduleDto();
		
		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScheduleId(Integer.valueOf(1));
		
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(1));
		
		shopScheduleDto.setScheduleDto(scheduleDto);
		shopScheduleDto.setShopDto(shopDto);
		Mockito.when(mockScheduleService.findScheduleById(Mockito.anyInt())).thenReturn(scheduleDto);
		Mockito.when(mockShopScheduleService.insertShopSchedule(shopScheduleDto)).thenReturn(shopScheduleDto);
		
		Mockito.when(mockShopService.findShopById(Mockito.anyInt())).thenReturn(shopDto);
		assertEquals(HttpStatus.OK,
				shopScheduleController.insertShopSchedule(1, 1).getStatusCode());
	}
	
	/**
	 * Method testGetShopSchedule. It tests the method getShopSchedule of the {@link ShopScheduleController}
	 * class. It should return an {@link ShopScheduleDto}
	 * 
	 */
	@Test
	public void testGetShopSchedule() {
		List<ShopScheduleDto> listShopScheduleDto = new ArrayList<>();
		
		ShopScheduleDto shopScheduleDto = new ShopScheduleDto();
		
		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScheduleId(Integer.valueOf(1));
		
		ShopDto shopDto = new ShopDto();
		shopDto.setShopId(Integer.valueOf(1));
		
		shopScheduleDto.setScheduleDto(scheduleDto);
		shopScheduleDto.setShopDto(shopDto);
		
		listShopScheduleDto.add(shopScheduleDto);

		Mockito.when(mockShopScheduleService.findByShopScheduleIdShopEntityShopId(Mockito.anyInt())).thenReturn(listShopScheduleDto);

		assertEquals(HttpStatus.OK, shopScheduleController.getShopSchedule(Integer.valueOf(1)).getStatusCode());
	}
	
	/**
	 * Method testDeleteShopschedule. It tests the method deleteShopschedule of the
	 * {@link ShopScheduleController} class.
	 */
	@Test
	public void testDeleteShopschedule() {
		List<ShopScheduleDto> listShopScheduleDto = new ArrayList<>();
		
		Mockito.when(mockShopScheduleService.findByShopScheduleIdShopEntityShopId(Mockito.anyInt())).thenReturn(listShopScheduleDto);
		
		assertEquals(HttpStatus.OK, shopScheduleController.deleteShopschedule(Mockito.anyInt()).getStatusCode());
	}

}

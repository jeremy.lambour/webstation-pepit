package com.techconsulting.ws.rest.controller.reservation.equipment;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.service.core.modeldto.reservation.equipment.ReservationEquipmentDto;
import com.techconsulting.ws.service.core.service.reservation.equipment.EquipmentReservationService;

/**
 * Test's Class for the {@link ReservationEquipmentController}
 */
public class ReservationEquipmentControllerTest {

	@InjectMocks
	ReservationEquipmentController reservationEquipController;

	@Mock
	EquipmentReservationService iEquipReservServ;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method testInsertReservEquipment. It tests the method insertReservEquipment
	 * of the {@link ReservationEquipmentController} class.
	 */
	@Test
	public void testInsertReservEquipment() {
		ReservationEquipmentDto reservEquipmentDto = new ReservationEquipmentDto();
		Mockito.when(iEquipReservServ.insertReservEquip(reservEquipmentDto)).thenReturn(reservEquipmentDto);

		assertEquals(HttpStatus.OK,
				reservationEquipController.insertReservEquipment(reservEquipmentDto).getStatusCode());
	}

	/**
	 * Method testGetListReservationEquipment. It tests the method
	 * getListReservationEquipment of the {@link ReservationEquipmentController}
	 * class.
	 */
	@Test
	public void testGetListReservationEquipment() {
		List<ReservationEquipmentDto> listReservEquip = new ArrayList<>();
		Mockito.when(iEquipReservServ.getListReservEquipUser(Mockito.anyInt())).thenReturn(listReservEquip);
		assertEquals(HttpStatus.OK, reservationEquipController.getListReservationEquipment(1).getStatusCode());
	}

	/**
	 * Method testCheckAvailibity. It tests the method checkAvailibity of the
	 * {@link ReservationEquipmentController} class.
	 */
	@Test
	public void testCheckAvailibity() {
		ReservationEquipmentDto reservEquipmentDto = new ReservationEquipmentDto();
		boolean check = true;
		Mockito.when(iEquipReservServ.checkAvailability(reservEquipmentDto)).thenReturn(check);
		assertEquals(HttpStatus.OK, reservationEquipController.checkAvailibity(reservEquipmentDto).getStatusCode());
	}

	/**
	 * Method testDeleteReservation. It tests the method deleteReservation of the
	 * {@link ReservationEquipmentController} class.
	 */
	@Test
	public void testDeleteReservation() {
		ReservationEquipmentDto reservEquipmentDto = new ReservationEquipmentDto();
		assertEquals(HttpStatus.OK, reservationEquipController.deleteReservation(reservEquipmentDto).getStatusCode());
	}
}

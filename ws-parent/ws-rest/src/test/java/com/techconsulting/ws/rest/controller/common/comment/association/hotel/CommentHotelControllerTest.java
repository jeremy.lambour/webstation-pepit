package com.techconsulting.ws.rest.controller.common.comment.association.hotel;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.service.core.modeldto.common.comment.CommentDTO;
import com.techconsulting.ws.service.core.modeldto.common.comment.association.hotel.CommentHotelDTO;
import com.techconsulting.ws.service.core.modeldto.flatManagement.hotel.HotelDto;
import com.techconsulting.ws.service.core.modeldto.user.UserDto;
import com.techconsulting.ws.service.core.service.common.comment.ICommentService;
import com.techconsulting.ws.service.core.service.common.comment.association.hotel.ICommentHotelService;

public class CommentHotelControllerTest {
	
	@InjectMocks
	CommentHotelController commentHotelController;
	
	@Mock
	private ICommentHotelService mockCommentHotelService;
	
	@Mock
	ICommentService mockCommentService;
	
	CommentHotelDTO commentHotelTestDto;
	
	CommentDTO commentTestDto;
	
	HotelDto hotelTestDto;
	
	UserDto userTestDto;
	
	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Init the model use by the test
	 */
	@Before
	public void initModel() {
		this.userTestDto = new UserDto();
		this.userTestDto.setUserId(0);
		
		
		this.commentTestDto = new CommentDTO();
		this.commentTestDto.setCommentId(0);
		this.commentTestDto.setContent("");
		this.commentTestDto.setFrom(userTestDto);
		
		this.hotelTestDto = new HotelDto();
		this.hotelTestDto.setHotelId(0);	
		
		this.commentHotelTestDto = new CommentHotelDTO();
		this.commentHotelTestDto.setCommentDto(this.commentTestDto);
		this.commentHotelTestDto.setHotelDto(this.hotelTestDto);
	}
	
	/**
	 * Method testInsertCommentHotel. It tests the method insertCommentHotel of the
	 * {@link CommentHotelController} class. It should return a {@link CommentHotelDTO}
	 */
	@Test
	public void testInsertCommentHotel() {
		Mockito.when(mockCommentService.insertComment(this.commentTestDto)).thenReturn(this.commentTestDto);
		Mockito.when(mockCommentHotelService.insertCommentHotel(this.commentHotelTestDto)).thenReturn(this.commentHotelTestDto);
		assertEquals(HttpStatus.OK, commentHotelController.insertCommentHotel(this.commentHotelTestDto).getStatusCode());
	}
	
	/**
	 * Method testGetListCommentHotelDTO. It tests the method getListCommentHotelDTO of the
	 * {@link CommentHotelController} class. It should return a list of {@link CommentHotelDTO}
	 */
	@Test
	public void testGetListCommentHotelDTO() {
		Mockito.when(mockCommentHotelService.findListCommentHotelByIdHotel(this.commentHotelTestDto.getHotelDto().getHotelId())).thenReturn(new ArrayList<CommentHotelDTO>());
		assertEquals(HttpStatus.OK, commentHotelController.getListCommentHotel(this.commentHotelTestDto.getHotelDto().getHotelId()).getStatusCode());
	}
	
	/**
	 * Method testDeleteCommentHotelDTO. It tests the method deleteCommentHotelDTO of the
	 * {@link CommentHotelController} class. It should return a {@link boolean} if the element is delete.
	 */
	@Test
	public void testDeleteCommentHotelDTO() {
		Mockito.when(mockCommentHotelService.deleteByCommentHotel(this.commentHotelTestDto)).thenReturn(Mockito.anyBoolean());
		assertEquals(HttpStatus.OK, commentHotelController.deleteCommentHotel(this.commentHotelTestDto).getStatusCode());
	}
	
}

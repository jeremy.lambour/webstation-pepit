package com.techconsulting.ws.rest.controller.reservation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.techconsulting.ws.service.core.modeldto.common.FestivePeriodDto;
import com.techconsulting.ws.service.core.modeldto.reservation.flat.ReservationFlatDto;
import com.techconsulting.ws.service.core.service.reservation.flat.ReservationFlatService;

/**
 * Test's Class for the {@link ReservationFlatController}
 */
public class ReservationFlatControllerTest {

	@InjectMocks
	ReservationFlatController reservationFlatController;

	@Mock
	ReservationFlatService iReservFlatServ;

	/**
	 * Init of the test's mock
	 */
	@Before
	public void initTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Method testInsertReservationFlat. It tests the method insertReservationFlat
	 * of the {@link ReservationFlatController} class.
	 */
	@Test
	public void testInsertReservationFlat() {
		ReservationFlatDto reservFlatDto = new ReservationFlatDto();
		Mockito.when(iReservFlatServ.insertReservationFlat(Mockito.any())).thenReturn(reservFlatDto);

		assertEquals(HttpStatus.OK,
				reservationFlatController.insertReservationFlat(reservFlatDto).getStatusCode());
	}
	
	/**
	 * Method testCheckFestivePeriod. It tests the method checkFestivePeriod
	 * of the {@link ReservationFlatController} class.
	 */
	@Test
	public void testCheckFestivePeriod() {
		List<FestivePeriodDto> listFestivePeriodDto = new ArrayList<>();
		FestivePeriodDto festivePeriodDto = new FestivePeriodDto();
		assertEquals(festivePeriodDto.getClass(),
				reservationFlatController.checkFestivePeriod(listFestivePeriodDto, new Date(), new Date()).getClass());
	}
	
	/**
	 * Method testGetReservationsFlatUser. It tests the method getReservationsFlatUser
	 * of the {@link ReservationFlatController} class.
	 */
	@Test
	public void testGetReservationsFlatUser() {
		List<ReservationFlatDto> listReservationFlatDto = new ArrayList<>();
		Mockito.when(iReservFlatServ.findByReservationFlatIdReservationDtoUserUserId(Mockito.anyInt())).thenReturn(listReservationFlatDto);

		assertEquals(HttpStatus.OK,
				reservationFlatController.getReservationsFlatUser(Mockito.anyInt()).getStatusCode());
	}

}

package com.techconsulting.ws.dao.repository.skiingSchool;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.skiingSchool.Lesson;

/**
 * {@link LessonRepository}
 * @author Jeremy 
 *
 */
@Repository
public interface LessonRepository extends JpaRepository<Lesson,Integer> {

	/**
	 * Find {@link Lesson} entities from their planning identifier
	 * @param id
	 * @return
	 */
	List<Lesson> findByLessonPlanningPlanningId(Integer id);
}

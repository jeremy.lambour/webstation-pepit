package com.techconsulting.ws.dao.repository.equipment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.equipment.SkiLift;

/**
 * JpaRepository use to question SkiLift table in database
 * @author Paul
 *
 */
@Repository
public interface SkiLiftRepository extends JpaRepository<SkiLift, Integer>{
	List<SkiLift> findSkiLiftByAgeRangeAgeRangeIdAndDomainAndPassTypePassTypeId(int ageRangeIdInt, String domain, int passTypeIdInt);
	List<SkiLift> findSkiLiftByAgeRangeAgeRangeIdAndPassTypePassTypeId(int ageRangeIdInt, int passTypeIdInt);

}

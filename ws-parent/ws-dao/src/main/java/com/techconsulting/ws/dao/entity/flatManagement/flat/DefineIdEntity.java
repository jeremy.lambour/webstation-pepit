package com.techconsulting.ws.dao.entity.flatManagement.flat;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.techconsulting.ws.dao.entity.common.FestivePeriodEntity;

/**
 * The DefineIdEntity class. This class is an association class between
 * {@link FlatEntity} and {@link FestivePeriodEntity}
 */
@Embeddable
public class DefineIdEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * The {@link FlatEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "flat_id", referencedColumnName = "flat_id")
	private FlatEntity flat;

	/**
	 * The {@link FestivePeriodEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "festive_period_id", referencedColumnName = "festive_period_id")
	private FestivePeriodEntity festivePeriodEntity;
	
	public DefineIdEntity() {
		super();
	}

	/**
	 * @return the flat
	 */
	public FlatEntity getFlat() {
		return flat;
	}

	/**
	 * @param flat the flat to set
	 */
	public void setFlat(FlatEntity flat) {
		this.flat = flat;
	}

	/**
	 * @return the festivePeriodEntity
	 */
	public FestivePeriodEntity getFestivePeriodEntity() {
		return festivePeriodEntity;
	}

	/**
	 * @param festivePeriodEntity the festivePeriodEntity to set
	 */
	public void setFestivePeriodEntity(FestivePeriodEntity festivePeriodEntity) {
		this.festivePeriodEntity = festivePeriodEntity;
	}
	
	

}

package com.techconsulting.ws.dao.repository.common.comment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.common.comment.Comment;

/**
 * JpaRepository use to question Comment table in database
 * @author Paul
 *
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

}


package com.techconsulting.ws.dao.repository.flatManagement.flat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.common.FestivePeriodEntity;
import com.techconsulting.ws.dao.entity.flatManagement.flat.DefineEntity;
import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;

/**
 * JpaRepository use to question Define table in database
 * @author gaillardc
 *
 */
public interface DefineRepository extends JpaRepository<DefineEntity, Integer>{
	DefineEntity findByDefineIdEntityFestivePeriodEntityAndDefineIdEntityFlat(FestivePeriodEntity festivePeriodEntity, FlatEntity flatEntity);
	List<DefineEntity> findDefineEntityByDefineIdEntityFlatFlatId(Integer idFlat);
}

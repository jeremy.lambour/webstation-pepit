package com.techconsulting.ws.dao.repository.skiingSchool;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.skiingSchool.Planning;

/**
 * {@link PlanningRepository}
 * @author Jeremy
 *
 */
@Repository
public interface PlanningRepository extends JpaRepository<Planning,Integer> {

	/**
	 * Find {@link Planning} by monitor id and season id
	 * @param monitorId
	 * @param seasonId
	 * @return
	 */
	public List<Planning> findByPlanningMonitorUserIdAndPlanningSeasonSeasonId(Integer monitorId,Integer seasonId);
}

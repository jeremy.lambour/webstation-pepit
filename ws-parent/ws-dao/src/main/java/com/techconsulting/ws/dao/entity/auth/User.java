package com.techconsulting.ws.dao.entity.auth;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Entity which represent User in application
 * @author Jeremy
 *
 */
@Entity
@SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1)
@Table(name = "user", schema = "public")
public class User implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * user id
	 */
	@GeneratedValue(generator = "user_user_id_seq")
	@Column(name = "user_id")
	private @Id Integer userId;

	/**
	 * user's username
	 */
	@Column(name = "login")
	private String username;

	/**
	 * user's password
	 */
	@Column(name = "password")
	private String password;

	/**
	 * The user's role
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "authority_id")
	private Authority role;

	/**
	 * user's first name
	 */
	@Column(name = "first_name")
	private String firstName;

	/**
	 * user's last name
	 */
	@Column(name = "last_name")
	private String lastName;

	/**
	 * user's birthdate
	 */
	@Column(name = "date_of_birth")
	private Timestamp birthDate;

	/**
	 * user's age
	 */
	@Column(name = "age")
	private int age;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "age_range", referencedColumnName = "age_range_id")
	private AgeRange ageRange;

	/**
	 * user's phone number
	 */
	@Column(name = "phone_number")
	private String phoneNumber;

	/**
	 * user's address
	 */
	@Column(name = "address")
	private String address;

	/**
	 * user's email
	 */
	@Column(name = "email")
	private String email;

	/**
	 * user's city
	 */
	@Column(name = "city")
	private String city;
	
	
	/**
	 * city's postal code
	 */
	@Column(name ="postal_code")
	private int postalCode;	
	
	/**
	 * boolean to enable user's account
	 */
	@Column(name = "isenabled")
	private boolean isEnabled;

	/**
	 * The user entity constructor
	 */
	public User() {
		super();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<Authority> roles = new HashSet<>();
		roles.add(role);
		return roles;
	}

	/**
	 * Getter of the user's id
	 * 
	 * @return the user's id
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * Setter of user's id
	 * 
	 * @param userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * Getter of the username
	 * 
	 * @return the username
	 */
	@Override
	public String getUsername() {
		return username;
	}

	/**
	 * Setter of user's username
	 * 
	 * @param username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Getter of the user's password
	 * 
	 * @return the user's password
	 */
	@Override
	public String getPassword() {
		return password;
	}

	/**
	 * Setter of the user's password
	 * 
	 * @param the user's password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Getter of the user's firstName
	 * 
	 * @return the user's firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter of the user's firstName
	 * 
	 * @param the user's firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter of the user's lastName
	 * 
	 * @return the user's lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter of the user's lastName
	 * 
	 * @param the user's lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Getter of the user's birthDate
	 * 
	 * @return the user's birthDate
	 */
	public Timestamp getBirthDate() {
		return birthDate;
	}

	/**
	 * Setter of the user's birthDate
	 * 
	 * @param the user's birthDate to set
	 */
	public void setBirthDate(Timestamp birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Getter of the user's age
	 * 
	 * @return the user's age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Setter of the user's age
	 * 
	 * @param the user's age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * Getter of the user's ageRange
	 * 
	 * @return the user's ageRange
	 */
	public AgeRange getAgeRange() {
		return ageRange;
	}

	/**
	 * Setter of the user's ageRange
	 * 
	 * @param the user's ageRange to set
	 */
	public void setAgeRange(AgeRange ageRange) {
		this.ageRange = ageRange;
	}

	/**
	 * Getter of the user's phoneNumber
	 * 
	 * @return the user's phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Setter of the user's phoneNumber
	 * 
	 * @param the user's phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Getter of the user's address
	 * 
	 * @return the user's address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Setter of the user's address
	 * 
	 * @param the user's address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Getter of the user's email
	 * 
	 * @return the user's email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter of the user's email
	 * 
	 * @param the user's email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter of the user's city
	 * 
	 * @return the user's city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Setter of the user's city
	 * 
	 * @param the user's city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Getter of the user's postal code
	 * @return the user's postal code
	 */
	public int getPostalCode() {
		return postalCode;
	}

	/**
	 * Setter of the user's postal code
	 * @param the user's postal code to set
	 */
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Getter of the user's role
	 * 
	 * @return the user's role
	 */
	public Authority getRole() {
		return role;
	}

	/**
	 * Setter of the user's role
	 * 
	 * @param the user's role to set
	 */
	public void setRole(Authority role) {
		this.role = role;
	}

	/**
	 * Getter of the user's status
	 * 
	 * @return the user's status
	 */
	public boolean getEnabled() {
		return this.isEnabled;
	}

	/**
	 * Setter of the user's status
	 * 
	 * @param the user's status to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return isEnabled;
	}

}

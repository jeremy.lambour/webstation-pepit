package com.techconsulting.ws.dao.entity.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The FestivePeriod's Class. This class describe the FestivePeriod entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "festivePeriod_seq", sequenceName = "festivePeriod_seq", allocationSize = 1)
@Table(name = "festivePeriod", schema = "public")
public class FestivePeriodEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * The festivePeriod's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "festive_period_id", unique = true, nullable = false)
	private Integer festivePeriodId;

	/**
	 * The festivePeriod's name
	 */
	@Column(name = "festive_period_name")
	private String festivePeriodName;

	/**
	 * The festivePeriod's beginning
	 */
	@Column(name = "festive_period_beginning")
	private Date festivePeriodBeginning;

	/**
	 * The festivePeriod's end
	 */
	@Column(name = "festive_period_end")
	private Date festivePeriodEnd;
	
	/**
	 * The {@link SeasonEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "season_id", referencedColumnName = "season_id")
	private SeasonEntity seasonEntity;
	
	public FestivePeriodEntity() {
		super();
	}

	/**
	 * @return the festivePeriodId
	 */
	public Integer getFestivePeriodId() {
		return festivePeriodId;
	}

	/**
	 * @param festivePeriodId the festivePeriodId to set
	 */
	public void setFestivePeriodId(Integer festivePeriodId) {
		this.festivePeriodId = festivePeriodId;
	}

	/**
	 * @return the festivePeriodName
	 */
	public String getFestivePeriodName() {
		return festivePeriodName;
	}

	/**
	 * @param festivePeriodName the festivePeriodName to set
	 */
	public void setFestivePeriodName(String festivePeriodName) {
		this.festivePeriodName = festivePeriodName;
	}

	/**
	 * @return the festivePeriodBeginning
	 */
	public Date getFestivePeriodBeginning() {
		return festivePeriodBeginning;
	}

	/**
	 * @param festivePeriodBeginning the festivePeriodBeginning to set
	 */
	public void setFestivePeriodBeginning(Date festivePeriodBeginning) {
		this.festivePeriodBeginning = festivePeriodBeginning;
	}

	/**
	 * @return the festivePeriodEnd
	 */
	public Date getFestivePeriodEnd() {
		return festivePeriodEnd;
	}

	/**
	 * @param festivePeriodEnd the festivePeriodEnd to set
	 */
	public void setFestivePeriodEnd(Date festivePeriodEnd) {
		this.festivePeriodEnd = festivePeriodEnd;
	}

	/**
	 * @return the seasonEntity
	 */
	public SeasonEntity getSeasonEntity() {
		return seasonEntity;
	}

	/**
	 * @param seasonEntity the seasonEntity to set
	 */
	public void setSeasonEntity(SeasonEntity seasonEntity) {
		this.seasonEntity = seasonEntity;
	}
	
	

}

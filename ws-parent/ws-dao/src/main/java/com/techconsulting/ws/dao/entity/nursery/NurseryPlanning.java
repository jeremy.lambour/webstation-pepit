package com.techconsulting.ws.dao.entity.nursery;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.techconsulting.ws.dao.entity.common.SeasonEntity;

/**
 * Represent an {@link NurseryPlanning} entity in database
 * @author Jeremy
 *
 */
@Entity
@Table(name="nursery_planning",schema="public")
@SequenceGenerator(name = "nursery_planning_seq", sequenceName = "nursery_planning_nursery_planning_id_seq", allocationSize = 1)
public class NurseryPlanning {

	/**
	 * {@link NurseryPlanning} identifier
	 */
	@Column(name="nursery_planning_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Id Integer nurseryPlanningId;
	
	/**
	 * {@link NurseryPlanning} name
	 */
	@Column(name="nursery_planning_name")
	private String nurseryPlanningName;
	
	
	/**
	 * {@link Nursery} which is responsible for this {@link NurseryPlanning}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nursery_id", referencedColumnName = "nursery_id")
	private Nursery nursery;
	
	/**
	 * {@link SeasonEntity} for {@link NurseryPlanning}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "season_id", referencedColumnName = "season_id")
	private SeasonEntity season;
	
	/**
	 * Begin date of {@link NurseryPlanning}
	 */
	@Column(name = "nursery_planning_beginning")
	private Timestamp nurseryPlanningBeginning;
	
	/**
	 * End date fo {@link NurseryPlanning}
	 */
	@Column(name = "nursery_planning_end")
	private Timestamp nurseryPlanningEnd;

	/**
	 * Getter for {@link NurseryPlanning} identifier
	 * @return an interger which represent current value of {@link NurseryPlanning} identifier
	 */
	public Integer getNurseryPlanningId() {
		return nurseryPlanningId;
	}

	/**
	 * Getter for {@link NurseryPlanning} name
	 * @return a string which represent current value of {@link NurseryPlanning} name
	 */
	public String getNurseryPlanningName() {
		return nurseryPlanningName;
	}

	/**
	 * Getter for {@link NurseryPlanning} {@link Nursery}
	 * @return a {@link Nursery} entity which represent current value of {@link NurseryPlanning} {@link Nursery} field
	 */
	public Nursery getNursery() {
		return nursery;
	}

	/**
	 * Getter for {@link NurseryPlanning} {@link SeasonEntity}
	 * @return a {@link SeasonEntity} entity which represent current value of {@link NurseryPlanning} {@link SeasonEntity} field
	 */
	public SeasonEntity getSeason() {
		return season;
	}

	/**
	 * Getter for {@link NurseryPlanning} begin date
	 * @return a timestamp which represent current value of {@link NurseryPlanning} begin date
	 */
	public Timestamp getNurseryPlanningBeginning() {
		return nurseryPlanningBeginning;
	}

	/**
	 * Getter for {@link NurseryPlanning} end date
	 * @return a timestamp which represent current value of {@link NurseryPlanning} end date
	 */
	public Timestamp getNurseryPlanningEnd() {
		return nurseryPlanningEnd;
	}

	/**
	 * Setter for {@link NurseryPlanning} identifier
	 * @param nurseryPlanningId next value for {@link NurseryPlanning} identifier
	 */
	public void setNurseryPlanningId(Integer nurseryPlanningId) {
		this.nurseryPlanningId = nurseryPlanningId;
	}

	/**
	 * Setter for {@link NurseryPlanning} name
	 * @param nurseryPlanningName next valeu for {@link NurseryPlanning} name
	 */
	public void setNurseryPlanningName(String nurseryPlanningName) {
		this.nurseryPlanningName = nurseryPlanningName;
	}

	/**
	 * Setter for {@link NurseryPlanning} {@link Nursery} field
	 * @param nursery next value for {@link NurseryPlanning} {@link Nursery} field
	 */
	public void setNursery(Nursery nursery) {
		this.nursery = nursery;
	}

	/**
	 * Setter for {@link NurseryPlanning} {@link SeasonEntity} field
	 * @param season next value for {@link NurseryPlanning} {@link SeasonEntity} field
	 */
	public void setSeason(SeasonEntity season) {
		this.season = season;
	}

	/**
	 * Setter for {@link NurseryPlanning} begin date
	 * @param nurseryPlanningBeginning next value for {@link NurseryPlanning} befin date
	 */
	public void setNurseryPlanningBeginning(Timestamp nurseryPlanningBeginning) {
		this.nurseryPlanningBeginning = nurseryPlanningBeginning;
	}

	/**
	 * Setter for {@link NurseryPlanning} end date
	 * @param nurseryPlanningEnd next value for {@link NurseryPlanning} end date
	 */
	public void setNurseryPlanningEnd(Timestamp nurseryPlanningEnd) {
		this.nurseryPlanningEnd = nurseryPlanningEnd;
	}
	
	
	
}

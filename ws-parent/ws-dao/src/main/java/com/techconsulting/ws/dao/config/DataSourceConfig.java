package com.techconsulting.ws.dao.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Define datasource config to connect web application to his postgres database
 * 
 * @author Jeremy
 *
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.techconsulting.ws.dao.repository")
@ComponentScan(basePackages = "com.techconsulting.ws.dao")
@EnableTransactionManagement
public class DataSourceConfig {

	/**
	 * Define JNDI database access
	 * 
	 * @return {@link DataSource}
	 * @throws NamingException
	 */
	@Bean
	public DataSource dataSource() throws NamingException {
		JndiDataSourceLookup jndiLookup = new JndiDataSourceLookup();
		return jndiLookup.getDataSource("java:comp/env/jdbc/webstation");
	}
}

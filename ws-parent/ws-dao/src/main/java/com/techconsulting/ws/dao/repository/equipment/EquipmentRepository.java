
package com.techconsulting.ws.dao.repository.equipment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.equipment.Equipment;

/**
 * JpaRepository use to question Equipment table in database
 * @author Paul
 *
 */
@Repository
public interface EquipmentRepository extends JpaRepository<Equipment, Integer> {
	List<Equipment> findByShopShopId(Integer shopId);
	

}

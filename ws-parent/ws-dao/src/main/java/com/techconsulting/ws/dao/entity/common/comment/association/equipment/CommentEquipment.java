package com.techconsulting.ws.dao.entity.common.comment.association.equipment;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;


/**
 * CommentEquipment's Entity
 * 
 * @author Paul
 *
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "comment_equipment_seq", sequenceName = "comment_equipment_seq", allocationSize = 1)
@Table(name = "comment_equipment")
public class CommentEquipment  implements Serializable {
	
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The {@link CommentEquipmentPK}
	 */
	@EmbeddedId
	private CommentEquipmentPK commentEquipmentPk;

	/**
	 * The {@link CommentEquipmentPK}'s constructor
	 */
	public CommentEquipment() {
		super();
	}

	/**
	 * Getter for property commentEquipmentPk
	 * 
	 * @return {@link CommentEquipmentPK}
	 */
	public CommentEquipmentPK getCommentEquipmentPk() {
		return commentEquipmentPk;
	}

	/**
	 * Setter for property commentEquipmentPk
	 * 
	 * @param {@link CommentEquipmentPK}
	 */
	public void setCommentEquipmentPk(CommentEquipmentPK commentEquipmentPk) {
		this.commentEquipmentPk = commentEquipmentPk;
	}
	
	
}

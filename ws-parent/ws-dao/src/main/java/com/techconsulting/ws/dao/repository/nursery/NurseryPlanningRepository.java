package com.techconsulting.ws.dao.repository.nursery;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.nursery.NurseryPlanning;

/**
 * {@link NurseryPlanning} Repository
 * @author ASUS
 *
 */
@Repository
public interface NurseryPlanningRepository extends JpaRepository<NurseryPlanning, Integer> {

	/**
	 * Get all {@link NurseryPlanning} with given nursery identifier and season identifier
	 * @param nurseryId
	 * @param seasonId
	 * @return List<NurseryPlanning> 
	 */
	public List<NurseryPlanning> findByNurseryNurseryIdAndSeasonSeasonId(Integer nurseryId,Integer seasonId);
}

package com.techconsulting.ws.dao.repository.flatManagement.hotel;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;

/**
 * JpaRepository use to question Hotel table in database
 * @author gaillardc
 *
 */
public interface HotelRepository extends JpaRepository<HotelEntity, Integer>{

}

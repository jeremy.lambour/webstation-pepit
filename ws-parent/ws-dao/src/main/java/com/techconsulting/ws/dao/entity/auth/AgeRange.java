package com.techconsulting.ws.dao.entity.auth;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity which represent User's AgeRange in application
 *
 */
@Entity
@Table(name = "age_range")
@SequenceGenerator(name = "age_range_seq", sequenceName = "age_range_age_range_id_seq", allocationSize = 1)
public class AgeRange implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ageRange's id
	 */
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "age_range_age_range_id_seq")
	@Column(name = "age_range_id")
	private @Id Integer ageRangeId;

	/**
	 * ageRange's name
	 */
	@Column(name = "age_range_name")
	private String ageRangeName;

	/**
	 * The ageRange entity constructor
	 */
	public AgeRange() {
		super();
	}

	/**
	 * The second ageRange entity constructor
	 */
	public AgeRange(String range) {
		this.ageRangeName = range;
	}

	/**
	 * Getter of the ageRange's id
	 * 
	 * @return the ageRange's id
	 */
	public Integer getAgeRangeId() {
		return ageRangeId;
	}

	/**
	 * Setter of the ageRange's id
	 * 
	 * @param the ageRange's id to set
	 */
	public void setAgeRangeId(Integer ageRangeId) {
		this.ageRangeId = ageRangeId;
	}

	/**
	 * Getter of the ageRange's name
	 * 
	 * @return the ageRange's name
	 */
	public String getAgeRangeName() {
		return ageRangeName;
	}

	/**
	 * Setter of the ageRange's name
	 * 
	 * @param the ageRange's name to set
	 */
	public void setAgeRangeName(String ageRangeName) {
		this.ageRangeName = ageRangeName;
	}

}

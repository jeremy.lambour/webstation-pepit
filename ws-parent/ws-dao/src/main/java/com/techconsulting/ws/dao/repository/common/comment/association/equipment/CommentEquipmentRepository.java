package com.techconsulting.ws.dao.repository.common.comment.association.equipment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.common.comment.association.equipment.CommentEquipment;

/**
 * JpaRepository use to question CommentEquipment table in database
 * 
 * @author Paul
 *
 */
@Repository
public interface CommentEquipmentRepository extends JpaRepository<CommentEquipment, Integer> {
	List<CommentEquipment> findByCommentEquipmentPkEquipmentEquipmentId(Integer idEquipement);

	boolean deleteByCommentEquipmentPkCommentCommentId(Integer idComment);

	boolean deleteByCommentEquipmentPkEquipmentEquipmentId(Integer idEquipement);
}

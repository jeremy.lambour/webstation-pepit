package com.techconsulting.ws.dao.repository.skiingSchool;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.skiingSchool.SkiingSchool;

/**
 * {@link SkiingSchoolRepository}
 * @author Jeremy
 *
 */
@Repository
public interface SkiingSchoolRepository extends JpaRepository<SkiingSchool,Integer>{

}

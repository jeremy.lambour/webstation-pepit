package com.techconsulting.ws.dao.entity.reservation.flat;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The ReservationFlat's Class. This class describe the Reservation for a Flat entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "reservationflat_seq", sequenceName = "reservationflat_seq", allocationSize = 1)
@Table(name = "reservationflat", schema = "public")
public class ReservationFlat implements Serializable{
private static final long serialVersionUID = 1L;
	
	/**
	 * Entity of {@link ReservationFlatId}
	 */
	@EmbeddedId
	ReservationFlatId reservationFlatId;
	
	public ReservationFlat() {
		super();
	}

	/**
	 * @return the reservationFlatId
	 */
	public ReservationFlatId getReservationFlatId() {
		return reservationFlatId;
	}

	/**
	 * @param reservationFlatId the reservationFlatId to set
	 */
	public void setReservationFlatId(ReservationFlatId newReservationFlatId) {
		reservationFlatId = newReservationFlatId;
	}
	

}

package com.techconsulting.ws.dao.entity.common.comment.association.equipment;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.techconsulting.ws.dao.entity.common.comment.Comment;
import com.techconsulting.ws.dao.entity.equipment.Equipment;

/**
 * Comment's Table association between comment and equipment
 * 
 * @author Paul
 *
 */
@Embeddable
public class CommentEquipmentPK implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The comment
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comment_id", referencedColumnName = "comment_id")
	private Comment comment;

	/**
	 * the equipment commented
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "equipment_id", referencedColumnName = "equipment_id")
	private Equipment equipment;

	/**
	 * The comment's equipment pk Constructor
	 */
	public CommentEquipmentPK() {
		super();
	}

	/**
	 * Getter for property comment
	 * 
	 * @return {@link Comment}
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * Setter for property comment
	 * 
	 * @param {@link Comment}
	 */
	public void setComment(Comment comment) {
		this.comment = comment;
	}

	/**
	 * Getter for property Equipment
	 * 
	 * @return {@link Equipment}
	 */
	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * Setter for property equipment
	 * 
	 * @param {@link Equipment}
	 */
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

}

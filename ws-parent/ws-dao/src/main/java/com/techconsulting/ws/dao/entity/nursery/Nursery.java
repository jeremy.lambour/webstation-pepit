package com.techconsulting.ws.dao.entity.nursery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Represent an {@link Nursery} entity in database
 * @author Jeremy
 *
 */
@Entity
@SequenceGenerator(name = "nursery_seq", sequenceName = "'nursery_nursery_id_seq", allocationSize = 1)
@Table(name="nursery",schema="public")
public class Nursery {

	/**
	 * {@link Nursery} entity identifier
	 */
	@Column(name="nursery_id",nullable=false,unique=true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Id Integer nurseryId;
	
	/**
	 * {@link Nursery} name
	 */
	@Column(name="nursery_name")
	private String nurseryName;
	
	/**
	 * {@link Nursery} phone number
	 */
	@Column(name="phone_number")
	private String nurseryPhoneNumber;

	/**
	 * Getter for NurseryId property
	 * @return an integer which represent {@link Nursery} identifier
	 */
	public Integer getNurseryId() {
		return nurseryId;
	}

	/**
	 * Getter for {@link Nursery} PhoneNumber
	 * @return a string which represent {@link Nursery} phone number
	 */
	public String getNurseryPhoneNumber() {
		return nurseryPhoneNumber;
	}

	/**
	 * Setter for NurseryId property
	 * @param nurseryId an integer which represent next value of {@link Nursery} identifier
	 */
	public void setNurseryId(Integer nurseryId) {
		this.nurseryId = nurseryId;
	}

	/**
	 * Setter for {@link Nursery} Phone number
	 * @param nurseryPhoneNumber next value for {@link Nursery} phone number
	 */
	public void setNurseryPhoneNumber(String nurseryPhoneNumber) {
		this.nurseryPhoneNumber = nurseryPhoneNumber;
	}

	/**
	 * Getter for {@link Nursery} name property
	 * @return a string which represent {@link Nursery} name
	 */
	public String getNurseryName() {
		return nurseryName;
	}

	/**
	 * Setter for {@link Nursery} name property
	 * @param nurseryName next valeu for {@link Nursery} name
	 */
	public void setNurseryName(String nurseryName) {
		this.nurseryName = nurseryName;
	}
	
	
	
	
	
}

package com.techconsulting.ws.dao.entity.skiingSchool;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.management.monitor.Monitor;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.techconsulting.ws.dao.entity.auth.User;

import net.bytebuddy.dynamic.scaffold.MethodRegistry.Handler.ForAbstractMethod;

/**
 * Lesson entity, Java class which describe an Lesson entity in database
 * 
 * @author Jeremy
 *
 */
@Entity
@SequenceGenerator(name = "lesson_seq", sequenceName = "lesson_lesson_id_seq", allocationSize = 1)
@Table(name = "lesson", schema = "public")
public class Lesson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Lesson identifier
	 */
	@GeneratedValue(generator = "lesson_seq")
	@Column(name = "lesson_id", unique = true, nullable = false)
	private @Id Integer lessonId;

	/**
	 * {@link User} who teach the lesson
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User monitor;

	/**
	 * {@link SkiingSchool} which teach the lesson
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "skiing_school_id", referencedColumnName = "skiing_school_id")
	private SkiingSchool skiingSchool;

	/**
	 * lesson level
	 */
	@Column(name = "lesson_level")
	private String lessonLevel;

	/**
	 * lesson sport
	 */
	@Column(name = "sport")
	private String sport;

	/**
	 * lesson club
	 */
	@Column(name = "club")
	private String club;

	/**
	 * lesson start time
	 */
	@Column(name = "date")
	private Timestamp date;
	/**
	 * lesson start time
	 */
	@Column(name = "start_time")
	private Timestamp startDateTime;

	/**
	 * lesson end time
	 */
	@Column(name = "end_time")
	private Timestamp endDateTime;

	/**
	 * lesson duration
	 */
	@Column(name = "duration")
	private Long duration;

	/**
	 * lesson meeting place
	 */
	@Column(name = "meeting_place")
	private String meetingPlace;

	/**
	 * {@link Lesson} {@link Planning}
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "planning_id", referencedColumnName = "planning_id")
	private Planning lessonPlanning;

	/**
	 * {@link Lesson} number places
	 */
	@Column(name = "lesson_number_places")
	private Integer lessonNumberPlaces;

	/**
	 * Getter for {@link Lesson} identifier property
	 * 
	 * @return an integer which represent current lesson identifier
	 */
	public Integer getLessonId() {
		return lessonId;
	}

	/**
	 * Setter for {@link Lesson} identifier property
	 * 
	 * @param lessonId next value for {@link Lesson} identifier
	 */
	public void setLessonId(Integer lessonId) {
		this.lessonId = lessonId;
	}

	/**
	 * Getter for {@link User} monitor property
	 * 
	 * @return {@link User} which represent lesson {@link Monitor}
	 */
	public User getMonitor() {
		return monitor;
	}

	/**
	 * Setter {@link User} monitor property
	 * 
	 * @param user next value for monitor field
	 */
	public void setMonitor(User user) {
		this.monitor = user;
	}

	/**
	 * Getter for {@link SkiingSchool} property
	 * 
	 * @return {@link SkiingSchool}
	 */
	public SkiingSchool getSkiingSchool() {
		return skiingSchool;
	}

	/**
	 * Setter for {@link SkiingSchool} property
	 * 
	 * @param skiingSchool next value
	 */
	public void setSkiingSchool(SkiingSchool skiingSchool) {
		this.skiingSchool = skiingSchool;
	}

	/**
	 * Getter for lessonLevel property
	 * 
	 * @return a string which represent lesson level property
	 */
	public String getLessonLevel() {
		return lessonLevel;
	}

	/**
	 * Setter for lesson level property
	 * 
	 * @param lessonLevel next value for lesson level property
	 */
	public void setLessonLevel(String lessonLevel) {
		this.lessonLevel = lessonLevel;
	}

	/**
	 * Getter for lesson club property
	 * 
	 * @return a string which represent lessson club property
	 */
	public String getClub() {
		return club;
	}

	/**
	 * Setter for lesson club property
	 * 
	 * @param club
	 */
	public void setClub(String club) {
		this.club = club;
	}

	/**
	 * Getter for start time property
	 * 
	 * @return
	 */
	public Timestamp getStartDateTime() {
		return startDateTime;
	}

	/**
	 * Setter for start time property
	 * 
	 * @param date next value for start time field
	 */
	public void setStartDateTime(Timestamp date) {
		this.startDateTime = date;
	}

	/**
	 * Getter for duration field
	 * 
	 * @return current value for duration field
	 */
	public Long getDuration() {
		return duration;
	}

	/**
	 * Setter for duration field
	 * 
	 * @param duration next value
	 */
	public void setDuration(Long duration) {
		this.duration = duration;
	}

	/**
	 * Getter for meeting place property
	 * 
	 * @return current value of meeting place
	 */
	public String getMeetingPlace() {
		return meetingPlace;
	}

	/**
	 * Setter for meeting place property
	 * 
	 * @param meetingPlace next value for meeting place property
	 */
	public void setMeetingPlace(String meetingPlace) {
		this.meetingPlace = meetingPlace;
	}

	/**
	 * Getter for end date time property
	 * 
	 * @return current value of end date time field
	 */
	public Timestamp getEndDateTime() {
		return endDateTime;
	}

	/**
	 * Setter for end date time property
	 * 
	 * @param endDateTime next value
	 */
	public void setEndDateTime(Timestamp endDateTime) {
		this.endDateTime = endDateTime;
	}

	/**
	 * Getter for sport property
	 * 
	 * @return current value
	 */
	public String getSport() {
		return sport;
	}

	/**
	 * Setter for sport property
	 * 
	 * @param sport next value
	 */
	public void setSport(String sport) {
		this.sport = sport;
	}

	/**
	 * Getter for {@link Lesson} {@link Planning} property
	 * 
	 * @return current value
	 */
	public Planning getLessonPlanning() {
		return lessonPlanning;
	}

	/**
	 * Setter for {@link Lesson} {@link Planning} property
	 * 
	 * @param lessonPlanning next value
	 */
	public void lesson(Planning lessonPlanning) {
		this.lessonPlanning = lessonPlanning;
	}

	public void setLessonPlanning(Planning lessonPlanning) {
		this.lessonPlanning = lessonPlanning;
	}

	/**
	 * Getter for date field
	 * 
	 * @return current value
	 */
	public Timestamp getDate() {
		return date;
	}

	/**
	 * Setter for date field
	 * 
	 * @param date next value
	 */
	public void setDate(Timestamp date) {
		this.date = date;
	}

	/**
	 * Getter for lesson number places property
	 * 
	 * @return an integer which represent lesson number places
	 */
	public Integer getLessonNumberPlaces() {
		return lessonNumberPlaces;
	}

	/**
	 * Setter for lesson number places
	 * 
	 * @param lessonNumberPlaces next value
	 */
	public void setLessonNumberPlaces(Integer lessonNumberPlaces) {
		this.lessonNumberPlaces = lessonNumberPlaces;
	}

}

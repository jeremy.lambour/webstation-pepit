package com.techconsulting.ws.dao.entity.auth.enume;

public enum UserAuthority {
	ADMIN_RULES, CLIENT_RULES
}

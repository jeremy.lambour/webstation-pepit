package com.techconsulting.ws.dao.entity.flatManagement.flat;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The Define's Class. This class describe the Define entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "define_seq", sequenceName = "define_seq", allocationSize = 1)
@Table(name = "define", schema = "public")
public class DefineEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * Entity of {@link DefineIdEntity}
	 */
	@EmbeddedId
	DefineIdEntity defineIdEntity;
	
	/**
	 * The flat's guidance
	 */
	@Column(name = "flat_price_per_night")
	private double flatPricePerNight;
	
	public DefineEntity() {
		super();
	}

	/**
	 * @return the defineIdEntity
	 */
	public DefineIdEntity getDefineIdEntity() {
		return defineIdEntity;
	}

	/**
	 * @param defineIdEntity the defineIdEntity to set
	 */
	public void setDefineIdEntity(DefineIdEntity defineIdEntity) {
		this.defineIdEntity = defineIdEntity;
	}

	/**
	 * @return the flatPricePerNight
	 */
	public double getFlatPricePerNight() {
		return flatPricePerNight;
	}

	/**
	 * @param flatPricePerNight the flatPricePerNight to set
	 */
	public void setFlatPricePerNight(double flatPricePerNight) {
		this.flatPricePerNight = flatPricePerNight;
	}
	
	
	
}

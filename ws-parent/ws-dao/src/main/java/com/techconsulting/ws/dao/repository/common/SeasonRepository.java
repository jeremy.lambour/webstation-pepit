package com.techconsulting.ws.dao.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.common.SeasonEntity;

/**
 * JpaRepository use to question Season table in database
 * @author gaillardc
 *
 */
public interface SeasonRepository extends JpaRepository<SeasonEntity, Integer>{

}

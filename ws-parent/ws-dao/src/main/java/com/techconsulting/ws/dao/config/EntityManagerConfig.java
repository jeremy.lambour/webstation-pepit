package com.techconsulting.ws.dao.config;

import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Config class use to create an entity manager 
 * @author Jeremy
 *
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.techconsulting.ws.dao.repository")
@ComponentScan(basePackages = "com.techconsulting.ws.dao")
@EnableTransactionManagement
public class EntityManagerConfig {

	@Autowired
	private DataSourceConfig config;
	/**
	 * Define entity manager factory
	 * 
	 * @return
	 * @throws NamingException
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.POSTGRESQL);
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setShowSql(true);
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.techconsulting.ws.dao.entity");
		factory.setDataSource(config.dataSource());
		

		return factory;
	}
}

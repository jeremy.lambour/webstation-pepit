package com.techconsulting.ws.dao.entity.shop;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.techconsulting.ws.dao.entity.auth.User;

/**
 * The Shop's Class. This class describe the Shop entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "shop_seq", sequenceName = "shop_seq", allocationSize = 1)
@Table(name = "shop", schema = "public")
public class ShopEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The shop's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "shop_id", unique = true, nullable = false)
	private Integer shopId;

	/**
	 * The shop's name
	 */
	@Column(name = "shop_name")
	private String shopName;

	/**
	 * The shop's address
	 */
	@Column(name = "shop_address")
	private String shopAddress;

	/**
	 * The shop's phone number
	 */
	@Column(name = "phone_number")
	private String phoneNumber;

	/**
	 * The shop's latitude
	 */
	@Column(name = "shop_latitude")
	private double shopLatitude;

	/**
	 * The shop's longitude
	 */
	@Column(name = "shop_longitude")
	private double shopLongitude;

	/**
	 * The shop's manager
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;

	/**
	 * The ShopEntity's constructor
	 */
	public ShopEntity() {
		super();
	}

	/**
	 * @return the shop_id
	 */
	public Integer getShopId() {
		return shopId;
	}

	/**
	 * @Id
	 * @GeneratedValue(strategy = GenerationType.IDENTITY)
	 * @Column(name = "user_id", unique = true, nullable = false)
	 * @param shop_id the shop_id to set
	 */
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the shop_name
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shop_name the shop_name to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the shop_address
	 */
	public String getShopAddress() {
		return shopAddress;
	}

	/**
	 * @param shop_address the shop_address to set
	 */
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	/**
	 * @return the phone_number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phone_number the phone_number to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the shop_latitude
	 */
	public double getShopLatitude() {
		return shopLatitude;
	}

	/**
	 * @param shop_latitude the shop_latitude to set
	 */
	public void setShopLatitude(double shopLatitude) {
		this.shopLatitude = shopLatitude;
	}

	/**
	 * @return the shop_longitude
	 */
	public double getShopLongitude() {
		return shopLongitude;
	}

	/**
	 * @param shop_longitude the shop_longitude to set
	 */
	public void setShopLongitude(double shopLongitude) {
		this.shopLongitude = shopLongitude;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}

package com.techconsulting.ws.dao.repository.reservation.equipment;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.techconsulting.ws.dao.entity.reservation.equipment.ReservationEquipment;

/**
 * JpaRepository use to question Reservation_equipment table in database
 * 
 * @author Paul
 *
 */
@Repository
public interface ReservationEquipmentRepository extends JpaRepository<ReservationEquipment, Integer> {
	List<ReservationEquipment> findByReservEquipmentEntityPkEquipmentEquipmentId(Integer idEquip);
	
	List<ReservationEquipment> findByReservEquipmentEntityPkReservationUserUserId(Integer idUser);
	
	void deleteByReservEquipmentEntityPkEquipmentEquipmentId(Integer idEquip);
	
	void deleteByReservEquipmentEntityPkReservationReservationId(Integer idReservation);
	
	List<ReservationEquipment> findBydateStart(Timestamp dateStart);
	
	List<ReservationEquipment> findReservationEquipmentByReservEquipmentEntityPkEquipmentShopShopId(Integer idShop);
	
	ReservationEquipment findByReservEquipmentEntityPkReservationReservationId(Integer idReserv);
	ReservationEquipment findByReservEquipmentEntityPkReservationReservationIdAndReservEquipmentEntityPkEquipmentEquipmentId(Integer idEquipment, Integer idReserv);
}


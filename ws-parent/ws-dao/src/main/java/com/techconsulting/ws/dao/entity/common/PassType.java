package com.techconsulting.ws.dao.entity.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The PassType's Class. This class describe the PassType entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "pass_type_pass_type_id_seq", sequenceName = "pass_type_pass_type_id_seq", allocationSize = 1)
@Table(name = "pass_type", schema = "public")
public class PassType implements Serializable {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The pass type's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pass_type_id", unique = true, nullable = false)
	private Integer passTypeId;

	/**
	 * Pass type's name
	 */
	@Column(name = "pass_type_name")
	private String passTypeName;

	/**
	 * Generic Constructor
	 */
	public PassType() {
		super();
	}

	/**
	 * Getter for property passTypeId
	 * 
	 * @return {@link Integer}
	 */
	public Integer getPassTypeId() {
		return passTypeId;
	}

	/**
	 * Setter for property passTypeId
	 * 
	 * @param {@link Integer}
	 */
	public void setPassTypeId(Integer passTypeId) {
		this.passTypeId = passTypeId;
	}

	/**
	 * Getter for property pass type's Name
	 * 
	 * @return {@link String}
	 */
	public String getPassTypeName() {
		return passTypeName;
	}

	/**
	 * Setter for property pass type's Name
	 * 
	 * @param {@link String}
	 */
	public void setPassTypeName(String passTypeName) {
		this.passTypeName = passTypeName;
	}
}

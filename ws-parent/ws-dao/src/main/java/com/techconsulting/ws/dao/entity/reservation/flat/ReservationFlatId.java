package com.techconsulting.ws.dao.entity.reservation.flat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.dao.entity.reservation.Reservation;

/**
 * The ReservationFlatId class. This class is an association class between
 * {@link FlatEntity} and {@link Reservation}
 */
@Embeddable
public class ReservationFlatId implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	/**
	 * The {@link FlatEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "flat_id", referencedColumnName = "flat_id")
	private FlatEntity flat;

	/**
	 * The {@link Reservation}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reservation_id", referencedColumnName = "reservation_id")
	private Reservation reservation;
	
	/**
	 * The reservation startDate
	 */
	@Column(name = "start_date")
	private Date startDate;
	
	/**
	 * The reservation endDate
	 */
	@Column(name = "end_date")
	private Date endDate;
	
	/**
	 * The reservation price
	 */
	@Column(name = "price")
	private Double price;
	
	public ReservationFlatId() {
		super();
	}

	/**
	 * @return the flat
	 */
	public FlatEntity getFlat() {
		return flat;
	}

	/**
	 * @param flat the flat to set
	 */
	public void setFlat(FlatEntity flat) {
		this.flat = flat;
	}

	/**
	 * @return the reservation
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

}

package com.techconsulting.ws.dao.entity.reservation.equipment;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.techconsulting.ws.dao.entity.equipment.Equipment;
import com.techconsulting.ws.dao.entity.reservation.Reservation;

/**
 * Reservation's Table association
 * 
 * @author Paul
 *
 */
@Embeddable
public class ReservationEquipmentPK implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The reservation
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reservation_id", referencedColumnName = "reservation_id")
	private Reservation reservation;

	/**
	 * the equipment reserved
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "equipment_id", referencedColumnName = "equipment_id")
	private Equipment equipment;

	/**
	 * The reservation equipmentPk Constructor
	 */
	public ReservationEquipmentPK() {
		super();
	}

	/**
	 * Getter of {@link Reservation}
	 * 
	 * @return {@link Reservation}
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * Setter of {@link Reservation}
	 * 
	 * @param {@link Reservation}
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * Getter of {@link Equipment}
	 * 
	 * @return {@link Equipment}
	 */
	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * Setter of {@link Equipment}
	 * 
	 * @param {@link Equipment}
	 */
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}
}

package com.techconsulting.ws.dao.entity.reservation.lift;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.entity.equipment.SkiLift;

/**
 * The ReservationLiftPk's embedded Class. This class describe the composite key
 * of ReservationLift entity in database
 */
@Embeddable
public class ReservationLiftPK implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The reservation
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ski_lift_id", referencedColumnName = "ski_lift_id")
	private SkiLift skiLift;

	/**
	 * The reservation's user
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;

	/**
	 * Reservation's name
	 */
	@Column(name = "name_reserv")
	private String nameReserv;

	/**
	 * Generic constructor
	 */
	public ReservationLiftPK() {
		super();
	}

	/**
	 * Getter of the reservation's ski Lift
	 * 
	 * @return {@link SkiLift}
	 */
	public SkiLift getSkiLift() {
		return skiLift;
	}

	/**
	 * Setter of the reservation's ski Lift
	 * 
	 * @param {@link SkiLift}
	 */
	public void setSkiLift(SkiLift skiLift) {
		this.skiLift = skiLift;
	}

	/**
	 * Getter of the reservation's user
	 * 
	 * @return {@link User}
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Setter of the reservation's user
	 * 
	 * @param {@link User}
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Getter of the reservation's name
	 * 
	 * @return {@link String}
	 */
	public String getNameReserv() {
		return nameReserv;
	}

	/**
	 * Setter of the reservation's name
	 * 
	 * @param {@link String}
	 */
	public void setNameReserv(String nameReserv) {
		this.nameReserv = nameReserv;
	}
}

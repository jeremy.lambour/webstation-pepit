package com.techconsulting.ws.dao.entity.skiingSchool;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.techconsulting.ws.dao.entity.auth.User;
import com.techconsulting.ws.dao.entity.common.SeasonEntity;

/**
 * Represent a {@link Planning} entity in database
 * @author ASUS
 *
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "planning_seq", sequenceName = "planning_planning_id_seq", allocationSize = 1)
@Table(name = "planning", schema = "public")
public class Planning implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * {@link Planning} identifier
	 */
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "planning_seq")
	@Column(name = "planning_id", unique = true, nullable = false)
	private @Id Integer planningId;
	

	/**
	 * {@link Planning} creator
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id_editor", referencedColumnName = "user_id")
	private User planningCreator;
	
	
	/**
	 * {@link Planning} monitor
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id_concerned", referencedColumnName = "user_id")
	private User planningMonitor;
	
	/**
	 * {@link Planning} lessons
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "season_id", referencedColumnName = "season_id")
	private SeasonEntity planningSeason;
	
	/**
	 * {@link Planning} name
	 */
	@Column(name = "planning_name")
	private String planningName;
	
	/**
	 * {@link Planning} begin date
	 */
	@Column(name = "planning_beginning")
	private Timestamp planningBeginning;
	
	/**
	 * {@link Planning} end date
	 */
	@Column(name = "planning_end")
	private Timestamp planningEnd;

	/**
	 * Getter for {@link Planning} identifier
	 * @return
	 */
	public Integer getPlanningId() {
		return planningId;
	}

	/**
	 * Setter for {@link Planning} identifier
	 * @param planningId
	 */
	public void setPlanningId(Integer planningId) {
		this.planningId = planningId;
	}

	/**
	 * Getter for {@link Planning} creator
	 * @return
	 */
	public User getPlanningCreator() {
		return planningCreator;
	}

	/**
	 * Setter for {@link Planning} creator
	 * @param planningCreator
	 */
	public void setPlanningCreator(User planningCreator) {
		this.planningCreator = planningCreator;
	}

	/**
	 * Getter for {@link Planning} monitor
	 * @return
	 */
	public User getPlanningMonitor() {
		return planningMonitor;
	}

	/**
	 * Setter for {@link Planning} monitor
	 * @param planningMonitor
	 */
	public void setPlanningMonitor(User planningMonitor) {
		this.planningMonitor = planningMonitor;
	}

	/**
	 * Getter for {@link Planning} name
	 * @return
	 */
	public String getPlanningName() {
		return planningName;
	}

	/**
	 * Setter for {@link Planning} name
	 * @param planningName
	 */
	public void setPlanningName(String planningName) {
		this.planningName = planningName;
	}

	/**
	 * Getter for {@link Planning} begin date
	 * @return
	 */
	public Timestamp getPlanningBeginning() {
		return planningBeginning;
	}

	/**
	 * Setter for {@link Planning} begin date
	 * @param planningBeginning
	 */
	public void setPlanningBeginning(Timestamp planningBeginning) {
		this.planningBeginning = planningBeginning;
	}

	/**
	 * Getter for {@link Planning} end date
	 * @return
	 */
	public Timestamp getPlanningEnd() {
		return planningEnd;
	}

	/**
	 * Setter {@link Planning} end date
	 * @param planningEnd
	 */
	public void setPlanningEnd(Timestamp planningEnd) {
		this.planningEnd = planningEnd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Getter for {@link Planning} {@link SeasonEntity}
	 * @return
	 */
	public SeasonEntity getPlanningSeason() {
		return planningSeason;
	}

	/**
	 * Setter for {@link Planning} {@link SeasonEntity}
	 * @param season
	 */
	public void setPlanningSeason(SeasonEntity season) {
		this.planningSeason = season;
	}
	
}

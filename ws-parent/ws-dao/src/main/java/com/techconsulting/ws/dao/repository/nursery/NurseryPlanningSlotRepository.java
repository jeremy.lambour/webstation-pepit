package com.techconsulting.ws.dao.repository.nursery;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.nursery.Nursery;
import com.techconsulting.ws.dao.entity.nursery.NurseryPlanning;
import com.techconsulting.ws.dao.entity.nursery.NurseryPlanningSlot;

/**
 * {@link NurseryPlanningSlotRepository}
 * @author Jeremy
 *
 */
@Repository
public interface NurseryPlanningSlotRepository extends JpaRepository<NurseryPlanningSlot, Integer> {

	/**
	 * find all {@link NurseryPlanning} with given  {@link Nursery} identifier
	 * @param id search nursery identifier
	 * @return List<NurseryPlanningSlot>
	 */
	public List<NurseryPlanningSlot> findByNurseryPlanningNurseryPlanningId(Integer id);
}

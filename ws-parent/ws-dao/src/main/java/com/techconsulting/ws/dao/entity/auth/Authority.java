package com.techconsulting.ws.dao.entity.auth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

/**
 * Entity which represent User granted authority used by Spring to restrict access to certain request
 * @author Jeremy
 *
 */
@Entity
@Table(name = "authority")
@SequenceGenerator(name = "authority_authority_id_seq", sequenceName = "authority_authority_id_seq", allocationSize = 1)
public class Authority implements GrantedAuthority{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * authority identifier
	 */
	@Id
	@GeneratedValue(generator = "authority_authority_id_seq")
	@Column(name = "authority_id")
	private Integer authorityId;
	
	/**
	 * authority name
	 */
	@Column(name = "authority_name")
	private String authorityName;
	
	/**
	 * Getter for authorityId property
	 * @return {@link Integer} 
	 */
	public Integer getAuthorityId() {
		return authorityId;
	}

	/**
	 * Setter for property authorityId
	 * @param authorityId
	 */
	public void setAuthorityId(Integer authorityId) {
		this.authorityId = authorityId;
	}

	/**
	 * Getter for authorityName
	 * @return {@link String}
	 */
	public String getAuthorityName() {
		return authorityName;
	}

	/**
	 * Setter for property AuthorityName 
	 * @param authorityName
	 */
	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	/**
	 * Getter for property AuthorityName
	 */
	@Override
	public String getAuthority() {
		return authorityName;
	}
	
	
}

package com.techconsulting.ws.dao.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.common.FestivePeriodEntity;

/**
 * JpaRepository use to question FestivePeriod table in database
 * @author gaillardc
 *
 */
public interface FestivePeriodRepository extends JpaRepository<FestivePeriodEntity, Integer>{
	FestivePeriodEntity findByFestivePeriodName(String name);
}

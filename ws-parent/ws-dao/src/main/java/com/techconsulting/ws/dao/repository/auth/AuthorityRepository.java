package com.techconsulting.ws.dao.repository.auth;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.auth.Authority;

/*** JpaRepository use to question Authority Table in database
 * 
 * @author Jeremy
 *
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
	/**
	 * Get {@link User} target by username give in parameter
	 * 
	 * @param login search username
	 * @return {@link User}
	 */
	Authority findByAuthorityName(String authorityName);

}

package com.techconsulting.ws.dao.entity.nursery;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Represent {@link NurseryPlanningSlot} entity in database
 * @author Jeremy
 *
 */
@Entity
@Table(name="nursery_planning_slot",schema="public")
@SequenceGenerator(name = "nursery_planning_slot_seq", sequenceName = "nursery_planning_slot_nursery_planning_slot_id_seq", allocationSize = 1)
public class NurseryPlanningSlot {

	/**
	 * {@link NurseryPlanningSlot} identifier
	 */
	@Column(name="nursery_planning_slot_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Id Integer nurseryPlanningSlotId;
	
	/**
	 * {@link NurseryPlanning} which this {@link NurseryPlanningSlot} is link with
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nursery_planning_id", referencedColumnName = "nursery_planning_id")
	private NurseryPlanning nurseryPlanning;
	
	/**
	 * {@link NurseryPlanningSlot} date
	 */
	@Column(name="date")
	private Timestamp date;
	
	/**
	 * {@link NurseryPlanningSlot} duration
	 */
	@Column(name="duration")
	private Integer duration;
	
	/**
	 * {@link NurseryPlanningSlot} start time
	 */
	@Column(name="start_time")
	private Timestamp startTime;
	
	/**
	 * {@link NurseryPlanningSlot} end time
	 */
	@Column(name="end_time")
	private Timestamp endTime;
	
	/**
	 * {@link NurseryPlanningSlot} number places
	 */
	@Column(name="nursery_slot_number_places")
	private Integer nurserySlotNumberPlaces;

	/**
	 * Getter for {@link NurseryPlanningSlot} identifier
	 * @return an integer which represent current value of {@link NurseryPlanningSlot} identifier
	 */
	public Integer getNurseryPlanningSlotId() {
		return nurseryPlanningSlotId;
	}

	/**
	 * Getter for {@link NurseryPlanning} property
	 * @return current value of {@link NurseryPlanning} property
	 */
	public NurseryPlanning getNurseryPlanning() {
		return nurseryPlanning;
	}

	/**
	 * Getter for {@link NurseryPlanningSlot} date
	 * @return a timestamp which represent {@link NurseryPlanningSlot} date current value
	 */
	public Timestamp getDate() {
		return date;
	}

	/**
	 * Getter for {@link NurseryPlanningSlot} duration
	 * @return an integer which represent {@link NurseryPlanningSlot} duration current value
	 */
	public Integer getDuration() {
		return duration;
	}

	/**
	 * Getter for {@link NurseryPlanningSlot} start time property
	 * @return a timestamp which represent {@link NurseryPlanningSlot} start time current value
	 */
	public Timestamp getStartTime() {
		return startTime;
	}

	/**
	 * Getter for {@link NurseryPlanningSlot} end time property
	 * @return a timestamp which represent {@link NurseryPlanningSlot} end time current value
	 */
	public Timestamp getEndTime() {
		return endTime;
	}

	/**
	 * Getter for {@link NurseryPlanningSlot} number places
	 * @return a integer which represent {@link NurseryPlanningSlot} number places current value
	 */
	public Integer getNurserySlotNumberPlaces() {
		return nurserySlotNumberPlaces;
	}

	/**
	 * Setter for {@link NurseryPlanningSlot} identifier
	 * @param nurseryPlanningSlotId 
	 */
	public void setNurseryPlanningSlotId(Integer nurseryPlanningSlotId) {
		this.nurseryPlanningSlotId = nurseryPlanningSlotId;
	}

	/**
	 * Setter for {@link NurseryPlanningSlot} {@link NurseryPlanning} field
	 * @param nurseryPlanning next value of {@link NurseryPlanning} field
	 */
	public void setNurseryPlanning(NurseryPlanning nurseryPlanning) {
		this.nurseryPlanning = nurseryPlanning;
	}

	/**
	 * Setter for {@link NurseryPlanningSlot} date 
	 * @param date next value of {@link NurseryPlanningSlot} date field
	 */
	public void setDate(Timestamp date) {
		this.date = date;
	}

	/**
	 * Setter for {@link NurseryPlanningSlot} duration field
	 * @param duration next value of {@link NurseryPlanningSlot} duration field
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * Setter for {@link NurseryPlanningSlot} start time field
	 * @param startTime
	 */
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	/**
	 * Setter for {@link NurseryPlanningSlot} end time field
	 * @param endTime
	 */
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	/**
	 * Setter for {@link NurseryPlanningSlot} number place field 
	 * @param nurserySlotNumberPlaces
	 */
	public void setNurserySlotNumberPlaces(Integer nurserySlotNumberPlaces) {
		this.nurserySlotNumberPlaces = nurserySlotNumberPlaces;
	}
	
	
}

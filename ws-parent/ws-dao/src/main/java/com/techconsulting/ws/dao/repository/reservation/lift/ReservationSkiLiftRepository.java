package com.techconsulting.ws.dao.repository.reservation.lift;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.techconsulting.ws.dao.entity.reservation.lift.ReservationLift;

@Repository
public interface ReservationSkiLiftRepository extends JpaRepository<ReservationLift, Integer>{

	List<ReservationLift> findByReservLiftPkUserUserId(Integer idUser);
	
	void deleteByReservLiftPkSkiLiftSkiLiftId(Integer idSkilift);
	
	void deleteByReservLiftPkUserUserId(Integer idUser);
}

package com.techconsulting.ws.dao.config;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.transaction.TransactionManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Config class use to define a {@link TransactionManager}
 * 
 * @author Jeremy
 *
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.techconsulting.ws.dao.repository")
@ComponentScan(basePackages = "com.techconsulting.ws.dao")
@EnableTransactionManagement
public class TransactionManagerConfig {

	@Autowired
	EntityManagerConfig config;

	/**
	 * Create Transaction manager bean use by spring to execute transaction
	 * 
	 * @param config {@link EntityManagerFactory}
	 * @return {@link PlatformTransactionManager}
	 * @throws NamingException
	 */
	@Bean
	public PlatformTransactionManager transactionManager() throws NamingException {

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(config.entityManagerFactory().getObject());
		return txManager;
	}

}

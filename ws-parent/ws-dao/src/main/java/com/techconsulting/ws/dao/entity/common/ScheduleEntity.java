package com.techconsulting.ws.dao.entity.common;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The Schedule's Class. This class describe the Schedule entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "schedule_seq", sequenceName = "schedule_seq", allocationSize = 1)
@Table(name = "schedule", schema = "public")
public class ScheduleEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The schedule's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "schedule_id", unique = true, nullable = false)
	private Integer scheduleId;

	/**
	 * The schedule's day name
	 */
	@Column(name = "schedule_day")
	private String scheduleDay;

	/**
	 * The schedule's open hour
	 */
	@Column(name = "schedule_open_hour")
	private String scheduleOpenHour;

	/**
	 * The schedule's close hour
	 */
	@Column(name = "schedule_close_hour")
	private String scheduleCloseHour;

	public ScheduleEntity() {
		super();
	}

	/**
	 * @return the scheduleId
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * @param scheduleId the scheduleId to set
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * @return the scheduleDay
	 */
	public String getScheduleDay() {
		return scheduleDay;
	}

	/**
	 * @param scheduleDay the scheduleDay to set
	 */
	public void setScheduleDay(String scheduleDay) {
		this.scheduleDay = scheduleDay;
	}

	/**
	 * @return the scheduleOpenHour
	 */
	public String getScheduleOpenHour() {
		return scheduleOpenHour;
	}

	/**
	 * @param scheduleOpenHour the scheduleOpenHour to set
	 */
	public void setScheduleOpenHour(String scheduleOpenHour) {
		this.scheduleOpenHour = scheduleOpenHour;
	}

	/**
	 * @return the scheduleCloseHour
	 */
	public String getScheduleCloseHour() {
		return scheduleCloseHour;
	}

	/**
	 * @param scheduleCloseHour the scheduleCloseHour to set
	 */
	public void setScheduleCloseHour(String scheduleCloseHour) {
		this.scheduleCloseHour = scheduleCloseHour;
	}

}

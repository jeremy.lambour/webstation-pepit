package com.techconsulting.ws.dao.repository.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.reservation.Reservation;

/**
 * JpaRepository use to question Reservation table in database
 * @author Paul
 *
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
}


package com.techconsulting.ws.dao.entity.equipment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.techconsulting.ws.dao.entity.shop.ShopEntity;

/**
 * Equipment's entity
 * @author Paul
 *
 */
@Entity
@Table(name = "equipment")
@SequenceGenerator(name = "equipment_equipment_id_seq", sequenceName = "equipment_equipment_id_seq", allocationSize = 1)
public class Equipment implements Serializable{
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * equipment identifier
	 */
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "equipment_equipment_id_seq")
	@Column(name = "equipment_id")
	private @Id Integer equipmentId;

	/**
	 * Equipment's name
	 */
	@Column(name = "equipment_name")
	private String equipmentName;
	
	/**
	 * Equipment's description
	 */
	@Column(name = "equipment_description")
	private String equipmentDescription;
	
	/**
	 * Equipment's state
	 */
	@Column(name = "equipment_state")
	private String equipmentState;
	
	/**
	 * Equipment's stock
	 */
	@Column(name = "equipment_stock")
	private Integer equipmentStock;
	
	/**
	 * Number of removed equipment from the stock
	 */
	@Column(name = "equipment_out")
	private Integer equipmentOut;
	
	/**
	 * Equipment's size
	 */
	@Column(name = "equipment_size")
	private String equipmentSize;
	
	/**
	 * Equipment's price location
	 */
	@Column(name = "price_location")
	private Integer priceLocation;
	
	/**
	 * the equipment's shop
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_id", referencedColumnName = "shop_id")
	private ShopEntity shop;

	/**
	 * The equipment constructor
	 */
	public Equipment() {
		super();
	}
	
	/**
	 * Getter for property equipmentId
	 * @return {@link Integer}
	 */
	public Integer getEquipmentId() {
		return equipmentId;
	}

	/**
	 * Setter for property equipmentId
	 * @param {@link Integer}
	 */
	public void setEquipmentId(Integer equipmentId) {
		this.equipmentId = equipmentId;
	}

	/**
	 * Getter for property equipmentName
	 * @return {@link String}
	 */
	public String getEquipmentName() {
		return equipmentName;
	}

	/**
	 * Setter for property equipmentName
	 * @param {@link String}
	 */
	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	/**
	 * Getter for property equipmentDescription
	 * @return {@link String}
	 */
	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	/**
	 * Setter for property equipmentDescription
	 * @param {@link String}
	 */
	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}

	/**
	 * Getter for property equipmentState
	 * @return {@link String}
	 */
	public String getEquipmentState() {
		return equipmentState;
	}

	/**
	 * Setter for property equipmentState
	 * @param {@link String}
	 */
	public void setEquipmentState(String equipmentState) {
		this.equipmentState = equipmentState;
	}

	/**
	 * Getter for property equipmentStock
	 * @return {@link Integer}
	 */
	public Integer getEquipmentStock() {
		return equipmentStock;
	}

	/**
	 * Setter for property equipmentStock
	 * @param {@link Integer}
	 */
	public void setEquipmentStock(Integer equipmentStock) {
		this.equipmentStock = equipmentStock;
	}
	
	/**
	 * Getter for property equipmentOut
	 * @return {@link Integer}
	 */
	public Integer getEquipmentOut() {
		return equipmentOut;
	}

	/**
	 * Setter for property equipmentOut
	 * @param {@link Integer}
	 */
	public void setEquipmentOut(Integer equipmentOut) {
		this.equipmentOut = equipmentOut;
	}

	/**
	 * Getter for property equipmentSize
	 * @return {@link Integer}
	 */
	public String getEquipmentSize() {
		return equipmentSize;
	}

	/**
	 * Setter for property equipmentSize
	 * @param {@link Integer}
	 */
	public void setEquipmentSize(String equipmentSize) {
		this.equipmentSize = equipmentSize;
	}
	
	/**
	 * Getter for property priceLocation
	 * @return {@link Integer}
	 */
	public Integer getPriceLocation() {
		return priceLocation;
	}

	/**
	 * Setter for property priceLocation
	 * @param {@link Integer}
	 */
	public void setPriceLocation(Integer priceLocation) {
		this.priceLocation = priceLocation;
	}

	/**
	 * Getter for property shop of an equipment
	 * @return {@link ShopEntity}
	 */
	public ShopEntity getShop() {
		return shop;
	}

	/**
	 * Setter for property shop of an equipment
	 * @param {@link ShopEntity}
	 */
	public void setShop(ShopEntity shop) {
		this.shop = shop;
	}
}

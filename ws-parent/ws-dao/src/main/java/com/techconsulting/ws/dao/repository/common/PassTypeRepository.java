package com.techconsulting.ws.dao.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.common.PassType;

/**
 * JpaRepository use to question SkiLift table in database
 * @author Paul
 *
 */
@Repository
public interface PassTypeRepository extends JpaRepository<PassType, Integer>{

}

package com.techconsulting.ws.dao.entity.common.comment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.techconsulting.ws.dao.entity.auth.User;

/**
 * The Comment's Class. This class describe a comment entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "comment_seq", sequenceName = "comment_seq", allocationSize = 1)
@Table(name = "comment", schema = "public")
public class Comment implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * The comment's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_id", unique = true, nullable = false)
	private Integer commentId;
	
	/**
	 * The comment's content
	 */
	@Column(name = "content")
	private String content;
	
	/**
	 * The user who send the comment
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User from;
	
	/**
	 * The comment's note
	 */
	@Column(name = "note")
	private int note;

	/**
	 * comment's constructor
	 */
	public Comment() {
		super();
	}

	/**
	 * Getter for property commentId
	 * 
	 * @return {@link Integer}
	 */
	public Integer getCommentId() {
		return commentId;
	}

	/**
	 * Setter for property commentId
	 * 
	 * @param commentId
	 */
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	/**
	 * Getter for property content
	 * 
	 * @return {@link String}
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Setter for property content
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Getter for property from
	 * 
	 * @return {@link User}
	 */
	public User getFrom() {
		return from;
	}

	/**
	 * Setter for property from
	 * 
	 * @param {@link User}
	 */
	public void setFrom(User from) {
		this.from = from;
	}

	/**
	 * Getter for property note
	 * 
	 * @return {@link Integer}
	 */
	public Integer getNote() {
		return note;
	}

	/**
	 * Setter for property note
	 * 
	 * @param {@link Integer}
	 */
	public void setNote(Integer note) {
		this.note = note;
	}
	
}

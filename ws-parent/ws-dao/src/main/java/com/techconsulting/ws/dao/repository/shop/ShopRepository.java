package com.techconsulting.ws.dao.repository.shop;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.shop.ShopEntity;

/**
 * JpaRepository use to question Shop table in database
 * @author gaillardc
 *
 */
public interface ShopRepository extends JpaRepository<ShopEntity, Integer> {
	List<ShopEntity> findShopByUserUserId(Integer idUser);

}

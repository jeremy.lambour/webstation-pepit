package com.techconsulting.ws.dao.repository.common.comment.association.flat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.common.comment.association.hotel.CommentHotel;

/**
 * JpaRepository use to question CommentHotel table in database
 * 
 * @author Paul
 *
 */
@Repository
public interface CommentHotelRepository extends JpaRepository<CommentHotel, Integer> {
	List<CommentHotel> findByCommentHotelPkHotelHotelId(Integer idHotel);

	boolean deleteByCommentHotelPkCommentCommentId(Integer idComment);

	boolean deleteByCommentHotelPkHotelHotelId(Integer idHotel);
}

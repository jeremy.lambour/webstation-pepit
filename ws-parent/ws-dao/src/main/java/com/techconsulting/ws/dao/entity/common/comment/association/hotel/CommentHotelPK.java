package com.techconsulting.ws.dao.entity.common.comment.association.hotel;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.techconsulting.ws.dao.entity.common.comment.Comment;
import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;

/**
 * Comment's Table association between flat and comment
 * 
 * @author Paul
 *
 */
@Embeddable
public class CommentHotelPK  implements Serializable  {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The comment
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comment_id", referencedColumnName = "comment_id")
	private Comment comment;

	/**
	 * the flat commented
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hotel_id", referencedColumnName = "hotel_id")
	private HotelEntity hotel;

	/**
	 * The comment's equipment pk Constructor
	 */
	public CommentHotelPK() {
		super();
	}

	/**
	 * Getter for property comment
	 * 
	 * @return {@link Comment}
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * Setter for property comment
	 * 
	 * @param {@link Comment}
	 */
	public void setComment(Comment comment) {
		this.comment = comment;
	}

	/**
	 * Getter for property hotel
	 * 
	 * @return {@link HotelEntity}
	 */
	public HotelEntity getHotel() {
		return hotel;
	}

	/**
	 * Setter for property hotel
	 * 
	 * @param {@link HotelEntity}
	 */
	public void setHotel(HotelEntity hotel) {
		this.hotel = hotel;
	}

	
}

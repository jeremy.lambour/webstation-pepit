package com.techconsulting.ws.dao.repository.reservation.flat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.dao.entity.reservation.flat.ReservationFlat;

public interface ReservationFlatRepository extends JpaRepository<ReservationFlat, Integer>{
	
	List<ReservationFlat> findByReservationFlatIdFlat(FlatEntity flat);
	
	List<ReservationFlat> findByReservationFlatIdReservationUserUserId(Integer userId);

}

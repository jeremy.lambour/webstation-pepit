package com.techconsulting.ws.dao.entity.reservation.equipment;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * ReservationEquipment's Entity
 * 
 * @author Paul
 *
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "reserv_equipment_seq", sequenceName = "reserv_equipment_seq", allocationSize = 1)
@Table(name = "reserv_equipment")
public class ReservationEquipment implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The {@link ReservationEquipmentPK}
	 */
	@EmbeddedId
	private ReservationEquipmentPK reservEquipmentEntityPk;

	/**
	 * the date who start the reservation
	 */
	@Column(name = "date_start")
	private Timestamp dateStart;

	/**
	 * the date who stop the reservation
	 */
	@Column(name = "date_end")
	private Timestamp dateEnd;

	/**
	 * Equipment quantity reserved
	 */
	@Column(name = "quantity")
	private Integer quantity;
	
	/**
	 * Reservation's price
	 */
	@Column(name = "price")
	private Integer price;
	
	/**
	 * Reservation's isGiven status
	 */
	@Column(name= "isgiven")
	private Boolean isGiven;
	
	/**
	 * The reservation equipment constructor
	 */
	public ReservationEquipment() {
		super();
	}

	/**
	 * Getter for property reservationId
	 * 
	 * @return {@link Integer}
	 */
	public ReservationEquipmentPK getReservEquipmentEntityPk() {
		return reservEquipmentEntityPk;
	}

	/**
	 * Setter for property {@link ReservationEquipmentPK}
	 * 
	 * @param {@link ReservationEquipmentPK}
	 */
	public void setReservEquipmentEntityPk(ReservationEquipmentPK reservEquipmentEntityPk) {
		this.reservEquipmentEntityPk = reservEquipmentEntityPk;
	}

	/**
	 * Getter for property dateStart
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateStart() {
		return dateStart;
	}

	/**
	 * Setter for property dateStart
	 * 
	 * @param a Timestamp
	 */
	public void setDateStart(Timestamp dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for property dateEnd
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateEnd() {
		return dateEnd;
	}

	/**
	 * Setter for property dateEnd
	 * 
	 * @param a Timestamp
	 */
	public void setDateEnd(Timestamp dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * Getter for property quantity
	 * 
	 * @return a Integer
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * Setter for property quantity
	 * 
	 * @param a Integer
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * Getter for property price
	 * 
	 * @return a Integer
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * Setter for property price
	 * 
	 * @param a Integer
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * @return the isGiven
	 */
	public Boolean getIsGiven() {
		return isGiven;
	}

	/**
	 * @param isGiven the isGiven to set
	 */
	public void setIsGiven(Boolean isGiven) {
		this.isGiven = isGiven;
	}

	
}

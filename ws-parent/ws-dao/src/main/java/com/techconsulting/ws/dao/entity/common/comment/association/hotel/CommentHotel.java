package com.techconsulting.ws.dao.entity.common.comment.association.hotel;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;


/**
 * CommentFlat's Entity
 * 
 * @author Paul
 *
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "comment_hotel_seq", sequenceName = "comment_hotel_seq", allocationSize = 1)
@Table(name = "comment_hotel")
public class CommentHotel  implements Serializable {
	
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The {@link CommentHotelPK}
	 */
	@EmbeddedId
	private CommentHotelPK commentHotelPk;

	/**
	 * The comment's hotel pk Constructor
	 */
	public CommentHotel() {
		super();
	}

	/**
	 * Getter for property commentHotelPk
	 * 
	 * @return {@link CommentHotelPK}
	 */
	public CommentHotelPK getCommentHotelPk() {
		return commentHotelPk;
	}

	/**
	 * Setter for property commentHotelPk
	 * 
	 * @param {@link CommentHotelPK}
	 */
	public void setCommentHotelPk(CommentHotelPK commentHotelPk) {
		this.commentHotelPk = commentHotelPk;
	}
}

package com.techconsulting.ws.dao.entity.skiingSchool;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * Define a skiingSchool Entity in database
 * @author j_lamb
 *
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "skiing_school_seq", sequenceName = "skiing_school_skiing_school_id_seq", allocationSize = 1)
@Table(name = "skiing_school", schema = "public")
public class SkiingSchool implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Skiing school identifier
	 */
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "skiing_school_seq")
	@Column(name = "skiing_school_id")
	private	@Id Integer skiingSchoolId;
	
	/**
	 * Skiing school name
	 */
	@Column(name = "skiing_school_name")
	private String skiingSchoolName;

	/**
	 * Skiing school phone number
	 */
	@Column(name = "phone_number")
	private String phoneNumber;

	/**
	 * Get skiing school identifier
	 * @return an Integer which represent skiing school identifier
	 */
	public Integer getSkiingSchoolId() {
		return skiingSchoolId;
	}

	/**
	 * Set skiing school identifier
	 * @param skiingSchoolId skiing school identifier
	 */
	public void setSkiingSchoolId(Integer skiingSchoolId) {
		this.skiingSchoolId = skiingSchoolId;
	}

	/**
	 * Get skiing school name
	 * @return String which represent skiing school name
	 */
	public String getSkiingSchoolName() {
		return skiingSchoolName;
	}

	/**
	 * Set skiing school name
	 * @param skiingSchoolName skiing school name
	 */
	public void setSkiingSchoolName(String skiingSchoolName) {
		this.skiingSchoolName = skiingSchoolName;
	}

	/**
	 * Set skiing school phone number
	 * @return a string which represent skiing school phone number 
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Set skiing school phone number
	 * @param phoneNumber new skiing school phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
}

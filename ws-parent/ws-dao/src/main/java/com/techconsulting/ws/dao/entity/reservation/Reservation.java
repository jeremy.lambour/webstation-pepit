package com.techconsulting.ws.dao.entity.reservation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.techconsulting.ws.dao.entity.auth.User;

/**
 * Reservation's Entity
 * 
 * @author Paul
 *
 */
@Entity
@Table(name = "reservation")
@SequenceGenerator(name = "reservation_reservation_id_seq", sequenceName = "reservation_reservation_id_seq", allocationSize = 1)
public class Reservation implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * reservation identifier
	 */
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "reservation_reservation_id_seq")
	@Column(name = "reservation_id")
	private @Id Integer reservationId;

	/**
	 * number of persons in reservation
	 */
	@Column(name = "nb_persons_reservation")
	private Integer nbPersons;

	/**
	 * the price of the insurance for loss equipment
	 */
	@Column(name = "insurance_for_loss_of_material")
	private Integer insuranceLossMaterial;

	/**
	 * the price when cancel a housing reservation
	 */
	@Column(name = "housing_cancellation_option")
	private Integer houseCancelOpt;

	/**
	 * the price when cancel a equipment reservation
	 */
	@Column(name = "equipment_cancellation_option")
	private Integer equipmentCancelOpt;

	/**
	 * the price when cancel the reservation
	 */
	@Column(name = "reservation_cancellation_option")
	private Integer reservCancelopt;

	/**
	 * the reservation's user
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;

	/**
	 * reservation's constructor
	 */
	public Reservation() {
		super();
	}

	/**
	 * Getter for property reservationId
	 * 
	 * @return {@link Integer}
	 */
	public Integer getReservationId() {
		return reservationId;
	}

	/**
	 * Setter for property reservationId
	 * 
	 * @param reservationId
	 */
	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	/**
	 * Getter for property nbPersons
	 * 
	 * @return {@link Integer}
	 */
	public Integer getNbPersons() {
		return nbPersons;
	}

	/**
	 * Setter for property nbPersons
	 * 
	 * @param nbPersons
	 */
	public void setNbPersons(Integer nbPersons) {
		this.nbPersons = nbPersons;
	}

	/**
	 * Getter for property insuranceLossMaterial
	 * 
	 * @return {@link Integer}
	 */
	public Integer getInsuranceLossMaterial() {
		return insuranceLossMaterial;
	}

	/**
	 * Setter for property insuranceLossMaterial
	 * 
	 * @param insuranceLossMaterial
	 */
	public void setInsuranceLossMaterial(Integer insuranceLossMaterial) {
		this.insuranceLossMaterial = insuranceLossMaterial;
	}

	/**
	 * Getter for property houseCancelOpt
	 * 
	 * @return {@link Integer}
	 */
	public Integer getHouseCancelOpt() {
		return houseCancelOpt;
	}

	/**
	 * Setter for property houseCancelOpt
	 * 
	 * @param houseCancelOpt
	 */
	public void setHouseCancelOpt(Integer houseCancelOpt) {
		this.houseCancelOpt = houseCancelOpt;
	}

	/**
	 * Getter for property equipmentCancelOpt
	 * 
	 * @return {@link Integer}
	 */
	public Integer getEquipmentCancelOpt() {
		return equipmentCancelOpt;
	}

	/**
	 * Setter for property equipmentCancelOpt
	 * 
	 * @param equipmentCancelOpt
	 */
	public void setEquipmentCancelOpt(Integer equipmentCancelOpt) {
		this.equipmentCancelOpt = equipmentCancelOpt;
	}

	/**
	 * Getter for property reservCancelopt
	 * 
	 * @return {@link Integer}
	 */
	public Integer getReservCancelopt() {
		return reservCancelopt;
	}

	/**
	 * Setter for property reservCancelopt
	 * 
	 * @param reservCancelopt
	 */
	public void setReservCancelopt(Integer reservCancelopt) {
		this.reservCancelopt = reservCancelopt;
	}

	/**
	 * Getter for property {@link User}
	 * 
	 * @return {@link User}
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Setter for property {@link User}
	 * 
	 * @param {@link User}
	 */
	public void setUser(User user) {
		this.user = user;
	}

}

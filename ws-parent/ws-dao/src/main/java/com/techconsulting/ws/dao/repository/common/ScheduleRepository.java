package com.techconsulting.ws.dao.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.common.ScheduleEntity;

/**
 * JpaRepository use to question Schedule table in database
 * @author gaillardc
 *
 */
public interface ScheduleRepository extends JpaRepository<ScheduleEntity, Integer> {
	
	/**
	 * Method that deletes a {@link ScheduleDTO} from a specific identifier
	 * 
	 * @Param - Integer idSchedule, the identifier of the {@link ScheduleDTO} we want to
	 *        delete
	 */
	void deleteByScheduleId(Integer idSchedule);

}

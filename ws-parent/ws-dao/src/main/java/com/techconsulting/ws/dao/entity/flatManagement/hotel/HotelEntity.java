package com.techconsulting.ws.dao.entity.flatManagement.hotel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.techconsulting.ws.dao.entity.auth.User;

/**
 * The Hotel's Class. This class describe the Hotel entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "hotel_seq", sequenceName = "hotel_seq", allocationSize = 1)
@Table(name = "hotel", schema = "public")
public class HotelEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * The hotel's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hotel_id", unique = true, nullable = false)
	private Integer hotelId;

	/**
	 * The hotel's name
	 */
	@Column(name = "hotel_name")
	private String hotelName;

	/**
	 * The hotel's rooms number
	 */
	@Column(name = "hotel_rooms_number")
	private Integer hotelRoomsNumber;

	/**
	 * The hotel's phone number
	 */
	@Column(name = "phone_number")
	private String phoneNumber;
	
	/**
	 * The hotel's mail_address
	 */
	@Column(name = "mail_address")
	private String mailAddress;
	
	/**
	 * The hotel's address
	 */
	@Column(name = "address")
	private String address;
	
	/**
	 * The {@link User}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;
	
	public HotelEntity() {
		super();
	}

	/**
	 * @return the hotelId
	 */
	public Integer getHotelId() {
		return hotelId;
	}

	/**
	 * @param hotelId the hotelId to set
	 */
	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}

	/**
	 * @return the hotelName
	 */
	public String getHotelName() {
		return hotelName;
	}

	/**
	 * @param hotelName the hotelName to set
	 */
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	/**
	 * @return the hotelRoomsNumber
	 */
	public Integer getHotelRoomsNumber() {
		return hotelRoomsNumber;
	}

	/**
	 * @param hotelRoomsNumber the hotelRoomsNumber to set
	 */
	public void setHotelRoomsNumber(Integer hotelRoomsNumber) {
		this.hotelRoomsNumber = hotelRoomsNumber;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the mailAddress
	 */
	public String getMailAddress() {
		return mailAddress;
	}

	/**
	 * @param mailAdress the mailAddress to set
	 */
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}

package com.techconsulting.ws.dao.repository.shop;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.shop.ShopScheduleEntity;

/**
 * JpaRepository use to question ShopSchedule table in database
 * @author gaillardc
 *
 */
public interface ShopScheduleRepository extends JpaRepository<ShopScheduleEntity, Integer> {
	List<ShopScheduleEntity> findByShopScheduleIdShopEntityShopId(Integer idShop);
	
	void deleteByShopScheduleIdShopEntityShopId(Integer idShop);
}

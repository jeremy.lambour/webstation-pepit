package com.techconsulting.ws.dao.entity.shop;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.techconsulting.ws.dao.entity.common.ScheduleEntity;

/**
 * The ShopScheduleIdEntity class. This class is an association class between
 * {@link ShopEntity} and {@link ScheduleEntity}
 */
@Embeddable
public class ShopScheduleIdEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The {@link ShopEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_id", referencedColumnName = "shop_id")
	private ShopEntity shopEntity;

	/**
	 * The {@link ScheduleEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id")
	private ScheduleEntity scheduleEntity;

	public ShopScheduleIdEntity() {
		super();
	}

	/**
	 * @return the shopEntity
	 */
	public ShopEntity getShopEntity() {
		return shopEntity;
	}

	/**
	 * @param shopEntity the shopEntity to set
	 */
	public void setShopEntity(ShopEntity shopEntity) {
		this.shopEntity = shopEntity;
	}

	/**
	 * @return the scheduleEntity
	 */
	public ScheduleEntity getScheduleEntity() {
		return scheduleEntity;
	}

	/**
	 * @param scheduleEntity the scheduleEntity to set
	 */
	public void setScheduleEntity(ScheduleEntity scheduleEntity) {
		this.scheduleEntity = scheduleEntity;
	}

}

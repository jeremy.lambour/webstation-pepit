package com.techconsulting.ws.dao.repository.nursery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.nursery.Nursery;

/**
 * {@link NurseryRepository}
 * @author Jeremy
 *
 */
@Repository
public interface NurseryRepository extends JpaRepository<Nursery, Integer> {

}

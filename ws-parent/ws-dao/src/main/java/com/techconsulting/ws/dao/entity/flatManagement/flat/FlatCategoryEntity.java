package com.techconsulting.ws.dao.entity.flatManagement.flat;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The FlatCategory's Class. This class describe the FlatCategory's entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "flatCategory_seq", sequenceName = "flatCategory_seq", allocationSize = 1)
@Table(name = "flatCategory", schema = "public")
public class FlatCategoryEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * The flatCategory's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "flat_category_id", unique = true, nullable = false)
	private Integer flatCategoryId;

	/**
	 * The flatCategory's name
	 */
	@Column(name = "flat_category_name")
	private String flatCategoryName;
	
	public FlatCategoryEntity() {
		super();
	}

	/**
	 * @return the flatCategoryId
	 */
	public Integer getFlatCategoryId() {
		return flatCategoryId;
	}

	/**
	 * @param flatCategoryId the flatCategoryId to set
	 */
	public void setFlatCategoryId(Integer flatCategoryId) {
		this.flatCategoryId = flatCategoryId;
	}

	/**
	 * @return the flatCategoryName
	 */
	public String getFlatCategoryName() {
		return flatCategoryName;
	}

	/**
	 * @param flatCategoryName the flatCategoryName to set
	 */
	public void setFlatCategoryName(String flatCategoryName) {
		this.flatCategoryName = flatCategoryName;
	}

}

package com.techconsulting.ws.dao.entity.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The Season's Class. This class describe the Season entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "season_seq", sequenceName = "season_seq", allocationSize = 1)
@Table(name = "season", schema = "public")
public class SeasonEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * The season's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "season_id", unique = true, nullable = false)
	private Integer seasonId;

	/**
	 * The season's name
	 */
	@Column(name = "season_name")
	private String seasonName;

	/**
	 * The season's beginning
	 */
	@Column(name = "season_beginning")
	private Date seasonBeginning;

	/**
	 * The season's end
	 */
	@Column(name = "season_end")
	private Date seasonEnd;
	
	/**
	 * The season's active status
	 */
	@Column(name = "active")
	private Boolean active;
	

	public SeasonEntity() {
		super();
	}

	/**
	 * @return the seasonId
	 */
	public Integer getSeasonId() {
		return seasonId;
	}

	/**
	 * @param seasonId the seasonId to set
	 */
	public void setSeasonId(Integer seasonId) {
		this.seasonId = seasonId;
	}

	/**
	 * @return the seasonName
	 */
	public String getSeasonName() {
		return seasonName;
	}

	/**
	 * @param seasonName the seasonName to set
	 */
	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}

	/**
	 * @return the seasonBeginning
	 */
	public Date getSeasonBeginning() {
		return seasonBeginning;
	}

	/**
	 * @param seasonBeginning the seasonBeginning to set
	 */
	public void setSeasonBeginning(Date seasonBeginning) {
		this.seasonBeginning = seasonBeginning;
	}

	/**
	 * @return the seasonEnd
	 */
	public Date getSeasonEnd() {
		return seasonEnd;
	}

	/**
	 * @param seasonEnd the seasonEnd to set
	 */
	public void setSeasonEnd(Date seasonEnd) {
		this.seasonEnd = seasonEnd;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
	
	

}

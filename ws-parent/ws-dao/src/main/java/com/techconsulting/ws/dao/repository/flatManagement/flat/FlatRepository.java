package com.techconsulting.ws.dao.repository.flatManagement.flat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatCategoryEntity;
import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatEntity;
import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;

/**
 * JpaRepository use to question Flat table in database
 * @author gaillardc
 *
 */
public interface FlatRepository extends JpaRepository<FlatEntity, Integer>{
	List<FlatEntity> findByHotelEntity(HotelEntity hotel);
	List<FlatEntity> findByFlatCategoryEntityAndHotelEntity(FlatCategoryEntity flatCategoryEntity, HotelEntity hotelEntity);
	
}

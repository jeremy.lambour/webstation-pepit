package com.techconsulting.ws.dao.entity.reservation.lift;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.techconsulting.ws.dao.entity.reservation.equipment.ReservationEquipmentPK;

/**
 * The ReservationLift's Class. This class describe the ReservationLift entity
 * in database
 */
@Entity
@Table(name = "reserv_skilift")
public class ReservationLift implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The {@link ReservationLiftPK}
	 */
	@EmbeddedId
	private ReservationLiftPK reservLiftPk;

	/**
	 * the date who start the reservation
	 */
	@Column(name = "datestart")
	private Timestamp dateStart;

	/**
	 * the date who stop the reservation
	 */
	@Column(name = "dateend")
	private Timestamp dateEnd;

	/**
	 * caution of the reservation
	 */
	@Column(name = "caution")
	private float caution;

	/**
	 * Inusurance for no snowing day
	 */
	@Column(name = "insurancesnow")
	private float insuranceSnow;

	/**
	 * price of the reservation
	 */
	@Column(name = "price")
	private float price;

	/**
	 * Generic constructor
	 */
	public ReservationLift() {
		super();
	}

	/**
	 * Getter for property reservationId
	 * 
	 * @return {@link Integer}
	 */
	public ReservationLiftPK getReservLiftPk() {
		return reservLiftPk;
	}

	/**
	 * Setter for property {@link ReservationEquipmentPK}
	 * 
	 * @param {@link ReservationEquipmentPK}
	 */
	public void setReservLiftPk(ReservationLiftPK reservELiftPk) {
		this.reservLiftPk = reservELiftPk;
	}

	/**
	 * Getter for property dateStart
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateStart() {
		return dateStart;
	}

	/**
	 * Setter for property dateStart
	 * 
	 * @param a Timestamp
	 */
	public void setDateStart(Timestamp dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for property dateEnd
	 * 
	 * @return a Timestamp
	 */
	public Timestamp getDateEnd() {
		return dateEnd;
	}

	/**
	 * Setter for property dateEnd
	 * 
	 * @param a Timestamp
	 */
	public void setDateEnd(Timestamp dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * Getter for property caution
	 * 
	 * @return {@link float}
	 */
	public float getCaution() {
		return caution;
	}

	/**
	 * Setter for property caution
	 * 
	 * @param {@link float}
	 */
	public void setCaution(float caution) {
		this.caution = caution;
	}

	/**
	 * Getter for property insurance Snow
	 * 
	 * @return {@link float}
	 */
	public float getInsuranceSnow() {
		return insuranceSnow;
	}

	/**
	 * Setter for property insurance Snow
	 * 
	 * @param {@link float}
	 */
	public void setInsuranceSnow(float insuranceSnow) {
		this.insuranceSnow = insuranceSnow;
	}

	/**
	 * Getter for property price
	 * 
	 * @return {@link float}
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * Setter for property price
	 * 
	 * @param {@link float}
	 */
	public void setPrice(float price) {
		this.price = price;
	}

}

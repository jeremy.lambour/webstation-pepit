package com.techconsulting.ws.dao.repository.auth;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.auth.User;


/**
 * JpaRepository use to question User table in database
 * @author Jeremy
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	/**
	 * Get {@link User} target by username give in parameter
	 * @param login search username
	 * @return {@link User}
	 */
	User findByUsername(String login);
	
	List<User> findByRoleAuthorityName(String authorityName);


}

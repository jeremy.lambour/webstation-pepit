package com.techconsulting.ws.dao.entity.equipment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.techconsulting.ws.dao.entity.auth.AgeRange;
import com.techconsulting.ws.dao.entity.common.PassType;

/**
 * The SkiLift's Class. This class describe the SkiLift entity in database
 */
@Entity
@Table(name = "ski_lift")
@SequenceGenerator(name = "ski_lift_ski_lift_id_seq", sequenceName = "ski_lift_ski_lift_id_seq", allocationSize = 1)
public class SkiLift implements Serializable {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * SkiLift identifier
	 */
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "ski_lift_ski_lift_id_seq")
	@Column(name = "ski_lift_id")
	private @Id Integer skiLiftId;

	/**
	 * Reservation's duration
	 */
	@Column(name = "duration")
	private String duration;

	/**
	 * Domain's name
	 */
	@Column(name = "domain")
	private String domain;

	/**
	 * Reservation's price
	 */
	@Column(name = "price")
	private float price;

	/**
	 * Pass's type
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pass_type", referencedColumnName = "pass_type_id")
	private PassType passType;

	/**
	 * Reservation's ageRange
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "age_range", referencedColumnName = "age_range_id")
	private AgeRange ageRange;

	/**
	 * Generic constructor
	 */
	public SkiLift() {
		super();
	}

	/**
	 * Getter of the identifier of ski lift
	 * 
	 * @return {@link Integer}
	 */
	public Integer getSkiLiftId() {
		return skiLiftId;
	}

	/**
	 * Setter of the identifier of ski lift
	 * 
	 * @param {@link Integer}
	 */
	public void setSkiLiftId(Integer skiLiftId) {
		this.skiLiftId = skiLiftId;
	}

	/**
	 * Getter of property duration
	 * 
	 * @return {@link String}
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * Setter of property duration
	 * 
	 * @param {@link String}
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * Getter of the property domain
	 * 
	 * @return {@link String}
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * Setter of the property domain
	 * 
	 * @param {@link String}
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * Getter of the property price reservation
	 * 
	 * @return {@link float}
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * Setter of the property price
	 * 
	 * @param {@link float}
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * Getter of the pass type
	 * 
	 * @return {@link PassType}
	 */
	public PassType getPassType() {
		return passType;
	}

	/**
	 * Setter of the pass type
	 * 
	 * @param {@link PassType}
	 */
	public void setPassType(PassType passType) {
		this.passType = passType;
	}

	/**
	 * Getter of the age range
	 * 
	 * @return {@link AgeRange}
	 */
	public AgeRange getAgeRange() {
		return ageRange;
	}

	/**
	 * Setter of the age range
	 * 
	 * @param {@link AgeRange}
	 */
	public void setAgeRange(AgeRange ageRange) {
		this.ageRange = ageRange;
	}

}

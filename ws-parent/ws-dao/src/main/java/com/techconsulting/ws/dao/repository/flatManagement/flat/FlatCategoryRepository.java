package com.techconsulting.ws.dao.repository.flatManagement.flat;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techconsulting.ws.dao.entity.flatManagement.flat.FlatCategoryEntity;

/**
 * JpaRepository use to question FlatCategory table in database
 * @author gaillardc
 *
 */
public interface FlatCategoryRepository extends JpaRepository<FlatCategoryEntity, Integer>{

}

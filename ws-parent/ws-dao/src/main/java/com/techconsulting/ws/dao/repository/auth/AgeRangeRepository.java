package com.techconsulting.ws.dao.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techconsulting.ws.dao.entity.auth.AgeRange;

/**
 * JpaRepository use to question AgeRange table in database
 * 
 * @author Jeremy
 *
 */
@Repository
public interface AgeRangeRepository extends JpaRepository<AgeRange, Integer> {
}

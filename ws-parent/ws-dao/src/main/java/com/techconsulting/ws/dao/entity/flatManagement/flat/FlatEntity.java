package com.techconsulting.ws.dao.entity.flatManagement.flat;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.techconsulting.ws.dao.entity.flatManagement.hotel.HotelEntity;

/**
 * The Flat's Class. This class describe the Flat entity in database
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "flat_seq", sequenceName = "flat_seq", allocationSize = 1)
@Table(name = "flat", schema = "public")
public class FlatEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The flat's identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "flat_id", unique = true, nullable = false)
	private Integer flatId;

	/**
	 * The flat's description
	 */
	@Column(name = "flat_description")
	private String flatDescription;

	/**
	 * The flat's guidance
	 */
	@Column(name = "flat_guidance")
	private String flatGuidance;

	/**
	 * The flat's capacity
	 */
	@Column(name = "flat_capacity")
	private Integer flatCapacity;
	
	/**
	 * The {@link HotelEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hotel_id", referencedColumnName = "hotel_id")
	private HotelEntity hotelEntity;
	
	/**
	 * The {@link FlatCategoryEntity}
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "flat_category_id", referencedColumnName = "flat_category_id")
	private FlatCategoryEntity flatCategoryEntity;
	
	public FlatEntity() {
		super();
	}

	/**
	 * @return the flatId
	 */
	public Integer getFlatId() {
		return flatId;
	}

	/**
	 * @param flatId the flatId to set
	 */
	public void setFlatId(Integer flatId) {
		this.flatId = flatId;
	}

	/**
	 * @return the flatDescription
	 */
	public String getFlatDescription() {
		return flatDescription;
	}

	/**
	 * @param flatDescription the flatDescription to set
	 */
	public void setFlatDescription(String flatDescription) {
		this.flatDescription = flatDescription;
	}

	/**
	 * @return the flatGuidance
	 */
	public String getFlatGuidance() {
		return flatGuidance;
	}

	/**
	 * @param flatGuidance the flatGuidance to set
	 */
	public void setFlatGuidance(String flatGuidance) {
		this.flatGuidance = flatGuidance;
	}

	/**
	 * @return the flatCapacity
	 */
	public Integer getFlatCapacity() {
		return flatCapacity;
	}

	/**
	 * @param flatCapacity the flatCapacity to set
	 */
	public void setFlatCapacity(Integer flatCapacity) {
		this.flatCapacity = flatCapacity;
	}

	/**
	 * @return the hotelEntity
	 */
	public HotelEntity getHotelEntity() {
		return hotelEntity;
	}

	/**
	 * @param hotelEntity the hotelEntity to set
	 */
	public void setHotelEntity(HotelEntity hotelEntity) {
		this.hotelEntity = hotelEntity;
	}

	/**
	 * @return the flatCategoryEntity
	 */
	public FlatCategoryEntity getFlatCategoryEntity() {
		return flatCategoryEntity;
	}

	/**
	 * @param flatCategoryEntity the flatCategoryEntity to set
	 */
	public void setFlatCategoryEntity(FlatCategoryEntity flatCategoryEntity) {
		this.flatCategoryEntity = flatCategoryEntity;
	}

	

}

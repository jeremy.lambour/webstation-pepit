package com.techconsulting.ws.dao.entity.shop;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * The ShopSchedule's Class. This class describe the association class between
 * ShopEntity and SchedulEntity
 */
@Entity
@Proxy(lazy = false)
@SequenceGenerator(name = "shopschedule_seq", sequenceName = "shopschedule_seq", allocationSize = 1)
@Table(name = "shopschedule", schema = "public")
public class ShopScheduleEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Entity of {@link ShopScheduleIdEntity}
	 */
	@EmbeddedId
	ShopScheduleIdEntity shopScheduleId;

	public ShopScheduleEntity() {
		super();
	}

	/**
	 * @return the shopScheduleId
	 */
	public ShopScheduleIdEntity getShopScheduleId() {
		return shopScheduleId;
	}

	/**
	 * @param shopScheduleId the shopScheduleId to set
	 */
	public void setShopScheduleId(ShopScheduleIdEntity shopScheduleId) {
		this.shopScheduleId = shopScheduleId;
	}

}
